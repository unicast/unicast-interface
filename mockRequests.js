'use strict';

var http = require("http");
var fs = require("fs");

const SESSIONS_USE_JSON = true;
const SESSIONS_PATH_JSON = './dist/sessions.json';

function MockRequests(){

}

const repositories = [{
    name: 'movie/HDD1',
    displayName: 'Media 1 (D)',
    state: 'offline'
},{
    name: 'movie/HDD2',
    displayName: 'Media 2 (L)',
    state: 'offline'
},{
    name: 'movie/HDD3',
    displayName: 'Media 3 (E)',
    state: 'online'
},{
    name: 'show/HDD1',
    displayName: 'Media 1 (D)',
    state: 'offline'
},{
    name: 'show/HDD3',
    displayName: 'Media 3 (E)',
    state: 'online'
}];

const all_genres = [
    "Action",
    "Adventure",
    "Animation",
    "Comedy",
    "Crime",
    "Documentary",
    "Drama",
    "Family",
    "Fantasy",
    "History",
    "Horror",
    "Music",
    "Mystery",
    "Romance",
    "Science Fiction",
    "TV Movie",
    "Thriller",
    "War",
    "Western"
];

const presets = {
	"491e655f-ab22-0c31-3f34-111561488bc6" : {
		"id" : "491e655f-ab22-0c31-3f34-111561488bc6",
		"title" : "Monday",
		"filters" : {
			"watched" : "all",
			"sort" : {
				"field" : "title",
				"direction" : "asc"
			},
			"collections" : {
				"6cb8a597-ba3c-4cb5-8629-bcf32636f78a" : "include",
				"6cb8a597-ba3c-4cb5-8629-bcf32636f78c" : "include",
				"6cb8a597-ba3c-4cb5-8629-bcf32636f78b" : "exclude"
			},
			"genres" : {}

		}
    },
    "777e655f-ab22-0c31-3f34-111561488bc7" : {
		"id" : "777e655f-ab22-0c31-3f34-111561488bc7",
		"title" : "Tuesday",
		"filters" : {
			"watched" : "all",
			"sort" : {
				"field" : "title",
				"direction" : "desc"
			},
			"collections" : {
				"6cb8a597-ba3c-4cb5-8629-bcf32636f78a" : "exclude",
				"6cb8a597-ba3c-4cb5-8629-bcf32636f78c" : "exclude"
			},
			"genres" : {}

		}
	}
};

const collections = [{
    color: "#FFFFFF",
    id: "6cb8a597-ba3c-4cb5-8629-bcf32636f780",
    children: [{
        color: "#FFFFFF",
        id: "6cb8a597-ba3c-4cb5-8629-bcf32636f78a",
        children: [],
        kinds: ["movie"],
        primary: true,
        title: "Class A"
    },{
        color: "#FFFFFF",
        id: "6cb8a597-ba3c-4cb5-8629-bcf32636f78b",
        children: [],
        kinds: ["movie"],
        primary: true,
        title: "Class B"
    },{
        color: "#FFFFFF",
        id: "6cb8a597-ba3c-4cb5-8629-bcf32636f78c",
        children: [],
        kinds: ["movie"],
        primary: true,
        title: "Class C"
    }],
    kinds: ["movie"],
    primary: true,
    title: "All Classes"
},{
    color: "#FFFFFF",
    id: "6cb8a597-ba3c-4cb5-8629-bcf32636f78d",
    children: [],
    kinds: ["movie"],
    primary: true,
    title: "Sundays"
}];

const movie = {
    addedAt: "2015-03-10T23:42:52.000Z",
    art: {
        background: "http://image.tmdb.org/t/p/original/xgH52bpPbu8uwwx5VAS6fbCyHfj.jpg",
        banner: null,
        poster: "http://image.tmdb.org/t/p/original/75QV0rNm6QHpMnpIGrZK0ev08rH.jpg",
        thumbnail: null
    },
    collections: [ collections[0].children[0], collections[1] ],
    external: {
        imdb:"tt0844471"
    },
    genres: ["Animation","Comedy","Family"],
    id: "382b239f-eb38-46d4-9ec2-7b6bb959fb76",
    internalId: 166,
    kind: "movie",
    lastPlayedAt: "2016-01-29T20:28:52.000Z",
    parentalRating: "Rated PG",
    repositoryPaths: [ repositories[2].name ],
    playCount: 1,
    plot: "Inventor Flint Lockwood creates a machine that makes clouds rain food, enabling the down-and-out citizens of Chewandswallow to feed themselves. But when the falling food reaches gargantuan proportions, Flint must scramble to avert disaster. Can he regain control of the machine and put an end to the wild weather before the town is destroyed?",
    quality: {
        codec: "x264",
        releaseGroup: "YIFY",
        resolution: "720p",
        source: "BluRay"
    },
    rating: 6.400000095367432,
    repository: "kodi",
    runtime: 5393,
    sources: [{
        id: "kodi://kodi/movie/166"
    },{
        id: "Cloudy.with.a.Chance.of.Meatballs.720p.BluRay.mp4"
    },{
        id: "Cloudy.with.a.Chance.of.Meatballs.720p.BluRay.srt"
    }],
    tagline: "Prepare to get served.",
    title: "Cloudy with a Chance of Meatballs",
    trailer: "https://www.youtube.com/watch?v=5kQnlBW02tI",
    watched: true,
    year: 2009,
    cachedArtwork: {
        background: "http://image.tmdb.org/t/p/original/xgH52bpPbu8uwwx5VAS6fbCyHfj.jpg",
        banner: null,
        poster: "http://image.tmdb.org/t/p/original/75QV0rNm6QHpMnpIGrZK0ev08rH.jpg",
        thumbnail: null
    }
};

const playlist = {
    id: "playlistId",
    references : [{ id: movie.id, kind: movie.kind }],
    items: [movie],
    createdAt: new Date().toISOString(),
    updatedAt: new Date().toISOString()
};

MockRequests.guidGenerator = function () {
    var S4 = function() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

MockRequests.parseQueryString = function( queryString ) {
    let params = {};
    let queries =[];
    let temp = [];
    // Split into key/value pairs
    queries = queryString.split("&");
    // Convert the array of strings into an object
    for ( let i = 0, l = queries.length; i < l; i++ ) {
        temp = queries[i].split('=');
        temp[0] = temp[0].replace(/%5B/g, '[').replace(/%5D/g, ']');
        params[temp[0]] = temp[1];
    }
    return params;
};

MockRequests.sleep = function ( ms ) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

MockRequests.getAndSendImage = function ( url, req, res, next ) {
    http.get(url, function(response) {

        var imageSize = parseInt(response.header("Content-Length"));
        var imageBuffer = new Buffer(imageSize);
        var bytes = 0;

        response.setEncoding("binary");

        response.on("data", function(chunk) {
            imageBuffer.write(chunk, bytes, "binary");
            bytes += chunk.length;
        });

        response.on("end", function() {
            res.setHeader("Content-Type", "image/png");
            res.send(imageBuffer);
            return next();
        });

    });
}

MockRequests.wrapInStorageContainer = function(key, value) {
    return {
        createdAt: new Date(),
        id: MockRequests.guidGenerator(),
        key: key,
        updatedAt: new Date(),
        value: value
    };
}

MockRequests.prototype.create = function(router) {
    router.get( '/api/player/:device/status', function( req, res, next ) {
        res.send({
            volume: {
                level: 50,
                muted: true
            },
            timestamp: new Date(),
            subtitlesStyle: {
                size: 18
            },
            state: 'PAUSED',
            session: 'b7185780-02e2-4e40-aba7-05c8bef6caf5',
            media: {
                time: {
                    duration: 5393.536,
                    current: 3742.586663,
                    speed: 0,

                },
                transcoding: {
                    duration: 5393.536,
                    current: 4569.981,
                    speed: 10.53,
                    stable: true
                },
                record: movie
            }
        });
        return next();
    });

    router.get( '/api/player/:device/preview/**', function( req, res, next ) {
        return MockRequests.getAndSendImage('http://image.tmdb.org/t/p/original/xgH52bpPbu8uwwx5VAS6fbCyHfj.jpg', req, res, next);
    });

    router.get( '/api/player/list', function( req, res, next ) {
        res.send([{
            name: 'MyChromecast1',
            type: 'chromecast',
            address: '192.168.0.60'
        },{
            name: 'OtherCast',
            type: 'chromecast',
            address: '192.168.0.62'
        }]);
        return next();
    });

    router.get( '/api/player/:device/playlists', function( req, res, next ) {
        res.send([playlist]);
        return next();
    });

    router.get( '/api/player/:device/playlists/' + playlist.id + '/items', function( req, res, next ) {
        res.send(playlist.items);
        return next();
    });

    router.get( '/api/player/:device/playlists/last', function( req, res, next ) {
        let queryArgs = MockRequests.parseQueryString(req.query());
        let withItems = queryArgs['items'] == 'true';
        res.send({ ...playlist, items: withItems ? playlist.items : undefined });
        return next();
    });

    router.get( '/api/media/sessions', function( req, res, next ) {
        let queryArgs = MockRequests.parseQueryString(req.query());
        let skip = Number(queryArgs['skip']);
        let take = Number(queryArgs['take']);

        if (SESSIONS_USE_JSON && fs.existsSync(SESSIONS_PATH_JSON)) {
            let str = fs.readFileSync(SESSIONS_PATH_JSON, { encoding: 'utf-8' });
            let history = (JSON.parse(str) || []).filter(e => !!e);
            history.sort((v1, v2) => v2.createdAt.localeCompare(v1.createdAt));
            res.send(history.slice(skip || 0, take + skip).map( e => ({...e, record: {...e.record, cachedArtwork: e.record.art} })));
        } else {
            res.send(Array(...Array(take)).map((me) => {
                return {
                    createdAt: new Date(new Date().getTime() - ((skip / take) * 86400000)).toISOString(), //Day by day
                    playlist: null,
                    playlistPosition: null,
                    position: 0,
                    positionHistory: [{end: 0, start: 0}],
                    receiver: "MyChromecast1",
                    record: movie,
                    id: MockRequests.guidGenerator()
                };
            }));
        }
        return next();



    });

    router.get( '/api/media/:kind/genres', function( req, res, next ) {
        res.send(all_genres);
        return next();
    });

    router.get( '/api/media/repositories', function( req, res, next ) {
        let queryArgs = MockRequests.parseQueryString(req.query());
        let kinds = new Set((queryArgs['kinds'] || '').split(','));
        res.send(repositories.filter(e => kinds.has(e.name.substring(0, e.name.indexOf('/')))));
        return next();
    });

    router.get( '/api/media/collection', function( req, res, next ) {
        let queryArgs = MockRequests.parseQueryString(req.query());
        let tree = queryArgs['tree'] == 'true';
        res.send(tree ? collections : collections.reduce((acc, x) => acc.concat(x.children && x.children.length > 0 ? x.children : x), []));
        return next();
    });

    router.get( '/api/media/show/:id', function( req, res, next ) {
        //TODO: Create a mock show
        res.send(movie);
        return next();
    });

    router.get( '/api/media/movie/:id', function( req, res, next ) {
        res.send(movie);
        return next();
    });

    let getSubtitles = async function( req, res, next ) {
        let queryArgs = MockRequests.parseQueryString(req.query());
        let language = (queryArgs['langs'] || 'por').split(',')[0];
        res.send([{
            id: "OneSubtitleForMedia",
            releaseName : "Subtitle X",
            encoding: 'UTF-8',
            format : 'srt',
            language, language,
            publishedAt : new Date(),
            downloads : 555,
            provider : 'Mocks'
        }]);
        return next();
    }

    router.get( '/api/media/subtitles/:kind/:id/local', getSubtitles);
    router.get( '/api/media/subtitles/:kind/:id/remote', getSubtitles);

    let getMediaList = function( req, res, next ) {
        let queryArgs = MockRequests.parseQueryString(req.query());
        if (queryArgs['filterSort[field]'] == '$userRank') {
            const max = 15;
            const zero = 5;
            let ret = new Array(max).fill(null).map((e, position) => ({...movie, id: MockRequests.guidGenerator(), $userRank: max - position, year: 2000 + (max - position)}));
            ret = ret.concat(new Array(zero).fill(null).map((e, position) => ({...movie, id: MockRequests.guidGenerator(), $userRank: 0, year: 2000 + max + 1 + position})));
            if (queryArgs['filterSort[direction]'] == 'desc') {
                ret = ret.reverse();
            }
            res.send(ret);
        } else if (queryArgs['skip'] === undefined || queryArgs['skip'] == 0 ){
            res.send([movie]);
        }
        return next();
    };

    router.get( '/api/media/movie', getMediaList);

    router.get( '/api/media/show', getMediaList);

    router.post( '/api/media/user-ranks/:list/set-rank-before', function( req, res, next ) {
        res.send();
        return next();
    });

    router.post( '/api/media/user-ranks/:list/set-rank-after', function( req, res, next ) {
        res.send();
        return next();
    });

    let syncClean = async function( req, res, next ) {
        await MockRequests.sleep(10000);
        res.send();
        return next();
    };

    router.post( '/api/media/providers/sync', syncClean);

    router.post( '/api/media/providers/clean', syncClean);

    router.get( '/api/tasks', function( req, res, next ) {
        res.send([]);
        return next();
    });

    router.get( '/api/storage/resyncList', function( req, res, next ) {
        res.send(MockRequests.wrapInStorageContainer( 'any' , []));
        return next();
    });

    router.get( '/api/storage/:id', function( req, res, next ) {
        res.send(MockRequests.wrapInStorageContainer( 'any' , presets));
        return next();
    });

    router.post( '/api/storage/:id', function( req, res, next ) {
        res.send(MockRequests.wrapInStorageContainer( 'any' , presets));
        return next();
    });

    const customAction = {
        "name":"angularAction",
        "label":"Angular Action",
        "icon":'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M185.7 268.1h76.2l-38.1-91.6-38.1 91.6zM223.8 32L16 106.4l31.8 275.7 176 97.9 176-97.9 31.8-275.7zM354 373.8h-48.6l-26.2-65.4H168.6l-26.2 65.4H93.7L223.8 81.5z"/></svg>',
        "group":"Languages"
    };

    const customActionGroup = {
        "name":"Languages",
        "actions": new Array(4).fill(null).map((v, i) => ({ ...customAction, name: customAction.name+i}))
    }

    router.get( '/api/custom-actions', function( req, res, next ) {
        res.send(new Array(5).fill(null).map((v, i) => ({ ...customActionGroup, name: customActionGroup.name + i })));
        return next();
    });


}

module.exports = {MockRequests};
class NgAotPluginClass {

    constructor(listOfComponents) {
        listOfComponents = listOfComponents || [];
        this.test = /component.ngfactory.js/;
        this.listOfComponents = {};
        for(let comp of listOfComponents){
            this.listOfComponents[comp] = new RegExp('require\\([^\\n]+node_modules/' + comp + '/dist/' + comp  + '.ngfactory[^)]+\\)', 'g');
        }
    }

    transform(file) {
        for (let key of Object.keys(this.listOfComponents)){
            file.contents = file.contents.replace(this.listOfComponents[key], 'require("~/modules/' + key + '.ngfactory")');
        }
    }
}

module.exports.NgAotPlugin = (listOfComponents) => new NgAotPluginClass(listOfComponents);
const { tsc, src, watch, task } = require( 'fuse-box/sparky' );
const ngc = require('@angular/compiler-cli/src/main').main;
const path = require( 'path' );
const fs = require ( 'mz/fs' );

task('ngc', async (ctx) => {
    await src( 'main.ts', { base: ctx.SRC_AOT } )
            .file('main.ts', convertTemplate)
            .exec();
    console.info('\tBuilding application');
    result = ngc(['-p', 'tsconfig.aot.json']);
    if (result <= 0) {
        ctx.MODULES_TO_BUILD = ctx.MODULES_TO_BUILD || [];
        for(mod of ctx.MODULES_TO_BUILD){
            let destTsConfig = path.join( ctx.NODE_MODULES, mod, 'tsconfig.json' );
            fs.copyFileSync('fuse/modulesTsConfig.json', destTsConfig);
            console.info('\tBuilding ' + mod);
            result += ngc(['-p', destTsConfig ]);
        }
    }
    if (result > 0) {
        throw new Error('AOT compiler found some errors on sources.');
    }
    return result;
});

function convertTemplate(file) {
    if (file.name == 'main.ts'){
        let contents = fs.readFileSync(file.filepath, 'utf8').trim();
        //Tricky thing to build the new main.ts
        contents = contents.replace('const isAotBuild: boolean = false;',
                                    'const isAotBuild: boolean = true;');
        contents = contents.replace('import { platformBrowserDynamic } from \'@angular/platform-browser-dynamic\';',
                                    'import { platformBrowser } from \'@angular/platform-browser\';');
        contents = contents.replace('import { AppModule } from \'./components/app.module\';',
                                    'import { AppModuleNgFactory } from \'./components/app.module.ngfactory\';');
        contents = contents.replace('platformBrowserDynamic().bootstrapModule(AppModule);',
                                    'platformBrowser().bootstrapModuleFactory(AppModuleNgFactory);');
        fs.writeFileSync(file.filepath, contents, 'utf8');
    }

}
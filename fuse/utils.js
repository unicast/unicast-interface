const { src } = require( 'fuse-box/sparky' );
const { FuseBox, LESSPlugin, CSSPlugin, RawPlugin, WebIndexPlugin, Sparky, QuantumPlugin } = require( 'fuse-box' );
const { Ng2TemplatePlugin } = require( 'ng2-fused' );
const less = require('less');
const path = require( 'path' );
const fs = require ( 'mz/fs' );

const { NgAotPlugin } = require( './ngAotPlugin' );

function buildFuse(isAot, ctx) {
    return FuseBox.init( {
        homeDir : (isAot ? ctx.SRC_AOT : ctx.SRC),
        target : 'browser@es6',
        output : path.join(ctx.DIST, '$name.js'),
        tsConfig: (isAot ? 'tsconfig.aot.json' : 'tsconfig.json'),
        useTypescriptCompiler : !isAot,
        hash: !isAot,
        ensureTsConfig: !isAot,
        cache: !isAot,
        modulesFolder: ctx.NODE_MODULES,
        sourceMaps: {
            inline: !isAot,
            project: !isAot,
            vendor: false
        },
        plugins: [
            isAot && NgAotPlugin(ctx.MODULES_TO_BUILD),
            Ng2TemplatePlugin(),
            [ '*.template.html', RawPlugin() ],
            [ '*.styles.css', RawPlugin() ],
            [ '*.styles.less', LESSPlugin( {
                paths: [
                    ctx.NODE_MODULES,
                    (isAot ? ctx.SRC_AOT : ctx.SRC )
                ]
            } ), RawPlugin() ],

            WebIndexPlugin( {
                template: path.join((isAot ? ctx.SRC_AOT : ctx.SRC ) ,'/index.html'),
            } ),
            isAot && QuantumPlugin({
                uglify: true,
                css: true,
                bakeApiIntoBundle: true,
                ensureES5: false,
                processPolyfill: true
            })
        ],
        alias: {
            'components': '~/components'
        }
    } );
};

module.exports = {
    buildTaskBuild: (isAot, ctx) => {
        const fuse = buildFuse(isAot, ctx);
        const extension = (isAot ? 'js' : 'ts');
        const styles = fuse.bundle( 'styles' )
            .plugin( LESSPlugin(), CSSPlugin( { group: "styles.css" } ) )
            .instructions( "> styles/web.less" );
        if (!isAot) {
            styles.watch();
        }

        const vendor = fuse.bundle( 'vendor' )
            .instructions( '~ main.' +  extension);

        const main = fuse.bundle( "main" )
            .instructions( '> [main.' + extension + ']' );
        if (!isAot) {
            main.watch();
        }

        return fuse.run();
    },
    clean: (folder) => {
        folder = folder || 'dist';
        return src( folder )
            .clean( folder )
            .exec();
    },
    copy: (source, destination) => {
        return src( '**/**.**', { base: source } )
            .dest( destination )
            .exec();
    },
    buildCss: (file, ctx) => {
        let contents = fs.readFileSync(file.filepath, 'utf8').trim();
        if (contents && contents.length > 0){
            return less.render(contents, {paths: [
                ctx.NODE_MODULES,
                ctx.SRC
            ], filename: file.filepath}).then( (result) => {
                    let newFilepath = file.filepath.replace(file.root, ctx.SRC_AOT);
                    fs.writeFileSync(newFilepath, result.css, 'utf8');
            }).catch( (error) => {
                if (error) {
                    console.error(error);
                }
            });
        }
    },
    npmPack: () => {
        require('child_process').execSync('npm pack', console.log);
    }
};
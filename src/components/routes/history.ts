import { Route } from '@angular/router';

import { HistoryComponent } from 'components/pages/history/history.component';
import { RouteType } from 'components/routes/utils';

export class HistoryRoutes {

    static router(): Route{
        return {
            path: RouteType.HISTORY, component: HistoryComponent
        };
    }
}


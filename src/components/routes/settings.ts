import { Route } from '@angular/router';

import { SettingsComponent } from 'components/pages/settings/settings.component';
import { RouteType } from 'components/routes/utils';

export class SettingsRoutes {

    static router(): Route{
        return {
            path: RouteType.SETTINGS, component: SettingsComponent
        };
    }
}


import { Route } from '@angular/router';

import { HomePageComponent } from "components/pages/home/home.component";
import { RouteType } from 'components/routes/utils';

//TODO: Create real routes to addons
export class AddonsRoutes {

    static router(): Route{
        return {
            path: RouteType.ADDONS, component: HomePageComponent
        };
    }
}


export enum RouteType {
    SHOW = 'shows',
    MOVIE = 'movies',
    CUSTOM = 'custom',
    HISTORY = 'history',
    ADDONS = 'addons',
    SETTINGS = 'settings',
    MEDIAS = 'medias',
    PEOPLE = 'people'
}
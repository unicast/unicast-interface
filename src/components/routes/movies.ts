import { Route } from '@angular/router';

import { MoviesDetailsComponent } from 'components/pages/movies/details/moviesDetails.component';
import { MoviesListComponent } from 'components/pages/movies/list/moviesList.component';
import { MoviesComponent } from 'components/pages/movies/movies.component';
import { RouteType } from 'components/routes/utils';

export class MoviesRoutes{

    static router(): Route{
        return {
            path: RouteType.MOVIE, component: MoviesComponent, children: [
                {path: ':id', component: MoviesDetailsComponent},
                {path: '', component: MoviesListComponent}
            ]
        };

    }
}





import { Route } from '@angular/router';

import { ShowsDetailsComponent } from 'components/pages/shows/details/showsDetails.component';
import { ShowsListComponent } from 'components/pages/shows/list/showsList.component';
import { ShowsComponent } from 'components/pages/shows/shows.component';
import { RouteType } from 'components/routes/utils';

export class ShowsRoutes {

    static router(): Route{
        return {
            path: RouteType.SHOW, component: ShowsComponent, children: [
                {path: ':id/:season/:episode', component: ShowsDetailsComponent},
                {path: ':id/:season', component: ShowsDetailsComponent},
                {path: ':id', component: ShowsDetailsComponent},
                {path: '', component: ShowsListComponent}
            ]
        };
    }
}

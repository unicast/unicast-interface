import { Route } from '@angular/router';

import { RouteType } from 'components/routes/utils';
import { PeopleComponent } from 'components/pages/people/people.component';
import { PeopleListComponent } from 'components/pages/people/list/peopleList.component';
import { PeopleDetailsComponent } from 'components/pages/people/details/peopleDetails.component';

export class PeopleRoutes{

    static router(): Route{
        return {
            path: RouteType.PEOPLE, component: PeopleComponent, children: [
                {path: ':id', component: PeopleDetailsComponent},
                {path: '', component: PeopleListComponent}
            ]
        };

    }
}





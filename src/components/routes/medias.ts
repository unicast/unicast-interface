import { Route } from '@angular/router';

import { MediaRedirectComponent } from 'components/common/mediaRedirect/mediaRedirect.component';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { RouteType } from 'components/routes/utils';

export class MediasRoutes {

    static router(): Route{
        return {
            path: RouteType.MEDIAS, children: [
                {path: MediaKind.TvShow + '/:id', component: MediaRedirectComponent, data: {kind: MediaKind.TvShow}},
                {path: MediaKind.TvSeason + '/:id', component: MediaRedirectComponent, data: {kind: MediaKind.TvSeason}},
                {path: MediaKind.TvEpisode + '/:id', component: MediaRedirectComponent, data: {kind: MediaKind.TvEpisode}},
                {path: MediaKind.Movie + '/:id', component: MediaRedirectComponent, data: {kind: MediaKind.Movie}},
                {path: MediaKind.Custom + '/:id', component: MediaRedirectComponent, data: {kind: MediaKind.Custom}},
                {path: ':kind/:id', component: MediaRedirectComponent },
            ]
        };
    }
}

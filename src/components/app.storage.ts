import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { StorageManager } from 'components/services/storageManager';
import { Device } from 'components/services/serverApis/helpers/device';
import { MediaListQueryFilters } from 'components/pages/common/sidemenuFilters/filtersMediaSidemenu.service';
import { HistoryListQueryFilters } from 'components/pages/common/sidemenuFilters/filtersHistorySidemenu.service';
import { HistoryQueryMediaList } from 'components/common/helpers/historyQueryMediaList';

export enum ValidOperations{
    DEVICE = 'actualDevice',
    LANG = 'currentLanguage',
    API = 'apiUrl',
    MEDIA_FILTERS = 'mediaFilters',
    HISTORY_FILTERS = 'historyFilters'
}

@Injectable()
export class AppStorage{
    private _storageManager: StorageManager;
    private _subscriptions: Map<string, Subscription>

    public constructor (){
        this._subscriptions = new Map<string, Subscription>();
    }

    private setByObservable<T>(name: string, obs: Observable<T> | T, func: (t: T) => void){
        let sub: Subscription = this._subscriptions.get(name);
        if (sub){
            sub.unsubscribe();
            this._subscriptions.set(name, null);
        }
        if (obs instanceof Observable) {
            this._subscriptions.set(name, obs.subscribe(func));
        } else {
            func(obs);
        }
    }

    public setStorage(storageManager: StorageManager){
        this._storageManager = storageManager;
    }

    public getActualDevice(): Device{
        return Device.fromJSON(this._storageManager.get(
            ValidOperations.DEVICE,
            Device.NOT_CONNECTED
        ));
    }

    public setActualDevice(device: Device | Observable<Device>): void{
        this.setByObservable<Device>(ValidOperations.DEVICE, device, (d: Device) => {
            this._storageManager.set(ValidOperations.DEVICE, d);
        });
    }

    public getCurrentLanguage(): string{
        return this._storageManager.get(ValidOperations.LANG, 'pt');
    }

    public setCurrentLanguage(lang: string): void{
        this._storageManager.set(ValidOperations.LANG, lang);
    }

    public getCurrentServerApi(): string{
        return this._storageManager.get(ValidOperations.API, window['API_HREF']);
    }

    public setCurrentServerApi(url: string): void{
        this._storageManager.set(ValidOperations.API, url);
    }

    private getSidemenuFilters<T>(operation: ValidOperations, name: string, defaultValue?: T): T {
        return this._storageManager.get(this.buildComplexName(operation, name), defaultValue || {});
    }

    private setSidemenuFilters<T>(operation: ValidOperations, name: string, filters: T | Observable<T>){
        let fullName = this.buildComplexName(operation, name);
        this.setByObservable<T>(fullName, filters, (f: T) => {
            this._storageManager.set(fullName, f);
        });
    }

    public removeSidemenuFilter(operation: ValidOperations, name: string) {
        this._storageManager.remove(this.buildComplexName(operation, name));
    }


    public getHistoryFilters(defaultValue?: HistoryListQueryFilters): HistoryListQueryFilters {
        let filters = this.getSidemenuFilters(ValidOperations.HISTORY_FILTERS, undefined, defaultValue);
        filters.media = (filters.media || []).map(e => HistoryQueryMediaList.of(e as any));
        return filters;
    }

    public setHistoryFilters(filters: HistoryListQueryFilters | Observable<HistoryListQueryFilters>){
        this.setSidemenuFilters(ValidOperations.HISTORY_FILTERS, undefined, filters);
    }

    public removeHistoryFilter() {
        this.removeSidemenuFilter(ValidOperations.HISTORY_FILTERS, undefined);
    }

    public getMediaFilters(name: string, defaultValue?: MediaListQueryFilters): MediaListQueryFilters {
        return this.getSidemenuFilters(ValidOperations.MEDIA_FILTERS, name, defaultValue);
    }

    public setMediaFilters(name: string, filters: MediaListQueryFilters | Observable<MediaListQueryFilters>){
        this.setSidemenuFilters(ValidOperations.MEDIA_FILTERS, name, filters);
    }

    public removeMediaFilter(name: string) {
        this.removeSidemenuFilter(ValidOperations.MEDIA_FILTERS, name);
    }

    private buildComplexName(starting: ValidOperations, ...names: Array<string>){
        return [starting.toString()].concat(names).filter(e => !!e).join('.');
    }
}
import { Component } from '@angular/core';

import { TopbarManager } from 'components/services/topbarManager';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { NavigationManager } from 'components/services/navigationManager';

@Component( {
    selector: 'uc-topbar',
    templateUrl: './topbar.template.html',
    styleUrls: [ './topbar.styles.less' ]
} )
export class TopBarComponent extends BaseUnicastComponent{

    public manager: TopbarManager;
    public serverApi: ServerApiManager;
    public navigation: NavigationManager;

    public searchOpen: boolean;
    public filtering: boolean;

    constructor ( manager : TopbarManager, serverApi: ServerApiManager, navigation: NavigationManager ) {
        super();
        this.manager = manager;
        this.serverApi = serverApi;
        this.navigation = navigation;
        this.searchOpen = false;
    }

    protected _binds() {
        this._subscriptions.push(this.serverApi.getMedia().filterChanged.subscribe(([filter])=> this.filtering = !!filter));
    }

}
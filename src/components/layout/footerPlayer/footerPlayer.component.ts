import { Component } from '@angular/core';

import { StandardMediaQueries } from 'components/services/mediaQueries';
import { MediaPlayControl } from 'components/common/services/mediaPlayControl';

//Helpers
import { WaitFor } from 'components/common/helpers/asyncClick.directive';

//Voice Commands
import { VoiceCommandsManager } from 'components/services/speechRecognition/voiceCommandsManager';
import { VoiceCommandPause } from 'components/services/speechRecognition/voiceCommands/voiceCommandPause';
import { VoiceCommandResume } from 'components/services/speechRecognition/voiceCommands/voiceCommandResume';
import { VoiceCommandSeek } from 'components/services/speechRecognition/voiceCommands/voiceCommandSeek';
import { VoiceCommandStop } from 'components/services/speechRecognition/voiceCommands/voiceCommandStop';
import { VoiceCommandStatus } from 'components/services/speechRecognition/voiceCommands/voiceCommandStatus';
import { VoiceCommandPlay } from 'components/services/speechRecognition/voiceCommands/voiceCommandPlay';


@Component( {
    selector: 'uc-footer-player',
    templateUrl: './footerPlayer.template.html',
    styleUrls: [ './footerPlayer.styles.less' ]
} )
export class FooterPlayerComponent {
    private _voiceCommand: VoiceCommandsManager;

    public mediaPlayControl: MediaPlayControl;
    public mediaQueries : StandardMediaQueries;

    constructor ( voiceCommand : VoiceCommandsManager, mediaQueries : StandardMediaQueries, mediaPlayControl: MediaPlayControl ) {
        this._voiceCommand = voiceCommand;
        this.mediaPlayControl = mediaPlayControl;
        this.mediaQueries = mediaQueries;

        this._registerCommands();
    }

    ngOnInit(){
        //this.updatePlaylists();
    }

    ngOnDestroy() {
        this._voiceCommand.unregister(this);
    }

    private _registerCommands(){
        this._voiceCommand.register(this, new VoiceCommandPlay());
        this._voiceCommand.register(this, new VoiceCommandStatus());
        this._voiceCommand.register(this, new VoiceCommandResume(), new VoiceCommandPause());
        this._voiceCommand.register(this, new VoiceCommandSeek());
        this._voiceCommand.register(this, new VoiceCommandStop());
    }

    startRecognition(event: WaitFor){
        this._voiceCommand.listenCommand({before: (data) => {
            if (event.release) {
                event.release();
            }
        }});
    }
}


import { Component, OnInit, Input } from '@angular/core';
import { MovieMediaRecordSchema } from 'components/schemas/mediaRecord.schema';

@Component({
    selector: 'uc-movie-badge, .uc-movie-badge',
    templateUrl: './movieBadge.template.html',
    styleUrls: [ './movieBadge.styles.less' ],
})

export class MovieBadgeComponent implements OnInit {
    @Input() item: MovieMediaRecordSchema;

    @Input() expanded : boolean = false;

    @Input() status? : string;

    constructor() {

    }

    ngOnInit() { }


}
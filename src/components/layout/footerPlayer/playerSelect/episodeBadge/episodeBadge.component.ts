import { Component, OnInit, Input } from '@angular/core';
import { TvEpisodeMediaRecordSchema } from 'components/schemas/mediaRecord.schema';


@Component({
    selector: 'uc-episode-badge, .uc-episode-badge',
    templateUrl: './episodeBadge.template.html',
    styleUrls: [ './episodeBadge.styles.less' ],
})

export class EpisodeBadgeComponent implements OnInit {
    @Input() item: TvEpisodeMediaRecordSchema;

    @Input() expanded : boolean = false;

    @Input() status? : string;

    constructor() {

    }

    ngOnInit() { }


}
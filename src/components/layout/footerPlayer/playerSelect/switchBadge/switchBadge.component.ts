import { Component, OnInit, Input } from '@angular/core';
import { PlayableMediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';


@Component({
    selector: 'uc-switch-badge, .uc-switch-badge',
    templateUrl: './switchBadge.template.html',
    styleUrls: [ './switchBadge.styles.less' ],
})

export class SwitchBadgeComponent implements OnInit {
    @Input() item: PlayableMediaRecordSchema;

    @Input() expanded : boolean = false;

    @Input() status? : string;

    public enumMediaKind: typeof MediaKind = MediaKind;

    constructor() {

    }

    ngOnInit() { }


}
import { Component, OnInit, EventEmitter, Input, Output, OnChanges } from '@angular/core';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { PlaylistModel } from 'components/models/playlist.model';
import { TvEpisodeMediaRecordSchema, MovieMediaRecordSchema, CustomMediaRecordSchema, PlayableMediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { WaitFor } from 'components/common/helpers/asyncClick.directive';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { NavigationManager } from 'components/services/navigationManager';

@Component( {
    selector: 'uc-player-remote-playlists',
    templateUrl: './playerRemotePlaylists.template.html',
    styleUrls: [ './playerRemotePlaylists.styles.less' ]
} )
export class PlayerRemotePlaylistsComponent extends BaseUnicastComponent implements OnInit, OnChanges {
    @Input() activePlaylist: PlaylistModel;

    @Output() activePlaylistChange : EventEmitter<any> = new EventEmitter();

    @Input() playlists : Array<PlaylistModel> = [];

    @Output() playlistsChange : EventEmitter<any[]> = new EventEmitter();

    @Input() playlistsListOpen : boolean = false;

    @Output() playlistsListOpenChange : EventEmitter<boolean> = new EventEmitter();

    @Output() layoutChange : EventEmitter<void> = new EventEmitter();

    private _serverApi: ServerApiManager;
    private _navigation: NavigationManager;

    private _playlistsLoaded: boolean = false;

    constructor(serverApi: ServerApiManager, navigation: NavigationManager) {
        super();
        this._serverApi = serverApi;
        this._navigation = navigation;

        this.activePlaylist = new PlaylistModel();

    }

    private _selectPlaylist(id?: string){
        let playlistApi = this._serverApi.getPlayer().getDeviceApi().getPlaylistApi();

        if (!id){
            this._obtainPlaylists();

            this.setPlaylistsListView( true );

            this.layoutChange.next( void 0 );

        } else if (!this.activePlaylist || id != this.activePlaylist.id) {
            let playlist = this.playlists.find((value: PlaylistModel, index: number) => value.id == id);
            if (playlist) {
                playlistApi.getItemsApi().get().subscribe((list: Array<TvEpisodeMediaRecordSchema | MovieMediaRecordSchema | CustomMediaRecordSchema>) => {
                    playlist.data.items = list;

                    this.activePlaylist = playlist;

                    this.activePlaylistChange.next( this.activePlaylist );

                    this.setPlaylistsListView( false );

                    this.layoutChange.next( void 0 );
                });
            }

        }
    }

    private _obtainPlaylists(){
        let playlistApi = this._serverApi.getPlayer().getDeviceApi().getPlaylistApi();

        playlistApi.list().subscribe(data => {
            this.playlists = data.playlists.sort((a,b) => { return b.data.createdAt.valueOf() - a.data.createdAt.valueOf()});

            //Some playlist was already select. Check if it exists
            if (!!playlistApi.getId()){
                this._selectPlaylist(playlistApi.getId());
            }

            if (!this._playlistsLoaded){
                this._playlistsLoaded = true;
            }
        });
    }

    protected _binds(){
        let playlistApi = this._serverApi.getPlayer().getDeviceApi().getPlaylistApi();

        this._subscriptions.push(playlistApi.parent().deviceObservable.subscribe((data) => {
            this._obtainPlaylists();
        }));

        this._subscriptions.push(playlistApi.playlistIdObservable.subscribe((id) => {
            this._selectPlaylist(id);
        }));

        this._subscriptions.push(playlistApi.getItemsApi().itemsObservable.subscribe((list: Array<TvEpisodeMediaRecordSchema | MovieMediaRecordSchema | CustomMediaRecordSchema>) => {
            let change: boolean = (this.activePlaylist.data.items.length == list.length);
            this.activePlaylist.data.items = list;
            if (change) {
                this.layoutChange.next( void 0 );
            }
        }));
    }

    ngOnChanges () {
        if ( !this.activePlaylist && this.playlists && !this.playlistsListOpen ) {
            this.setPlaylistsListView( true );
        }
    }

    setPlaylistsListView ( value : boolean ) {
        if ( this.playlistsListOpen != value ) {
            this.playlistsListOpen = value;

            this.playlistsListOpenChange.next( value );

            this.layoutChange.next( void 0 );
        }
    }

    togglePlaylistsListView () {
        this.setPlaylistsListView( !this.playlistsListOpen );
    }

    selectPlaylist ( playlist: PlaylistModel ) {
        let playlistApi = this._serverApi.getPlayer().getDeviceApi().getPlaylistApi();
        if (playlist.id != playlistApi.getId()){
            playlistApi.setId(playlist.id);
        } else {
            this.togglePlaylistsListView();
        }
    }

    removePlaylist(event: WaitFor){
        let req = this._serverApi.getPlayer().getDeviceApi().getPlaylistApi().remove();
        req.subscribe((data) => {
            this._obtainPlaylists();
            this._serverApi.getPlayer().getDeviceApi().getPlaylistApi().last().subscribe((playlist) => {
                this.selectPlaylist(playlist);
            });
        });
        //event.waitFor(req);
    }

    createPlaylist(event: WaitFor){
        let req = this._serverApi.getPlayer().getDeviceApi().getPlaylistApi().create();
        req.subscribe((data) => {
            this.playlists.splice(0,0,data);
            this.selectPlaylist(data);
        });
        //event.waitFor(req);
    }

    playItem(event: WaitFor, item: PlayableMediaRecordSchema, position: number){
        event.waitFor(this._serverApi.getPlayer().getDeviceApi().play(item.kind, item.id, {playlistId: this.activePlaylist.id, playlistPosition: position }))
    }

    removeItem(event: WaitFor, item: PlayableMediaRecordSchema, position: number){
        event.waitFor(this._serverApi.getPlayer().getDeviceApi().getPlaylistApi().getItemsApi().delete(position));
    }

    levelUp(event: WaitFor, item: PlayableMediaRecordSchema, position: number){
        this._move(event, position, -1);
    }

    levelDown(event: WaitFor, item: PlayableMediaRecordSchema, position: number){
        this._move(event, position, 1);
    }

    goto(event: WaitFor, item: PlayableMediaRecordSchema, position: number) {
        this._navigation.navigateTo(item);
        event.release();
    }

    private _move(event: WaitFor, position: number, inc: number){
        event.waitFor(this._serverApi.getPlayer().getDeviceApi().getPlaylistApi().getItemsApi().reorder(position, position + inc));
    }

    animatedLayoutChange ( duration : number, step = 20 ) {
        let elapsed = 0;

        const token = setInterval( () => {
            elapsed += step;

            this.layoutChange.next();

            if ( elapsed > duration + step ) {
                clearInterval( token );
            }
        }, step );
    }
}
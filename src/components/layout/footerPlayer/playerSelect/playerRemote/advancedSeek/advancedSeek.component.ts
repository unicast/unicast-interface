import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Time } from 'components/common/helpers/time';


@Component( {
    selector: 'uc-advanced-seek',
    templateUrl: './advancedSeek.template.html',
    styleUrls: [ './advancedSeek.styles.less' ]
} )
export class AdvancedSeekComponent {
    @Input()
    baseTime : Time = new Time( 1, 30, 0 );

    @Input()
    offsetTime : Time = new Time();

    @Input()
    maxTime : Time = new Time( 3 );

    @Input()
    minTime : Time = new Time();

    @Output()
    offsetTimeChanged : EventEmitter<Time> = new EventEmitter();

    @Output()
    canceled : EventEmitter<null> = new EventEmitter();

    @Output()
    confirmed : EventEmitter<Time> = new EventEmitter();

    @Input()
    previewImage: string;

    get minOffset () : Time {
        return this.minTime.minus( this.baseTime );
    }

    get maxOffset () : Time {
        return this.maxTime.minus( this.baseTime );
    }

    seekSteps = [ {
        label: '30 min',
        time: new Time( 0, 30 )
    }, {
        label: '5 min',
        time: new Time( 0, 5 )
    }, {
        label: '1 min',
        time: new Time( 0, 1 )
    }, {
        label: '15 sec',
        time: new Time( 0, 0, 15 )
    } ];

    public setOffsetTime ( offsetTime : Time ) {
        offsetTime = Time.clamp( this.minOffset, this.maxOffset, offsetTime );

        if ( Math.round( this.offsetTime.toSeconds() ) != Math.round( offsetTime.toSeconds() ) ) {
            this.offsetTime = offsetTime;

            this.offsetTimeChanged.next( this.offsetTime );
        }
    }

    public moveStep ( time : Time, negative : boolean ) {
        let offsetTime = negative ?
            this.offsetTime.minus( time ) :
            this.offsetTime.plus( time );

        this.setOffsetTime( offsetTime );
    }

    public cancel () {
        this.canceled.next( void 0 );
    }

    public confirm () {
        this.confirmed.next( this.offsetTime );
    }
}
import { Component, EventEmitter, Output } from '@angular/core';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { HistoryRecordSchema } from 'components/schemas/mediaRecord.schema';
import { WaitFor } from 'components/common/helpers/asyncClick.directive';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { NavigationManager } from 'components/services/navigationManager';
import { Observable } from 'rxjs/Observable';

@Component( {
    selector: 'uc-player-remote-sessions',
    templateUrl: './playerRemoteSessions.template.html',
    styleUrls: [ './playerRemoteSessions.styles.less' ]
} )
export class PlayerRemoteSessionsComponent extends BaseUnicastComponent {
    private static readonly MAX_HISTORY: number = 10;
    private static readonly DELAY_SECONDS: number = 5;
    @Output() layoutChange : EventEmitter<void> = new EventEmitter();

    private _serverApi: ServerApiManager;
    private _navigation: NavigationManager;

    public items: Array<HistoryRecordSchema> = [];
    public loaded: boolean = false;

    constructor(serverApi: ServerApiManager, navigation: NavigationManager) {
        super();
        this._serverApi = serverApi;
        this._navigation = navigation;
    }

    protected _obtainHistory() {
        let req = this._serverApi.getMedia().getHistoryApi().list(PlayerRemoteSessionsComponent.MAX_HISTORY, 0, { records: true });
        req.subscribe( (data) => {
            this.loaded = true;
            this.items = data;
            this.layoutChange.next( void 0 );
        });
        return req;
    }

    protected _binds(){
        let deviceApi = this._serverApi.getPlayer().getDeviceApi();

        this._subscriptions.push(deviceApi.deviceObservable.subscribe((data) => {
            this._obtainHistory();
        }));
    }

    public playItem(event: WaitFor, item: HistoryRecordSchema){
        let startTime: number = Math.max(item.position - PlayerRemoteSessionsComponent.DELAY_SECONDS, 0);
        this.executeAndReload(this._serverApi.getPlayer().getDeviceApi().play(item.record.kind, item.record.id, { startTime }), event);
    }

    public queueItem(event: WaitFor, item: HistoryRecordSchema){
        this.executeAndReload(this._serverApi.getPlayer().getDeviceApi().getPlaylistApi().getItemsApi().queue(item.record), event);
    }

    public removeItem(event: WaitFor, item: HistoryRecordSchema){
        this.executeAndReload(this._serverApi.getMedia().getHistoryApi().remove(item.id), event);
    }

    private executeAndReload( action: Observable<any>, event: WaitFor ) {
        event.waitFor( action.do( e => this._obtainHistory() ) );
    }

    public goto(event: WaitFor, item: HistoryRecordSchema, position: number) {
        this._navigation.navigateTo(item.record);
        event.release();
    }

    animatedLayoutChange ( duration : number, step = 20 ) {
        let elapsed = 0;

        const token = setInterval( () => {
            elapsed += step;

            this.layoutChange.next();

            if ( elapsed > duration + step ) {
                clearInterval( token );
            }
        }, step );
    }
}
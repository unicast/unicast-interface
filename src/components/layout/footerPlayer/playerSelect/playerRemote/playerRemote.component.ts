import { Component, EventEmitter, Output, Input, HostBinding, OnDestroy } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { Subscription } from 'rxjs';

export interface RemoteTab {
    name: string;
    title: string;
    icon: string;
}

@Component( {
    selector: 'uc-player-remote',
    templateUrl: './playerRemote.template.html',
    styleUrls: [ './playerRemote.styles.less' ]
} )
export class PlayerRemoteComponent implements OnDestroy {
    @Output() layoutChange : EventEmitter<void> = new EventEmitter();

    @Output( 'close' ) closeChange : EventEmitter<void> = new EventEmitter();

    @Input() @HostBinding( 'class.fullscreen' )
    isFullscreen : boolean = false;

    public remoteTabs : RemoteTab[] = [ {
        name: 'controls',
        title: 'Remote Controller',
        icon: 'gamepad'
    }, {
        name: 'playlists',
        title: 'Playlists',
        icon: 'list-ol'
    }, {
        name: 'sessions',
        title: 'History',
        icon: 'history'
    }, {
        name: 'customActions',
        title: 'Custom Actions',
        icon: 'flask'
    } ]

    public selectedTab : RemoteTab = this.remoteTabs[ 0 ];

    protected _router: Router;

    private _routerSubscription: Subscription;

    constructor (router: Router) {
        this._router = router;

        this._routerSubscription = this._router.events
            .filter(ev => ev instanceof NavigationStart)
            .subscribe(_ => {
                if (this.isFullscreen) {
                    this.close();
                }
            });
    }

    public selectTab ( name : string ) {
        this.selectedTab = this.remoteTabs.find( tab => tab.name == name );

        this.layoutChange.next( void 0 );
    }

    public close () {
        this.closeChange.next( void 0 );
    }

    public ngOnDestroy() {
        this._routerSubscription.unsubscribe();
    }

}

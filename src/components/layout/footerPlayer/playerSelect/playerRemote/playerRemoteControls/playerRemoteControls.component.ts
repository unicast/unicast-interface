import { Component, EventEmitter, Input, Output } from '@angular/core';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { WaitFor } from 'components/common/helpers/asyncClick.directive';
import { VoiceCommandsManager } from 'components/services/speechRecognition/voiceCommandsManager';
import { MediaPlayControl } from 'components/common/services/mediaPlayControl';
import { StandardMediaQueries } from 'components/services/mediaQueries';
import { DeviceVolumeStatus } from 'components/schemas/status.schema';
import { IProgressChangedEvent } from 'components/common/slider/slider.component';
import { Time } from 'components/common/helpers/time';
import { Toast } from 'components/common/toast/toast.service';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { TranscodingTaskSchema } from 'components/schemas/task.schema';
import { MirroringManager } from 'components/services/mirroringManager';

export enum ButtonGroups {
    Groups = "GROUPS",
    Manage = "MANAGE",
    Subtitles = "SUBTITLES",
    Volume = "VOLUME"
}

@Component( {
    selector: 'uc-player-remote-controls',
    templateUrl: './playerRemoteControls.template.html',
    styleUrls: [ './playerRemoteControls.styles.less' ]
} )
export class PlayerRemoteControlsComponent extends BaseUnicastComponent {
    public readonly SUBTITLES_CHANGE: number = 0.25;

    @Input() advancedSeekOpen : boolean = false;

    @Output() advancedSeekOpenChange : EventEmitter<boolean> = new EventEmitter();

    @Output() layoutChange : EventEmitter<void> = new EventEmitter();

    public actualItem : MediaRecordSchema;
    public previewImage: string;

    private _serverApi: ServerApiManager;
    private _voiceCommand: VoiceCommandsManager;

    public transcoding : TranscodingTaskSchema[] = [];
    public mediaPlayControl: MediaPlayControl;
    public mediaQueries : StandardMediaQueries;
    public volume: DeviceVolumeStatus;
    public buttonGroups: typeof ButtonGroups = ButtonGroups;
    public openedGroup: ButtonGroups;
    public toast : Toast;
    public mirroring: MirroringManager

    constructor ( serverApi : ServerApiManager, voiceCommand : VoiceCommandsManager, mediaQueries : StandardMediaQueries, mediaPlayControl: MediaPlayControl, toast : Toast,
                mirroring: MirroringManager ) {
        super();
        this._serverApi = serverApi;
        this._voiceCommand = voiceCommand;
        this.mediaPlayControl = mediaPlayControl;
        this.mediaQueries = mediaQueries;
        this.volume = { muted: false, level: 100 };
        this.actualItem = this._serverApi.getMedia().emptyRecord();
        this.openedGroup = ButtonGroups.Groups;
        this.toast = toast;
        this.mirroring = mirroring;
    }

    _binds(){
        let deviceApi = this._serverApi.getPlayer().getDeviceApi();
        let status = deviceApi.getStatus();
        this._subscriptions.push(status.getValue().subscribe((data) => {
            if (status.isLoaded()){
                this.actualItem = {...data.media.record};
            } else {
                this.actualItem = this._serverApi.getMedia().emptyRecord();
            }

            this.volume = {...data.volume};

            this.loadRunningTasks( false );

            this.layoutChange.next( void 0 );
        }));
    }

    ngOnInit () {
        super.ngOnInit();

        this.loadRunningTasks();
    }

    async loadRunningTasks ( metricsHistory : boolean = true ) {
        this.transcoding = await this._serverApi.getTask().list( 'transcoding', 'running', { metricsHistory } ).toPromise();

        this.transcoding.sort( ( a, b ) => a.startedAt - b.startedAt );

        this.layoutChange.next( void 0 );
    }

    async stopTranscoding ( transcoding : TranscodingTaskSchema ) {
        const index = this.transcoding.findIndex( task => task.id == transcoding.id );

        transcoding = await this._serverApi.getTask().stop( transcoding.id ).toPromise();

        if ( index >= 0 ) {
            this.transcoding[ index ] = transcoding;
        }

        this.layoutChange.next( void 0 );
    }

    getTranscodingSpeed ( task : TranscodingTaskSchema ) {
        const metric = task.metrics.find( m => m.name == 'Speed' );

        if ( !metric || !metric.lastPoint ) {
            return 'N/A';
        }

        return metric.lastPoint[ 2 ];
    }

    openGroup(group: ButtonGroups){
        this.openedGroup = group;
        this.layoutChange.next( void 0 );
    }

    changeVolume(event: IProgressChangedEvent<number>){
        if (!event.outsideChange){
            this._serverApi.getPlayer().getDeviceApi().setVolume( Math.round(event.actualValue) );
        }
    }

    toggleMute(event: WaitFor){
        let deviceApi = this._serverApi.getPlayer().getDeviceApi();
        event.waitFor((this.volume.muted ? deviceApi.unmute() : deviceApi.mute()));
    }

    replayMedia(event: WaitFor) {
        let deviceApi = this._serverApi.getPlayer().getDeviceApi();
        let statusApi = deviceApi.getStatus();
        event.waitFor(statusApi.requestValue().flatMap( (status) => {
            return deviceApi.play(status.media.record.kind, status.media.record.id)
        }));
    }

    startRecognition(event: WaitFor){
        this._voiceCommand.listenCommand({before: (data) => {
            if (event.release) {
                event.release();
            }
        }});
    }

    changeSubtitleSize(event: WaitFor, change: number){
        let deviceApi = this._serverApi.getPlayer().getDeviceApi();
        let statusApi = deviceApi.getStatus();
        event.waitFor(statusApi.requestValue().flatMap( (status) =>
            deviceApi.setSubtitlesSize( (statusApi.getSubtitlesSize() || 0 ) + change )
        ));
    }

    cycleStylesSubtitle(event: WaitFor) {
        let deviceApi = this._serverApi.getPlayer().getDeviceApi();
        event.waitFor(deviceApi.execute('cycle-subtitles-style'));
    }

    changeSubtitleTime(event: WaitFor, change: number) {
        let deviceApi = this._serverApi.getPlayer().getDeviceApi();
        event.waitFor(deviceApi.execute( change > 0 ? 'increase-subtitles-offset' : 'decrease-subtitles-offset' ).do( status =>{
            if ( status.media.options.subtitlesOffset !== undefined ) {
                this.toast.show( status.media.options.subtitlesOffset + 'ms' );
            } else {
                this.toast.show( 'Subtitles shifted' );
            }
        }));
    }

    previewChangeSubtitleTime(change: number){
        this.toast.show(`Subtitles shifted (${change})`);
    }

    changeAudioTime(event: WaitFor, change: number) {
        let deviceApi = this._serverApi.getPlayer().getDeviceApi();
        event.waitFor(deviceApi.execute( change > 0 ? 'increase-audio-offset' : 'decrease-audio-offset' ).do( status =>{
            if ( status.media.options.audioOffset !== undefined ) {
                this.toast.show( status.media.options.audioOffset + 'ms' );
            } else {
                this.toast.show( 'Audio shifted' );
            }
        }));
    }

    previewAudioSubtitleTime(change: number){
        this.toast.show(`Subtitles shifted (${change})`);
    }

    restartMedia(event: WaitFor){
        let deviceApi = this._serverApi.getPlayer().getDeviceApi();
        let statusApi = deviceApi.getStatus();
        event.waitFor(statusApi.requestValue().flatMap( (status) => {
            return deviceApi.play(statusApi.getLastValue().media.record.kind, status.media.record.id,
                                { startTime: Time.convertToSeconds(status.media.time.current) })
        }));
    }

    disconnect(event: WaitFor){
        event.waitFor(this._serverApi.getPlayer().getDeviceApi().disconnect());
    }

    turnoff(event: WaitFor){
        event.waitFor(this._serverApi.getPlayer().getDeviceApi().turnOff());
    }

    setAdvancedSeekOpen ( opened : boolean ) {
        if ( opened == this.advancedSeekOpen ) {
            return;
        }

        if ( opened ){
            this.changePreviewImage(new Time());
        }

        this.advancedSeekOpen = opened;

        this.layoutChange.next( void 0 );
    }

    changePreviewImage(offset: Time) {
        this.previewImage = this.mediaPlayControl.getPreviewUrl(offset);
    }
}

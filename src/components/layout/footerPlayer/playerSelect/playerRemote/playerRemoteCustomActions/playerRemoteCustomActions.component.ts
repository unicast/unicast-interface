import { Component, EventEmitter, Output } from '@angular/core';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { CustomActionGroup, CustomAction } from 'components/schemas/customActions.schema';
import { WaitFor } from 'components/common/helpers/asyncClick.directive';
import { ApplicationContextManager } from 'components/services/applicationContextManager';
import { IconPosition } from 'components/common/buttons/button/button.component';

@Component( {
    selector: 'uc-player-remote-custom-actions',
    templateUrl: './playerRemoteCustomActions.template.html',
    styleUrls: [ './playerRemoteCustomActions.styles.less' ]
} )
export class PlayerRemoteCustomActionsComponent extends BaseUnicastComponent {
    public readonly IconPosition: typeof IconPosition = IconPosition

    @Output() layoutChange : EventEmitter<void> = new EventEmitter();

    private _serverApi: ServerApiManager;
    private _applicationContext: ApplicationContextManager;

    public loaded: boolean;
    public groups: Array<CustomActionGroup> = [];

    constructor(serverApi: ServerApiManager, applicationContext: ApplicationContextManager) {
        super();
        this._serverApi = serverApi;
        this._applicationContext = applicationContext;
    }

    protected _binds(){
        this._serverApi.getCustomActions().list().subscribe((list) => {
            this.loaded = true;
            this.groups = list;
            this.layoutChange.next( void 0 );
        });
    }


    public actionClick(action: CustomAction, event: WaitFor) {
        event.waitFor( this._serverApi.getCustomActions().execute(action.name, this._applicationContext.getContext()) );
    }

}
import { Component, OnInit, Input, OnChanges, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

import { LayoutUtils } from 'components/common/helpers/layoutUtils';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { PlayableMediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { NavigationManager } from 'components/services/navigationManager';

@Component( {
    selector: 'uc-media-badge',
    templateUrl: './mediaBadge.template.html',
    styleUrls: [ './mediaBadge.styles.less' ],
    animations: [
        trigger( 'infoOpenClose', [
            state( 'closed', style( { paddingTop: '80px' } ) ),
            state( 'open', style( { paddingTop: '0px' } ) ),

            transition( 'open <=> closed', animate( '0.3s ease-in-out' ) )
        ] ),
        trigger( 'toolbarOpenClose', [
            state( 'closed', style( { 'height': 0, overflow: 'hidden', paddingTop: '0px', opacity: 0 } ) ),
            state( 'open', style( { 'height': '*', overflow: 'hidden', paddingTop: '15px', opacity: 1 } ) ),

            transition( 'open <=> closed', animate( '0.3s ease-in-out' ) )
        ] )
    ],
    host: {
        '(click)': 'onMediaClick()'
    }
} )

export class MediaBadgeComponent implements OnInit, OnChanges {
    @Input() item : PlayableMediaRecordSchema;

    @Input() status : string;

    @Input() title : string;

    @Input() subtitle : string;

    @Input() artwork : string;

    @Input() expanded : boolean = true;

    @Input() toolbarOpen : boolean = false;

    @Output() toolbarOpenChange : EventEmitter<boolean> = new EventEmitter<boolean>();

    @ViewChild( 'toolbarRef' ) toolbarRef : ElementRef;

    protected toolbarHeight : number = 80;

    get hasToolbar () : boolean {
        return this.toolbarRef && this.toolbarRef.nativeElement.children.length > 0;
    }

    protected artworkSafe : SafeStyle;

    protected sanitizer : DomSanitizer;

    protected _navigation : NavigationManager;

    constructor( sanitizer : DomSanitizer, navigation: NavigationManager ) {
        this.sanitizer = sanitizer;
        this._navigation = navigation;
    }

    ngOnInit() { }

    ngOnChanges () {
        if ( this.artwork ) {
            this.artworkSafe = this.sanitizer.bypassSecurityTrustStyle( this.artwork );
        } else {
            this.artwork = LayoutUtils.EMPTY_IMG;
        }
    }

    goto() {
        if (this.item != null) {
            this._navigation.navigateTo(this.item);
        }
    }

    onMediaClick () {
        if ( !this.hasToolbar ) {
            this.goto();
        } else {
            this.toggleToolbar();
        }
    }

    toggleToolbar () {
        if ( this.hasToolbar ) {
            this.toolbarOpen = !this.toolbarOpen;

            this.toolbarOpenChange.next( this.toolbarOpen );
        }
    }

    openToolbar () {
        if ( !this.toolbarOpen && this.hasToolbar ) {
            this.toolbarOpen = true;

            this.toolbarOpenChange.next( this.toolbarOpen );
        }
    }

    closeToolbar () {
        if ( this.toolbarOpen && this.hasToolbar ) {
            this.toolbarOpen = false;

            this.toolbarOpenChange.next( this.toolbarOpen );
        }
    }
}

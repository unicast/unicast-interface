import { Component, OnInit, Input } from '@angular/core';
import { CustomMediaRecordSchema } from 'components/schemas/mediaRecord.schema';

@Component({
    selector: 'uc-custom-badge, .uc-custom-badge',
    templateUrl: './customBadge.template.html',
    styleUrls: [ './customBadge.styles.less' ],
})
export class CustomBadgeComponent implements OnInit {
    @Input() item: CustomMediaRecordSchema;

    @Input() expanded : boolean = false;

    @Input() status? : string;

    constructor() {

    }

    ngOnInit() { }


}
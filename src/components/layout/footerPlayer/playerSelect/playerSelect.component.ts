import { Component, ElementRef, ViewChild, Input, HostBinding } from '@angular/core';

//Helpers
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { PopoverDirective } from "components/common/popover/popover.directive";
import { Device } from 'components/services/serverApis/helpers/device';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';

@Component( {
    selector: 'uc-player-select',
    templateUrl: './playerSelect.template.html',
    styleUrls: [ './playerSelect.styles.less' ]
} )
export class PlayerSelectComponent extends BaseUnicastComponent {
    host : ElementRef;

    playersListOpen : boolean = false;

    playerRemoteOpen : boolean = false;

    playerPlaylistsOpen : boolean = false;

    deviceList: Array<Device> = [];

    selectedDevice: Device;

    isConnecting: boolean = false;

    @Input() @HostBinding( 'class.player-select-wide' )
    isFullscreen : boolean = false;

    private _serverApi: ServerApiManager;

    @ViewChild( 'remotePopover' ) remotePopover : PopoverDirective;
    @ViewChild( 'menuPopover' ) menuPopover : PopoverDirective;

    constructor ( host: ElementRef, serverApi: ServerApiManager ) {
        super();
        this.host = host;

        this._serverApi = serverApi;

        this.selectedDevice = Device.NOT_CONNECTED;

        this.updateDevicesList();

    }

    protected _binds() {

        this._subscriptions.push(this._serverApi.getPlayer().getDeviceApi().deviceObservable.subscribe((data) => {
            this.selectedDevice = data;
            if (this.playerPlaylistsOpen || this.playerRemoteOpen){
                this.updateRemotePopover();
            }
        }));

    }

    updateDevicesList () {
        this.isConnecting = true;
        let deviceApi = this._serverApi.getPlayer().getDeviceApi();
        deviceApi.list().subscribe((data) => {
            let update: boolean = (this.deviceList.length != data.devices.length);
            this.deviceList = data.devices;
            this.isConnecting = false;
            if (update){
                this.menuPopover.update();
            }
            let device = this.deviceList.find((d: Device, index: number) => d.equals(this.selectedDevice));
            if (this.deviceList.length > 0){
                /* If the device didn't exists on the list, but the list is returning something
                 * it should save a new device as the defined, since it shouldn't be any temporary error with
                 * the network.
                 */
                if (!device){
                    deviceApi.setDevice(this.deviceList[0]);
                }
            } else {
                this.selectedDevice = Device.NOT_CONNECTED;
            }
        }, (err) => {
            this.isConnecting = false;
            this.selectedDevice = Device.NOT_CONNECTED;
        });
    }

    togglePlayersList () {
        this.playersListOpen = !this.playersListOpen;
        if (this.playersListOpen){
            this.updateDevicesList();
        }
    }

    closePlayerRemote () {
        this.playerRemoteOpen = false;
    }

    togglePlayerRemote () {
        this.playerRemoteOpen = !this.playerRemoteOpen;

        if ( this.playerRemoteOpen && this.playersListOpen ) {
            this.playersListOpen = false;
        }
    }

    togglePlaylistsView () {
        this.playerPlaylistsOpen = !this.playerPlaylistsOpen;
    }

    updateRemotePopover () {
        this.remotePopover.update();
    }

    selectDevice (device: Device) {
        this._serverApi.getPlayer().getDeviceApi().setDevice(device);

        this.playersListOpen = false;
    }
}
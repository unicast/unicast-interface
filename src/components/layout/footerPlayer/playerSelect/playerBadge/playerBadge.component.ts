import { Component, Input, HostBinding } from '@angular/core';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { RequestsState } from 'components/services/serverApis/playerServers/playerServerApi';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';

@Component( {
    selector: 'uc-player-badge',
    templateUrl: './playerBadge.template.html',
    styleUrls: [ './playerBadge.styles.less' ]
} )

export class PlayerBadgeComponent extends BaseUnicastComponent {
    @Input() name : string;

    @Input() type : string;

    @Input() icon : string;

    @Input() colorable : boolean = false;

    @HostBinding( 'class.isRequesting' )
    colorIsRequesting: boolean;

    @HostBinding( 'class.isError' )
    colorIsError: boolean;

    private _serverApi: ServerApiManager;

    public defaultTypeIcons : any = {
        '': 'ambulance',
        chromecast: 'chrome',
        local: 'desktop',
        mpv: 'play-circle-o',
        kodi: 'ticket'
    }

    constructor ( serverApi: ServerApiManager) {
        super();
        this._serverApi = serverApi;
    }

    protected _binds() {
        if (this.colorable){
            this._subscriptions.push(this._serverApi.getPlayer().connectionState.subscribe((data) => {
                switch (data){
                    case RequestsState.Requesting:
                        this.colorIsRequesting = true;
                        this.colorIsError = false;
                        break;
                    case RequestsState.InError:
                        this.colorIsRequesting = false;
                        this.colorIsError = true;
                        break;
                    default:
                        this.colorIsRequesting = false;
                        this.colorIsError = false;
                }
            }));
        }

    }


}
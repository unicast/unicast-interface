import { Component, Input, HostBinding } from "@angular/core";

@Component({
    selector: 'uc-player-control',
    template: `<ng-content></ng-content>`,
    styleUrls: [ './playerControl.styles.less' ]
} )
export class PlayerControlComponent {
    @Input( ) @HostBinding('class.disabled') disabled: boolean = false;

}
import { Directive, HostBinding, Input, EventEmitter, Output, NgZone } from '@angular/core';
import { ElementRef } from '@angular/core';

@Directive( {
    selector: '[ucHamburger]',
    exportAs: 'hamburger'
} )
export class HamburgerDirective {
    @Input( 'ucHamburgerEnabled' )
    public enabled : boolean = true;

    public element : ElementRef;

    public hammer : HammerManager;

    public offset : number = 0;

    public commitedOffset : number = 0;

    public isValidPan : boolean = false;

    public isOpen : boolean = true;

    public maxWidth : number = 306;

    public enableMaxWidthAt : number = 400;

    public width : number = this.maxWidth;

    public minGutter : number = 0;

    protected zone : NgZone;

    @HostBinding( 'style.width' )
    widthStyle : string = '' + this.width + 'px';

    @Output( 'ucHamburgerOpenChange' )
    public isOpenChange : EventEmitter<boolean> = new EventEmitter( true );

    constructor ( element : ElementRef, zone : NgZone ) {
        this.element = element;
        this.zone = zone;
    }

    isGestureEnabled () {
        return this.enabled;
    }

    ngOnInit () {
        this.zone.runOutsideAngular( () => {
            this.hammer = new Hammer( window.document.body );
                    
            this.hammer.on( 'panstart', this.handlePanStart.bind( this ) );
            // Pan behaviour is unstable
            // this.hammer.on( 'panleft panright', this.handlePan.bind( this ) );
            // this.hammer.on( 'panend', this.handlePanEnd.bind( this ) );

            this.hammer.on( 'swipeleft', this.handleSwipeLeft.bind( this ) );
            this.hammer.on( 'swiperight', this.handleSwipeRight.bind( this ) );
        } );
        
        this.offset = this.getPanelWidth();
    }

    ngOnDestroy () {
        this.hammer.destroy();
    }

    ngOnChanges ( changes ) {
        setTimeout( () => {
            if ( 'enabled' in changes ) {
                if ( changes.enabled.currentValue ) {
                    this.close();
                } else {
                    this.open();
                }
            }
        } );
    }

    setAnimation ( animation : string ) {
        this.element.nativeElement.style.transition = animation;
    }

    enableAnimation () {
        this.setAnimation( 'transform ease-in-out 0.7s' );
    }

    disableAnimation () {
        this.setAnimation( 'none' );
    }

    setOffset ( offset : number ) {
        if ( offset != this.offset ) {
            this.offset = offset;
    
            this.element.nativeElement.style.transform = `translate( ${ this.offset - this.getPanelWidth() }px )`
        }
    }

    setCommitedOffset ( offset : number ) {
        this.commitedOffset = offset;

        this.setOffset( offset );
    }

    getWindowSize () {
        // Taken from https://stackoverflow.com/questions/3437786/get-the-size-of-the-screen-current-web-page-and-browser-window
        var w = 0;var h = 0;
        //IE
        if ( !window.innerWidth ) {
            if ( !( document.documentElement.clientWidth == 0 ) ) {
            //strict mode
                w = document.documentElement.clientWidth;
                h = document.documentElement.clientHeight;
            } else {
            //quirks mode
                w = document.body.clientWidth;
                h = document.body.clientHeight;
            }
        } else {
            //w3c
            w = window.innerWidth;
            h = window.innerHeight;
        }

        return { width: w, height: h };
    }

    getPanelWidth () : number {
        this.width = this.getWindowSize().width - this.minGutter;

        if ( this.width > this.enableMaxWidthAt ) {
            this.width = Math.min( this.maxWidth, this.width );
        }

        this.widthStyle = '' + this.width + 'px';

        return this.width;
    }

    handlePanStart ( ev : HammerInput ) {
        ev.preventDefault();

        this.disableAnimation();

        this.isValidPan = this.isOpen || ev.pointers[ 0 ].clientX <= 100;
    }

    handlePan ( ev : HammerInput ) {
        if ( !this.isValidPan ) return;

        ev.preventDefault();
        
        const dragOffset = ev.deltaX;
                
        this.setOffset( Math.max( 0, Math.min( this.commitedOffset + dragOffset, this.getPanelWidth() ) ) );
    }

    handleSwipeLeft ( ev : HammerInput ) {
        if ( !this.isGestureEnabled() ) return;

        if ( !this.isValidPan ) return;
        
        ev.preventDefault();

        this.close();

        this.hammer.stop( false );
    }

    handleSwipeRight ( ev : HammerInput ) {
        if ( !this.isGestureEnabled() ) return;
        
        if ( !this.isValidPan ) return;

        ev.preventDefault();

        this.open();

        this.hammer.stop( false );
    }

    handlePanEnd ( ev : HammerInput ) {
        if ( !this.isValidPan ) return;

        if ( Math.abs( ev.deltaX ) > ( this.getPanelWidth() / 2 ) ) {
            if ( ev.deltaX >= 0 ) {
                this.open();
            } else {
                this.close();
            }
        } else {
            this.enableAnimation();

            this.setOffset( this.commitedOffset );
        }
    }

    open () {
        if ( this.isOpen ) return;

        this.enableAnimation();

        this.zone.run( () => {
            this.isOpen = true;
    
            this.setCommitedOffset( this.getPanelWidth() );
            
            this.isOpenChange.next( this.isOpen );
        } );
    }

    close () {
        if ( !this.isOpen ) return;

        this.enableAnimation();

        this.zone.run( () => {
            this.isOpen = false;
    
            this.setCommitedOffset( 0 );
            
            this.isOpenChange.next( this.isOpen );
        } );
    }
}
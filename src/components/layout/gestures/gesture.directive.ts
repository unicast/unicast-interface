import { Directive, Output, EventEmitter, NgZone } from '@angular/core';
import { ElementRef } from '@angular/core';
import Hammer from 'hammerjs';

@Directive( {
    selector: '[ucGestureSwipe]'
} )
export class GestureSwipeDirective {
    @Output( 'ucGestureSwipe' ) action : EventEmitter<HammerInput> = new EventEmitter();

    public enable : boolean = true;

    public element : ElementRef;

    public hammer : HammerManager;

    protected zone : NgZone;

    constructor ( element : ElementRef, zone : NgZone ) {
        this.element = element;
        this.zone = zone;
    }

    isGestureEnabled () {
        return this.enable;
    }

    ngOnInit () {
        this.zone.runOutsideAngular( () => {
            this.hammer = new Hammer.Manager( this.element.nativeElement, {
                enable: () => this.isGestureEnabled(),
                domEvents: true
            } );
    
            this.hammer.add( new Hammer.Swipe( { direction: Hammer.DIRECTION_HORIZONTAL } ) );
    
            this.hammer.on( 'swipeleft swiperight', this.handleSwipe.bind( this ) );
        } );
    }

    ngOnDestroy () {
        this.hammer.destroy();
    }

    handleSwipe ( ev : HammerInput ) {
        ev.preventDefault();

        ev.srcEvent.stopPropagation();
        
        ev.srcEvent.cancelBubble = true;
        
        this.zone.run( () => this.action.next( ev ) );
    }
}
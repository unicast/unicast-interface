import { Directive } from '@angular/core';
import { ArtManager } from '../../services/artManager';
import { ElementRef } from '@angular/core';

@Directive( {
    selector: '[pageArt]'
} )
export class PageArtDirective {
    public art: ArtManager;

    public element : ElementRef;

    public backgroundProperty: string;

    public backgroundSizeProperty: string;

    constructor ( art : ArtManager, element : ElementRef  ) {
        this.art = art;

        this.element = element;

        this.clearBackground();
    }

    ngOnInit () {
        this.art.current.subscribe( art => this.onArtChange( art ) );
    }

    onArtChange ( art ) {
        if ( art ) {
            this.setBackgroundImage( art.background );
        } else {
            this.clearBackground();
        }
    }

    updateStyles () {
        this.element.nativeElement.style.position = 'fixed';
        this.element.nativeElement.style.top = 0;
        this.element.nativeElement.style.left = 0;
        this.element.nativeElement.style.right = 0;
        this.element.nativeElement.style.bottom = 0;
        this.element.nativeElement.style.zIndex = -1;
        this.element.nativeElement.style.backgroundColor = 0;
        this.element.nativeElement.style.background = this.backgroundProperty;
        this.element.nativeElement.style.backgroundSize = this.backgroundSizeProperty;
        this.element.nativeElement.style.backgroundRepeat = 'no-repeat';
        this.element.nativeElement.style.backgroundAttachment = 'fixed';
        this.element.nativeElement.style.backgroundClip = 'border-box';
        this.element.nativeElement.style.backgroundOrigin = 'padding-box';
        this.element.nativeElement.style.backgroundPosition = '0% 0%, center center';
    }

    setBackgroundImage ( image : string ) {
        this.backgroundProperty = `linear-gradient(
            rgba(0, 0, 0, 0.50),
            rgba(0, 0, 0, 0.50)
        ), url("${ image }") no-repeat center center fixed`;

        this.backgroundSizeProperty = 'cover';

        this.updateStyles();
    }

    clearBackground () {
        this.backgroundProperty = null;
        this.backgroundSizeProperty = 'cover';

        this.updateStyles();
    }
}
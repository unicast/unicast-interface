import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component( {
    selector: 'uc-menu-subsection',
    templateUrl: './menuSubsection.template.html',
    styleUrls: [ './menuSubsection.styles.less' ],

} )
export class MenuSubsectionComponent {
    @Input() collapsible: boolean = false;
    @Input() collapsed: boolean = true;
    @Input() label;
    @Output() collapseChanged: EventEmitter<boolean> = new EventEmitter();
}
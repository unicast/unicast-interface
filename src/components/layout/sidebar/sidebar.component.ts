import { Component } from '@angular/core';

import { SidebarManager } from '../../services/sidebarManager';

@Component( {
    selector: 'uc-sidebar',
    templateUrl: './sidebar.template.html',
    styleUrls: [ './sidebar.styles.less' ]
} )
export class SideBarComponent {

    public regions: Array<string>;

    public manager: SidebarManager;

    constructor ( manager : SidebarManager ) {
        this.regions = [ 'primary', 'secondary' ];

        this.manager = manager;
    }
}
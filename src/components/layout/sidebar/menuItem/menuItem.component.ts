import { Component, Input } from '@angular/core';

@Component( {
    selector: 'uc-menu-item, [uc-menu-item]',
    templateUrl: './menuItem.template.html',
    styleUrls: [ './menuItem.styles.less' ]  
} )
export class MenuItemComponent {
    @Input() icon;
}
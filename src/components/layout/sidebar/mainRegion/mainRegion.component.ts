import { Component } from '@angular/core';
import { SidebarManager } from 'components/services/sidebarManager';


@Component( {
    selector: 'uc-sidebar-main-region',
    templateUrl: './mainRegion.template.html',
    styleUrls: [ './mainRegion.styles.less' ]
} )
export class SideBarMainRegionComponent {
    sidebarManager : SidebarManager;

    constructor ( sidebarManager : SidebarManager ) {
        this.sidebarManager = sidebarManager;
    }

    mainActionClicked () {
        this.sidebarManager.mainActionClicked.next();
    }
}
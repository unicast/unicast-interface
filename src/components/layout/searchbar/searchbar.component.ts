import { Component, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';


@Component( {
    selector: 'uc-search-bar',
    templateUrl: './searchbar.template.html',
    styleUrls: [ './searchbar.styles.less' ]
} )
export class SearchBarComponent extends BaseUnicastComponent {

    private static readonly WAIT_MS: number = 300;

    @ViewChild('searchBox') input: ElementRef;

    @Input('selected') selected: boolean;

    @Output('blur') blur: EventEmitter<Event>;

    private _serverApi: ServerApiManager;
    private _timeout;

    constructor(serverApi: ServerApiManager){
        super();
        this._serverApi = serverApi;
        this._timeout = undefined;

        this.selected = false;

        this.blur = new EventEmitter<Event>();
    }

    ngOnInit(): void {
        super.ngOnInit();
        if ( this.selected ){
            this.input.nativeElement.focus();
        }
        this.input.nativeElement.value = this._serverApi.getMedia().getActualFilter();
    }

    prepareFilter(value: string) {
        if (this._timeout !== undefined){
            clearTimeout(this._timeout);
        }
        this._timeout = setTimeout(() => {
            this.search(value, false);
        }, SearchBarComponent.WAIT_MS);

    }

    search(value: string, byUser: boolean) {
        if (byUser){
            this.input.nativeElement.blur();
        }
        this._serverApi.getMedia().filter(value, byUser);
    }
}

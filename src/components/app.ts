import { Component, Injector, ViewChild } from '@angular/core';

// Services
import { SidebarManager } from 'components/services/sidebarManager';
import { TopbarManager } from 'components/services/topbarManager';
import { PageTitle } from 'components/common/services/pageTitle';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { TranslatorManager } from 'components/services/translatorManager';
import { LocalStorageManager } from 'components/services/storageManager';
import { ServerApiController } from 'components/services/serverApis/serverApiController';
import { NotificationsService } from 'components/common/commandCenter/notifications/notifications.service';
import { AppStorage } from 'components/app.storage';

import { ServerApiHost } from 'components/services/serverApis/serverApiHost';
import { SideBarMainRegionComponent } from 'components/layout/sidebar/mainRegion/mainRegion.component';
import { NotificationOptions, NotificationType } from 'components/common/commandCenter/notifications/notification.component';
import { EventType, EventsOptions } from './services/serverApis/helpers/eventOptions';
import { StandardMediaQueries } from 'components/services/mediaQueries';
import { HamburgerDirective } from 'components/layout/hamburger/hamburger.directive';
import { ApplicationContextManager, ContextToken } from 'components/services/applicationContextManager';
import { PlaylistContextSchema, DeviceContextSchema } from 'components/schemas/applicationContexts.schema';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';

@Component({
    selector: '.unicast-app',
    templateUrl: './app.template.html',
    styleUrls: [ './app.styles.less' ],
})
export class AppComponent extends BaseUnicastComponent {
    isHamburgerEnabled : boolean = false;

    isHamburgerOpen : boolean = true;

    @ViewChild( 'sidemenu' )
    sidemenu : HamburgerDirective;

    private serverApi: ServerApiManager;
    private applicationContext: ApplicationContextManager;
    private contextTokens: Array<ContextToken<any>>;

    constructor ( sidebar : SidebarManager, topbar : TopbarManager, injector : Injector, serverApi: ServerApiManager,
                  notification: NotificationsService, localStorage: LocalStorageManager, appStorage: AppStorage,
                  pageTitle: PageTitle, serverApiController: ServerApiController, translator: TranslatorManager,
                  mediaQueries : StandardMediaQueries, applicationContext: ApplicationContextManager ) {
        super();
        this.serverApi = serverApi;
        this.applicationContext = applicationContext;

        // Make the sidebar closable/openable only on smaller screens
        mediaQueries.isSmartphoneScreen.subscribe( is => this.isHamburgerEnabled = is );

        sidebar.mainActionClicked.subscribe( () => {
            this.closeHamburger();
        } );

        sidebar.setPrimaryMenu( SideBarMainRegionComponent, injector );

        //Define titles
        topbar.setDefaultTitle( 'UniCast' );
        pageTitle.setSuffix(' - Unicast');

        //Init the LocalStorage recipient (needs to be one of the first because it's used by lot of Services)
        appStorage.setStorage(localStorage);

        //Init ServerApi services
        serverApi.start(new ServerApiHost(appStorage.getCurrentServerApi()), serverApiController);

        //Init translator services
        translator.getTranslation().use(appStorage.getCurrentLanguage());

        //Ensure that anytime the device is switched, the preference is saved
        appStorage.setActualDevice(serverApi.getPlayer().getDeviceApi().deviceObservable);

        //Try to select at least one device, in order to put all in motion
        serverApi.getPlayer().startDevice(appStorage.getActualDevice());

        //Subscribe all the error messages from the server to be presented as notification
        notification.showStream(serverApi.getBase().getEvents().filter<EventsOptions>(
            (value: EventsOptions) => value.type == EventType.ERROR
        ).map<EventsOptions, NotificationOptions>(
            (v) => { return {...v, type: NotificationType.ERROR } }
        ));
    }

    ngOnInit() {
        super.ngOnInit();
        //Register global context handlers (not only available when a component is visible), and their changes
        this.registerGlobalContextKeys();
    }

    registerGlobalContextKeys() {
        this.contextTokens = [];

        //Device
        const deviceContextToken: ContextToken<DeviceContextSchema> = this.applicationContext.registerToken<DeviceContextSchema>(['device']);
        this.serverApi.getPlayer().getDeviceApi().deviceObservable.subscribe(device => {
            this.applicationContext.setContext(deviceContextToken, 'device', {name: device.name});
        });
        this.contextTokens.push(deviceContextToken);

        //Playlists
        const playlistContextToken: ContextToken<PlaylistContextSchema> = this.applicationContext.registerToken<PlaylistContextSchema>(['playlist']);
        this.serverApi.getPlayer().getDeviceApi().getPlaylistApi().playlistIdObservable.subscribe(playlistId => {
            this.applicationContext.setContext(playlistContextToken, 'playlist', {id: playlistId});
        });
        this.contextTokens.push(playlistContextToken);

    }

    ngOnDestroy() {
        super.ngOnDestroy();
        this.contextTokens.forEach(t => this.applicationContext.unregisterToken(t));
    }

    closeHamburger() {
        if ( this.sidemenu != null && this.isHamburgerEnabled ) {
            this.sidemenu.close();
        }
    }
}

import { Component } from '@angular/core';
import { SidebarManager } from 'components/services/sidebarManager';


@Component( {
    selector: 'uc-people-details-sidemenu',
    templateUrl: './sidemenu.template.html',
    styleUrls: [ './sidemenu.styles.less' ]
} )
export class PeopleDetailsSidemenuComponent {
    public sidebar: SidebarManager;

    constructor(sidebar: SidebarManager){
        this.sidebar = sidebar;

    }

}
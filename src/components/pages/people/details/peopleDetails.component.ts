import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { PageTitle } from 'components/common/services/pageTitle';
import { SidebarManager } from 'components/services/sidebarManager';
import { ArtManager } from 'components/services/artManager';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { DefaultMediaButtonActions } from 'components/pages/common/buttonActions/defaultMediaButtonActions';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { PersonRecordSchema } from 'components/schemas/mediaRecord.schema';
import { NavigationManager } from 'components/services/navigationManager';
import { StandardMediaQueries } from 'components/services/mediaQueries';
import { PeopleDetailsSidemenuComponent } from 'components/pages/people/details/sidemenu/sidemenu.component';

@Component( {
    selector: 'uc-people-details',
    templateUrl: './peopleDetails.template.html',
    styleUrls: [ './peopleDetails.styles.less' ],
    providers: [ DefaultMediaButtonActions ]
} )
export class PeopleDetailsComponent extends BaseUnicastComponent {
    public sidebar: SidebarManager;
    public sidemenuToken: any;
    public art: ArtManager;
    public navigation: NavigationManager;
    public route: ActivatedRoute;
    public serverApi: ServerApiManager;
    public mediaQueries : StandardMediaQueries;

    public htmlTitle: PageTitle;

    public personArtManagerToken: any;
    public person: PersonRecordSchema;

    constructor ( navigation: NavigationManager, route : ActivatedRoute, sidebar : SidebarManager, art : ArtManager, serverApi: ServerApiManager, mediaQueries : StandardMediaQueries, htmlTitle: PageTitle, defaultActions: DefaultMediaButtonActions) {
        super();
        this.sidebar = sidebar;
        this.sidemenuToken = sidebar.setSecondaryMenu( PeopleDetailsSidemenuComponent );
        this.art = art;
        this.navigation = navigation;
        this.route = route;
        this.serverApi = serverApi;
        this.mediaQueries = mediaQueries;
        this.htmlTitle = htmlTitle;
    }

    protected _binds () {
        this._subscriptions.push(this.route.params.subscribe( async params => {
            this.loadPerson( params.id );
        } ));

        this._subscriptions.push(this.serverApi.getMedia().filterChanged.skip(1).subscribe(([value, byUser]) => {
            if (byUser){
                this.navigation.navigateToPeople();
            }
        }));
    }

    ngOnDestroy () {
        super.ngOnDestroy();
        this.sidebar.disposeSecondaryMenu( this.sidemenuToken );
        this.onPersonChange( null );
    }

    loadPerson ( id: string ) {
        this.serverApi.getMedia().getPeopleApi().get( id, { credits: true } ).subscribe( data => {
           this.person = data;
           this.onPersonChange( data );
        });
    }

    onPersonChange ( person: PersonRecordSchema ) {
        if ( this.personArtManagerToken ) {
            this.art.dispose( this.personArtManagerToken );
        }

        if ( person ) {
            this.personArtManagerToken = this.art.register( person.cachedArtwork );
            this.htmlTitle.setTitle( person.name );
        }

    }

}
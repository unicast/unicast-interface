import { Component } from '@angular/core';
import { PageTitle } from 'components/common/services/pageTitle';

import { TopbarManager } from '../../services/topbarManager';

@Component( {
    selector: 'uc-people',
    templateUrl: './people.template.html',
    styleUrls: [ './people.styles.less' ]
} )
export class PeopleComponent {

    constructor ( topbar : TopbarManager, htmlTitle: PageTitle) {
        let title = 'People'
        topbar.setTitle( title );
        htmlTitle.setTitle( title );
    }

}

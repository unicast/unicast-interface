import { Component, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { PeopleListSidemenuComponent } from './sidemenu/sidemenu.component';
import { TopbarManager } from 'components/services/topbarManager';
import { PageTitle } from 'components/common/services/pageTitle';
import { StandardMediaQueries } from 'components/services/mediaQueries';
import { FiltersSidemenuService } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { SessionCache } from 'components/common/services/sessionCache';
import { MediaListHelper } from 'components/pages/common/mediaListHelper';
import { SidebarManager } from 'components/services/sidebarManager';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { DefaultMediaButtonActions } from 'components/pages/common/buttonActions/defaultMediaButtonActions';
import { FiltersMediaSidemenuService, MediaListQueryFilters } from 'components/pages/common/sidemenuFilters/filtersMediaSidemenu.service';
import { DelayedExecutions } from 'components/common/services/delayedExecutions';
import { ApplicationContextManager } from 'components/services/applicationContextManager';

@Component( {
    selector: 'uc-people-list',
    templateUrl: './peopleList.template.html',
    styleUrls: [ './peopleList.styles.less' ],
    providers: [ FiltersMediaSidemenuService.forRoot(''), DefaultMediaButtonActions ]
} )
export class PeopleListComponent extends MediaListHelper<MediaListQueryFilters> {

    public router: Router;
    public selectedMovie : string;

    constructor ( router: Router, injector : Injector, cache : SessionCache, sidemenuService : FiltersSidemenuService<MediaListQueryFilters>, sidebar : SidebarManager,
                  serverApi:  ServerApiManager, topbar: TopbarManager, htmlTitle: PageTitle, mediaQueries : StandardMediaQueries,
                  defaultButtons: DefaultMediaButtonActions, delayedExecutions: DelayedExecutions, applicationContext: ApplicationContextManager ) {
        super([ PeopleListComponent, 'people' ] , sidebar, cache, sidemenuService, serverApi, topbar, htmlTitle, mediaQueries, defaultButtons, delayedExecutions, applicationContext);
        this.setSecondaryMenu( PeopleListSidemenuComponent.name, injector );

        this.step = 24;
        this.router = router;

        this.serverApi.getMedia().getCollectionApi().list('people').subscribe( (data) => {
            this.collections = data;
        });

    }

    listByApi(take: number, skip: number, opts: any): Observable<any> {
        return this.serverApi.getMedia().getPeopleApi().list(take, skip, { ...opts, credits: true });//.map( l => l.filter(e => !!e.name || (!!e.cachedArtwork && !!e.cachedArtwork.poster)));
    }
}
import { Component } from '@angular/core';

import { FiltersSidemenuService } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { MediaListQueryFilters } from 'components/pages/common/sidemenuFilters/filtersMediaSidemenu.service';

@Component( {
    selector: 'uc-people-list-sidemenu',
    templateUrl: './sidemenu.template.html',
    styleUrls: [ './sidemenu.styles.less' ]
} )
export class PeopleListSidemenuComponent {
    public service: FiltersSidemenuService<MediaListQueryFilters>;

    constructor ( service : FiltersSidemenuService<MediaListQueryFilters> ) {
        this.service = service;
    }

}
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { StandardMediaQueries } from 'components/services/mediaQueries';
import { DefaultMediaButtonActions } from 'components/pages/common/buttonActions/defaultMediaButtonActions';


@Component( {
    selector: 'uc-seasons-list',
    templateUrl: './seasonsList.template.html',
    styleUrls: [ './seasonsList.styles.less' ],
    providers: [ DefaultMediaButtonActions ]
} )
export class SeasonsListComponent {
    @Input() seasons : Array<any>;

    @Input() active : Array<any>;

    @Input() sectionBackground : String;

    @Output() activeChange : EventEmitter<any> = new EventEmitter();

    mediaQueries : StandardMediaQueries;

    constructor ( mediaQueries : StandardMediaQueries ) {
        this.mediaQueries = mediaQueries;
    }

    select ( season ) {
        this.active = season;

        this.activeChange.next( this.active );
    }
}
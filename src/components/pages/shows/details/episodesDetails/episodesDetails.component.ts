import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';

// Components
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { TvEpisodeMediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { StandardMediaQueries } from 'components/services/mediaQueries';
import { SingularSubject } from 'components/common/helpers/singularSubject';
import { DefaultMediaButtonActions } from 'components/pages/common/buttonActions/defaultMediaButtonActions';
import { TitleDropdownAction } from 'components/common/section/section.component';

@Component( {
    selector: 'uc-episodes-details',
    templateUrl: './episodesDetails.template.html',
    styleUrls: [ './episodesDetails.styles.less' ],
    providers: [ DefaultMediaButtonActions ]
} )
export class EpisodesDetailsComponent {
    @Input() episode : TvEpisodeMediaRecordSchema;

    @Input() hasNext : boolean;

    @Input() hasPrevious : boolean;

    @Input() subtitlesIsOpened: boolean;

    @Input() sectionBackground : string;

    @Input() titleDropdowns: Array<TitleDropdownAction>;

    @Output() titleDropdownsSelect : EventEmitter<[number, any]> = new EventEmitter();

    @Output() previous : EventEmitter<any> = new EventEmitter();

    @Output() next : EventEmitter<any> = new EventEmitter();

    @Output() close : EventEmitter<any> = new EventEmitter();

    @Output() subtitlesIsOpenedChange: EventEmitter<boolean> = new EventEmitter<boolean>();

    public serverApi: ServerApiManager;

    public mediaQueries : StandardMediaQueries;

    public subtitlesOpened: boolean;

    constructor ( serverApi : ServerApiManager, mediaQueries : StandardMediaQueries, defaultActions: DefaultMediaButtonActions ) {
        this.serverApi = serverApi;
        this.mediaQueries = mediaQueries;
        defaultActions.subtitles.injectAction((media) => this.toggleSubtitles(media));
    }

    play (episode): Observable<any>{
        return this.serverApi.getPlayer().getDeviceApi().play( MediaKind.TvEpisode, episode.id);
    }

    queue(episode): Observable<any>{
        return this.serverApi.getPlayer().getDeviceApi().getPlaylistApi().getItemsApi().queue(episode);
    }

    goToPrevious () {
        if ( this.hasPrevious ) {
            this.previous.next( this.episode );
        }
    }

    goToNext () {
        if ( this.hasNext ) {
            this.next.next( this.episode );
        }
    }

    onCloseClicked () {
        this.close.next( this.episode );
    }

    toggleSubtitles( episode ) : Observable<any> {
        this.subtitlesIsOpened=!this.subtitlesIsOpened;
        this.subtitlesIsOpenedChange.emit(this.subtitlesIsOpened);
        return SingularSubject.oneValue( episode );
    }

    watch( episode ) : Observable<any> {
        let ret = this.serverApi.getMedia().getShowApi().getEpisodeApi().watched(episode.id, !episode.watched);
        ret.subscribe( (ep) => {
            episode.watched = ep.watched;
        });
        return ret;
    }
}
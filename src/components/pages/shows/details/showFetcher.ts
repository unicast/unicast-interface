import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { DefaultChooser } from 'components/pages/shows/details/chooser';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { TvSeasonMediaRecordSchema, TvShowMediaRecordSchema, TvEpisodeMediaRecordSchema } from 'components/schemas/mediaRecord.schema';

@Injectable()
export class ShowFetcher {

    public serverApi : ServerApiManager;
    public show : TvShowMediaRecordSchema;
    public showChange : EventEmitter<TvShowMediaRecordSchema> = new EventEmitter();
    public seasonsList : Array<TvSeasonMediaRecordSchema> = [];//{find: Function};
    public seasonsListChange : EventEmitter<Array<TvSeasonMediaRecordSchema>> = new EventEmitter();
    public season : TvSeasonMediaRecordSchema;
    public seasonChange : EventEmitter<TvSeasonMediaRecordSchema> = new EventEmitter();
    public episodesList : Array<TvEpisodeMediaRecordSchema>;
    public episodesListChange : EventEmitter<Array<TvEpisodeMediaRecordSchema>> = new EventEmitter();
    public episode : TvEpisodeMediaRecordSchema;
    public episodeChange : EventEmitter<TvEpisodeMediaRecordSchema> = new EventEmitter();
    public isEpisodeChosenAutomatically : boolean = true;
    public chooser = new DefaultChooser();
    public http: HttpClient;

    constructor ( http : HttpClient, serverApi: ServerApiManager ) {
        this.http = http;
        this.serverApi = serverApi;
    }

    fetchShow ( show ): Promise<TvShowMediaRecordSchema> {
        return this.serverApi.getMedia().getShowApi().get( show, 'collections,seasons').toPromise();
    }

    async changeShow ( show: string, force: boolean = false ): Promise<TvShowMediaRecordSchema> {
        if ( this.show && this.show.id === show && !force ) {
            return this.show;
        }

        const remoteShow = await this.fetchShow( show );

        this.show = remoteShow;

        this.showChange.next( this.show );

        this.seasonsList = remoteShow.seasons;

        this.seasonsListChange.next( this.seasonsList );

        return remoteShow;
    }

    reload (){
        this.change( this.show.id, this.season.number, (this.isEpisodeChosenAutomatically ? undefined : this.episode.id), true );
    }

    fetchEpisodesList ( show, season ): Promise<Array<TvEpisodeMediaRecordSchema>> {
        return this.serverApi.getMedia().getShowApi().getEpisodeApi().getEpisodes(show, season).toPromise();
    }

    async changeSeason ( remoteShow: TvShowMediaRecordSchema, season: number , force: boolean = false ): Promise<TvSeasonMediaRecordSchema> {
        if ( this.season && this.season.number == season && this.season.tvShowId == remoteShow.id && !force ) {
            return this.season;
        }

        const remoteSeason: TvSeasonMediaRecordSchema = season ? this.seasonsList.find( s => s.number == season ) : this.chooser.chooseSeason( this.seasonsList );

        if ( remoteSeason ) {
            const remoteEpisodesList = await this.fetchEpisodesList( remoteShow.id, remoteSeason.number );

            this.season = remoteSeason;

            this.seasonChange.next( this.season );

            this.episodesList = remoteEpisodesList;

            this.episodesListChange.next( this.episodesList );

            return remoteSeason;
        }
    }

    async changeEpisode ( remoteShow: TvShowMediaRecordSchema, episode: string, force: boolean = false ): Promise<TvEpisodeMediaRecordSchema> {
        if ( this.episode && this.episode.id == episode && this.isEpisodeChosenAutomatically == !episode && !force ) {
            return this.episode;
        }

        this.isEpisodeChosenAutomatically = !episode;

        const remoteEpisode:TvEpisodeMediaRecordSchema = episode ? this.episodesList.find( ep => ep.id == episode ) : this.chooser.chooseEpisode( this.episodesList );

        this.episode = remoteEpisode;

        this.episodeChange.next( this.episode );

        return remoteEpisode;
    }

    async change ( show: string, season: number, episode: string, force: boolean = false ) {
        const newShow = await this.changeShow( show, force );

        const newSeason = await this.changeSeason( newShow, season, force );

        const newEpisode = await this.changeEpisode( newShow, episode, force );

        return { show: newShow, season: newSeason,  episode: newEpisode };
    }
}
import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { PageTitle } from 'components/common/services/pageTitle';
import { SidebarManager } from 'components/services/sidebarManager';
import { ArtManager } from 'components/services/artManager';
import { ShowFetcher } from 'components/pages/shows/details/showFetcher';
import { TvShowMediaRecordSchema, TvSeasonMediaRecordSchema, TvEpisodeMediaRecordSchema, ArtRecordKind } from 'components/schemas/mediaRecord.schema';
import { ShowsDetailsSidemenuComponent } from 'components/pages/shows/details/sidemenu/sidemenu.component';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { NotificationsService } from 'components/common/commandCenter/notifications/notifications.service';
import { NavigationManager } from 'components/services/navigationManager';
import { MediaImage, SelectedImage, GalleryModalComponent } from 'components/common/modal/galleryModal/galleryModal.component';
import { ArtworkGalleryPipe } from 'components/common/helpers/artworkGalleryPipe';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { Observable } from 'rxjs/Observable';
import { SingularSubject } from 'components/common/helpers/singularSubject';
import { StandardMediaQueries } from 'components/services/mediaQueries';
import { SynchronizedBy, Semaphore } from 'data-semaphore';
import { EpisodesAggregator } from 'components/common/helpers/episodesAggregator';
import { TitleDropdownAction, ValueAndtext } from 'components/common/section/section.component';
import { AvailableEpisodeSources2DropdownPipe } from 'components/common/helpers/episodesAggregatorPipe';
import { MirroringManager } from 'components/services/mirroringManager';
import { ApplicationContextManager, ContextToken } from 'components/services/applicationContextManager';
import { ShowContextSchema } from 'components/schemas/applicationContexts.schema';


@Component( {
    selector: 'show-details',
    templateUrl: './showsDetails.template.html',
    styleUrls: [ './showsDetails.styles.less' ],
    providers: [ ShowFetcher ]
} )
export class ShowsDetailsComponent extends BaseUnicastComponent {
    @ViewChild( 'gallery' )
    galleryModal : GalleryModalComponent;


    public sidemenuToken: any;
    public showDetails : TvShowMediaRecordSchema;
    public episodesList : Array<TvEpisodeMediaRecordSchema>;
    public seasonsList: Array<TvSeasonMediaRecordSchema>;
    public activeSeason : TvSeasonMediaRecordSchema;
    public hasPreviousEpisode : boolean;
    public hasNextEpisode : boolean;
    public activeEpisode : TvEpisodeMediaRecordSchema;
    public subtitlesIsOpened: boolean;
    public castOpened: boolean;
    public galleryImages: Array<MediaImage> = [];

    public episodesAggregator: EpisodesAggregator;
    public byGroup: Array<string> = [ 'source', 'video.resolution' ];
    public groupFilter: Array<string> = [ ];
    public listTitleDropdowns: Array<TitleDropdownAction> = [];

    public seasonEpisodesActivated: boolean;
    public episodesDetailsVisible: boolean;

    public showArtManagerToken: any;

    public serverApi: ServerApiManager;
    public navigation: NavigationManager;
    public route: ActivatedRoute;
    public art: ArtManager;
    public applicationContext: ApplicationContextManager;
    public contextToken: ContextToken<ShowContextSchema>;
    public sidebar: SidebarManager;
    public fetcher: ShowFetcher;
    public mediaQueries : StandardMediaQueries;
    public htmlTitle: PageTitle;
    public notifications: NotificationsService;
    public mirroring: MirroringManager;
    public semaphoreNavigation: Semaphore = new Semaphore( 1 );

    constructor ( serverApi: ServerApiManager, navigation : NavigationManager, route : ActivatedRoute, sidebar : SidebarManager, art : ArtManager, applicationContext: ApplicationContextManager,
                  fetcher : ShowFetcher, mediaQueries : StandardMediaQueries, htmlTitle: PageTitle, notifications: NotificationsService,
                  mirroring: MirroringManager ) {
        super();
        this.sidemenuToken = sidebar.setSecondaryMenu( ShowsDetailsSidemenuComponent );
        this.serverApi = serverApi;
        this.navigation = navigation;
        this.route = route;
        this.art = art;
        this.applicationContext = applicationContext;
        this.contextToken = applicationContext.registerToken<ShowContextSchema>(['show', 'season', 'episode']);
        this.sidebar = sidebar;
        this.fetcher = fetcher;
        this.mediaQueries = mediaQueries;
        this.htmlTitle = htmlTitle;
        this.notifications = notifications;
        this.mirroring = mirroring;
        this.subtitlesIsOpened = false;
        this.castOpened = false;
        this.mirroring = mirroring;
    }

    private buildgalleryImages() {
        this.galleryImages = new ArtworkGalleryPipe().transform(this.showDetails, this.seasonsList, 'poster');
    }

    protected _binds () {
        this._subscriptions.push(this.fetcher.showChange.subscribe( show => {
            this.showDetails = show;
            this.applicationContext.setContext(this.contextToken, 'show', { id: show.id, kind: show.kind });
            this.onShowChange( show );
        } ));
        this._subscriptions.push(this.fetcher.seasonsListChange.subscribe( (list: Array<TvSeasonMediaRecordSchema>) => {
            this.seasonsList = list;
            this.buildgalleryImages();
        }));
        this._subscriptions.push(this.fetcher.seasonChange.subscribe( season => {
            this.activeSeason = season;
            this.applicationContext.setContext(this.contextToken, 'season', { id: season.id, number: season.number ,kind: season.kind });
            this.seasonEpisodesActivated = false;
            this.episodesDetailsVisible = false;
        } ));

        this._subscriptions.push(this.fetcher.episodesListChange.subscribe( list =>  {
            this.episodesAggregator = new EpisodesAggregator(list, this.byGroup);
            let backupFilters = [].concat(this.groupFilter).concat([undefined]);
            this.groupFilter = [ undefined ];
            for (let i = 0; i < backupFilters.length && !this.groupFilter[this.groupFilter.length - 1]; i++) {
                this.changeGroupFilter(this.groupFilter.length - 1 , backupFilters[i]);
            }
        }));

        this._subscriptions.push(this.fetcher.episodeChange.subscribe( episode => {
            this.activeEpisode = episode;
            this.applicationContext.setContext(this.contextToken, 'episode', { id: episode.id, number: episode.number, seasonNumber: episode.seasonNumber, kind: episode.kind });
            if ( !this.seasonEpisodesActivated && this.fetcher.isEpisodeChosenAutomatically ) {
                this.seasonEpisodesActivated = true;
            } else {
                this.seasonEpisodesActivated = true;
                this.episodesDetailsVisible = true;
            }
        } ));

        this._subscriptions.push(this.route.params.subscribe( async params => {
            const newSelection = await this.fetcher.change( params.id, params.season, params.episode );
            let newMedia;
            if (params.episode) {
                newMedia = newSelection.episode;
            } else if (params.season) {
                newMedia = newSelection.season;
            } else {
                newMedia = newSelection.show;
            }
            this.mirroring.mirrorMedia(newMedia);
            this.subtitlesIsOpened = !!+params.subtitles;
        } ));

        this._subscriptions.push(this.serverApi.getMedia().filterChanged.skip(1).subscribe(([value, byUser]) => {
            if (byUser){
                this.navigation.navigateToShow();
            }
        }));
    }

    ngOnDestroy () {
        super.ngOnDestroy();
        this.sidebar.disposeSecondaryMenu( this.sidemenuToken );
        this.applicationContext.unregisterToken( this.contextToken );
        this.onShowChange( null );
    }

    onArtChange( show: TvShowMediaRecordSchema, artRecordKind?: string ){
        if ( !show || !artRecordKind || artRecordKind == ArtRecordKind.Background ) {
            if ( this.showArtManagerToken ) {
                this.art.dispose( this.showArtManagerToken );
            }

            if ( show ) {
                this.showArtManagerToken = this.art.register( show.cachedArtwork );
            }
        }
    }

    onShowChange ( show: TvShowMediaRecordSchema ) {
        this.onArtChange(show);

        if ( show ) {
            this.htmlTitle.setTitle( show.title );
        }

        this.sidebar.setMedia( show );
    }

    showSeason ( show, season ) {
        show = show || this.showDetails;
        season = season || this.activeSeason;

        this.navigation.navigateToShow( show.id, season.number );
    }

    @SynchronizedBy( "semaphoreNavigation" )
    showEpisode ( show, season, episode ) {
        this.navigation.navigateToShow( show.id, season.number, episode.id, { subtitles: ( this.subtitlesIsOpened ? 1 : 0) });
    }

    @SynchronizedBy( "semaphoreNavigation" )
    toggleSubtitles ( subtitles ) {
        this.subtitlesIsOpened = !!subtitles;
        this.navigation.navigateToShow( this.showDetails.id, this.activeSeason.number, this.activeEpisode.id, { subtitles: ( this.subtitlesIsOpened ? 1 : 0) });
    }

    toggleCast () {
        this.castOpened = !this.castOpened;
    }

    goToImdb(event: MouseEvent) {
        this.navigation.navigateToOutside('imdb.com/title/' + this.showDetails.external['imdb'], { event });
    }

    changeGroupFilter ( position: number = 0, filter: string = undefined ) {
        //Ensure the new list of available
        let availableGroups = new Set(this.episodesAggregator.getAvailableGroups());
        //Ensure filter is valid on current group's list
        filter = availableGroups.has(filter) ? filter : undefined;
        //Update filter
        if (position >= 0 && position < this.groupFilter.length) {
            this.groupFilter.splice(position, this.groupFilter.length - position, filter);
        }
        //Remove all the empty (can be the current one or the last one)
        this.groupFilter = this.groupFilter.filter(e => e !== undefined && e !== null);
        //Don't filter by group if no filter selected
        let showAll: boolean = this.groupFilter.length == 0;
        //Check if an empty box is needed to allow a filter selection
        if ( (showAll || !this.episodesAggregator.isGroupComplete(this.groupFilter)) && availableGroups.size > 1 && availableGroups.size - this.groupFilter.length > 0 ) {
            this.groupFilter.push(null);
        }
        //Generate the new episode's list
        if ( showAll ) {
            this.episodesList = this.episodesAggregator.getAllEpisodes();
        } else {
            this.episodesList = this.episodesAggregator.getEpisodesForGroup(this.groupFilter);
            //Check if the active episode exists on the new list. Otherwise ty to find the new one
            this.activeEpisode = this.getNeighborEpisode(this.activeEpisode, 0, true);
        }
        //Generate the new list of options to select
        this.listTitleDropdowns = this.groupFilter.map( e => {
            return {
                hasNone: e !== undefined && e !== null,
                value: { value: e, text: AvailableEpisodeSources2DropdownPipe.filterValueToText(e) },
                options: this.calculateAvailableGroups(e, availableGroups)
            };
        });
    }

    private calculateAvailableGroups(current: string, availableGroups: Set<string>): Array<ValueAndtext> {
        let ret: Array<ValueAndtext> = [];
        availableGroups.forEach(e => ret.push({ value: e, text: AvailableEpisodeSources2DropdownPipe.filterValueToText(e), selected: e == current }));
        availableGroups.delete(current);
        return ret;
    }

    changeSeason ( season ) {
        this.showSeason( this.showDetails, season );
        this.seasonEpisodesActivated = false;
        this.episodesDetailsVisible = false;
    }

    changeEpisode ( episode ) {
        this.showEpisode( this.showDetails, this.activeSeason, episode );
    }

    getPreviousEpisode ( episode: TvEpisodeMediaRecordSchema, byNumber: boolean = false): TvEpisodeMediaRecordSchema {
        return this.getNeighborEpisode(episode, -1, byNumber);
    }

    getNextEpisode ( episode: TvEpisodeMediaRecordSchema, byNumber: boolean = false ): TvEpisodeMediaRecordSchema {
        return this.getNeighborEpisode(episode, 1, byNumber);
    }

    getNeighborEpisode( episode: TvEpisodeMediaRecordSchema, distance: number, byNumber: boolean = false): TvEpisodeMediaRecordSchema {
        let testMethod = (byNumber) ? (ep: TvEpisodeMediaRecordSchema) => ep.number == episode.number : (ep:TvEpisodeMediaRecordSchema) => ep.id == episode.id;
        if (episode) {
            const index = this.episodesList.indexOf( this.episodesList.find( testMethod ) );
            const newIndex = index + distance;

            if ( index >= 0 && newIndex >= 0 && newIndex < this.episodesList.length ) {
                return this.episodesList[ newIndex ];
            } else if ( !byNumber ) { //Give a try by number insetad id. Maybe not a full list
                return this.getNeighborEpisode(episode, distance, true);
            }
        }
        return null;
    }

    loadPreviousEpisodeDetails ( episode: TvEpisodeMediaRecordSchema ) {
        const previous = this.getPreviousEpisode( episode );

        if ( previous ) {
            return this.changeEpisode( previous );
        }
    }

    loadNextEpisodeDetails ( episode: TvEpisodeMediaRecordSchema ) {
        const next = this.getNextEpisode( episode );

        if ( next ) {
            return this.changeEpisode( next );
        }
    }

    showEpisodesList () {
        this.changeSeason( this.activeSeason );
        this.seasonEpisodesActivated = false;
        this.episodesDetailsVisible = false;
    }

    markWatch(value: boolean) {
        let startKey: string = 'SHOWS.DETAILS.' + (value ?  'MARK_ALL_WATCHED' : 'MARK_ALL_NOT_WATCHED');
        this.notifications.ask({
                message: startKey + '.MESSAGE',
                title: startKey + '.TITLE'
            },{
                yes: () => {
                    this.serverApi.getMedia().getShowApi().watched( this.showDetails.id, value ).subscribe(() => {
                        this.showDetails.watched = value;
                        this.fetcher.reload();
                    });
                }
            },
            Infinity
        );
    }

    episodeSwipe ( ev : HammerInput ) {
        if ( ev.type == 'swipeleft' ) {
            this.loadNextEpisodeDetails( this.activeEpisode );
        } else if ( ev.type === 'swiperight' ) {
            this.loadPreviousEpisodeDetails( this.activeEpisode );
        }
    }

    gallerySelectedType(event: SelectedImage) {
        if (event.selected.isLeaf) {
            let id: string, artwork: string;
            if (typeof event.parent.art == 'number') {
                id = event.parent.id;
                artwork = 'poster';
            } else {
                id = this.showDetails.id;
                artwork = event.parent.art;
            }

            this.serverApi.getMedia().setArtwork(event.selected.type, id,  artwork, event.selected.id ).subscribe(e => {
                this.galleryModal.close();
                if (typeof event.parent.art == 'number') {
                    this.seasonsList[event.parent.art - 1].cachedArtwork[artwork] = event.selected.value;
                } else {
                    this.showDetails.cachedArtwork[artwork] = event.selected.value;
                    this.onArtChange(this.showDetails, artwork);
                }
            });
        }
    }

    fecth(mediaImage?: MediaImage): Observable<Array<MediaImage>> {
        if (!mediaImage) {
            return SingularSubject.oneValue(new ArtworkGalleryPipe().transform(this.showDetails, this.seasonsList, 'poster'));
        }
        if (mediaImage.id.indexOf('/') > -1) {
            return SingularSubject.oneValue([]);
        }
        if (typeof mediaImage.art == 'number') {
            return this.serverApi.getMedia().getShowApi().getSeasonApi().getArtwork( mediaImage.id ).map( data => {
                return data.map( (e, i) => { return {id: e.id, value: e.url, type: MediaKind.TvSeason, art: i+1, isLeaf: true}});
            });
        } else {
            return this.serverApi.getMedia().getShowApi().getArtwork(this.showDetails.id).map( data => {
                return data.filter(e => e.kind == mediaImage.art).map( (e, i) => { return {id: e.id, value: e.url, type: MediaKind.TvShow, art: i+1, isLeaf: true}});
            });
        }
    }
}

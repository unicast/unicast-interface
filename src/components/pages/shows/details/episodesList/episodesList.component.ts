import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';

// Components
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { StandardMediaQueries } from 'components/services/mediaQueries';
import { TvEpisodeMediaRecordSchema, TvSeasonMediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { SingularSubject } from 'components/common/helpers/singularSubject';
import { DefaultMediaButtonActions } from 'components/pages/common/buttonActions/defaultMediaButtonActions';
import { TitleDropdownAction, ViewAction } from 'components/common/section/section.component';

export enum ActiveView {
    Grid = 'grid',
    Table = 'table',
}

@Component( {
    selector: 'uc-episodes-list',
    templateUrl: './episodesList.template.html',
    styleUrls: [ './episodesList.styles.less' ],
    providers: [ DefaultMediaButtonActions ]
} )
export class EpisodesListComponent{
    @Input() episodes : Array<TvEpisodeMediaRecordSchema>;

    @Input() season : Array<TvSeasonMediaRecordSchema>;

    @Input() active : TvEpisodeMediaRecordSchema;

    @Input() sectionBackground : string;

    @Input() subtitlesIsOpened: boolean;

    @Input() titleDropdowns: Array<TitleDropdownAction>;

    @Output() titleDropdownsSelect : EventEmitter<[number, any]> = new EventEmitter();

    @Output() activeChange : EventEmitter<TvEpisodeMediaRecordSchema> = new EventEmitter();

    @Output() subtitlesIsOpenedChange: EventEmitter<boolean> = new EventEmitter<boolean>();

    public serverApi: ServerApiManager;

    public mediaQueries : StandardMediaQueries;

    public countWatched: number = 0;

    public autoSelect : boolean = true;

    public activeView : ActiveView = ActiveView.Grid;

    public views : ViewAction[] = [{
        name: 'grid',
        title: 'Grid',
        icon: 'th',
        active: true
    }, {
        name: 'table',
        title: 'Table',
        icon: 'table',
    }];

    constructor ( serverApi : ServerApiManager, mediaQueries : StandardMediaQueries, defaultActions: DefaultMediaButtonActions ) {
        this.serverApi = serverApi;
        this.mediaQueries = mediaQueries;
        defaultActions.subtitles.injectAction( media => this.toggleSubtitles((media as TvEpisodeMediaRecordSchema)));
    }

    private recountWatched() {
        this.countWatched = 0;
        for(let episode of  this.episodes){
            this.countWatched += (episode.watched ? 1 : 0);
        }
    }

    ngOnChanges(change){
        if (change.episodes) {
            this.recountWatched();
        }
    }

    select ( episode: TvEpisodeMediaRecordSchema ) {
        this.active = episode;

        this.activeChange.next( this.active );
    }

    play ( episode: TvEpisodeMediaRecordSchema ) {
        this.serverApi.getPlayer().getDeviceApi().play( MediaKind.TvEpisode, episode.id );
    }

    queue( episode: TvEpisodeMediaRecordSchema ): Observable<any> {
        return this.serverApi.getPlayer().getDeviceApi().getPlaylistApi().getItemsApi().queue( episode );
    }

    toggleSubtitles( episode: TvEpisodeMediaRecordSchema ): Observable<any> {
        this.select( episode );
        this.subtitlesIsOpened = !this.subtitlesIsOpened;
        this.subtitlesIsOpenedChange.emit(this.subtitlesIsOpened);
        return SingularSubject.oneValue(episode);
    }

    toggleWatched ( episode: TvEpisodeMediaRecordSchema ) : Observable<any> {
        return this.serverApi.getMedia().getShowApi().getEpisodeApi().watched(episode.id, !episode.watched);
    }

    markAll() {
        let watched: boolean = this.countWatched != this.episodes.length
        let obs = this.serverApi.getMedia().getShowApi().getSeasonApi().watched((this.active || this.episodes[0]).tvSeasonId, watched);
        obs.subscribe( () => {
            for(let episode of  this.episodes){
                episode.watched = watched;
            }
            this.countWatched = (watched ? this.episodes.length : 0);
        });
    }

    onViewClick (view : ViewAction) {
        this.activeView = view.name as ActiveView;
    }
}

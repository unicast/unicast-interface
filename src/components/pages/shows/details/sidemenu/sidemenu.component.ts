import { Component } from '@angular/core';
import { SidebarManager } from 'components/services/sidebarManager';
import { MediaListQueryFilters } from 'components/pages/common/sidemenuFilters/filtersMediaSidemenu.service';
import { FiltersSidemenuService } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';


@Component( {
    selector: 'uc-shows-details-sidemenu',
    templateUrl: './sidemenu.template.html',
    styleUrls: [ './sidemenu.styles.less' ]
} )
export class ShowsDetailsSidemenuComponent {
    public sidebar: SidebarManager;

    constructor(sidebar: SidebarManager, service : FiltersSidemenuService<MediaListQueryFilters>){
        this.sidebar = sidebar;
        service.initState('repositories');
        service.initState('collections');
    }

}
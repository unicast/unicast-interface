export class DefaultChooser {
    chooseSeason ( seasonsList ) {
        const watched = seasonsList.filter( season => season.watchedEpisodesCount > 0 );

        const maxWatchedNumber = Math.max( 0, ...watched.map( s => s.number ) );

        if ( !maxWatchedNumber ) {
            return seasonsList[ 0 ];
        }

        const maxWatched = watched.find( season => season.number == maxWatchedNumber );

        if ( maxWatched.watchedEpisodesCount == maxWatched.episodesCount ) {
            const maxNumber = Math.max( 1, ...seasonsList.map( s => s.number ) );

            const notWatched = seasonsList.filter( season => !watched.includes( season ) ).filter( season => season.number > maxWatchedNumber );

            const minNotWatchedNumber = Math.min( maxNumber, ...notWatched.map( s => s.number ) );

            return seasonsList.find( season => season.number == minNotWatchedNumber );
        }

        return maxWatched;
    }

    chooseEpisode ( episodes ) {
        const maxEpisodeNumber = Math.max( 0, ...episodes.map( ep => ep.number ) );

        //const maxWatchedEpisodeNumber = Math.max( 0, ...episodes.filter( ep => ep.watched ).map( ep => ep.number ) );

        const minNotWatchedEpisodeNumber = Math.min( maxEpisodeNumber, ...episodes.filter( ep => !ep.watched ).map( ep => ep.number ) );

        const minNotWatchedEpisode = episodes.find( ep => ep.number == minNotWatchedEpisodeNumber );

        return minNotWatchedEpisode || episodes.find( ep => ep.number === maxEpisodeNumber );
    }
}
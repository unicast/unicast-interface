import { Component } from '@angular/core';

import { FiltersSidemenuService } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { MediaListQueryFilters } from 'components/pages/common/sidemenuFilters/filtersMediaSidemenu.service';
import { SortFields } from 'components/pages/common/sidemenuFilters/sort/sidemenuFiltersSort.component';
import { ApplicationContextManager, ContextToken } from 'components/services/applicationContextManager';
import { FiltersContextSchema } from 'components/schemas/applicationContexts.schema';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';

@Component( {
    selector: 'uc-shows-list-sidemenu',
    templateUrl: './sidemenu.template.html',
    styleUrls: [ './sidemenu.styles.less' ]
} )
export class ShowsListSidemenuComponent extends BaseUnicastComponent {
    public service: FiltersSidemenuService<MediaListQueryFilters>;
    public applicationContext: ApplicationContextManager;
    public contextToken: ContextToken<FiltersContextSchema<MediaListQueryFilters>>;

    public sortFields: SortFields = {
        title: { label: 'SORT.FIELDS.TITLE' },
        year: { label: 'SORT.FIELDS.YEAR' },
        rating: { label: 'SORT.FIELDS.RATING' },
        addedAt: { label: 'SORT.FIELDS.ADDED_AT' },
        playCount: { label: 'Played Count' },
        lastPlayedAt: { label: 'SORT.FIELDS.LAST_PLAYED_AT' },
        $userRank: { label: 'SORT.FIELDS.USER_RANK', available: () => !!this.service.currentPresetId }
    };

    constructor ( service : FiltersSidemenuService<MediaListQueryFilters>, applicationContext: ApplicationContextManager ) {
        super();
        this.service = service;
        this.applicationContext = applicationContext;
        this.contextToken = applicationContext.registerToken<FiltersContextSchema<MediaListQueryFilters>>(['filters']);
        service.initState('collections');
    }

    protected _binds() {
        this._subscriptions.push(this.service.filtersChanges.subscribe(() => this.notifyUpdateFilters() ));
        this.notifyUpdateFilters();
    }

    protected notifyUpdateFilters() {
        this.applicationContext.setContext(this.contextToken, 'filters', this.service.filters);
    }

    public ngOnDestroy() {
        this.applicationContext.unregisterToken(this.contextToken);
    }


}
import { Component, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ShowsListSidemenuComponent } from 'components/pages/shows/list/sidemenu/sidemenu.component';
import { TopbarManager } from 'components/services/topbarManager';
import { PageTitle } from 'components/common/services/pageTitle';
import { StandardMediaQueries } from 'components/services/mediaQueries';
import { FiltersSidemenuService } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { SessionCache } from 'components/common/services/sessionCache';
import { MediaListHelperSortable } from 'components/pages/common/mediaListHelperSortable';
import { SidebarManager } from 'components/services/sidebarManager';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { DefaultMediaButtonActions } from 'components/pages/common/buttonActions/defaultMediaButtonActions';
import { FiltersMediaSidemenuService, MediaListQueryFilters } from 'components/pages/common/sidemenuFilters/filtersMediaSidemenu.service';
import { DelayedExecutions } from 'components/common/services/delayedExecutions';
import { ApplicationContextManager } from 'components/services/applicationContextManager';

@Component( {
    selector: 'uc-shows-list',
    templateUrl: './showsList.template.html',
    styleUrls: [ './showsList.styles.less' ],
    providers: [ FiltersMediaSidemenuService.forRoot('show') ]
} )
export class ShowsListComponent extends MediaListHelperSortable<MediaListQueryFilters> {
    constructor ( sidebar: SidebarManager, injector: Injector, cache : SessionCache, sidemenuService : FiltersSidemenuService<MediaListQueryFilters>,
                  serverApi:  ServerApiManager, topbar: TopbarManager, htmlTitle: PageTitle, mediaQueries : StandardMediaQueries,
                  defaultButtons: DefaultMediaButtonActions, delayedExecutions: DelayedExecutions, applicationContext: ApplicationContextManager ) {
        super([ ShowsListComponent.name, 'shows' ] , sidebar, cache, sidemenuService, serverApi, topbar, htmlTitle, mediaQueries, defaultButtons, delayedExecutions, applicationContext);
        this.setSecondaryMenu( ShowsListSidemenuComponent, injector );
        this.step = 18;
        this.defaultButtons = [];
        this.actionButtons = [];

        this.serverApi.getMedia().getCollectionApi().list( 'show' ).subscribe( (data) => {
            this.collections = data;
        } );
    }

    listByApi(take: number, skip: number, opts: any): Observable<any> {
        return this.serverApi.getMedia().getShowApi().list(take, skip, { ...opts, collections: true } );
    }
}
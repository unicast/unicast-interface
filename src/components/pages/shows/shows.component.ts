import { Component } from '@angular/core';
import { PageTitle } from 'components/common/services/pageTitle';

import { TopbarManager } from '../../services/topbarManager';

@Component( {
    selector: 'uc-shows',
    templateUrl: './shows.template.html'
} )

export class ShowsComponent {
    constructor ( topbar : TopbarManager, htmlTitle: PageTitle) {
        let title = 'Shows';
        topbar.setTitle( title );
        htmlTitle.setTitle( title );
    }
}

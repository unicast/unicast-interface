import { Component, ViewChild } from '@angular/core';

import { PageTitle } from 'components/common/services/pageTitle';
import { TopbarManager } from 'components/services/topbarManager';
import { WaitFor } from 'components/common/helpers/asyncClick.directive';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { AppStorage } from 'components/app.storage';
import { ServerApiHost } from 'components/services/serverApis/serverApiHost';
import { TranslatorManager, TranslationTuple } from 'components/services/translatorManager';
import { ModalComponent } from 'components/common/modal/modal.component';
import { SyncOptions, SyncDefaultOptions } from 'components/schemas/sync.schema';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { MediaRecordSchema, PlayableMediaRecordSchema, TvEpisodeMediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { diff } from 'deep-object-diff';

@Component( {
    selector: 'uc-settings',
    templateUrl: './settings.template.html',
    styleUrls: [ './settings.styles.less' ]
} )
export class SettingsComponent {
    @ViewChild( 'syncModal' )
    syncModal : ModalComponent;

    @ViewChild( 'syncReportModal' )
    syncReportModal : ModalComponent;

    private _serverApi: ServerApiManager;
    private _appStorage: AppStorage;

    runningSyncOptionsVisible : boolean = false;

    runningSyncTask : any = null;

    syncReportFilter = {
        showChanges: true,
        showNewErrors: true,
        showOldErrors: false,
        search: ''
    };

    syncReports : SyncReportView[] = [];
    syncReportsFiltered : SyncReportView[] = [];

    apiUrl: string;
    languages: Array<TranslationTuple> = [];
    currentLanguage: string;
    syncOptions: SyncOptions;
    advancedChar: string = '>';

    constructor ( topbar : TopbarManager, htmlTitle: PageTitle, serverApi : ServerApiManager, appStorage: AppStorage ) {
        this._serverApi = serverApi;
        this._appStorage = appStorage;

        let title = 'Settings';
        topbar.setTitle( title );
        htmlTitle.setTitle( title );
    }

    ngOnInit() {
        this.apiUrl = this._appStorage.getCurrentServerApi();
        this.languages = TranslatorManager.getAvailableLanguages();
        this.currentLanguage = this._appStorage.getCurrentLanguage();

        this.runningSyncTask = null;
    }

    changeApi(url: string){
        this.apiUrl = url;
        this._appStorage.setCurrentServerApi(url);
        this._serverApi.restart(new ServerApiHost(url));
        this._appStorage.setActualDevice(this._serverApi.getPlayer().getDeviceApi().deviceObservable);
        this._serverApi.getPlayer().startDevice(this._appStorage.getActualDevice());
    }

    changeCurrentLanguage(lang: string){
        this._appStorage.setCurrentLanguage(lang);
    }

    syncModalShow() {
        if ( this.runningSyncTask == null ) {
            this.runningSyncOptionsVisible = true;

            this.syncOptions = { ...SyncDefaultOptions };
        }

        this.syncReportModal.show();
    }

    isPlayableRecord ( record : MediaRecordSchema ) : record is PlayableMediaRecordSchema {
        return 'sources' in record;
    }

    buildRecordPoster ( record : MediaRecordSchema ) : string {
        const prefix = ( url: string ) => this._serverApi.getBase().getUrl( [ 'media', 'artwork', 'scrapers', url ] );

        if ( record != null ) {
            let artwork = record.cachedArtwork || record.art;

            if ( artwork['poster'] != null ) {
                return prefix( btoa( artwork['poster'] ) );
            }

            if ( artwork[ 'tvshow' ] != null ) {
                artwork = artwork[ 'tvshow' ];

                if ( artwork['poster'] != null ) {
                    return prefix( btoa( artwork['poster'] ) );
                }
            }
        }

        return null;
    }

    buildRecordFiles ( record : MediaRecordSchema, tag ?: string ) : { path: string, tag ?: string }[] {
        if ( record != null && this.isPlayableRecord( record ) ) {
            if ( record.sources.length > 0 ) {
                if ( tag != null ) {
                    return [ { path: record.sources[ 0 ].id, tag: tag } ];
                } else {
                    return [ { path: record.sources[ 0 ].id } ];
                }
            }
        }

        return [];
    }

    buildRecordTitle ( record: MediaRecordSchema ): string {
        if ( record.kind === MediaKind.TvEpisode ) {
            const episode = record as TvEpisodeMediaRecordSchema;

            const episodeNumber = ( episode.number.toString() as any ).padStart( 2, '0' );
            const seasonNumber = ( episode.seasonNumber.toString()as any ).padStart( 2, '0' );

            return `S${seasonNumber}E${episodeNumber} - ${episode.title}`;
        } else {
            return record.title;
        }
    }

    buildDiff ( objectA: any, objectB: any ) {
        var objDiff = diff( objectA, objectB );

        if ( Object.keys( objDiff ).length > 0 ) {
            return JSON.stringify( objDiff, null, 4 );
        }

        return null;
    }

    buildSyncReportView ( report : MediaSyncReport ) : SyncReportView {
        let files : any[] = [];
        let poster : string = null;
        let title : string = null;
        let diff : any = null;

        switch ( report.type ) {
            case 'error':
                // BEWARE report.media can be null here
                poster = this.buildRecordPoster( report.media );

                return { icon: 'exclamation', type: 'error', title: 'error', poster, message: report.label, files: report.file ? [ { path: report.file } ] : [], new: report.new };
            case 'create':
                files = this.buildRecordFiles( report.record );
                poster = this.buildRecordPoster( report.record );
                title = this.buildRecordTitle( report.record );

                return { icon: 'plus', type: 'create', title: 'create', poster, subtitle: report.record.kind, message: title, files };
            case 'update':
                files = this.buildRecordFiles( report.newRecord );
                poster = this.buildRecordPoster( report.newRecord );
                title = this.buildRecordTitle( report.newRecord );
                diff = this.buildDiff( report.oldRecord, report.newRecord );

                return { icon: 'pencil', type: 'update', title: 'update', poster, subtitle: report.newRecord.kind, message: title, files, diff };
            case 'move':
                files = [
                    ...this.buildRecordFiles( report.oldRecord, 'FROM' ),
                    ...this.buildRecordFiles( report.newRecord, 'TO' )
                ];

                poster = this.buildRecordPoster( report.newRecord );
                title = this.buildRecordTitle( report.newRecord );

                return { icon: 'scissors', type: 'move', title: 'move', poster, subtitle: report.newRecord.kind, message: title, files };
            case 'remove':
                files = this.buildRecordFiles( report.record );
                poster = this.buildRecordPoster( report.record );
                title = this.buildRecordTitle( report.record );

                return { icon: 'minus', type: 'remove', title: 'remove', poster, subtitle: report.record.kind, message: title, files };
        }
    }

    fuzz ( query : string, text : string ) : boolean {
        if ( text == void 0 ) return false;

        return text.toLowerCase().includes( query.toLowerCase() );
    }

    filterSyncReports () {
        this.syncReportsFiltered = this.syncReports.filter( report => {
            if ( report.type == 'error' ) {
                return ( this.syncReportFilter.showNewErrors && report.new )
                    || ( this.syncReportFilter.showOldErrors && !report.new );
            } else {
                return this.syncReportFilter.showChanges;
            }
        } ).filter( report => {
            return this.fuzz( this.syncReportFilter.search, report.subtitle )
                || this.fuzz( this.syncReportFilter.search, report.message )
                || ( report.files && report.files.some( file => this.fuzz( this.syncReportFilter.search, file.path ) ) );
        } );
    }

    async refreshRunningTask ( id : string ) {
        this.runningSyncTask = await this._serverApi.getTask().get( id, {
            'filter[reportsIndex]': this.syncReports.length
        } ).toPromise();

        const newReports = this.runningSyncTask.reports;

        this.syncReports.push( ...newReports.map( r => this.buildSyncReportView( r ) ) );

        this.filterSyncReports();
    }

    async sync () {
        try {
            this.runningSyncOptionsVisible = false;

            const task = await this._serverApi.getMedia().getProvidersApi().sync({ ...this.syncOptions, wait: false }).toPromise();

            this.syncReports = task.reports.map( r => this.buildSyncReportView( r ) );

            this.filterSyncReports();

            this.runningSyncTask = task;

            this.syncReportModal.show();

            while ( this.runningSyncTask.state == 'running' ) {
                await new Promise<void>( resolve => setTimeout( resolve, 500 ) );

                await this.refreshRunningTask( task.id );
            }
        } finally {
            this.runningSyncTask.state = 'finished';
        }
    }

    clean(event: WaitFor){
        event.waitFor(this._serverApi.getMedia().getProvidersApi().clean());
    }

    repair(event: WaitFor){
        event.waitFor(this._serverApi.getMedia().getProvidersApi().repair());
    }

    clearFilters(event: WaitFor){
        this._serverApi.getStorage().removeItem('media.movie');
        this._serverApi.getStorage().removeItem('media.show');
        this._appStorage.removeMediaFilter('movie');
        this._appStorage.removeMediaFilter('show');
        this._appStorage.removeHistoryFilter();
        event.release();
    }

}

export interface SyncReportView {
    type : string;
    icon : string;
    title : string;
    poster ?: string;
    subtitle ?: string;
    message : string;
    files : { path : string, tag ?: string }[];
    new ?: boolean;
    diff ?: any;
}

export type MediaSyncReport =
      { type: 'error', kind : MediaKind, label : string, file ?: string, media ?: MediaRecordSchema, new : boolean }
    | { type: 'create', record : MediaRecordSchema }
    | { type: 'update', oldRecord : MediaRecordSchema, newRecord : MediaRecordSchema, changes : any }
    | { type: 'move', oldRecord : MediaRecordSchema, newRecord : MediaRecordSchema }
    | { type: 'remove', record : MediaRecordSchema };

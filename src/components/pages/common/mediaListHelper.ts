import { Injector } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import { SidebarManager } from 'components/services/sidebarManager';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { StandardMediaQueries } from 'components/services/mediaQueries';
import { FiltersSidemenuService } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { SessionCache } from 'components/common/services/sessionCache';
import { TopbarManager } from 'components/services/topbarManager';
import { PageTitle } from 'components/common/services/pageTitle';
import { DefaultMediaButtonActions } from 'components/pages/common/buttonActions/defaultMediaButtonActions';
import { SelectCollectionButtonAction, UnselectCollectionButtonAction } from 'components/pages/common/buttonActions/selectCollectionButtonAction';
import { MediaButtonAction } from 'components/pages/common/buttonActions/buttonAction';
import { CollectionSchema } from 'components/schemas/mediaRecord.schema';
import { ListChangeInformation } from 'components/pages/common/listSelector/listSelector.component';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { DelayedExecutions } from 'components/common/services/delayedExecutions';
import { ApplicationContextManager, ContextToken } from 'components/services/applicationContextManager';
import { PageContextSchema } from 'components/schemas/applicationContexts.schema';

export enum AutoScrollDirection {
    Up = '-1',
    Down = '1'
}

export abstract class MediaListHelper<T extends {}> extends BaseUnicastComponent{
    public _allRequested: boolean;
    protected _requesting: boolean;
    protected _key: Array<any>;

    public sidemenuToken: any;
    public step: number;
    public data: Array<any> = [];
    public actionButtons: Array<MediaButtonAction> = [];
    public collections: Array<CollectionSchema> = [];
    public selectedCollection: CollectionSchema = null;

    public sidebar: SidebarManager;
    public serverApi: ServerApiManager;
    public mediaQueries : StandardMediaQueries;
    public sidemenuService : FiltersSidemenuService<T>;
    public sessionCache : SessionCache;
    public delayedExecutions: DelayedExecutions;
    public applicationContext: ApplicationContextManager;
    public defaultButtons : Array<MediaButtonAction> = [];
    public tags: Array<string> = undefined;
    public pageContextToken: ContextToken<PageContextSchema>;

    public groupedData : GroupedArray<any, any> = null;
    public currentLoadSubscription: Subscription = null;

    public constructor(key: Array<any>, sidebar: SidebarManager, cache : SessionCache, sidemenuService : FiltersSidemenuService<T>,
                       serverApi:  ServerApiManager, topbar: TopbarManager, htmlTitle: PageTitle, mediaQueries : StandardMediaQueries,
                       defaultButtons: DefaultMediaButtonActions, delayedExecutions: DelayedExecutions, applicationContext: ApplicationContextManager){
        super();
        this._key = key;
        this._requesting = false;
        this._allRequested = false;
        this.actionButtons = defaultButtons.getAllActions();

        this.sidebar = sidebar;
        this.sidemenuService = sidemenuService;
        this.mediaQueries = mediaQueries;
        this.sessionCache = cache;
        this.serverApi = serverApi;
        this.defaultButtons = defaultButtons.getAllActions();
        this.delayedExecutions = delayedExecutions;
        this.applicationContext = applicationContext;

        htmlTitle.setTitle( topbar.title );

        this.step = 24;

        this.mediaQueries = mediaQueries;
    }

    abstract listByApi(take: number, skip: number, opts: any) : Observable<any>;

    flatten ( data : object, prefix: string = "" ) : object {
        return this.serverApi.getBase().flatten(data, prefix);
    }

    get length () : number {
        if ( this.groupedData != null ) {
            return this.groupedData.length;
        } else {
            return this.data.length;
        }
    }

    public lastIndex: number = -1;

    protected updateLastIndex ( items: {$index: number}[], base: number = 0 ) {
        this.lastIndex = items.map( a => a.$index ).reduce( (a, b) => Math.max(a, b), 0 );
    }

    reload (force: boolean = false) : Subscription {
        if (!this._requesting || force) {
            if (this.currentLoadSubscription != null) {
                this.currentLoadSubscription.unsubscribe();
            }

            this._requesting = true;

            const step = Math.max( this.length, this.step );

            this.currentLoadSubscription = this.listByApi( step, 0, this.flatten( this.sidemenuService.filters, 'filter' ) ).subscribe( data => {
                this.currentLoadSubscription = null;

                this.updateLastIndex( data );

                if (this.groupedData != null) {
                    this.groupedData.clear();
                    this.groupedData.addAll(data);
                } else {
                    this.data = data;
                }

                this.sessionCache.set( this._key, this.data );

                this._allRequested = data.length < step;
                this._requesting = false;
            } );

            return this.currentLoadSubscription;
        }
    }

    loadMore () : Subscription {
        if (!this._requesting && !this._allRequested) {
            if (this.currentLoadSubscription != null) {
                this.currentLoadSubscription.unsubscribe();
            }

            this._requesting = true;

            const skip = this.lastIndex + 1;

            this.currentLoadSubscription = this.listByApi( this.step, skip, this.flatten( this.sidemenuService.filters, 'filter' ) ).subscribe( data => {
                this.currentLoadSubscription = null;
                
                this.updateLastIndex( data, this.lastIndex );

                if (this.groupedData != null) {
                    this.groupedData.addAll( data );
                } else {
                    this.data = this.data.concat( data );
                }

                this.sessionCache.set( this._key, this.data );

                this._allRequested = data.length < this.step;
                this._requesting = false;
            } );

            return this.currentLoadSubscription;
        }
    }

    setSecondaryMenu ( menu: any, injector : Injector ) {
        this.sidemenuToken = this.sidebar.setSecondaryMenu( menu, injector );

        this._subscriptions.push(this.sidemenuService.filtersChanges.subscribe( filters => {
            this.reload(true);
        } ));
    }

    ngOnInit () {
        super.ngOnInit();

        this.pageContextToken = this.applicationContext.registerToken<PageContextSchema>(['page']);
        this.applicationContext.setContext(this.pageContextToken, 'page', { name: this._key[this._key.length - 1], type: 'list'});

        this.sidemenuService.loadPresets().subscribe(v => {
            this.sessionCache.get<string>( [ this._key[0], 'preset' ] )
                .ifPresent( preset => this.loadCachedPreset(preset) )
                .ifEmpty( () => this.sessionCache.set( [ this._key[0], 'preset' ], this.sidemenuService.currentPresetId ) );

            this.sessionCache.get<T>( [ this._key[0], 'filters' ] )
                .ifPresent( filters => this.loadCachedFilters(filters) )
                .ifEmpty( () => this.sessionCache.set( [ this._key[0], 'filters' ], this.sidemenuService.filters ) );
        });

        this.sessionCache.get<any[]>( this._key )
            .ifPresent( medias => {
                this.data = medias;
                this.updateLastIndex(this.data);
            } )
            .ifEmpty( () => this.loadMore() );

        // TODO Pensar
        this.sessionCache.get<string>( [ this._key[0], 'textFilter' ] )
            .ifPresent( textFilter => this.data = (textFilter == this.serverApi.getMedia().getActualFilter() ? this.data : []) );

        this._subscriptions.push(this.serverApi.getMedia().filterChanged.skip(1).subscribe(([text, byUser]) => {
            if ( !text || !text.includes( '?' ) || byUser ) {
                this.sessionCache.set( [ this._key[0], 'textFilter' ], text );
                this.reload(true);
            }
        }));

        this._subscriptions.push( this.sidemenuService.presetChanges.subscribe( presetId => {
            !!presetId && this.sessionCache.set( [this._key[0], 'filters'], this.sidemenuService.filters);
            this.sessionCache.set( [this._key[0], 'preset'], presetId);
        }) );

        this._subscriptions.push( this.sidemenuService.filtersChanges.subscribe( e => {
            this.sessionCache.set( [this._key[0], 'filters'], this.sidemenuService.filters);
        }) );

        this.loadMore();
    }

    protected loadCachedPreset(preset: string) {
        this.sidemenuService.currentPresetId = preset;
    }

    protected loadCachedFilters(filters: T) {
        this.sidemenuService.publishFilters(filters);
    }

    ngOnDestroy () {
        super.ngOnDestroy();
        this.sidebar.disposeSecondaryMenu( this.sidemenuToken );
        this.applicationContext.unregisterToken( this.pageContextToken );
    }

    onScroll(){
        this.loadMore();
    }

    selectCollection(collection: ListChangeInformation) {
        if (!collection.id){
            this.actionButtons = this.defaultButtons;
            this.tags = undefined;
        } else {
            this.actionButtons = [new SelectCollectionButtonAction(this.serverApi, collection.id, collection.title),
                                 new UnselectCollectionButtonAction(this.serverApi, collection.id, collection.title)];
            this.tags = ['collections.title'];
        }
    }
}


export interface GroupedArrayItem<T, K> {
    key : K;
    label : string;
    items : T[];
}

export class GroupedArray<T, K> {
    public length : number = 0;

    public groups : GroupedArrayItem<T, K>[] = [];
    protected keyer : ( items : T ) => K;
    protected labeler : ( key : K ) => string;
    protected prepend : boolean = false;

    constructor ( keyer : ( items : T ) => K, labeler : ( key : K ) => string = () => null, prepend : boolean = false ) {
        this.keyer = keyer;
        this.labeler = labeler;
        this.prepend = prepend;
    }

    public clear () : void {
        this.groups = [];
        this.length = 0;
    }

    public addAll ( items : T[] ) : void {
        let lastGroup = this.groups.length > 0 ? this.groups[ this.groups.length - 1 ] : null;

        for ( let item of items ) {
            const key = this.keyer( item );

            if ( lastGroup != null && lastGroup.key == key ) {
                if ( this.prepend ) {
                    lastGroup.items.unshift( item );
                } else {
                    lastGroup.items.push( item );
                }
            } else {
                const label = this.labeler( key );

                lastGroup = { key, label, items: [ item ] };

                this.groups.push( lastGroup );
            }
        }

        this.length += items.length;
    }

    public * [ Symbol.iterator ] () {
        yield * this.groups;
    }
}
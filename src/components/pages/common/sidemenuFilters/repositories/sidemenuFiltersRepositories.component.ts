import { Component, Input, SimpleChanges } from '@angular/core';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { FiltersSidemenuService, FilteringList } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { FiltersBaseType } from 'components/pages/common/sidemenuFilters/sidemenuFiltersBase';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { SidemenuFiltersListsComponent } from 'components/pages/common/sidemenuFilters/lists/sidemenuFiltersLists.component';
import { SidemenuListsRepositoriesApi } from 'components/pages/common/sidemenuFilters/repositories/sidemenuFiltersRepositoriesApi';

type RepositoriesFilterType = FiltersBaseType<FilteringList>;

@Component( {
    selector: 'uc-sidemenu-filters-repositories',
    templateUrl: '../lists/sidemenuFiltersLists.template.html',
    styleUrls: [ '../lists/sidemenuFiltersLists.styles.less' ]
} )
export class SidemenuFiltersRepositoriesComponent extends SidemenuFiltersListsComponent {
    @Input()
    public type: MediaKind;

    public api: SidemenuListsRepositoriesApi;

    constructor(service : FiltersSidemenuService<RepositoriesFilterType>, serverApi:  ServerApiManager) {
        super(service);
        this.filterName = 'reposiotries';
        this.api = new SidemenuListsRepositoriesApi(serverApi);
        this.translations = { ...this.translations, title: 'Repositórios' };
    }

    public ngOnChanges(changes: SimpleChanges) {
        super.ngOnChanges(changes);
        this.api.setType(this.type);
    }

}

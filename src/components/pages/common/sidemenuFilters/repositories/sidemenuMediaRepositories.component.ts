import { Component, SimpleChanges } from '@angular/core';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { FiltersSidemenuService, FilteringList } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { FiltersBaseType } from 'components/pages/common/sidemenuFilters/sidemenuFiltersBase';
import { SidemenuMediaListComponent } from 'components/pages/common/sidemenuFilters/lists/sidemenuMediaLists.component';
import { SidemenuListsRepositoriesApi } from 'components/pages/common/sidemenuFilters/repositories/sidemenuFiltersRepositoriesApi';

type ReposiotriesFilterType = FiltersBaseType<FilteringList>;

@Component( {
    selector: 'uc-sidemenu-media-reposiotries',
    templateUrl: '../lists/sidemenuFiltersLists.template.html',
    styleUrls: [ '../lists/sidemenuFiltersLists.styles.less' ]
} )
export class SidemenuMediaRepositoriesComponent extends SidemenuMediaListComponent {
    public api: SidemenuListsRepositoriesApi;

    constructor( service : FiltersSidemenuService<ReposiotriesFilterType>, serverApi: ServerApiManager) {
        super(service);
        this.api = new SidemenuListsRepositoriesApi(serverApi);
        this.filterName = 'repositories';
        this.translations = { ...this.translations, title: 'Repositórios'};
        this.hideNotFiltered = true;
    }

    public ngOnChanges(changes: SimpleChanges){
        if (changes.media && changes.media.currentValue) {
            this.api.setType(changes.media.currentValue.kind);
        }

        super.ngOnChanges(changes);
    }

    //Override and do nothing
    toggleServiceFilter ( event:any , itemId : string ) {

    }

}

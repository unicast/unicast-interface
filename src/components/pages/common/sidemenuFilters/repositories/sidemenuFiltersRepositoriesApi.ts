import { SidemenuListsItemApi, ItemSchema } from 'components/pages/common/sidemenuFilters/lists/sidemenuFiltersLists.component';
import { Observable } from 'rxjs/Observable';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { SidemenuMediaListsItemApi } from 'components/pages/common/sidemenuFilters/lists/sidemenuMediaLists.component';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { SidemenuListsEmptyApi } from 'components/pages/common/sidemenuFilters/lists/sidemenuFiltersEmptyApi';

export class SidemenuListsRepositoriesApi extends SidemenuListsEmptyApi implements SidemenuListsItemApi, SidemenuMediaListsItemApi {
    private serverApi: ServerApiManager;
    private type: MediaKind;

    public constructor(serverApi: ServerApiManager) {
        super();
        this.serverApi = serverApi;
    }

    public setType(type: MediaKind) {
        this.type = type;
    }

    public fetch(): Observable<Array<ItemSchema>> {
        return this.serverApi.getMedia().getRepositoryApi().list([this.type], { virtual: true }).map(list => list.map( e => ({id: e.name, title: e.displayName }) ));
    }

    public getMediaItems(media: MediaRecordSchema): Array<ItemSchema> {
        return (media.repositoryPaths || []).map(e => ({ id: e, title: e}));
    }
}
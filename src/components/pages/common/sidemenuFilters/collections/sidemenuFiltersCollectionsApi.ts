import { SidemenuListsItemApi, ItemSchema } from 'components/pages/common/sidemenuFilters/lists/sidemenuFiltersLists.component';
import { Observable } from 'rxjs/Observable';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { SidemenuMediaListsItemApi } from 'components/pages/common/sidemenuFilters/lists/sidemenuMediaLists.component';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';

export class SidemenuListsCollectionsApi implements SidemenuListsItemApi, SidemenuMediaListsItemApi {
    private serverApi: ServerApiManager;
    private type: string;

    public constructor(serverApi: ServerApiManager) {
        this.serverApi = serverApi;
    }

    public setType(type: string) {
        this.type = type;
    }

    public fetch(): Observable<Array<ItemSchema>> {
        return this.serverApi.getMedia().getCollectionApi().list(this.type, { tree: true });
    }

    public add(item: ItemSchema): Observable<ItemSchema> {
        return this.serverApi.getMedia().getCollectionApi().add([this.type], item.title, item.parentId);
    }

    public delete(id: string): Observable<ItemSchema> {
        return this.serverApi.getMedia().getCollectionApi().delete(this.type, id);
    }

    public insertMedia(media: MediaRecordSchema, itemId: string): Observable<MediaRecordSchema> {
        return this.serverApi.getMedia().getCollectionApi().insertMedia(media.kind, itemId, media.id);
    }

    public removeMedia(media: MediaRecordSchema, itemId: string): Observable<MediaRecordSchema> {
        return this.serverApi.getMedia().getCollectionApi().removeMedia(media.kind, itemId, media.id);
    }

    public getMediaItems(media: MediaRecordSchema): Array<ItemSchema> {
        return media.collections;
    }
}
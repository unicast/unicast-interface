import { Component, SimpleChanges } from '@angular/core';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { FiltersSidemenuService, FilteringList } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { FiltersBaseType } from 'components/pages/common/sidemenuFilters/sidemenuFiltersBase';
import { SidemenuMediaListComponent } from 'components/pages/common/sidemenuFilters/lists/sidemenuMediaLists.component';
import { SidemenuListsCollectionsApi } from 'components/pages/common/sidemenuFilters/collections/sidemenuFiltersCollectionsApi';

type CollectionsFilterType = FiltersBaseType<FilteringList>;

@Component( {
    selector: 'uc-sidemenu-media-collections',
    templateUrl: '../lists/sidemenuFiltersLists.template.html',
    styleUrls: [ '../lists/sidemenuFiltersLists.styles.less' ]
} )
export class SidemenuMediaCollectionsComponent extends SidemenuMediaListComponent {
    public api: SidemenuListsCollectionsApi;

    constructor( service : FiltersSidemenuService<CollectionsFilterType>, serverApi: ServerApiManager) {
        super(service);
        this.api = new SidemenuListsCollectionsApi(serverApi);
        this.filterName = 'collections';
        this.translations = { ...this.translations, title: 'COLLECTION.TITLE', newItem: 'COLLECTION.NEW', confirmationTitle: 'COLLECTION.DELETE.TITLE', confirmationMessage: 'COLLECTION.DELETE.MESSAGE'}
        this.allowManageItems = true;
    }

    public ngOnChanges(changes: SimpleChanges){
        if (changes.media && changes.media.currentValue) {
            this.api.setType(changes.media.currentValue.kind);
        }

        super.ngOnChanges(changes);
    }

}

import { Component, Input, SimpleChanges } from '@angular/core';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { FiltersSidemenuService, FilteringList } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { CollectionSchema } from 'components/schemas/mediaRecord.schema';
import { FiltersBaseType } from 'components/pages/common/sidemenuFilters/sidemenuFiltersBase';
import { SidemenuFiltersListsComponent } from 'components/pages/common/sidemenuFilters/lists/sidemenuFiltersLists.component';
import { SidemenuListsCollectionsApi } from 'components/pages/common/sidemenuFilters/collections/sidemenuFiltersCollectionsApi';

type CollectionsFilterType = FiltersBaseType<FilteringList>;

export interface CollectionUiSchema extends CollectionSchema {
    depth: number;
    hasChildren: boolean;
    isCollapsed: boolean;
    isHidden: boolean;
}

@Component( {
    selector: 'uc-sidemenu-filters-collections',
    templateUrl: '../lists/sidemenuFiltersLists.template.html',
    styleUrls: [ '../lists/sidemenuFiltersLists.styles.less' ]
} )
export class SidemenuFiltersCollectionsComponent extends SidemenuFiltersListsComponent{
    @Input() type: string;
    public api: SidemenuListsCollectionsApi;

    constructor(service : FiltersSidemenuService<CollectionsFilterType>, serverApi:  ServerApiManager) {
        super(service);
        this.filterName = 'collections';
        this.api = new SidemenuListsCollectionsApi(serverApi);
        this.translations = { ...this.translations, title: 'COLLECTION.TITLE', newItem: 'COLLECTION.NEW', confirmationTitle: 'COLLECTION.DELETE.TITLE', confirmationMessage: 'COLLECTION.DELETE.MESSAGE'};
        this.allowManageItems = true;
    }

    public ngOnChanges(changes: SimpleChanges) {
        super.ngOnChanges(changes);
        this.api.setType(this.type);
    }

}

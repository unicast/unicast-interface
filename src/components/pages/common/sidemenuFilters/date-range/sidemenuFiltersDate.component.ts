import { Component, Output, EventEmitter, Input } from '@angular/core';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { DateRange } from 'components/common/date-range/date-range.component';
import { SidemenuFilterBase, FiltersBaseType } from 'components/pages/common/sidemenuFilters/sidemenuFiltersBase';
import { FiltersSidemenuService } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';

type DateRangeFilterType = FiltersBaseType<Date>;

@Component( {
    selector: 'uc-sidemenu-filters-date',
    templateUrl: './sidemenuFiltersDate.template.html',
    styleUrls: [ './sidemenuFiltersDate.styles.less' ]
} )
export class SidemenuFiltersDateComponent extends SidemenuFilterBase<DateRangeFilterType> {

    public serverApi: ServerApiManager;

    @Input('startDateRange')
    public dateRange: DateRange = undefined;

    @Output()
    public changed: EventEmitter<DateRange> = new EventEmitter();

    constructor (service : FiltersSidemenuService<DateRangeFilterType>, serverApi:  ServerApiManager) {
        super(service);
        this.serverApi = serverApi;
        this.filterName = 'date';
    }

    ngOnInit() {
        super.ngOnInit();
        this.dateRange = { start: this.service.filters[this.filterName + 'Start'], end: this.service.filters[this.filterName + 'End'] };
    }

    onChange(event: DateRange) {
        this.changed.emit(event);
        this.service.filters[this.filterName + 'Start'] = event.start;
        this.service.filters[this.filterName + 'End'] = event.end;
        this.service.filtersChanges.emit(this.service.filters);
    }

}
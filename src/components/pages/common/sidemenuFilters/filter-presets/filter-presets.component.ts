import { Component, ViewChild } from '@angular/core';

import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { InputModalComponent, InputButtonClick } from 'components/common/modal/inputModal/inputModal.component';
import { FiltersSidemenuService } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';

@Component({
    selector: 'uc-filter-presets',
    templateUrl: './filter-presets.template.html',
    styleUrls: [ './filter-presets.styles.less' ]
} )
export class FilterPresetsComponent<T> extends BaseUnicastComponent {
    @ViewChild( 'inputNewName' )
    modal : InputModalComponent<string>;

    public service: FiltersSidemenuService<T>;

    constructor ( service : FiltersSidemenuService<T> ) {
        super();
        this.service = service;
    }

    saveFilters() {
        if (!this.service.currentPresetId) {
            this.modal.open().filter(e => e.type == 'ok' ).subscribe( (e: InputButtonClick<string>) => {
                this.service.saveFilters(e.value);
            });
        } else {
            this.service.saveFilters();
        }
    }
}
import { Injectable, Inject, Provider, Optional } from "@angular/core";
import { AppStorage } from 'components/app.storage';
import { FiltersSidemenuService, SortDirection, FilterGroup, FilteringList } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { SortFilter } from 'components/pages/common/sidemenuFilters/sort/sidemenuFiltersSort.component';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';

export interface MediaListQueryFilters {
    sort ?: SortFilter,
    watched ?: FilterGroup;
    repositories ?: FilteringList;
    collections ?: FilteringList;
    genres ?: FilteringList;
    resolutions ?: FilteringList;
    videoCodecs ?: FilteringList;
    colorspaces ?: FilteringList;
    bitdepths ?: FilteringList;
    channels ?: FilteringList;
    audioCodecs ?: FilteringList;
    languages ?: FilteringList;
    sources ?: FilteringList;
}

export interface MediaListQueryFiltersType {
    type: string;
}

@Injectable()
export class FiltersMediaSidemenuService extends FiltersSidemenuService<MediaListQueryFilters> {

    public static DEFAULT_VALUE: MediaListQueryFilters = {
        watched: FilterGroup.All,
        sort: { field: 'title', direction: SortDirection.Ascending },
        repositories: {},
        collections: {},
        genres: {},
        resolutions: {},
        videoCodecs: {},
        bitdepths: {},
        colorspaces: {},
        channels: {},
        audioCodecs: {},
        languages: {},
        sources: {},
    };

    public kind: string;

    constructor(appStorage: AppStorage, serverApiManager: ServerApiManager, @Optional() @Inject('Kind') kind: string) {
        super(appStorage, serverApiManager);
        this.kind = kind;
        this.filterPrefix = 'media' + (kind ? '.' + kind : '');
        if (this.kind) {
            this.filters = {...FiltersMediaSidemenuService.DEFAULT_VALUE, ...this.appStorage.getMediaFilters(this.kind, FiltersMediaSidemenuService.DEFAULT_VALUE)};
        } else {
            this.filters = {};
        }
    }

    protected mergeWithDefault(filter: MediaListQueryFilters) : MediaListQueryFilters {
        return { ...FiltersMediaSidemenuService.DEFAULT_VALUE, ...filter };
    }

    public static forRoot(kind: string): Array<Provider> {
        return [{ provide: 'Kind', useValue: kind },{
            provide: FiltersSidemenuService,
            useClass: FiltersMediaSidemenuService,
            deps: [AppStorage, ServerApiManager, [new Inject('Kind')]]
        }];
    }

    saveFilters(title?: string) {
        if (this.kind) {
            return super.saveFilters(title);
        }
    }

}

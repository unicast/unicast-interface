import { EventEmitter } from "@angular/core";
import { AppStorage } from 'components/app.storage';
import { ServerStorageRecord } from 'components/services/serverApis/storageServerApi';
import { Observable } from 'rxjs/Observable';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { ObjectUtils } from 'components/common/helpers/objectUtils';

export enum FilterGroup {
    All = 'all',
    Include = 'include',
    Exclude = 'exclude'
}

export enum SortDirection {
    Ascending = 'asc',
    Descending = 'desc'
}

export type FilteringList = { [ name : string ] : FilterGroup };

export type FilterPreset<T> = { id: string, title: string, filters: T };

export type ListPresets<T> = { [name: string]: FilterPreset<T> };

export type SectionState<T> = Partial<{[k in keyof T]: boolean}>;

export class FiltersSidemenuService<T> {

    public appStorage: AppStorage;
    public serverApiManager: ServerApiManager;
    protected filterPrefix: string = 'filters';

    public presets: ServerStorageRecord<ListPresets<T>> = null;
    public currentPresetId: string = null;
    public filters : T;
    public sectionState : SectionState<T>;

    public filtersChanges : EventEmitter<T> = new EventEmitter();
    public presetChanges : EventEmitter<string> = new EventEmitter();
    public statesChanges : EventEmitter<keyof T> = new EventEmitter();

    constructor(appStorage: AppStorage, serverApiManager: ServerApiManager) {
        this.appStorage = appStorage;
        this.serverApiManager = serverApiManager;
        this.sectionState = {};
    }

    protected mergeWithDefault(filter: T) : T {
        return filter;
    }

    initState(id: keyof T, value: boolean = true) {
        if (this.sectionState[id] === undefined) {
            this.sectionState[id] = value;
        }
    }

    public publishStates(state: SectionState<T>) {
        this.sectionState = state;
    }

    public changeState(id: keyof T, value: boolean) {
        this.sectionState[id] = value;
        this.statesChanges.emit(id);
    }

    public loadPresets(): Observable<ServerStorageRecord<ListPresets<T>>> {
        let ret = this.serverApiManager.getStorage().getItem<ListPresets<T>>(this.filterPrefix);
        ret.subscribe( e => this.presets = e );
        return ret;
    }

    public changePreset(id: string) {
        this.currentPresetId = id;
        if (this.presets && this.presets.value[id]) {
            this.publishFilters(this.presets.value[this.currentPresetId].filters);
        }
        this.presetChanges.emit(id);
    }

    public removePreset(id: string): Observable<ListPresets<T>> {
        let ret = this.loadPresets().map(e => {
            delete e.value[id];
            return e.value;
        });
        return this.storeNewPresets(ret);
    }

    public saveFilters(title: string = 'default'): Observable<ListPresets<T>> {
        let ret = this.loadPresets().map(e => {
            let filters = e ? e.value : {};
            let id = this.currentPresetId || ObjectUtils.guidGenerator();
            title = (filters[id] ? filters[id].title : title);
            filters[id] = {id, title, filters: this.filters};
            return filters;
        });
        return this.storeNewPresets(ret);
    }

    private storeNewPresets(ret: Observable<ListPresets<T>>): Observable<ListPresets<T>>{
        ret.subscribe( filters => {
            this.serverApiManager.getStorage().setItem(this.filterPrefix, filters);
            this.presets.value = filters;
        });
        return ret;
    }

    public publishFilters(filters: T) {
        this.filters = this.mergeWithDefault(JSON.parse(JSON.stringify(filters)));
        this.emitFilterChanges();
    }

    public setFilter(key: keyof T, value: any, silent: boolean = false): void {
        this.filters[key] = value;
        if (!silent) {
            this.emitFilterChanges();
        }
    }

    public injectFilters(filters: {}) {
        const prefix: string = 'filter';
        let keys = Object.keys(filters).filter(e => e.startsWith(prefix)).map(e => e.substr(prefix.length));
        for (let key of keys) {
            let value = (key.indexOf('[]') > -1 ? [].concat(filters[prefix + key]).filter(e => !!e) : filters[prefix + key])
            this.setFilter(key.charAt(0).toLocaleLowerCase() + key.substr(1).replace('[]', '') as keyof T, value, true);
        }
        this.emitFilterChanges();
    }

    protected emitFilterChanges() {
        this.filtersChanges.emit(this.filters);
    }


}
import { Component, Input } from '@angular/core';

import { FiltersSidemenuService, SortDirection } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { SidemenuFilterCyclingBase } from 'components/pages/common/sidemenuFilters/sidemenuFiltersCyclingBase';
import { FiltersBaseType } from 'components/pages/common/sidemenuFilters/sidemenuFiltersBase';

export type SortFilter = { field : string, direction : SortDirection, list?: string };

export type SortDefinition = { label: string, available?: () => boolean };
export type SortFields = { [ field : string ] : SortDefinition };

type SortFilterType = FiltersBaseType<SortFilter>;

@Component( {
    selector: 'uc-sidemenu-filters-sort',
    templateUrl: './sidemenuFiltersSort.template.html',
    styleUrls: [ './sidemenuFiltersSort.styles.less' ]
} )
export class SidemenuFiltersSortComponent extends SidemenuFilterCyclingBase<SortFilterType>{
    @Input()
    public fieldsSort : SortFields = {};

    constructor ( service : FiltersSidemenuService<SortFilterType>) {
        super(service);
    }

    ngOnInit() {
        super.ngOnInit();

        if (!this.service.filters[this.filterName].field) {
            //Start with the first available value
            this.internaltoggleSortFilter(false);
        }
    }

    internaltoggleSortFilter (backwards: boolean) {
        const fields = Object.keys( this.fieldsSort );

        //Start with the current index and cycle after
        //If it returns the value -1, it will start by the first
        let nextIndex = fields.indexOf( this.service.filters[this.filterName].field );

        //Continue the cycling while a method exists and returns false as availability
        do {
            nextIndex = ( nextIndex + (backwards ? -1 : 1) ) % fields.length;
            nextIndex = nextIndex < 0 ? fields.length - 1 : nextIndex;
        } while ( this.fieldsSort[ fields[ nextIndex ] ].available && !this.fieldsSort[ fields[ nextIndex ] ].available() )

        this.service.filters[this.filterName].field = fields[ nextIndex ];

        this.service.filters[this.filterName].list = this.service.currentPresetId;

        this.service.filtersChanges.next( this.service.filters );
    }

    toggleSortFilter ( event: MouseEvent ) {
        this.internaltoggleSortFilter(event.ctrlKey);
    }

    toggleSortOrderFilter () {
        this.service.filters[this.filterName].direction = this.service.filters[this.filterName].direction == SortDirection.Ascending ?
            SortDirection.Descending :
            SortDirection.Ascending;

        this.service.filtersChanges.next( this.service.filters );
    }

}
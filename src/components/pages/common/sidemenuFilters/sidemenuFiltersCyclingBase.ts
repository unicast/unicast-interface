import { FiltersSidemenuService, FilterGroup } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { SidemenuFilterBase } from 'components/pages/common/sidemenuFilters/sidemenuFiltersBase';


export class SidemenuFilterCyclingBase<T> extends SidemenuFilterBase<T> {
    protected cyclingOrder: Array<FilterGroup> = [FilterGroup.All, FilterGroup.Include, FilterGroup.Exclude];

    private cyclingOrderMap: Map<FilterGroup, [FilterGroup, FilterGroup]>;

    constructor ( service : FiltersSidemenuService<T>) {
        super(service);
        this.buildCyclingOrderMap();
    }

    protected buildCyclingOrderMap() {
        this.cyclingOrderMap = new Map<FilterGroup, [FilterGroup, FilterGroup]>();
        let lastOne: number = this.cyclingOrder.length - 1;

        //Handle the first one
        this.cyclingOrderMap.set(this.cyclingOrder[0], [this.cyclingOrder[lastOne], this.cyclingOrder[1]]);
        //Handle the middle
        for (let i = 1; i < lastOne; i++){
            this.cyclingOrderMap.set(this.cyclingOrder[i], [this.cyclingOrder[i-1], this.cyclingOrder[i+1]]);
        }
        //Handle the last one
        this.cyclingOrderMap.set(this.cyclingOrder[lastOne], [this.cyclingOrder[lastOne - 1], this.cyclingOrder[0]]);

    }

    toggleFilterGroup ( filter : FilterGroup, inverse: boolean = false ) {
        if ( !filter ) {
            filter = FilterGroup.All;
        }

        return this.cyclingOrderMap.get(filter)[inverse ? 0 : 1];
    }

    toggleServiceFilter ( event: MouseEvent, id?: string ) {
        if (id !== undefined) {
            this.service.filters[this.filterName][ id ] = this.toggleFilterGroup( this.service.filters[this.filterName][ id ], event.ctrlKey );
        } else {
            this.service.filters[this.filterName] = this.toggleFilterGroup( this.service.filters[this.filterName], event.ctrlKey );
        }

        this.service.filtersChanges.next( this.service.filters );
    }
}
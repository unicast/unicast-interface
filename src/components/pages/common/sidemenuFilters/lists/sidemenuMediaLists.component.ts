import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { FiltersSidemenuService, FilterGroup, FilteringList } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { FiltersBaseType } from 'components/pages/common/sidemenuFilters/sidemenuFiltersBase';
import { SidemenuFiltersListsComponent, ItemSchema, SidemenuListsItemApi } from 'components/pages/common/sidemenuFilters/lists/sidemenuFiltersLists.component';


type ListsFilterType = FiltersBaseType<FilteringList>;

export interface SidemenuMediaListsItemApi extends SidemenuListsItemApi {
    insertMedia(media: MediaRecordSchema, itemId: string): Observable<MediaRecordSchema>;
    removeMedia(media: MediaRecordSchema, itemId: string): Observable<MediaRecordSchema>;
    getMediaItems(media: MediaRecordSchema): Array<ItemSchema>;
}

@Component( {
    selector: 'uc-sidemenu-media-lists',
    templateUrl: './sidemenuFiltersLists.template.html',
    styleUrls: [ './sidemenuFiltersLists.styles.less' ]
} )
export class SidemenuMediaListComponent extends SidemenuFiltersListsComponent {
    @Input() media: MediaRecordSchema;

    public api: SidemenuMediaListsItemApi;

    constructor ( service : FiltersSidemenuService<ListsFilterType>) {
        super(service);
        this.filteredItems = {};
    }


    ngOnInit(){

        if (this.media) {
            this.ngOnChanges({media: this.media});

            super.ngOnInit();
        }

        //Remove filters subscriptions
        this.clearSubscriptions();

    }

    ngOnChanges(change){
        if (change.media && change.media.currentValue){
            this.loadItems();

            this.filteredItems = {};
            (this.api.getMediaItems(this.media) || []).forEach( item => this.filteredItems[ item.id ] = FilterGroup.Include );
        }
    }

    ngOnDestroy() {
        super.ngOnDestroy();
    }

    toggleServiceFilter ( event:any , itemId : string ) {
        if (this.filteredItems[ itemId ] == FilterGroup.Include) {
            this.api.removeMedia(this.media, itemId);
            this.filteredItems[ itemId ] = FilterGroup.All;
        } else {
            this.api.insertMedia(this.media, itemId);
            this.filteredItems[ itemId ] = FilterGroup.Include;
        }

    }

}

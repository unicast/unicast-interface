import { SidemenuListsItemApi, ItemSchema } from 'components/pages/common/sidemenuFilters/lists/sidemenuFiltersLists.component';
import { Observable } from 'rxjs/Observable';
import { SidemenuMediaListsItemApi } from 'components/pages/common/sidemenuFilters/lists/sidemenuMediaLists.component';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';

class ApiMethodNotImplemented extends Error {
    constructor() {
        super('Not Managable from Interface');
    }
}

export class SidemenuListsEmptyApi implements SidemenuListsItemApi, SidemenuMediaListsItemApi {

    public constructor() {

    }

    public fetch(): Observable<Array<ItemSchema>> {
        throw new ApiMethodNotImplemented();
    }

    public add(item: ItemSchema): Observable<ItemSchema> {
        throw new ApiMethodNotImplemented();
    }

    public delete(id: string): Observable<ItemSchema> {
        throw new ApiMethodNotImplemented();
    }

    public insertMedia(media: MediaRecordSchema, itemId: string): Observable<MediaRecordSchema> {
        throw new ApiMethodNotImplemented();
    }

    public removeMedia(media: MediaRecordSchema, itemId: string): Observable<MediaRecordSchema> {
        throw new ApiMethodNotImplemented();
    }

    public getMediaItems(media: MediaRecordSchema): Array<ItemSchema> {
        return [];
    }
}
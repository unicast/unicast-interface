import { Component, ViewChild, ElementRef, Input, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { FiltersSidemenuService, FilteringList } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { SidemenuFilterCyclingBase } from 'components/pages/common/sidemenuFilters/sidemenuFiltersCyclingBase';
import { FiltersBaseType } from 'components/pages/common/sidemenuFilters/sidemenuFiltersBase';

type ListsFilterType = FiltersBaseType<FilteringList>;

export interface ItemSchema {
    id: string;
    title: string;
    parentId?: string;
    children?: Array<ItemSchema>;
}

export interface ItemUiSchema extends ItemSchema {
    depth: number;
    hasChildren: boolean;
    isCollapsed: boolean;
    isHidden: boolean;
}

export interface SidemenuListsTranslation {
    title: string;
    addItem: string;
    deleteItem: string;
    newItem: string;
    createItem: string;
    confirmationTitle: string;
    confirmationMessage: string;
}

export interface SidemenuListsItemApi {
    fetch(): Observable<Array<ItemSchema>>;

    add(item: ItemSchema): Observable<ItemSchema>;

    delete(id: string): Observable<ItemSchema>;
}

@Component( {
    selector: 'uc-sidemenu-filters-lists',
    templateUrl: './sidemenuFiltersLists.template.html',
    styleUrls: [ './sidemenuFiltersLists.styles.less' ]
} )
export class SidemenuFiltersListsComponent extends SidemenuFilterCyclingBase<ListsFilterType>{
    @ViewChild('newItemName') input: ElementRef;

    //@Input() type: string;

    @Input() public allowManageItems: boolean = false;

    @Input() public api: SidemenuListsItemApi;

    @Input() public title: string | undefined;

    public translations: SidemenuListsTranslation;

    public loadingItems : boolean = false;

    public addingItem: boolean = false;

    public addingItemParent: ItemUiSchema = null;

    public editingName: string = '';

    public itemsTree : Map<string, ItemUiSchema[]> = new Map();

    public items : Array<ItemUiSchema> = [];

    public filteredItems: FilteringList = {};

    public hideNotFiltered: boolean = false;

    public itemsContextMenuTarget: ItemUiSchema = null;

    public defaultTranslations: SidemenuListsTranslation;

    constructor ( service : FiltersSidemenuService<ListsFilterType>) {
        super(service);
        this.filterName = 'list';
        this.translations = {
            title: this.filterName,
            addItem: 'BUTTONS.GENERAL.ADD',
            deleteItem: 'BUTTONS.GENERAL.DELETE',
            newItem: 'GENERAL.NEW',
            createItem: 'BUTTONS.GENERAL.CREATE',
            confirmationTitle: 'GENERAL.DELETE',
            confirmationMessage: 'GENERAL.DELETE',
        };
    }

    ngOnInit(){
        super.ngOnInit();
        this.loadItems();
        this._subscriptions.push(this.service.filtersChanges.subscribe( e=> {
            this.filteredItems = this.service.filters[this.filterName] || {};
        }));
    }

    ngOnChanges(changes: SimpleChanges){
        if ( this.title != null && this.title != this.translations.title ) {
            this.translations = { ...this.translations, title: this.title };
        }
    }

    protected reindexList (oldId: string, newId: string) {
        const item = this.getItem(oldId);

        if (item == null) return;

        item.id = newId;

        const children = this.itemsTree.get(oldId);

        this.itemsTree.set(newId, children);

        this.itemsTree.delete(oldId);
    }

    protected getItemIndexRange (id: string) {
        const len = this.items.length;

        if (id == null) return [0, len];

        let min = 0;

        for (; min < len; min++) {
            if (this.items[min].id == id) {
                min++;
                break;
            }
        }

        if (min == len) return [0, len];

        const minDepth = this.items[min - 1].depth + 1;

        let max = min;

        for (max = min; max < len; max++) {
            if (this.items[max].depth < minDepth) {
                break;
            }
        }

        return [min, max];
    }

    protected insertItemNode(item: ItemSchema) : ItemUiSchema {
        const parent = this.getItem(item.parentId);

        let itemUi : ItemUiSchema = {
            ...item,
            depth: parent ? parent.depth + 1 : 0,
            hasChildren: false,
            isCollapsed: true,
            isHidden: false,
            children: []
        };

        if (parent != null) {
            parent.hasChildren = true;

            this.itemsTree.get(parent.id).push(itemUi);
        }

        this.itemsTree.set(itemUi.id, []);

        //Ordered insertion
        const cols = this.items;
        const [min, max] = this.getItemIndexRange(item.parentId);

        for (let p = min; p <= max; p++) {
            if (p == max || (cols[p].parentId == item.parentId && cols[p].title > item.title)) {
                cols.splice(p, 0, itemUi);
                break;
            }
        }

        this.expandItemPath(itemUi.id);

        return itemUi;
    }

    protected updateItemNode (item: ItemSchema) : ItemUiSchema {
        const itemSchema = this.getItem(item.id);

        if (itemSchema == null) return;

        Object.assign(itemSchema, item);

        return itemSchema;
    }

    protected removeItemNode (id: string) {
        for (let child of this.itemsTree.get(id) || []) {
            this.removeItemNode(child.id);
        }

        const index = this.items.findIndex(col => col.id == id);

        if (index >= 0) {

            const parent = this.items[index].parentId;

            this.items.splice(index, 1);

            this.itemsTree.delete(id);

            const parentChildren = this.itemsTree.get(parent);

            if (parentChildren != null) {
                const childIndex = parentChildren.findIndex(col => col.id == id);

                if (childIndex >= 0) {
                    parentChildren.splice(childIndex, 1);
                }

                if (parentChildren.length == 0) {
                    const parentItem = this.getItem(parent);

                    parentItem.isCollapsed = true;
                    parentItem.hasChildren = false;
                }
            }
        }
    }

    protected pushItemTree(item: ItemSchema, depth: number = 0) {
        const itemUi : ItemUiSchema = {
            ...item,
            depth: depth,
            hasChildren: (item.children || []).length > 0,
            isCollapsed: true,
            isHidden: depth > 0
        };

        this.items.push(itemUi);

        if (item.children != null) {
            const children = item.children.map(c => this.pushItemTree(c, depth + 1));

            this.itemsTree.set(item.id, children);
        } else {
            this.itemsTree.set(item.id, []);
        }

        return itemUi;
    }

    loadItems() {
        this.loadingItems = true;

        this.filteredItems = this.service.filters[this.filterName] || {};
        this.api.fetch().subscribe( items => {
            this.loadingItems = false;

            this.itemsTree.clear();

            this.items = [];

            items.forEach(c => this.pushItemTree(c));

        } );
    }

    setItemsVisibility(items: ItemUiSchema[], visible: boolean) {
        for (let child of items) {
            // if isHidden == visible, then it's visibility will toggle, which means
            // we need to update it and its children's visibility as well
            if (child.isHidden == visible) {
                child.isHidden = !visible;

                if (visible == false || !child.isCollapsed) {
                    this.setItemsVisibility(this.itemsTree.get(child.id), visible);
                }
            }
        }
    }

    getItem (itemId: string) : ItemUiSchema {
        return this.items.find(it => it.id == itemId);
    }

    collapseItem (itemId: string) {
        const item = this.getItem(itemId);

        if (item == null) return;

        if (item.isCollapsed == false) {
            item.isCollapsed = true;

            this.setItemsVisibility(this.itemsTree.get(item.id), false);

            this.closeItemContextMenu();
        }
    }

    expandItem (itemId: string) {
        const item = this.getItem(itemId);

        if (item == null) return;

        if (item.isCollapsed == true) {
            item.isCollapsed = false;

            this.setItemsVisibility(this.itemsTree.get(item.id), true);

            this.closeItemContextMenu();
        }
    }

    toggleCollapsedItem (item: ItemUiSchema) {
        if (item.isCollapsed) {
            this.expandItem(item.id);
        } else {
            this.collapseItem(item.id);
        }
    }

    * iterateItemParents (item: ItemUiSchema) : IterableIterator<ItemUiSchema> {
        while (item.parentId != null) {
            item = this.getItem(item.parentId);

            if (item == null) break;

            yield item;
        }
    }

    expandItemPath (itemId: string) {
        let item = this.getItem(itemId);

        if (item == null) return;

        item.isHidden = false;

        for (let parent of this.iterateItemParents(item)) {
            parent.isCollapsed = false;

            this.setItemsVisibility(this.itemsTree.get(parent.id), true);
        }
    }

    toggleItemContextMenu (item: ItemUiSchema) {
        if (this.itemsContextMenuTarget != null && this.itemsContextMenuTarget.id == item.id) {
            this.itemsContextMenuTarget = null;
        } else {
            this.itemsContextMenuTarget = item;
        }
    }

    closeItemContextMenu () {
        this.itemsContextMenuTarget = null;
    }

    showAddItemForm (item: ItemUiSchema = null) {
        this.addingItemParent = item;
        this.addingItem = true;

        this.closeItemContextMenu();
    }

    submitAddItemForm () {
        this.addNewItem(this.editingName, this.addingItemParent);

        this.closeAddItemForm();
    }

    closeAddItemForm () {
        this.addingItem = false;

        this.addingItemParent = null;

        this.editingName = '';
    }

    detectKey(event: KeyboardEvent) {
        switch (event.keyCode){
            case 13: //Enter
                this.submitAddItemForm();
            case 27: //Esc
                this.closeAddItemForm();
                break;
        }
    }

    showInput() {
        this.addingItem = true;
    }

    addNewItem(name: string, parent: ItemUiSchema = null) {
        const item = this.insertItemNode({
            title: name,
            id: 'tmp' + this.items.length,
            parentId: parent ? parent.id : null,
        });

        //Request it
        this.api.add(item).subscribe( (data) => {
            this.reindexList(item.id, data.id);

            this.updateItemNode(data);
        }, () => {
            this.removeItemNode(item.id);
        });
    }

    deleteItem(id: string, position: number) {
        // let elem = this.collections.splice(position, 1);
        // const collection = this.collections[position];
        this.api.delete(id).subscribe( () => {
            this.removeItemNode(id);
            //Nothing to receive
        }, () => {
            //Error, re-add the element
            // this.collections.splice(position, 0, elem[0]);
            // this.insertCollectionNode(collection);
        });
    }

}

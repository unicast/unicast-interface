import { Input } from '@angular/core';

import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { FiltersSidemenuService } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';

export type FiltersBaseType<B> = { [k in string]: B };

export class SidemenuFilterBase<T extends FiltersBaseType<any>> extends BaseUnicastComponent {
    public service : FiltersSidemenuService<T>;

    @Input()
    public filterName: string = '';

    constructor (service : FiltersSidemenuService<T>) {
        super();
        this.service = service;
    }
}

import { Injectable, Provider } from "@angular/core";
import { AppStorage } from 'components/app.storage';
import { FiltersSidemenuService, } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { HistoryQueryMediaList } from 'components/common/helpers/historyQueryMediaList';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';

export interface HistoryListQueryFilters {
    dateStart: Date;
    dateEnd: Date;
    media: Array<HistoryQueryMediaList>;
}

@Injectable()
export class FiltersHistorySidemenuService extends FiltersSidemenuService<HistoryListQueryFilters> {

    public static DEFAULT_VALUE = {
        dateStart: undefined,
        dateEnd: undefined,
        media: []
    };

    constructor(appStorage: AppStorage, serverApiManager: ServerApiManager) {
        super(appStorage, serverApiManager);
        this.filterPrefix = 'history';
        this.filters = this.appStorage.getHistoryFilters(FiltersHistorySidemenuService.DEFAULT_VALUE);
    }

    public static forRoot(): Array<Provider> {
        return [{
            provide: FiltersSidemenuService,
            useClass: FiltersHistorySidemenuService,
            deps: [AppStorage, ServerApiManager]
        }];
    }


    public publishFilters(filters: HistoryListQueryFilters) {
        this.filters = {
            ...filters,
            media : (filters.media || []).map(e => new HistoryQueryMediaList(e.getkind(), e.getId(), e.getTitle()))
        }
        this.filtersChanges.emit(this.filters);
    }

    public setFilter(key: string, value: any, silent: boolean = false): void {
        if (key == 'media') {
            super.setFilter(key, (value || []).map( (e:string) => e.split(',')).map( (e: string[]) => new HistoryQueryMediaList(e[0], e[1], e[1])), silent);
        }
    }

}
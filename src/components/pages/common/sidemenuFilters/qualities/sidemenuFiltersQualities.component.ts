import { Component, Input, SimpleChanges } from '@angular/core';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { FiltersSidemenuService, FilteringList } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { FiltersBaseType } from 'components/pages/common/sidemenuFilters/sidemenuFiltersBase';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { PlayableMediaQualities } from 'components/schemas/mediaRecord.schema';
import { SidemenuListsQualitiesApi, SidemenuListsQualitiesPropertyApi } from 'components/pages/common/sidemenuFilters/qualities/sidemenuFiltersQualitiesApi';

type QualititesFilterType = FiltersBaseType<FilteringList>;

@Component( {
    selector: 'uc-sidemenu-filters-qualities',
    templateUrl: './sidemenuFiltersQualities.template.html',
} )
export class SidemenuFiltersQualitiesComponent {
    @Input() public type: MediaKind;

    public service: FiltersSidemenuService<QualititesFilterType>;

    public qualitiesApi: SidemenuListsQualitiesApi;

    public api: Record<keyof PlayableMediaQualities, SidemenuListsQualitiesPropertyApi>;

    constructor(service : FiltersSidemenuService<QualititesFilterType>, serverApi: ServerApiManager) {
        this.service = service;
        this.qualitiesApi = new SidemenuListsQualitiesApi(serverApi);
        this.api = {
            resolutions: this.qualitiesApi.forProperty('resolutions'),
            videoCodecs: this.qualitiesApi.forProperty('videoCodecs'),
            colorspaces: this.qualitiesApi.forProperty('colorspaces'),
            bitdepths: this.qualitiesApi.forProperty('bitdepths'),
            audioCodecs: this.qualitiesApi.forProperty('audioCodecs'),
            channels: this.qualitiesApi.forProperty('channels'),
            languages: this.qualitiesApi.forProperty('languages'),
            sources: this.qualitiesApi.forProperty('sources'),
        };
    }

    public ngOnChanges(changes: SimpleChanges) {
        this.qualitiesApi.setType(this.type);
    }
}

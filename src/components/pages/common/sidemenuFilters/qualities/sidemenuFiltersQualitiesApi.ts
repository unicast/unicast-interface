import { SidemenuListsItemApi, ItemSchema } from 'components/pages/common/sidemenuFilters/lists/sidemenuFiltersLists.component';
import { Observable } from 'rxjs/Observable';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { SidemenuMediaListsItemApi } from 'components/pages/common/sidemenuFilters/lists/sidemenuMediaLists.component';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { SidemenuListsEmptyApi } from 'components/pages/common/sidemenuFilters/lists/sidemenuFiltersEmptyApi';
import { PlayableMediaQualities } from 'components/schemas/mediaRecord.schema';

export class SidemenuListsQualitiesApi {
    private serverApi: ServerApiManager;
    private type: MediaKind;
    private _qualities : Observable<PlayableMediaQualities> | null = null;

    public constructor(serverApi: ServerApiManager) {
        this.serverApi = serverApi;
    }

    public setType(type: MediaKind) {
        this.type = type;
        this._qualities = null;
    }

    public fetch(): Observable<PlayableMediaQualities> {
        if ( this._qualities == null ) {
            this._qualities = this.serverApi.getMedia().listQualitites(this.type);
        }

        return this._qualities;
    }

    public forProperty(qualityProperty: keyof PlayableMediaQualities): SidemenuListsQualitiesPropertyApi {
        return new SidemenuListsQualitiesPropertyApi(this, qualityProperty);
    }
}

export class SidemenuListsQualitiesPropertyApi extends SidemenuListsEmptyApi implements SidemenuListsItemApi, SidemenuMediaListsItemApi {
    private parentApi: SidemenuListsQualitiesApi;
    private qualityProperty: keyof PlayableMediaQualities;

    public constructor(parentApi: SidemenuListsQualitiesApi, qualityProperty: keyof PlayableMediaQualities) {
        super();
        this.parentApi = parentApi;
        this.qualityProperty = qualityProperty;
    }

    public fetch(): Observable<Array<ItemSchema>> {
        return this.parentApi.fetch()
            .map(qualities => qualities[this.qualityProperty])
            .map(list => list.map( e => ({id: e, title: e }) ));
    }

}

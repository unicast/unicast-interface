import { Component, Input, Output, EventEmitter } from '@angular/core';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Observable } from 'rxjs/Observable';


import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { SidemenuFilterBase, FiltersBaseType } from 'components/pages/common/sidemenuFilters/sidemenuFiltersBase';
import { FiltersSidemenuService } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { HistoryQueryMediaList } from 'components/common/helpers/historyQueryMediaList';

type MediaFilterType = FiltersBaseType<Array<HistoryQueryMediaList>>;

@Component( {
    selector: 'uc-sidemenu-filters-media',
    templateUrl: './sidemenuFiltersMedia.template.html',
    styleUrls: [ './sidemenuFiltersMedia.styles.less' ]
} )
export class SidemenuFiltersMediaComponent extends SidemenuFilterBase<MediaFilterType> {

    public serverApi: ServerApiManager;

    public isDisabled: boolean = false;

    @Input()
    public startList: Array<HistoryQueryMediaList>;

    @Input()
    private maxElements: number = 10;

    public listObservable: Observable<Array<MediaRecordSchema>> = Observable.of([]);

    @Output()
    private selected: EventEmitter<MediaRecordSchema> = new EventEmitter();

    @Output()
    private changed: EventEmitter<Array<MediaRecordSchema>> = new EventEmitter();


    constructor (  service : FiltersSidemenuService<MediaFilterType>, serverApi:  ServerApiManager) {
        super(service);
        this.serverApi = serverApi;
        this.filterName = 'media';
    }

    ngOnInit() {
        super.ngOnInit();
        forkJoin(
            ...this.service.filters[this.filterName].map(e => this.serverApi.getMedia().get(e.getkind(), e.getId()))
        ).subscribe( list => {
            this.startList = list.map(e => HistoryQueryMediaList.of(e));
        });
    }

    public fetch(str: string) {
        this.listObservable = forkJoin(
            this.serverApi.getMedia().getMovieApi().list(this.maxElements, 0, { search: str }),
            this.serverApi.getMedia().getShowApi().list(this.maxElements, 0, { search: str })
        ).map( ([movies, shows]) => {
            return [].concat(movies).concat(shows).sort((a, b) => a.title.localeCompare(b.title)).slice(0, this.maxElements);
        }).first();
    }

    public onSelect(event: MediaRecordSchema): void {
        this.selected.next(event);
    }

    public onChange(event: Array<MediaRecordSchema>) {
        this.changed.emit(event);
        this.service.filters[this.filterName] = event.map(e => HistoryQueryMediaList.of(e));
        this.service.filtersChanges.emit(this.service.filters);
    }

}
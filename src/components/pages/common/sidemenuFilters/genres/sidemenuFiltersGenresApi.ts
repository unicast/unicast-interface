import { SidemenuListsItemApi, ItemSchema } from 'components/pages/common/sidemenuFilters/lists/sidemenuFiltersLists.component';
import { Observable } from 'rxjs/Observable';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { SidemenuMediaListsItemApi } from 'components/pages/common/sidemenuFilters/lists/sidemenuMediaLists.component';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { SidemenuListsEmptyApi } from 'components/pages/common/sidemenuFilters/lists/sidemenuFiltersEmptyApi';
import { KindServerApi } from 'components/services/serverApis/mediaServers/kindServerApi';

export class SidemenuListsGenresApi extends SidemenuListsEmptyApi implements SidemenuListsItemApi, SidemenuMediaListsItemApi {
    private serverApi: ServerApiManager;
    private type: MediaKind;

    public constructor(serverApi: ServerApiManager) {
        super();
        this.serverApi = serverApi;
    }

    public setType(type: MediaKind) {
        this.type = type;
    }

    public fetch(): Observable<Array<ItemSchema>> {
        let kindServerApi: KindServerApi<any> = (this.type == MediaKind.Movie) ? this.serverApi.getMedia().getMovieApi() : this.serverApi.getMedia().getShowApi();
        return kindServerApi.getGenreApi().list().map(list => list.map( e => ({id: e, title: e }) ));
    }

}
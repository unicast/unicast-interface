import { Component, Input, SimpleChanges } from '@angular/core';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { FiltersSidemenuService, FilteringList } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { FiltersBaseType } from 'components/pages/common/sidemenuFilters/sidemenuFiltersBase';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { SidemenuListsGenresApi } from 'components/pages/common/sidemenuFilters/genres/sidemenuFiltersGenresApi';
import { SidemenuFiltersListsComponent } from 'components/pages/common/sidemenuFilters/lists/sidemenuFiltersLists.component';

type GenresFilterType = FiltersBaseType<FilteringList>;

@Component( {
    selector: 'uc-sidemenu-filters-genres',
    templateUrl: '../lists/sidemenuFiltersLists.template.html',
    styleUrls: [ '../lists/sidemenuFiltersLists.styles.less' ]
} )
export class SidemenuFiltersGenresComponent extends SidemenuFiltersListsComponent {
    @Input()
    public type: MediaKind;

    public api: SidemenuListsGenresApi;

    constructor(service : FiltersSidemenuService<GenresFilterType>, serverApi:  ServerApiManager) {
        super(service);
        this.filterName = 'genres';
        this.api = new SidemenuListsGenresApi(serverApi);
        this.translations = { ...this.translations, title: 'Genres' };
    }

    public ngOnChanges(changes: SimpleChanges) {
        super.ngOnChanges(changes);
        this.api.setType(this.type);
    }
    /*
    public serverApi: ServerApiManager;

    public loadingGenres : boolean = false;

    public genres : Array<string> = [];

    constructor ( service : FiltersSidemenuService<GenresFilterType>, serverApi:  ServerApiManager) {
        super(service);
        this.serverApi = serverApi;
        this.filterName = 'genres';
    }

    ngOnInit(){
        super.ngOnInit();

        this.loadingGenres = true;

        this.serverApi.getMedia().getMovieApi().getGenreApi().list().subscribe( genres => {
            this.loadingGenres = false;

            this.genres = genres;
        } );
    }*/

}

import { Component } from '@angular/core';

import { FiltersSidemenuService, FilterGroup } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { SidemenuFilterCyclingBase } from 'components/pages/common/sidemenuFilters/sidemenuFiltersCyclingBase';
import { FiltersBaseType } from 'components/pages/common/sidemenuFilters/sidemenuFiltersBase';

type WatchFilterType = FiltersBaseType<FilterGroup>;

@Component( {
    selector: 'uc-sidemenu-filters-watch',
    templateUrl: './sidemenuFiltersWatch.template.html',
    styleUrls: [ './sidemenuFiltersWatch.styles.less' ]
} )
export class SidemenuFiltersWatchComponent extends SidemenuFilterCyclingBase<WatchFilterType>{

    constructor ( service : FiltersSidemenuService<WatchFilterType>) {
        super(service);
        this.filterName = 'watched';
    }

}
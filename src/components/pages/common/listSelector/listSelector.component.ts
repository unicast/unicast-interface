import { Component, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';

import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';

@Component( {
    selector: 'uc-list-selector',
    templateUrl: './listSelector.template.html',
    styleUrls: [ './listSelector.styles.less' ]
} )
export class ListSelectorComponent<T extends ListChangeInformation> extends BaseUnicastComponent {
    @Input() currentItemId : string;
    public curentItemTitle : string;

    @Input() list: Array<T> = [];

    @Input() allowRemoval: boolean = false;

    @Input() titleLabel: string = '';

    @Input() emptyIcon: string = 'ban';
    @Input() emptyLabel: string = '';

    @Output() onItemChange: EventEmitter<ListChangeInformation> = new EventEmitter();

    @Output() onRemoval: EventEmitter<ListChangeInformation> = new EventEmitter();

    ngOnChanges(change: SimpleChanges) {
        if (change['currentItemId']) {
            let element = this.list.find(e => e.id == this.currentItemId);
            this.selectItem(this.currentItemId, element ? element.title : '', true);
        }
    }

    removeItem(currentItemId: string, curentItemTitle: string) {
        if (this.currentItemId == currentItemId) {
            this.selectItem(null, null);
        }
        this.onRemoval.emit( { id: currentItemId, title: curentItemTitle });
    }

    selectItem(currentItemId: string, curentItemTitle: string, silent: boolean = false) {
        this.currentItemId = currentItemId;
        this.curentItemTitle = curentItemTitle;

        !silent && this.onItemChange.emit({id: currentItemId, title: curentItemTitle});
    }
}

export interface ListChangeInformation {
    id: string;
    title: string;
    identation?: number;
}
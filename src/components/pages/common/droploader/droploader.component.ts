import { Component, Output, EventEmitter, HostListener } from '@angular/core';

@Component( {
    selector: 'uc-droploader',
    templateUrl: './droploader.template.html',
    styleUrls: [ './droploader.styles.less' ]
} )
export class DroploaderComponent {
    protected _visible: boolean = false;

    public get visible () { return this._visible }
    public set visible (value : boolean) {
        if (this._visible !== value) {
            this._visible = value;

            this.visibleChange.emit(this._visible);
        }
    }

    @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();

    @Output() files: EventEmitter<File[]> = new EventEmitter<File[]>();

    @HostListener('dragover', ['$event'])
    public onDragOver(evt : DragEvent) {
        evt.preventDefault();
        evt.stopPropagation();

        evt.dataTransfer.dropEffect = "copy";

        this.visible = true;
    }

    @HostListener('dragleave', ['$event'])
    public onDragLeave(evt : DragEvent) {
        evt.preventDefault();
        evt.stopPropagation();

        this.visible = false;
    }

    @HostListener('drop', ['$event'])
    public onDrop(evt : DragEvent) {
        evt.preventDefault();
        evt.stopPropagation();

        const files = evt.dataTransfer.files;

        if (files != null && files.length > 0) {
            const array: File[] = [];

            for (var i = 0; i < files.length; i++) {
                array.push(files[i]);
            }

            this.files.emit(array);
        }

        this.visible = false;
    }
}

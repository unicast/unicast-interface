import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { GroupedSubtitlesSchema, MediaRecordSchema, SubtitleSchema, TvEpisodeMediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { SubtitleSyncAlign, SubtitleType } from 'components/services/serverApis/mediaServers/subtitleServerApi';
import { WaitFor } from 'components/common/helpers/asyncClick.directive';
import { TitleDropdownAction } from 'components/common/section/section.component';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { of } from 'rxjs/observable/of';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';


@Component( {
    selector: 'uc-subtitles',
    templateUrl: './subtitles.template.html',
    styleUrls: [ './subtitles.styles.less' ]
} )
export class SubtitlesComponent extends BaseUnicastComponent {
    public readonly SubtitleSyncAlign: typeof SubtitleSyncAlign = SubtitleSyncAlign;
    public readonly SubtitleType: typeof SubtitleType = SubtitleType;

    @Input() nextMedia: MediaRecordSchema;
    @Input() media: MediaRecordSchema;
    @Input() mediaList: SubtitlesGroup[];
    @Input() isOpened: boolean;
    @Input() hideTitle: boolean = false;
    @Input() bodyOnly: boolean = false;
    @Output() isOpenedChange: EventEmitter<boolean>;

    public lastVerifiedSubtitle : string = null;

    private _serverApi: ServerApiManager;

    editing : string = '';
    editingName: string = '';

    titleDropdowns: Array<TitleDropdownAction>;

    episodeOffset: number = 0;

    get singleMedia () {
        return this.media == null || (this.media.kind != MediaKind.TvShow && this.media.kind != MediaKind.TvSeason);
    }

    constructor ( serverApi : ServerApiManager ) {
        super();
        this._serverApi = serverApi;

        this.isOpened = false;
        this.isOpenedChange = new EventEmitter<boolean>();

        this.reload();
    }

    ngOnInit() {
        super.ngOnInit();
    }

    ngOnChanges(change){
        if (this.isOpened && (change.isOpened || change.media) ){
            this.reload(/* clean: */ change.media != null);
        }
    }

    async reload (clean : boolean = false) {
        if (this.media == null) {
            this.mediaList = [];
        } else {
            if (this.singleMedia) {
                await this.reloadSingle(clean);
            } else {
                await this.reloadMultiple(clean);
            }
        }

        let subtitleApi = this._serverApi.getMedia().getSubtitleApi();
        let lang = (subtitleApi.getLanguages() || ['por'])[0];
        this.titleDropdowns = [{
            value: { value: lang, text: 'Language: ' + lang },
            options: ['por', 'pob', 'eng'].map(e => ({value: e, text: e})),
            hasNone: false
        }];
    }

    protected async reloadSingle(clean : boolean = false) {
        if (clean || this.mediaList == null || this.mediaList.length > 1) {
            this.mediaList = [];
        }

        if (clean || this.mediaList.length == 0) {
            this.mediaList.push(new SubtitlesGroup(this.media));
        }

        try {
            this.setLoading(true);

            await Promise.all([
                this.reloadSingleLocal(),
                this.reloadSingleRemote(),
            ]);
        } finally {
            this.setLoading(false);
        }

        if (this.nextMedia != null) {
            this.loadSubtitlesSingle(this.nextMedia, SubtitleType.Remote);
        }
    }

    protected async reloadSingleLocal() {
        try {
            this.mediaList[0].local = await this.loadSubtitlesSingle(this.media, SubtitleType.Local);
        } finally {
            this.setLoading(false, SubtitleType.Local);
        }
    }

    protected async reloadSingleRemote() {
        try {
            this.mediaList[0].remote = await this.loadSubtitlesSingle(this.media, SubtitleType.Remote);
        } finally {
            this.setLoading(false, SubtitleType.Remote);
        }
    }

    protected async reloadMultiple(clean : boolean = false) {
        if (clean) {
            this.mediaList = [new SubtitlesGroup(this.media)];
        }

        this.setLoading(true);

        try {
            const subtitles = await this.loadSubtitlesMultiple(this.media);

            this.updateMediaList(subtitles);
        } finally {
            this.setLoading(false);
        }
    }

    protected loadSubtitlesSingle(media : MediaRecordSchema, type : SubtitleType) {
        let subtitleApi = this._serverApi.getMedia().getSubtitleApi();

        const observable = subtitleApi.list(media.kind, media.id, type, { episodeOffset: this.episodeOffset });

        return observable.toPromise();
    }

    protected loadSubtitlesMultiple(media : MediaRecordSchema) {
        let subtitleApi = this._serverApi.getMedia().getSubtitleApi();

        const observable = subtitleApi.listGrouped(media.kind, media.id, SubtitleType.Local, { episodeOffset: this.episodeOffset });

        return observable.toPromise();
    }

    protected setLoading(loading : boolean, type : SubtitleType = null) {
        if (this.mediaList != null) {
            for (const group of this.mediaList) {
                if (type == null || type == SubtitleType.Local) {
                    group.localLoading = loading;
                }

                if (type == null || type == SubtitleType.Remote) {
                    group.remoteLoading = loading;
                }
            }
        }
    }

    protected updateMediaList(subtitles : GroupedSubtitlesSchema[]) {
        subtitles = [...subtitles];

        subtitles.sort((a, b) => {
            if (a.media.kind === MediaKind.TvEpisode && b.media.kind === MediaKind.TvEpisode) {
                const epA = a.media as TvEpisodeMediaRecordSchema;
                const epB = b.media as TvEpisodeMediaRecordSchema;

                if (epA.seasonNumber == epB.seasonNumber) {
                    return epA.number - epB.number;
                } else {
                    return epA.seasonNumber - epB.seasonNumber;
                }
            } else {
                return 0;
            }
        });

        this.mediaList = subtitles.map(group => new SubtitlesGroup(group.media, group.subtitles));
    }

    async changeEpisodeOffset (event: WaitFor, offset: number ) {
        this.episodeOffset += offset;

        await this.reload(true);

        event.release();
    }

    async changeLanguage(event: [number, string]) {
        let subtitleApi = this._serverApi.getMedia().getSubtitleApi();
        subtitleApi.setLanguages(event[1]);

        await this.reload(false);
    }

    inputName(entry: SubtitleSchema){
        this.editing = entry.id;
        this.editingName = entry.releaseName + '';
    }

    detectKey(event: KeyboardEvent, entry: SubtitleSchema) {
        switch (event.keyCode){
            case 13: //Enter
                const oldName = entry.releaseName;
                entry.releaseName = this.editingName;
                let subtitleApi = this._serverApi.getMedia().getSubtitleApi();
                subtitleApi.rename(this.media.kind, this.media.id, entry).catch((e, o) => {
                    entry.releaseName = oldName;
                    return of(entry);
                }).subscribe();
            case 27: //Esc
                this.editing = null;
                break;
        }
    }

    titleClick(){
        this.isOpened = false;
        this.isOpenedChange.emit(this.isOpened);
    }

    add(event: WaitFor, group: SubtitlesGroup, entry: SubtitleSchema){
        let subtitleApi = this._serverApi.getMedia().getSubtitleApi();
        let req = subtitleApi.add(group.media.kind, group.media.id, [entry]);
        req.subscribe((data) => {
            group.local = data;
        });
        event.waitFor(req);
    }

    validate(event: WaitFor, group: SubtitlesGroup, entry: SubtitleSchema, type: SubtitleType){
        this.lastVerifiedSubtitle = entry.id;
        let subtitleApi = this._serverApi.getMedia().getSubtitleApi();
        let req = subtitleApi.validate(group.media.kind, group.media.id, entry.id, type, {episodeOffset: this.episodeOffset});
        req.subscribe( (data) => {
            console.info("Validation ended");
        });
        event.waitFor(req);
    }

    synchronize(event: WaitFor, group: SubtitlesGroup, entry: SubtitleSchema, type: SubtitleType, mode: SubtitleSyncAlign){
        let subtitleApi = this._serverApi.getMedia().getSubtitleApi();
        let req = subtitleApi.synchronize(group.media.kind, group.media.id, entry.id, type, mode, {episodeOffset: this.episodeOffset});
        req.subscribe(data => {
            console.log(data.uri);
            window.open(data.uri, "_self");
        });
        event.waitFor(req);
    }

    remove(event: WaitFor, group: SubtitlesGroup, entry: SubtitleSchema){
        let subtitleApi = this._serverApi.getMedia().getSubtitleApi();
        let req = subtitleApi.remove(group.media.kind, group.media.id, entry.id)
        req.subscribe((data) => {
            group.local = data;
        });
        event.waitFor(req);
    }

    async upload(files : File[]) {
        let subtitleApi = this._serverApi.getMedia().getSubtitleApi();

        const entries = await this.convertFileListToSubtitles(files);

        if (this.singleMedia) {
            let req = subtitleApi.add(this.media.kind, this.media.id, entries);
            this.mediaList[1].local = await req.toPromise();
        } else {
            let req = subtitleApi.add(this.media.kind, this.media.id, entries, true);
            this.updateMediaList(await req.toPromise())
        }
    }

    protected getBase64FileContents(file : Blob) {
        return new Promise<string>((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(this.parseDataUrl(reader.result as string));
            reader.onerror = error => reject(error);
        });
    }

    protected parseDataUrl(url : string) {
        return url.split(',')[1];
    }

    protected async convertFileListToSubtitles(files : File[]) : Promise<UploadedSubtitle[]> {
        return await Promise.all(files.map(file => this.convertFileToSubtitle(file)));
    }

    protected async convertFileToSubtitle(file : File) : Promise<UploadedSubtitle> {
        return {
            id: null,
            releaseName: file.name,
            fileName: file.name,
            contents: await this.getBase64FileContents(file),
            downloads: 0,
            encoding: null,
            format: null,
            language: null,
            provider: 'uploadedsubtitles',
            publishedAt: new Date(),
            score: 1,
        };
    }
}

export class SubtitlesGroup {
    public media : MediaRecordSchema;

    public local : Array<SubtitleSchema> = [];

    public localLoading : boolean = false;

    public remote : Array<SubtitleSchema> = [];

    public remoteLoading : boolean = false;

    public constructor(media : MediaRecordSchema, local : Array<SubtitleSchema> = []) {
        this.media = media;
        this.local = local;
    }
}

export interface UploadedSubtitle {
    id: string;
    releaseName : string;
    encoding : string;
    format : string;
    language : string;
    publishedAt : Date;
    downloads : number;
    provider : string;
    score : number;
    fileName : string;
    contents : string;
}

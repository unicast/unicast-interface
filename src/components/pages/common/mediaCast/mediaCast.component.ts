import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { MediaRecordSchema, PersonRecordSchema } from 'components/schemas/mediaRecord.schema';
import { MediaServerApi } from 'components/services/serverApis/mediaServers/mediaServerApi';
import { StandardMediaQueries } from 'components/services/mediaQueries';

@Component( {
    selector: 'uc-media-cast',
    templateUrl: './mediaCast.template.html',
    styleUrls: [ './mediaCast.styles.less' ]
} )
export class MediaCastComponent {
    @Input() media: MediaRecordSchema;
    @Input() isOpened: boolean;
    @Output() isOpenedChange: EventEmitter<boolean>;

    public lastVerifiedSubtitle : string = null;

    private _serverApi: ServerApiManager;
    public mediaQueries: StandardMediaQueries;

    mainCastLength : number = 12;

    showAll : boolean = false;

    cast : PersonRecordSchema[] = [];

    castAll : PersonRecordSchema[] = [];

    castLoading : boolean = false;

    castLoaded : boolean = false;

    constructor ( serverApi : ServerApiManager, mediaQueries: StandardMediaQueries ) {
        this._serverApi = serverApi;
        this.mediaQueries = mediaQueries;

        this.isOpened = false;

        this.isOpenedChange = new EventEmitter<boolean>();
    }

    titleClick () {
        this.isOpened = false;
        this.isOpenedChange.emit( this.isOpened );
    }

    ngOnChanges ( change ) {
        if ( change.media ) {
            this.castLoaded = false;
        }

        if ( this.isOpened && ( change.isOpened || change.media ) && !this.castLoaded ) {
            let mediaApi = this._serverApi.getMedia();

            this.loadAll( mediaApi, this.media );
        }
    }

    private async loadAll ( mediaApi : MediaServerApi, media : MediaRecordSchema ) {
        this.castLoading = true;

        try {
            this.castAll = await mediaApi.getCast( media.kind, media.id ).toPromise();

            this.cast = this.showAll ? this.castAll : this.castAll.slice( 0, this.mainCastLength );

            this.castLoaded = true;
        } finally {
            this.castLoading = false;
        }
    }

    toggleShowAll () {
        this.showAll = !this.showAll;

        this.cast = this.showAll ? this.castAll : this.castAll.slice( 0, this.mainCastLength );
    }
}
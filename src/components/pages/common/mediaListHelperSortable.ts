import { MediaListHelper } from 'components/pages/common/mediaListHelper';
import { SortFilter } from 'components/pages/common/sidemenuFilters/sort/sidemenuFiltersSort.component';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { ListChangeInformation } from 'components/pages/common/listSelector/listSelector.component';
import { SelectRankButtonAction, UnselectRankButtonAction } from 'components/pages/common/buttonActions/selectRankButtonAction';
import { MoveToTopRankButtonAction, MoveToLeftRankButtonAction, MoveToRightRankButtonAction, MoveToBottomRankButtonAction } from 'components/pages/common/buttonActions/moveToRankButtonAction';

export abstract class MediaListHelperSortable<T extends { sort ?: SortFilter }> extends MediaListHelper<T> {
    private defaultSortField: string;

    ngOnInit() {
        super.ngOnInit();

        this._subscriptions.push( this.sidemenuService.presetChanges.subscribe( presetId => {
            const sortField = this.sidemenuService.filters.sort.field as keyof MediaRecordSchema;
            if (sortField == '$userRank' && !presetId) {
                this.sidemenuService.filters.sort.field = this.defaultSortField;
                this.sidemenuService.filters.sort.list = undefined;
                this.sidemenuService.publishFilters( this.sidemenuService.filters );
            }
        }));

        this._subscriptions.push( this.sidemenuService.filtersChanges.subscribe( e => {
            this.checkIsSelectedByUserRank();
        }));
    }

    ngAfterViewInit() {
        this.defaultSortField = this.sidemenuService.filters.sort.field;
    }

    selectCollection(collection: ListChangeInformation) {
        super.selectCollection(collection);
        if (!collection.id){
            this.checkIsSelectedByUserRank(); //When removing, check if UserRank isn't active
        }
    }

    checkIsSelectedByUserRank() {
        const sortField = this.sidemenuService.filters.sort.field as keyof MediaRecordSchema;
        const presetId = this.sidemenuService.currentPresetId;
        if (sortField == '$userRank' && presetId) {
            if (this.actionButtons == this.defaultButtons) {
                let newList = [];
                this.actionButtons = [new SelectRankButtonAction(this.serverApi, newList),
                                        new UnselectRankButtonAction(this.serverApi, newList),
                                        new MoveToTopRankButtonAction(this.serverApi, presetId, this),
                                        new MoveToBottomRankButtonAction(this.serverApi, presetId, this),
                                        new MoveToLeftRankButtonAction(this.serverApi, presetId, newList, this),
                                        new MoveToRightRankButtonAction(this.serverApi, presetId, newList, this)];
                this.tags = [ '$userRank' ];
            }
        } else if ( this.actionButtons.length > 0 && this.actionButtons[0] instanceof SelectRankButtonAction ) {
            //Only remove it when the user rank was selected before
            this.actionButtons = this.defaultButtons;
            this.tags = undefined;
        }
    }
}
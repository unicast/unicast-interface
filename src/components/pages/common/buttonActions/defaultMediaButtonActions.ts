import { Injectable } from '@angular/core';

import { MediaButtonAction } from 'components/pages/common/buttonActions/buttonAction';
import { PlayButtonAction } from 'components/pages/common/buttonActions/playButtonAction';
import { QueueButtonAction } from 'components/pages/common/buttonActions/queueButtonAction';
import { SubtitlesButtonAction } from 'components/pages/common/buttonActions/subtitlesButtonAction';
import { WatchButtonAction, UnwatchButtonAction } from 'components/pages/common/buttonActions/watchButtonAction';


@Injectable()
export class DefaultMediaButtonActions {

    public readonly play: PlayButtonAction;
    public readonly queue: QueueButtonAction;
    public readonly subtitles: SubtitlesButtonAction;
    public readonly watched: WatchButtonAction;
    public readonly unwatched: UnwatchButtonAction;

    private defaultButtons: Array<MediaButtonAction>;

    constructor(play: PlayButtonAction, queue: QueueButtonAction, subtitles: SubtitlesButtonAction, watched: WatchButtonAction, unwatched: UnwatchButtonAction){
        this.play = play;
        this.queue = queue;
        this.subtitles = subtitles;
        this.watched = watched;
        this.unwatched = unwatched;
        this.defaultButtons = [this.play, this.queue, this.subtitles, this.watched, this.unwatched];
    }

    getAllActions(): Array<MediaButtonAction> {
        return this.defaultButtons;
    }
}
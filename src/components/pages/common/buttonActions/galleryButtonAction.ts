import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { MediaButtonAction } from 'components/pages/common/buttonActions/buttonAction';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';

@Injectable()
export class GalleryButtonAction extends MediaButtonAction {

    public key: string = 'gallery';
    public label: string = 'BUTTONS.ACTIONS.GALLERY';
    public theme: string = undefined;
    public icon: string = 'picture-o';
    public importance: number = 4;

    constructor(serverApi: ServerApiManager){
        super(serverApi);
    }

    execute(media: MediaRecordSchema, ...args: any[]): Observable<any>  {
        return;
    }

    /*test(media: MediaRecordSchema): boolean{

        switch (media.kind){
            case MediaKind.Movie:
            case MediaKind.TvEpisode:
                return true;
            default:
                return false;
        }
    }*/
}
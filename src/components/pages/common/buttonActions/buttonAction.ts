import { Observable } from 'rxjs/Observable';

import { PlayableMediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';

export abstract class ButtonAction<T>{
    public key: string;
    public label: string;
    public theme? : string;
    public icon? : string;
    public importance? : number;
    public abstract execute(obj: T, ...args: Array<any>): Observable<any>;
    protected abstract test(obj: T, ...args: Array<any>): boolean;
}

export abstract class ServerApiButtonAction<T> extends ButtonAction<T> {
    protected _serverApi: ServerApiManager;

    constructor(serverApi: ServerApiManager){
        super();
        this._serverApi = serverApi;
    }

    getServerApi(): ServerApiManager {
        return this._serverApi;
    }
}

export abstract class MediaButtonAction extends ServerApiButtonAction<PlayableMediaRecordSchema> {

    public abstract execute(media: PlayableMediaRecordSchema, ...args: Array<any>): Observable<any>;

    public test(media: PlayableMediaRecordSchema): boolean {
        return true;
    }
}
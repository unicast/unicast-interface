import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { MediaButtonAction } from 'components/pages/common/buttonActions/buttonAction';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { SingularSubject } from 'components/common/helpers/singularSubject';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';

export type SubtitlesActionMethod = (media: MediaRecordSchema) => Observable<any>;

@Injectable()
export class SubtitlesButtonAction extends MediaButtonAction {

    private _action: SubtitlesActionMethod;

    public key: string = 'subtitles';
    public label: string = 'BUTTONS.ACTIONS.SUBTITLES';
    public theme: string = undefined;
    public icon: string = 'creative-commons';
    public importance: number = 4;

    constructor(serverApi: ServerApiManager){
        super(serverApi);
        this.injectAction(null);
    }

    injectAction(action?: SubtitlesActionMethod){
        if (action){
            this._action = action;
        } else {
            this._action = (media) => {return SingularSubject.oneValue(false);}
        }
    }

    execute(media: MediaRecordSchema, ...args: any[]): Observable<any>  {
        return this._action(media);
    }

    test(media: MediaRecordSchema): boolean{
        switch (media.kind){
            case MediaKind.Movie:
            case MediaKind.TvEpisode:
                return true;
            default:
                return false;
        }
    }


    static createDynamicProvider(){
        return {
                 provide: SubtitlesButtonAction,
                 useFactory: (serverApi: ServerApiManager) => {return new SubtitlesButtonAction(serverApi)},
                 deps: [ServerApiManager]
                }
    }
}
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { MediaButtonAction } from 'components/pages/common/buttonActions/buttonAction';
import { PlayableMediaRecordSchema } from 'components/schemas/mediaRecord.schema';


@Injectable()
export class QueueButtonAction extends MediaButtonAction {

    public key: string = 'queue';
    public label: string = 'BUTTONS.ACTIONS.QUEUE';
    public theme: string = 'info';
    public icon: string = 'plus-circle';
    public importance: number = 2;

    constructor(serverApi: ServerApiManager){
        super(serverApi);
    }

    execute(media: PlayableMediaRecordSchema, ...args: any[]): Observable<any>  {
        return this._serverApi.getPlayer().getDeviceApi().getPlaylistApi().getItemsApi().queue( media );
    }

}
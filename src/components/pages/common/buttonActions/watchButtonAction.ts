import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { MediaButtonAction } from 'components/pages/common/buttonActions/buttonAction';
import { PlayableMediaRecordSchema } from 'components/schemas/mediaRecord.schema';

export abstract class AbstractWatchButtonAction extends MediaButtonAction {
    private _value: boolean;

    public key: string = 'watched';
    public theme: string = 'warning';
    public importance: number = 4;

    constructor(serverApi: ServerApiManager, value: boolean){
        super(serverApi);
        this._value = value;
    }

    execute(media: PlayableMediaRecordSchema, ...args: any[]): Observable<any> {
        let obs = this._serverApi.getMedia().watched(media.kind, media.id, !this._value);
        obs.subscribe((data: PlayableMediaRecordSchema) => {
            media.watched = data.watched;
        });
        return obs;
    }

    test(media: PlayableMediaRecordSchema): boolean {
        return !!media.watched == this._value;
    }

}

@Injectable()
export class WatchButtonAction extends AbstractWatchButtonAction {

    public label: string = 'BUTTONS.ACTIONS.WATCHED';
    public theme: string = 'warning';
    public icon: string = 'eye';

    constructor(serverApi: ServerApiManager){
        super(serverApi, false);
    }


}

@Injectable()
export class UnwatchButtonAction extends AbstractWatchButtonAction {

    public label: string = 'BUTTONS.ACTIONS.NOT_WATCHED';
    public theme: string = 'warning';
    public icon: string = 'eye-slash';

    constructor(serverApi: ServerApiManager){
        super(serverApi, true);
    }
}
import { Observable } from 'rxjs/Observable';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { MediaButtonAction } from 'components/pages/common/buttonActions/buttonAction';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { RankDirection } from 'components/services/serverApis/mediaServers/userRankServerApi';

export type ObjectWithList = { data : Array<MediaRecordSchema> };

class MoveToRankButtonAction extends MediaButtonAction {
    protected listId: string;
    protected list: Array<MediaRecordSchema>;
    protected mediaList: ObjectWithList;
    protected direction: RankDirection;

    public key: string = 'move';
    public theme: string = 'white';
    public importance: number = 3;

    constructor(serverApi: ServerApiManager, listId: string, list: Array<MediaRecordSchema>, mediaList: ObjectWithList){
        super(serverApi);
        this.listId = listId;
        this.list = list;
        this.mediaList = mediaList;
    }

    private reverseDirection(direction: RankDirection): RankDirection {
        return direction == RankDirection.After ? RankDirection.Before : RankDirection.After;
    }

    execute(media: MediaRecordSchema, ...args: any[]): Observable<any>{
        const isDesc = this.mediaList.data.length > 1
                    && this.mediaList.data[0].$userRank <= this.mediaList.data[1].$userRank
                    && this.mediaList.data[this.mediaList.data.length - 1].$userRank > 0;
        let obs = this._serverApi.getMedia().getUserRankApi().setRankDirection(isDesc ? this.reverseDirection(this.direction) : this.direction, this.listId , media, this.list);
        obs.subscribe(e => {
            //Remove the ones being moved from the main list
            this.list.forEach(e => this.mediaList.data.splice(this.mediaList.data.indexOf(e), 1));

            //Find auxiliary values to calculate userRank
            const firstZero = this.mediaList.data.findIndex(e => e.$userRank == 0);
            let startPosition = 0;
            let finishPostion = this.list.length;
            let increment = 1;
            if (isDesc) { //Is on Desc. Revert the cycle
                startPosition = finishPostion = this.mediaList.data.length - 1;
                for(let i = startPosition; this.mediaList.data[i].$userRank > 0 && i >= 0; i--, finishPostion = i );
                //Because of the added ones
                startPosition += this.list.length;
                increment = -1;
            } else {
                finishPostion = (firstZero == -1 ? this.mediaList.data.length : firstZero) + this.list.length;
            }
            let maxValue = Math.abs(finishPostion - startPosition);

            //Find the current position to put them
            let currentIndex = !media
                        ? ( this.direction == RankDirection.After ? (isDesc ? finishPostion + 1 : 0) : (isDesc ? startPosition : firstZero) )
                        : Math.max(this.mediaList.data.indexOf(media), isDesc ? finishPostion + 1 : 0) + ( this.direction == RankDirection.After ? 0 : 1 );

            //Insert the list on the new position
            this.mediaList.data.splice(currentIndex, 0, ...this.list);

            //Update all the user rank values
            for (let i = startPosition; (finishPostion - i) * increment > 0; i += increment) {
                this.mediaList.data[i] = {...this.mediaList.data[i], $userRank: maxValue--};
            }
            //Empty the selected list
            this.list.splice(0, this.list.length);

        });
        return obs;
    }

    test(media: MediaRecordSchema): boolean{
        return this.list.length > 0 && media.$userRank > 0 && this.list.indexOf(media) == -1;
    }

}

export class MoveToLeftRankButtonAction extends MoveToRankButtonAction {

    public key: string = 'moveLeft';
    public label: string = 'BUTTONS.ACTIONS.MOVELEFT';
    public icon: string = 'chevron-left';

    constructor(serverApi: ServerApiManager, listId: string, list: Array<MediaRecordSchema>, mediaList: ObjectWithList){
        super(serverApi, listId, list, mediaList);
        this.direction = RankDirection.After;
    }

}

export class MoveToRightRankButtonAction extends MoveToRankButtonAction {
    public key: string = 'moveRight';
    public label: string = 'BUTTONS.ACTIONS.MOVERIGHT';
    public icon: string = 'chevron-right';

    constructor(serverApi: ServerApiManager, listId: string, list: Array<MediaRecordSchema>, mediaList: ObjectWithList){
        super(serverApi, listId, list, mediaList);
        this.direction = RankDirection.Before;
    }
}

export class MoveToTopRankButtonAction extends MoveToRankButtonAction {
    public key: string = 'moveTop';
    public label: string = 'BUTTONS.ACTIONS.MOVETOP';
    public icon: string = 'level-up';

    constructor(serverApi: ServerApiManager, listId: string, mediaList: ObjectWithList){
        super(serverApi, listId, [], mediaList);
        this.direction = RankDirection.After;
    }

    execute(media: MediaRecordSchema, ...args: any[]): Observable<any>{
        this.list = [ media ];
        return super.execute(undefined, ...args);
    }

    test(media: MediaRecordSchema): boolean{
        return true;
    }
}

export class MoveToBottomRankButtonAction extends MoveToRankButtonAction {
    public key: string = 'moveBottom';
    public label: string = 'BUTTONS.ACTIONS.MOVEBOTTOM';
    public icon: string = 'level-down';

    constructor(serverApi: ServerApiManager, listId: string, mediaList: ObjectWithList){
        super(serverApi, listId, [], mediaList);
        this.direction = RankDirection.Before;
    }

    execute(media: MediaRecordSchema, ...args: any[]): Observable<any>{
        this.list = [ media ];
        return super.execute(undefined, ...args);
    }

    test(media: MediaRecordSchema): boolean{
        return true;
    }
}
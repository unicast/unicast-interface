import { Observable } from 'rxjs/Observable';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { MediaButtonAction } from 'components/pages/common/buttonActions/buttonAction';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';

export class SelectCollectionButtonAction extends MediaButtonAction {
    protected _collectionId: string;
    protected _collectionName: string;

    public key: string = 'select';
    public label: string = 'BUTTONS.ACTIONS.ADD';
    public theme: string = 'success';
    public icon: string = 'check';
    public importance: number = 1;

    constructor(serverApi: ServerApiManager, collectionId: string, collectionName: string){
        super(serverApi);
        this._collectionId = collectionId;
        this._collectionName = collectionName;
    }

    execute(media: MediaRecordSchema, ...args: any[]): Observable<any>{
        let obs = this._serverApi.getMedia().getCollectionApi().insertMedia(media.kind, this._collectionId, media.id);
        obs.subscribe(( nmedia ) => {
            media.collections.push( {id: this._collectionId, title: this._collectionName} );
        });
        return obs;
    }

    test(media: MediaRecordSchema): boolean{
        return media.collections.findIndex((value) => this._collectionId == value.id) == -1;
    }

}

export class UnselectCollectionButtonAction extends SelectCollectionButtonAction {
    public label: string = 'BUTTONS.ACTIONS.REMOVE';
    public theme: string = 'danger';
    public icon: string = 'times';

    execute(media: MediaRecordSchema, ...args: any[]): Observable<any>{
        let obs = this._serverApi.getMedia().getCollectionApi().removeMedia(media.kind, this._collectionId, media.id);
        obs.subscribe(( nmedia ) => {
            media.collections = media.collections.filter((value) => this._collectionId != value.id);
        });
        return obs;
    }

    test(media: MediaRecordSchema): boolean{
        return !super.test(media);
    }
}
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { MediaButtonAction } from 'components/pages/common/buttonActions/buttonAction';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';


export class SelectRankButtonAction extends MediaButtonAction {
    protected list: Array<MediaRecordSchema>;

    public key: string = 'select';
    public label: string = 'BUTTONS.ACTIONS.SELECT';
    public theme: string = 'success';
    public icon: string = 'check';
    public importance: number = 1;

    constructor(serverApi: ServerApiManager, list: Array<MediaRecordSchema>){
        super(serverApi);
        this.list = list;
    }

    protected getIndex(media: MediaRecordSchema) {
        return this.list.findIndex(e => e.id == media.id);
    }

    execute(media: MediaRecordSchema, ...args: any[]): Observable<any>{
        const index = this.getIndex(media);
        let result = false;
        if (index == -1) {
            this.list.push(media);
            result = true;
        }

        return of(result);
    }

    test(media: MediaRecordSchema): boolean{
        return this.getIndex(media) == -1;
    }

}

export class UnselectRankButtonAction extends SelectRankButtonAction {
    public label: string = 'BUTTONS.ACTIONS.UNSELECT';
    public theme: string = 'danger';
    public icon: string = 'times';

    execute(media: MediaRecordSchema, ...args: any[]): Observable<any>{
        const index = this.getIndex(media);
        let result = false;
        if (index > -1) {
            this.list.splice(index, 1);
            result = true;
        }
        return of(result);
    }

    test(media: MediaRecordSchema): boolean{
        return !super.test(media);
    }
}
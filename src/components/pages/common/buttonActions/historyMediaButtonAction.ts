import { Observable } from 'rxjs/Observable';

import { SingularSubject } from 'components/common/helpers/singularSubject';
import { HistoryRecordSchema } from 'components/schemas/mediaRecord.schema';
import { ServerApiButtonAction, MediaButtonAction } from 'components/pages/common/buttonActions/buttonAction';
import { InputModalComponent, InputButtonClick } from 'components/common/modal/inputModal/inputModal.component';

export class HistoryMediaButtonAction extends ServerApiButtonAction<HistoryRecordSchema>{

    private component: {modal: InputModalComponent<string>};
    private action: MediaButtonAction;

    constructor(buttonAction: MediaButtonAction, component: {modal: InputModalComponent<string>}){
        super(buttonAction.getServerApi());
        //Inherit the properties
        Object.assign(this, buttonAction);
        this.action = buttonAction;
        this.component = component;
    }

    execute(session: HistoryRecordSchema, ...args: any[]): Observable<any>  {
        if (session.id == '_blank'){
            //Preserve the original empty media
            session = {...session, record: {...session.record}};
            this.component.modal.show();
            //The merge+first will ensure that the other observable will be unscribed someone's click on any
            let value = this.component.modal.confirm.merge(this.component.modal.cancel).first();
            value.subscribe( (e: InputButtonClick<string>) => {
                switch(e.type){
                    case 'ok':
                        let text: string = e.value;
                        if (!text) {
                            return SingularSubject.oneValue(false);
                        }
                        text = text.trim();
                        switch(text.substring(0, 1)){
                            case '{':
                                text = '[' + text + ']'
                            case '[':
                                session.record.sources = JSON.parse(text);
                                break;
                            default:
                                session.record.sources = [{ id: text }];
                        }
                        return this.action.execute(session.record, ...args);
                }
            });
            return value;
        } else {
            return this.action.execute(session.record, ...args);
        }

    }

    public test(media: HistoryRecordSchema): boolean {
        return this.action.test(media.record);
    }
}
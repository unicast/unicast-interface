import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { MediaButtonAction } from 'components/pages/common/buttonActions/buttonAction';
import { PlayableMediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';


@Injectable()
export class PlayButtonAction extends MediaButtonAction {

    public key: string = 'play';
    public label: string = 'BUTTONS.ACTIONS.PLAY';
    public theme: string = 'success';
    public icon: string = 'play-circle-o';
    public importance: number = 1;

    constructor(serverApi: ServerApiManager){
        super(serverApi);
    }

    execute(media: PlayableMediaRecordSchema, ...args: any[]): Observable<any>  {
        if (media.kind == MediaKind.Custom){
            return this._serverApi.getPlayer().getDeviceApi().play( media.kind, media.id, { sources: media.sources } );
        } else {
            return this._serverApi.getPlayer().getDeviceApi().play( media.kind, media.id );
        }

    }

}
import { Observable } from 'rxjs/Observable';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { ServerApiButtonAction } from 'components/pages/common/buttonActions/buttonAction';
import { HistoryRecordSchema } from 'components/schemas/mediaRecord.schema';

export class RemoveHistoryButtonAction extends ServerApiButtonAction<HistoryRecordSchema> {
    public key: string = 'remove';
    public label: string = 'BUTTONS.ACTIONS.REMOVE';
    public theme: string = 'danger';
    public icon: string = 'times';
    public importance: number = 1;

    constructor(serverApi: ServerApiManager){
        super(serverApi);
    }

    execute(session: HistoryRecordSchema, ...args: any[]): Observable<any>{
        return this._serverApi.getMedia().getHistoryApi().remove(session.id);
    }

    test(session: HistoryRecordSchema): boolean{
        return session.id != '_blank';
    }

}
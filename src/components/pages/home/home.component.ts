import { Component } from '@angular/core';
import { TopbarManager } from '../../services/topbarManager';

@Component( {
    selector: 'uc-home-page',
    templateUrl: './home.template.html',
    styleUrls: [ './home.styles.less' ]
} )
export class HomePageComponent {
    constructor ( topbar : TopbarManager ) {
        topbar.clearTitle();
    }
}
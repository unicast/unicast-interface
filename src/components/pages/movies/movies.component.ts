import { Component } from '@angular/core';
import { PageTitle } from 'components/common/services/pageTitle';

import { TopbarManager } from '../../services/topbarManager';

@Component( {
    selector: 'uc-movies',
    templateUrl: './movies.template.html',
    styleUrls: [ './movies.styles.less' ]
} )
export class MoviesComponent {

    constructor ( topbar : TopbarManager, htmlTitle: PageTitle) {
        let title = 'Movies'
        topbar.setTitle( title );
        htmlTitle.setTitle( title );
    }

}

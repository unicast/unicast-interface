import { Component, Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { MoviesListSidemenuComponent } from './sidemenu/sidemenu.component';
import { TopbarManager } from 'components/services/topbarManager';
import { PageTitle } from 'components/common/services/pageTitle';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { StandardMediaQueries } from 'components/services/mediaQueries';
import { FiltersSidemenuService } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { SessionCache } from 'components/common/services/sessionCache';
import { MediaListHelperSortable } from 'components/pages/common/mediaListHelperSortable';
import { SidebarManager } from 'components/services/sidebarManager';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { DefaultMediaButtonActions } from 'components/pages/common/buttonActions/defaultMediaButtonActions';
import { CollectionSchema, MediaRecordSchema, MovieMediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { SingularSubject } from 'components/common/helpers/singularSubject';
import { FiltersMediaSidemenuService, MediaListQueryFilters } from 'components/pages/common/sidemenuFilters/filtersMediaSidemenu.service';
import { DelayedExecutions } from 'components/common/services/delayedExecutions';
import { ApplicationContextManager } from 'components/services/applicationContextManager';

function flattenCollections ( flattenArray: (CollectionSchema & {identation?: number})[], treeArray: CollectionSchema[], level: number = 0 ) {
    for (const item of treeArray) {
        flattenArray.push( {
            ...item,
            identation: level,
        } );

        if (item.children != null && item.children.length > 0) {
            flattenCollections(flattenArray, item.children, level + 1);
        }
    }

    return flattenArray;
}

@Component( {
    selector: 'uc-movies-list',
    templateUrl: './moviesList.template.html',
    styleUrls: [ './moviesList.styles.less' ],
    providers: [ FiltersMediaSidemenuService.forRoot('movie'), DefaultMediaButtonActions ]
} )
export class MoviesListComponent extends MediaListHelperSortable<MediaListQueryFilters> {
    public activatedRoute: ActivatedRoute;
    public router: Router;
    public selectedMovie : string;

    constructor ( router: Router, activatedRoute: ActivatedRoute, injector : Injector, cache : SessionCache, sidemenuService : FiltersSidemenuService<MediaListQueryFilters>, sidebar : SidebarManager,
                  serverApi:  ServerApiManager, topbar: TopbarManager, htmlTitle: PageTitle, mediaQueries : StandardMediaQueries,
                  defaultButtons: DefaultMediaButtonActions, delayedExecutions: DelayedExecutions, applicationContext: ApplicationContextManager ) {
        super([ MoviesListComponent.name, 'movies' ] , sidebar, cache, sidemenuService, serverApi, topbar, htmlTitle, mediaQueries, defaultButtons, delayedExecutions, applicationContext);
        this.setSecondaryMenu( MoviesListSidemenuComponent, injector );

        this.step = 24;
        this.activatedRoute = activatedRoute;
        this.router = router;
        defaultButtons.subtitles.injectAction((media) => this.toggleSubtitles(media));

        this.serverApi.getMedia().getCollectionApi().list('movie', { tree: true }).subscribe( (data) => {
            this.collections = flattenCollections([], data);
        });
    }

    public ngOnInit () {
        super.ngOnInit();

        this._subscriptions.push( this.activatedRoute.params.subscribe( async params => {
            if ( typeof params.query === 'string' ) {
                this.serverApi.getMedia().filter( params.query, true );
            }
        } ) );
    }

    toggleSubtitles ( media: MediaRecordSchema ): Observable<any>{
        this.router.navigate(['movies', media.id, {subtitles: 1}])
        return SingularSubject.oneValue(media);
    }

    play ( movie : MovieMediaRecordSchema ) : Observable<any> {
        return this.serverApi.getPlayer().getDeviceApi().play( MediaKind.Movie, movie.id );
    }

    queue ( movie : MovieMediaRecordSchema ) : Observable<any> {
        return this.serverApi.getPlayer().getDeviceApi().getPlaylistApi().getItemsApi().queue( movie );
    }

    toggleWatched ( movie: MovieMediaRecordSchema ) : Observable<any> {
        return this.serverApi.getMedia().getMovieApi().watched(movie.id, !movie.watched);
    }

    listByApi(take: number, skip: number, opts: any): Observable<any> {
        return this.serverApi.getMedia().getMovieApi().list(take, skip, { ...opts, collections: true });
    }
}

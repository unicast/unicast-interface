import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { PageTitle } from 'components/common/services/pageTitle';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { DeviceStatusSchema } from 'components/schemas/status.schema';
import { SidebarManager } from 'components/services/sidebarManager';
import { ArtManager } from 'components/services/artManager';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { MoviesDetailsSidemenuComponent } from 'components/pages/movies/details/sidemenu/sidemenu.component';
import { SingularSubject } from 'components/common/helpers/singularSubject';
import { DefaultMediaButtonActions } from 'components/pages/common/buttonActions/defaultMediaButtonActions';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { MovieMediaRecordSchema, MediaRecordSchema, ArtRecordKind } from 'components/schemas/mediaRecord.schema';
import { NavigationManager } from 'components/services/navigationManager';
import { MediaImage, GalleryModalComponent, SelectedImage } from 'components/common/modal/galleryModal/galleryModal.component';
import { ArtworkGalleryPipe } from 'components/common/helpers/artworkGalleryPipe';
import { StandardMediaQueries } from 'components/services/mediaQueries';
import { MirroringManager } from 'components/services/mirroringManager';
import { NonExpirableListManager } from 'components/services/nonExpirableListManager';
import { ApplicationContextManager, ContextToken } from 'components/services/applicationContextManager';
import { MovieContextSchema } from 'components/schemas/applicationContexts.schema';

@Component( {
    selector: 'uc-movies-details',
    templateUrl: './moviesDetails.template.html',
    styleUrls: [ './moviesDetails.styles.less' ],
    providers: [ DefaultMediaButtonActions ]
} )
export class MoviesDetailsComponent extends BaseUnicastComponent {
    @ViewChild( 'gallery' )
    galleryModal : GalleryModalComponent;

    public sidebar: SidebarManager;
    public sidemenuToken: any;
    public art: ArtManager;
    public applicationContext: ApplicationContextManager;
    public contextToken: ContextToken<MovieContextSchema>;
    public navigation: NavigationManager;
    public route: ActivatedRoute;
    public serverApi: ServerApiManager;
    public movie: MovieMediaRecordSchema;
    public mediaQueries : StandardMediaQueries;
    public nonExpirableListManager: NonExpirableListManager;
    public movieArtManagerToken: any;
    public htmlTitle: PageTitle;
    public mirroring: MirroringManager;
    public subtitlesOpened: boolean;
    public castOpened: boolean;
    public mediaIsOnResyncList: boolean;

    constructor ( navigation: NavigationManager, route : ActivatedRoute, sidebar : SidebarManager, art : ArtManager, applicationContext: ApplicationContextManager,
                serverApi: ServerApiManager, mediaQueries : StandardMediaQueries, htmlTitle: PageTitle, defaultActions: DefaultMediaButtonActions, mirroring: MirroringManager,
                nonExpirableListManager: NonExpirableListManager) {
        super();
        this.sidebar = sidebar;
        this.sidemenuToken = sidebar.setSecondaryMenu( MoviesDetailsSidemenuComponent );
        this.art = art;
        this.applicationContext = applicationContext;
        this.contextToken = applicationContext.registerToken<MovieContextSchema>(['movie']);
        this.navigation = navigation;
        this.route = route;
        this.serverApi = serverApi;
        this.mediaQueries = mediaQueries;
        this.htmlTitle = htmlTitle;
        this.mirroring = mirroring;
        this.nonExpirableListManager = nonExpirableListManager;
        this.subtitlesOpened = false;
        this.castOpened = false;
        defaultActions.subtitles.injectAction((movie) => this.toggleSubtitles(movie));
    }

    protected _binds () {
        this._subscriptions.push(this.route.params.subscribe( async params => {
            this.loadMovie( params.id );
            this.subtitlesOpened = !!+params.subtitles;
        } ));

        this._subscriptions.push(this.serverApi.getMedia().filterChanged.skip(1).subscribe(([value, byUser]) => {
            if (byUser){
                this.navigation.navigateToMovie();
            }
        }));
    }

    ngOnDestroy () {
        super.ngOnDestroy();
        this.sidebar.disposeSecondaryMenu( this.sidemenuToken );
        this.applicationContext.unregisterToken( this.contextToken );
        this.onMovieChange( null );
    }

    play(movie: MovieMediaRecordSchema): Observable<DeviceStatusSchema>{
        return this.serverApi.getPlayer().getDeviceApi().play( MediaKind.Movie, movie.id);
    }

    queue(movie: MovieMediaRecordSchema): Observable<any>{
        return this.serverApi.getPlayer().getDeviceApi().getPlaylistApi().getItemsApi().queue(movie);
    }

    loadMovie ( id: string ) {
        if (!this.movie || id != this.movie.id) {
            this.serverApi.getMedia().getMovieApi().get(id, 'collections').subscribe(data => {
                this.movie = data;
                this.applicationContext.setContext(this.contextToken, 'movie', { id: data.id, kind: data.kind });
                this.updateMediaIsOnResyncList();
                this.mirroring.mirrorMedia(this.movie);
                this.onMovieChange(data);
            });
        }
    }

    updateMediaIsOnResyncList() {
        this.nonExpirableListManager.containsInResyncMediaList(this.movie).first().subscribe(e => this.mediaIsOnResyncList = e);
    }

    resyncListToggleMedia() {
        const op: (value: MediaRecordSchema) => Observable<boolean> =
            (this.mediaIsOnResyncList ? this.nonExpirableListManager.removeFromResyncMediaList : this.nonExpirableListManager.addToResyncMediaList).bind(this.nonExpirableListManager);

        op(this.movie).subscribe( changed => changed && this.updateMediaIsOnResyncList() );
    }

    goToImdb(event: MouseEvent) {
        this.navigation.navigateToOutside('imdb.com/title/' + this.movie.external['imdb'], { event });
    }

    onArtChange( movie: MovieMediaRecordSchema, artRecordKind?: string ){
        if ( !movie || !artRecordKind || artRecordKind == ArtRecordKind.Background ) {
            if ( this.movieArtManagerToken ) {
                this.art.dispose( this.movieArtManagerToken );
            }

            if ( movie ) {
                this.movieArtManagerToken = this.art.register( movie.cachedArtwork );
            }
        }
    }

    onMovieChange ( movie: MovieMediaRecordSchema ) {
        this.onArtChange(movie);

        if ( movie ) {
            this.htmlTitle.setTitle( movie.title );
        }

        this.sidebar.setMedia( movie );
    }

    toggleSubtitles( movie: MediaRecordSchema ) : Observable<MediaRecordSchema> {
        this.subtitlesOpened=!this.subtitlesOpened;
        let obj = {};
        if (this.subtitlesOpened){
            obj['subtitles'] = 1;
        }
        this.navigation.navigateToMovie(this.movie.id, obj);
        return SingularSubject.oneValue( movie );
    }

    toggleCast ( movie: MediaRecordSchema ) : Observable<MediaRecordSchema> {
        this.castOpened = !this.castOpened;

        return SingularSubject.oneValue( movie );
    }

    toggleWatch( movie: MovieMediaRecordSchema ) {
        let ret = this.serverApi.getMedia().getMovieApi().watched(movie.id, !movie.watched);
        ret.subscribe( (mv) => {
            movie.watched = mv.watched;
        })
        return ret;
    }


    gallerySelectedType(event: SelectedImage) {
        if (event.selected.isLeaf) {
            let artwork = event.parent.art + "";
            this.serverApi.getMedia().getMovieApi().setArtwork(this.movie.id,  artwork, event.selected.id ).subscribe(e => {
                this.galleryModal.close();
                this.movie.cachedArtwork[artwork] = event.selected.value;
                this.onArtChange(this.movie, artwork);
            });
        }
    }

    fecth(mediaImage?: MediaImage): Observable<Array<MediaImage>> {
        if (!mediaImage) {
            return SingularSubject.oneValue(new ArtworkGalleryPipe().transform(this.movie));
        }
        return this.serverApi.getMedia().getMovieApi().getArtwork(this.movie.id).map( data => {
            return data.filter(e => e.kind == mediaImage.art).map( (e, i) => { return {id: e.id, value: e.url, type: MediaKind.Movie, art: i+1, isLeaf: true}});
        });
    }

}
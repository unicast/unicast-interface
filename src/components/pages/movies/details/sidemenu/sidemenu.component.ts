import { Component } from '@angular/core';
import { SidebarManager } from 'components/services/sidebarManager';
import { FiltersSidemenuService } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { MediaListQueryFilters } from 'components/pages/common/sidemenuFilters/filtersMediaSidemenu.service';


@Component( {
    selector: 'uc-movies-details-sidemenu',
    templateUrl: './sidemenu.template.html',
    styleUrls: [ './sidemenu.styles.less' ]
} )
export class MoviesDetailsSidemenuComponent {
    public sidebar: SidebarManager;

    constructor(sidebar: SidebarManager, service : FiltersSidemenuService<MediaListQueryFilters>){
        this.sidebar = sidebar;
        service.initState('repositories');
        service.initState('collections');
    }

}
import { Component } from '@angular/core';

import { FiltersSidemenuService } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { HistoryListQueryFilters } from 'components/pages/common/sidemenuFilters/filtersHistorySidemenu.service';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { ApplicationContextManager, ContextToken } from 'components/services/applicationContextManager';
import { FiltersContextSchema } from 'components/schemas/applicationContexts.schema';

@Component( {
    selector: 'uc-history-sidemenu',
    templateUrl: './sidemenu.template.html',
    styleUrls: [ './sidemenu.styles.less' ]
} )
export class HistorySidemenuComponent extends BaseUnicastComponent {
    public service: FiltersSidemenuService<HistoryListQueryFilters>;
    public applicationContext: ApplicationContextManager;
    public contextToken: ContextToken<FiltersContextSchema<HistoryListQueryFilters>>;

    constructor ( service : FiltersSidemenuService<HistoryListQueryFilters>, applicationContext: ApplicationContextManager ) {
        super();
        this.service = service;
        this.applicationContext = applicationContext;
        this.contextToken = applicationContext.registerToken<FiltersContextSchema<HistoryListQueryFilters>>(['filters']);
    }

    protected _binds() {
        this._subscriptions.push(this.service.filtersChanges.subscribe(() => this.notifyUpdateFilters() ));
        this.notifyUpdateFilters();
    }

    protected notifyUpdateFilters() {
        this.applicationContext.setContext(this.contextToken, 'filters', this.service.filters);
    }

    public ngOnDestroy() {
        this.applicationContext.unregisterToken(this.contextToken);
    }

}
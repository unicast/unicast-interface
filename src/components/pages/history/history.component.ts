import { Component, ViewChild, Injector } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { PageTitle } from 'components/common/services/pageTitle';
import { TopbarManager } from 'components/services/topbarManager';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { HistoryRecordSchema } from 'components/schemas/mediaRecord.schema';
import { RemoveHistoryButtonAction } from 'components/pages/common/buttonActions/removeHistoryButtonAction';
import { MediaListHelper, GroupedArray } from 'components/pages/common/mediaListHelper';
import { SessionCache } from 'components/common/services/sessionCache';
import { FiltersSidemenuService } from 'components/pages/common/sidemenuFilters/filtersSidemenu.service';
import { SidebarManager } from 'components/services/sidebarManager';
import { StandardMediaQueries } from 'components/services/mediaQueries';
import { DefaultMediaButtonActions } from 'components/pages/common/buttonActions/defaultMediaButtonActions';
import { HistoryMediaButtonAction } from 'components/pages/common/buttonActions/historyMediaButtonAction';
import { InputModalComponent } from 'components/common/modal/inputModal/inputModal.component';
import { NavigationManager } from 'components/services/navigationManager';
import { WaitFor } from 'components/common/helpers/asyncClick.directive';
import { ServerApiButtonAction } from 'components/pages/common/buttonActions/buttonAction';
import { HistorySidemenuComponent } from 'components/pages/history/sidemenu/sidemenu.component';
import { FiltersHistorySidemenuService, HistoryListQueryFilters } from 'components/pages/common/sidemenuFilters/filtersHistorySidemenu.service';
import { DelayedExecutions } from 'components/common/services/delayedExecutions';
import { HistoryMediaModalComponent } from 'components/common/modal/historyMediaModal/historyMediaModal.component';
import { FriendlyDatePipe } from 'components/common/helpers/friendlyDatePipe';
import { ApplicationContextManager } from 'components/services/applicationContextManager';

@Component({
    selector: 'uc-history',
    templateUrl: './history.template.html',
    styleUrls: [ './history.styles.less' ],
    providers: [ FiltersHistorySidemenuService.forRoot() ]
} )
export class HistoryComponent extends MediaListHelper<HistoryListQueryFilters> {
    @ViewChild( 'inputNewSource' )
    modal : InputModalComponent<string>;

    @ViewChild( 'historyMediaInfo' )
    historyMediaInfo : HistoryMediaModalComponent;

    private _navigation: NavigationManager;
    private route: ActivatedRoute;

    private playButton: HistoryMediaButtonAction;
    private queueButton: HistoryMediaButtonAction;
    private removeButton: RemoveHistoryButtonAction;

    public groupedData = new GroupedArray<HistoryRecordSchema, string>(
        this.getRecordDay.bind( this ),
        this.getRecordKeyLabel.bind( this ),
        true
    );
    public data = [];
    public actionButtons = [];
    public selectedMedia;
    public blankSpace = this.getBlankSpace();

    constructor ( cache : SessionCache, sidemenuService : FiltersSidemenuService<HistoryListQueryFilters>, sidebar : SidebarManager,
        serverApi:  ServerApiManager, topbar: TopbarManager, htmlTitle: PageTitle, mediaQueries : StandardMediaQueries,
        defaultButtons: DefaultMediaButtonActions, navigation: NavigationManager, route : ActivatedRoute, injector: Injector,
        delayedExecutions: DelayedExecutions, applicationContext: ApplicationContextManager ) {
        super([ HistoryComponent.name, 'history' ] , sidebar, cache, sidemenuService, serverApi, topbar, htmlTitle, mediaQueries, defaultButtons, delayedExecutions, applicationContext);

        this._navigation = navigation;
        this.route = route;

        this.playButton = new HistoryMediaButtonAction(defaultButtons.play, this);
        this.queueButton = new HistoryMediaButtonAction(defaultButtons.queue, this);
        this.removeButton = new RemoveHistoryButtonAction(serverApi);
        this.actionButtons = [ this.playButton, this.queueButton, this.removeButton ];

        this.setSecondaryMenu( HistorySidemenuComponent, injector );

        let title = 'History';
        topbar.setTitle( title );
        htmlTitle.setTitle( title );
        this.step = 23;
    }

    protected getRecordDay ( record : HistoryRecordSchema ) {
        const d = new Date( record.createdAt );

        return `${d.getFullYear()}/${d.getMonth()}/${d.getDate()}`;
    }

    protected getRecordKeyLabel ( key : string ) {
        const parts = key.split( '/' );

        const date = new Date( +parts[ 0 ], +parts[ 1 ], +parts[ 2 ] );

        return FriendlyDatePipe.transform(date);
    }

    ngOnInit() {
        this.sidemenuService.injectFilters(this.route.snapshot.queryParams);
        super.ngOnInit();
    }

    listByApi(take: number, skip: number, opts: any): Observable<any> {
        const query = { records: true, ...opts };

        return this.serverApi.getMedia().getHistoryApi().list(take, skip, query);
    }

    getBlankSpace(): HistoryRecordSchema{
        let empty = this.serverApi.getMedia().emptyRecord();
        return {
            id: '_blank',
            record: {
                ...empty,
                id: undefined,
                kind: MediaKind.Custom,
                title: 'New Custom Media',
                runtime: 0,
                sources: [],
                metadata: null,
                playCount: 0,
                watched: false,
                lastPlayedAt: new Date(),
                addedAt: new Date()
            }
        };
    }

    public goto ( event: WaitFor, item: HistoryRecordSchema ) {
        event.waitFor(this._navigation.navigateTo(item.record));
    }

    public playItem ( event: WaitFor, item: HistoryRecordSchema ) {
        this.executeAndReload(this.playButton, event, item);
    }

    public queueItem ( event: WaitFor, item: HistoryRecordSchema ) {
        this.executeAndReload(this.queueButton, event, item);
    }

    public removeItem ( event: WaitFor, item: HistoryRecordSchema ) {
        this.executeAndReload(this.removeButton, event, item);
    }

    public infoItem(item: HistoryRecordSchema) {
        this.historyMediaInfo.open(item);
    }

    private executeAndReload( button: ServerApiButtonAction<HistoryRecordSchema>, event: WaitFor, item: HistoryRecordSchema ) {
        event.waitFor( button.execute(item).do( e => this.reload() ) );
    }
}

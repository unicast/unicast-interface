import { ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HomePageComponent } from 'components/pages/home/home.component';
import { MoviesRoutes } from 'components/routes/movies';
import { ShowsRoutes } from 'components/routes/shows';
import { AddonsRoutes } from 'components/routes/addons';
import { SettingsRoutes } from 'components/routes/settings';
import { HistoryRoutes } from 'components/routes/history';
import { MediasRoutes } from 'components/routes/medias';
import { PeopleRoutes } from 'components/routes/people';

var AppRoutes = [
    { path: '', index: true, component: HomePageComponent },
    MediasRoutes.router(),
    MoviesRoutes.router(),
    ShowsRoutes.router(),
    PeopleRoutes.router(),
    HistoryRoutes.router(),
    AddonsRoutes.router(),
    SettingsRoutes.router()
];


export var APP_ROUTER_PROVIDDERS: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
import { Directive, Input, HostBinding } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

import { LayoutUtils } from 'components/common/helpers/layoutUtils';

@Directive({
    selector: '[uc-background]',
    host: {
        '[style.background-attachment]': '"scroll, fixed"',
        '[style.background-clip]': '"border-box, border-box"',
        '[style.background-origin]': '"padding-box, padding-box"'
    }
} )

export class BackgroundDirective {
    @HostBinding('style.background-image')
    backgroundSafe:SafeStyle = LayoutUtils.EMPTY_IMG;

    @HostBinding('style.background-size') @Input('background-size')
    backgroundSize: string = 'cover';

    @HostBinding('style.background-repeat') @Input('background-repeat')
    backgroundRepeat: string = 'repeat, no-repeat';

    @HostBinding('style.background-position') @Input('background-position')
    backgroundPosition: string = '0% 0%, center center';

    @Input('uc-background') background : string;

    @Input('background-width') backgroundWidth: number;

    protected sanitizer : DomSanitizer;

    constructor( sanitizer : DomSanitizer ) {
        this.sanitizer = sanitizer;
    }

    ngOnChanges (change) {
        if ( change.background ) {
            let width = this.backgroundWidth && !this.background.startsWith('data:') ? '?width=' + this.backgroundWidth : '';
            this.backgroundSafe = this.sanitizer.bypassSecurityTrustStyle( 'url(' + this.background + width + ')' );
        } else {
            this.backgroundSafe = LayoutUtils.EMPTY_IMG;
        }
    }

}
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { DropdownComponent } from './dropdown.component';

@Injectable()
export class DropdownService {
    public level: number = 0;

    public nestedDropdownOpened: Subject<DropdownComponent> = new Subject();
}
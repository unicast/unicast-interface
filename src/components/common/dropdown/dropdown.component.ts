import { 
    Component, 
    Input, 
    Output, 
    ViewChild,
    HostListener,
    EventEmitter, 
    
    // Injector
    Optional, 
    Self, 
    SkipSelf,

    // Services
    ElementRef, 
    NgZone, 
    
    AfterContentChecked, 
    OnDestroy,
} from '@angular/core';
import { DropdownService } from 'components/common/dropdown/dropdown.service';
import { Subscription } from 'rxjs/Subscription';

@Component( {
    selector: '.uc-dropdown, [uc-dropdown], uc-dropdown',
    templateUrl: './dropdown.template.html',
    styleUrls: [ './dropdown.styles.less' ],
    providers: [ DropdownService ]
} )
export class DropdownComponent implements OnDestroy, AfterContentChecked {
    opened : boolean = false;

    /** When true, the dropdown will close itself if the clicks inside the container. Defaults to false. */
    @Input() closeOnDropdownClick : boolean = false;

    /** When true, the dropdown will close itself if the user clicks outside of it. Defaults to true. */
    @Input() autoclose : boolean = true;

    /** When false, the dropdown will ave position: absolute. When true, whe dropdown will be position inside the document. Defaults to `false`. */
    @Input() embedded : boolean = false;

    /** When true, the dropdown body will have the same width as the trigger. Defaults to false. */
    @Input() fullWidth : boolean = false;

    /** When true, the dropdown body will have the same width as the trigger. Defaults to false. */
    @Input() width : number = null;

    /** Is triggered when the dropdown is either opened or closed */
    @Output() openedChange: EventEmitter<boolean> = new EventEmitter();

    @ViewChild( "trigger" ) triggerRef : ElementRef;

    @ViewChild( 'dropdownContainer' ) dropdownContainer : ElementRef;
    
    parentDropdownService: DropdownService;

    dropdownService: DropdownService;

    expandHeightDelay: number = 0;

    private expandedSectionElement: HTMLElement = null;

    /** Represents the last bottom position of the dropdownContainer, used to check if it has changed */
    private dropdownContainerLastBottom: number = 0;

    /** Stores the last change to the section's height required to show the whole dropdown */
    private expandedSectionHeightDelta: number = 0;

    /** Stores the clientHeight of the section before the minHeight being applied */
    private expandedSectionOriginalHeight: number = null;

    private expandedSectionTimeout: any = null;

    private expandedSectionDebounced: boolean = false;

    protected nestedDropdownOpened: DropdownComponent = null;

    protected nestedDropdownOpenedSubscription: Subscription = null;

    protected elementRef : ElementRef;

    protected zone: NgZone;

    public constructor ( 
        elementRef : ElementRef, 
        zone : NgZone, 
        @Optional() @SkipSelf() parentDropdownService: DropdownService,
        @Self() dropdownService: DropdownService
    ) {
        this.elementRef = elementRef;
        this.zone = zone;
        this.parentDropdownService = parentDropdownService;
        this.dropdownService = dropdownService;

        this.nestedDropdownOpenedSubscription = this.dropdownService.nestedDropdownOpened.subscribe( component => {
            if (this.nestedDropdownOpened != null && this.nestedDropdownOpened != component) {
                this.nestedDropdownOpened.close();
            }

            this.nestedDropdownOpened = component;
        } );
    }

    @HostListener( 'document:click', [ '$event' ] )
    protected clickOutside ( event ) {
        if ( !this.elementRef.nativeElement.contains( event.target ) && this.opened && this.autoclose ) {
            this.toggle();
        }
    }

    public clickContainer ( event?: Event ) {
        if (this.opened && this.closeOnDropdownClick) {
            this.toggle();
        }
    }

    public ngAfterContentChecked () {
        if (this.opened && !this.embedded && this.expandedSectionElement != null) {
            this.updateExpansion();
        }
    }

    public getBottomPosition ( element: ElementRef | HTMLElement ) : number {
        const htmlElement =element instanceof HTMLElement 
            ? element : element.nativeElement as HTMLElement;

        return htmlElement.getBoundingClientRect().bottom + document.documentElement.scrollTop;
    }

    public updateExpansion () {
        if ( this.expandedSectionTimeout != null ) {
            this.expandedSectionDebounced = true;
            return;
        }

        this.zone.runOutsideAngular(() => {
            this.expandedSectionTimeout = setTimeout(() => {
                this.expandedSectionTimeout = null;
    
                const dropdownPosition = this.getBottomPosition( this.dropdownContainer );
                
                // Only run this code the first time, let's assume the width doesn't change
                // while the popup is open.
                if ( this.dropdownContainerLastBottom === 0 && !this.fullWidth ) {
                    const containerRight = ( this.dropdownContainer.nativeElement as HTMLElement ).getBoundingClientRect().right;

                    this.setMaxWidth( Math.min( document.body.clientWidth - 25, containerRight ) - 25 );
                }

                if ( Math.abs( dropdownPosition - this.dropdownContainerLastBottom ) > 1 ) {
                    const sectionBottomPosition = this.getBottomPosition( this.expandedSectionElement );
                    
                    // If it'z zero, this is either the first time we're updating the section's height
                    // Or we've tried to update it before but there was no need (the dropdown was already fully visible)
                    if ( this.expandedSectionHeightDelta === 0 ) {
                        // Value in pixels regarding how much more height the section element
                        // needs to have to be able to show the container
                        const delta = Math.max( 0, dropdownPosition - sectionBottomPosition );
                        
                        this.addMinHeight( this.expandedSectionElement, delta );
                    } else {
                        const delta = dropdownPosition - this.dropdownContainerLastBottom;
    
                        this.updateMinHeight( this.expandedSectionElement, delta );
                    }
    
                    this.dropdownContainerLastBottom = dropdownPosition;
                }
                
                this.dropdownContainerLastBottom = dropdownPosition;
    
                if ( this.expandedSectionDebounced ) {
                    this.expandedSectionDebounced = false;
    
                    this.updateExpansion();
                }
            }, this.expandHeightDelay);
        } );
    }

    public open () {
        if (!this.opened) {
            this.toggle();
        }
    }

    public close () {
        if (this.opened) {
            this.toggle();
        }
    }

    public toggle () {
        this.opened = !this.opened;
        this.openedChange.next( this.opened );

        if (this.opened && this.parentDropdownService != null) {
            this.parentDropdownService.nestedDropdownOpened.next(this);
        }

        if (this.opened && !this.embedded) {
            const sections = document.querySelectorAll('section');

            const bottomSection = Array.from(sections)
                .filter(isHtmlElement)
                .reduce(compareVerticalSections, null);

            if (bottomSection != null) {
                this.expandedSectionElement = bottomSection;

                this.updateExpansion();
            }
        } else {
            if ( this.expandedSectionElement != null ) {
                this.expandedSectionElement.style.minHeight = '';
                
                this.expandedSectionElement = null;
                this.expandedSectionHeightDelta = 0;
                this.expandedSectionOriginalHeight = null;
            }
            
            if ( this.expandedSectionTimeout != null ) {
                clearTimeout( this.expandedSectionTimeout );
                
                this.expandedSectionTimeout = null;
            }
            
            this.expandedSectionDebounced = false;
            this.dropdownContainerLastBottom = 0;
        }
    }

    protected setMaxWidth ( width: number ) {
        const element = this.dropdownContainer.nativeElement as HTMLElement;

        if ( width != null ) {
            element.style.maxWidth = Math.floor( width ).toString() + 'px';
        } else {
            element.style.maxWidth = '';
        }
    }

    protected addMinHeight ( element: HTMLElement, delta : number ) {
        const computedStyle = window.getComputedStyle(element, null);
        const paddingTop = parseInt(computedStyle.getPropertyValue('padding-top'), 10);
        const paddingBottom = parseInt(computedStyle.getPropertyValue('padding-bottom'), 10);

        const padding = paddingTop + paddingBottom;

        const clientHeight = this.expandedSectionOriginalHeight == null
            ? element.clientHeight
            : this.expandedSectionOriginalHeight;

        this.expandedSectionOriginalHeight = clientHeight;
        
        if ( delta > 0 ) {
            element.style.minHeight = ( clientHeight + delta - padding + 20 ) + 'px';
        } else {
            element.style.minHeight = '';
        }

        this.expandedSectionHeightDelta = Math.max( 0, delta );
    }

    protected updateMinHeight ( element: HTMLElement, delta : number ) {
        this.addMinHeight( element, this.expandedSectionHeightDelta + delta )
    }

    public ngOnDestroy () {
        this.close();

        this.nestedDropdownOpenedSubscription.unsubscribe();
    }
}

function isHtmlElement (elem: Element): elem is HTMLElement {
    return elem instanceof HTMLElement;
}

function compareVerticalSections<E1 extends HTMLElement, E2 extends HTMLElement> (elem1: E1, elem2: E2): E1 | E2 {
    if (elem1 == null) {
        return elem2;
    }

    if (elem2 == null) {
        return elem1;
    }

    const elem1Bottom = elem1.offsetTop + elem1.offsetHeight;
    const elem2Bottom = elem2.offsetTop + elem2.offsetHeight;

    if (elem1Bottom >= elem2Bottom) {
        return elem1;
    } else {
        return elem2;
    }
}
import { Component, Output, Input, EventEmitter, NgZone } from '@angular/core';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { DelayedExecutions } from 'components/common/services/delayedExecutions';

export enum AutoScrollDirection {
    Up = '-1',
    Down = '1'
}

@Component( {
    selector: '.uc-autoscroll-up-down, [uc-autoscroll-up-down], uc-autoscroll-up-down',
    templateUrl: './up-down.template.html',
    styleUrls: [ './up-down.styles.less' ]
} )
export class AutoScrollUpDownComponent extends BaseUnicastComponent {
    @Input() scrollableElement: HTMLElement = document.body.parentElement;

    @Input() speeds: Array<number> = [75, 200];

    @Input() iconsUp: Array<string> = ['angle-up', 'angle-double-up'];

    @Input() iconsDown: Array<string> = ['angle-down', 'angle-double-down'];

    @Output() scrollUp: EventEmitter<number> = new EventEmitter();

    @Output() scrollDown: EventEmitter<number> = new EventEmitter();

    autoScrollEmitter: EventEmitter<number> = new EventEmitter();

    protected delayedExecutions: DelayedExecutions;
    protected ngZone: NgZone;
    protected scrollInterval: number;
    protected nextFrame: number;

    public currentUpSpeed: number = -1;
    public currentDownSpeed: number = -1;
    public previousScrollTop: number = 0;
    public currentVelocity: number = 0;

    constructor(delayedExecutions: DelayedExecutions, ngZone: NgZone) {
        super();
        this.delayedExecutions = delayedExecutions;
        this.ngZone = ngZone;
    }

    ngOnInit() {
        super.ngOnInit();

        this._subscriptions.push(this.autoScrollEmitter.debounceTime(300).subscribe( e => {
            this.autoScroll(e);
        }));
    }


    autoScrollUpEmit() {
        this._autoScrollUpDownEmit(AutoScrollDirection.Up);
    }

    autoScrollDownEmit() {
        this._autoScrollUpDownEmit(AutoScrollDirection.Down);
    }

    private _autoScrollUpDownEmit(moving: AutoScrollDirection) {
        let current: keyof AutoScrollUpDownComponent = 'currentUpSpeed';
        let inverse: keyof AutoScrollUpDownComponent = 'currentDownSpeed';
        let signal: number = -1;
        if (moving == AutoScrollDirection.Down) { //Defauls values are for Up
            current = 'currentDownSpeed';
            inverse = 'currentUpSpeed';
            signal = 1;
        }
        if (this.currentVelocity != 0) {
            this.disableAutoScroll();
        } else {
            this[current] = (this[current] + 1) % this.speeds.length;
            this.autoScrollEmitter.emit(signal * this.speeds[this[current]]);
            this[inverse] = -1;
        }
    }

    disableAutoScroll() {
        cancelAnimationFrame(this.nextFrame);
        this.currentUpSpeed = this.currentDownSpeed = -1;
        this.currentVelocity = 0;
    }

    autoScroll(direction: AutoScrollDirection, time?: number): void;
    autoScroll(velocity: number, time?: number): void;
    autoScroll(value: number | AutoScrollDirection, time: number = 50): void {
        let velocity = typeof value === 'number' ? value : 10 * Number(value);
        this.currentVelocity = velocity;

        if ( velocity == 0) {
            this.disableAutoScroll();
        } else {
            let emitter: EventEmitter<number> = (velocity < 0 ? this.scrollUp : this.scrollDown);
            //If any observable was specified, don't execute the self scroll
            if (emitter.observers.length > 0){
                emitter.emit(Math.abs(velocity))
            } else {
                this.previousScrollTop = this.scrollableElement.scrollTop - velocity;
                
                this.ngZone.runOutsideAngular( () => { this._intenalScroll(); } );
            }
        }

    }


    private lastTime : any = 0;

    protected _intenalScroll = ( now : DOMHighResTimeStamp = 0 ) => {
        if ( now == 0 ) {
            this.lastTime = performance.now();
            
            this.nextFrame = requestAnimationFrame( this._intenalScroll );

            return;
        }

        const delta = now - this.lastTime;

        this.lastTime = now;
        
        let velocity = this.currentVelocity;
        
        let newValue = this.scrollableElement.scrollTop + ( velocity * ( delta / 1000.0 ) );

        if (velocity == 0 || newValue < 0 || newValue + this.scrollableElement.clientHeight >= this.scrollableElement.scrollHeight ) {
            this.ngZone.run( () => this.disableAutoScroll() );
        } else {
            this.scrollableElement.scrollTo( { top: newValue } )

            this.nextFrame = requestAnimationFrame( this._intenalScroll );
        }
    }

}
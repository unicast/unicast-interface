import { Component, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';

export type Icons = {icon: string, size: string};

export type ButtonClickEvent = { button: ButtonComponent, event: MouseEvent };

export enum IconPosition {
    Right = 'right',
    Top = 'top',
    Left = 'left',
    Bottom = 'bottom'
}

export enum IconType {
    None = 'none',
    Svg = 'svg',
    Ico = 'ico'
}

@Component( {
    selector: '.uc-button, [uc-button], uc-button',
    templateUrl: './button.template.html',
    styleUrls: [ './button.styles.less' ]
} )
export class ButtonComponent {
    public readonly IconType: typeof IconType = IconType;

    @Input( ) @HostBinding('class.disabled') disabled: boolean = false;
    @Input( ) svg: string;
    @Input( ) icon: string;
    @Input( ) iconOnly: boolean;
    @Input( ) iconPosition: IconPosition = IconPosition.Left;
    @Input( ) buttonSuffix ?: string;
    @Output( ) buttonSuffixClick : EventEmitter<ButtonClickEvent> = new EventEmitter();
    @Output( ) action: EventEmitter<ButtonClickEvent> = new EventEmitter();

    _svg: SafeHtml;
    _icons: Array<Icons>;
    _iconType: IconType;

    private _sanitizer: DomSanitizer;

    constructor(sanitizer: DomSanitizer) {
        this._sanitizer = sanitizer;
    }

    ngOnChanges( value ){
        if (value.icon) {
            this._icons = ( value.icon.currentValue !== undefined ? this.transformIcons(value.icon.currentValue) : []);
        }

        if (value.svg) {
            this._svg = this._sanitizer.bypassSecurityTrustHtml(this.svg || '');
        }

        this._iconType = this.selectIconType();
    }

    transformIcons(str: string): Array<Icons>{
        let arr: Array<string> = str.split("|");
        return arr.map<Icons>((data: string) => {
            let narr: Array<string> = data.split(",");
            let ret: Icons = {icon: narr[0], size: ''};
            if (narr.length > 1){
                ret.size = narr[1] + "em";
            } else {
                ret.size = "";
            }
            return ret;
        });
    }

    private selectIconType(): IconType {
        let iconType = IconType.None;

        //Svg has priority over icon
        if (this._svg) {
            iconType = IconType.Svg;
        } else if (this._icons) {
            iconType = IconType.Ico;
        }

        return iconType;
    }

    private _onClick(action: EventEmitter<ButtonClickEvent>, event: MouseEvent) {
        if (this.disabled) {
            event.preventDefault();
            event.stopPropagation();
        } else {
            action.emit({ button: this, event });
        }
    }

    onClick( event: MouseEvent ) {
        this._onClick(this.action, event);
    }

    onSuffixClick( event: MouseEvent ) {
        this._onClick(this.buttonSuffixClick, event);
    }
}
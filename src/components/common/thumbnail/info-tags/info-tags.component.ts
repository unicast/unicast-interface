import { Component, Input } from '@angular/core';

@Component( {
    selector: 'uc-thumbnail-info-tags',
    templateUrl: './info-tags.template.html',
    styleUrls: [ './info-tags.styles.less' ]
} )
export class ThumbnailInfoTagsComponent {
    @Input() tags;
}
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { StandardMediaQueries, ScreenSize } from 'components/services/mediaQueries';
import { ThumbnailService } from 'components/common/thumbnail/thumbnail.service';
import { MediaButtonAction } from 'components/pages/common/buttonActions/buttonAction';

@Component( {
    selector: 'uc-thumbnail-context-menu',
    templateUrl: './context-menu.template.html',
    styleUrls: [ './context-menu.styles.less' ]
} )
export class ThumbnailContextMenuComponent {
    protected mediaQueries : StandardMediaQueries;

    protected screenSizeValue : ScreenSize = ScreenSize.Desktop;

    protected mediaQueriesSubscription : Subscription;

    protected thumbnailService : ThumbnailService;

    constructor ( mediaQueries : StandardMediaQueries, thumbnail : ThumbnailService ) {
        this.mediaQueries = mediaQueries;
        this.thumbnailService = thumbnail;

        this.mediaQueriesSubscription = this.mediaQueries.screenSize.subscribe( size => this.screenSizeValue = size );
    }

    @Input() active : boolean;

    @Input( 'actionObject' ) actionObject : unknown;

    @Input( 'items' ) items : Array<MediaButtonAction>;

    @Input( 'orientation' ) orientation : string = 'vertical';

    @Output( 'action' ) action : EventEmitter<ThumbnailContextMenuItem> = new EventEmitter();

    protected ngOnChanges (change) {
        this.thumbnailService.hasToolbar = this.items.length > 0;
    }

    public getButtonSize ( importance : number ) : number {
        if ( this.screenSizeValue === ScreenSize.Desktop ) {
            if ( this.orientation === 'vertical' ) {
                if ( importance <= 2 ) {
                    return 2;
                } else if ( importance <= 4 ) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                if ( importance <= 1 ) {
                    return 2;
                } else if ( importance <= 3 ) {
                    return 1;
                } else {
                    return 0;
                }
            }
        } else if ( this.screenSizeValue <= ScreenSize.Tablet ) {
            if ( this.orientation === 'vertical' ) {
                if ( importance <= 2 ) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                if ( importance <= 1 ) {
                    return 1;
                } else if ( importance <= 3 ) {
                    return 0;
                } else {
                    return 0;
                }
            }
        }
    }

    public getButtonSizeClass ( importance : number ) : string {
        const size : number = this.getButtonSize( importance );

        if ( size == 0 ) {
            return 'button-xs';
        } else if ( size == 1 ) {
            return 'button-sm';
        } else if ( size == 2 ) {
            return 'button-md';
        } else {
            return 'button-lg';
        }
    }

    ngOnDestroy () {
        this.thumbnailService.hasToolbar = false;
        this.mediaQueriesSubscription.unsubscribe();
    }
}

export interface ThumbnailContextMenuItem {
    key : string;
    label : string;
    icon ?: string;
    theme ?: string;
    importance ?: number;
}
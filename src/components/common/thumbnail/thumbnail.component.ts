import { Component, Input, ViewChild, Optional, HostBinding, Output, EventEmitter, HostListener } from '@angular/core';
import { Router, UrlTree } from '@angular/router';

import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { SectionBackground } from 'components/common/services/sectionBackground';
import { ThumbnailService } from 'components/common/thumbnail/thumbnail.service';
import { LayoutUtils } from 'components/common/helpers/layoutUtils';
import { StandardMediaQueries, ScreenSize } from 'components/services/mediaQueries';

export enum ThumbnailSize {
    Small = 0,
    Normal = 1,
    Large = 2
}

@Component( {
    selector: 'uc-thumbnail',
    templateUrl: './thumbnail.template.html',
    styleUrls: [ './thumbnail.styles.less' ],
    host: {
        '[class.darker]': 'darkerBackground'
    },
    providers: [ ThumbnailService ]
} )
export class ThumbnailComponent extends BaseUnicastComponent {
    @ViewChild( 'overlay' ) overlay;
    @ViewChild( 'overlayInfo' ) overlayInfo;
    @Input( 'overlayFaded' ) overlayFaded;

    @Input() link: string[] | string;
    @Input() image;
    @Input() label;
    @Input() caption;
    @Input() rating;
    @Input() state;
    @Input() width: number;
    @Input() orientation : string = 'vertical';

    @Output( 'action' ) action : EventEmitter<void> = new EventEmitter();

    @Input('forceOverlayInfo') forceOverlayInfo: boolean = false;

    @HostBinding( 'class.active' ) @Input( 'active' ) active;
    @HostBinding( 'class.highlighted' ) @Input( 'highlighted' ) highlighted;

    public sectionBackground: SectionBackground;
    public router: Router;
    public service : ThumbnailService;
    public mediaQueries : StandardMediaQueries;

    public showOverlayToolbar: boolean;
    public showOverlayInfo: boolean;
    public darkerBackground: boolean;

    public layoutUtils = LayoutUtils;

    public size : ThumbnailSize = ThumbnailSize.Normal;

    constructor ( @Optional() sectionBackground: SectionBackground, router: Router, service : ThumbnailService, mediaQueries : StandardMediaQueries ) {
        super();
        this.sectionBackground = sectionBackground;
        this.router = router;
        this.service = service;
        this.mediaQueries = mediaQueries;

        this.darkerBackground = false;
    }

    protected _binds() {
        if ( this.sectionBackground ) {
            this._subscriptions.push( this.sectionBackground.levelChanges.subscribe( this.onSectionBackgroundChange.bind( this ) ) );
        }

        this._subscriptions.push( this.mediaQueries.screenSize.subscribe( size => {
            if ( size == ScreenSize.Smartphone || size == ScreenSize.Tablet ) {
                this.size = ThumbnailSize.Small;
            } else {
                this.size = ThumbnailSize.Normal;
            }
        } ) );
    }

    @HostListener('click', ['$event'])
    onMouseClick ( $event: MouseEvent ) {
        if ( this.link ) {
            if ( $event.ctrlKey ) {
                let url : UrlTree = typeof this.link === 'string' ? this.router.createUrlTree( [ this.link ] ) : this.router.createUrlTree( this.link );

                window.open( this.router.serializeUrl( url ), '_blank' );
            } else {
                this.router.navigate(
                    typeof this.link === 'string' ? [ this.link ] : this.link
                );
            }
        } else {
            this.action.emit();
        }
    }

    getImageWidth () {
        if ( this.width ) {
            return this.width;
        } else if ( this.orientation === 'vertical' ) {
            switch ( this.size ) {
                case ThumbnailSize.Small: return 100;
                case ThumbnailSize.Normal: return 200;
                case ThumbnailSize.Large: return 300;
            }
        } else if ( this.orientation === 'horizontal' ) {
            switch ( this.size ) {
                case ThumbnailSize.Small: return 210;
                case ThumbnailSize.Normal: return 300;
                case ThumbnailSize.Large: return 420;
            }
        }
    }

    ngAfterContentInit () {
        if ( this.service.hasToolbar ) {
            this.showOverlayInfo = true;
        }
    }

    onSectionBackgroundChange ( level ) {
        this.darkerBackground = level > 2;
    }

    ngOnChanges () {
        if ( this.active ) {
            this.onActivate();
        } else {
            this.onDeactivate();
        }
    }

    onActivate () {
        this.showOverlayToolbar = this.service.hasToolbar;

        if ( this.service.hasToolbar || this.forceOverlayInfo ) {
            this.showOverlayInfo = !this.showOverlayToolbar;
        } else {
            this.showOverlayInfo = false;
        }
    }

    onDeactivate () {
        this.showOverlayToolbar = false;

        this.showOverlayInfo = this.service.hasToolbar || this.forceOverlayInfo;
    }
}
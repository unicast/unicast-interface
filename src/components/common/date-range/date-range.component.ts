import { Component, Input, forwardRef, EventEmitter, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, Validator, ValidationErrors, AbstractControl, NG_VALIDATORS } from '@angular/forms';

import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';

export type DateRange = { start?: Date, end?: Date };

@Component( {
    selector: 'uc-date-range',
    templateUrl: './date-range.template.html',
    styleUrls: [ './date-range.styles.less' ],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => DateRangeComponent),
        multi: true
    },{
        provide: NG_VALIDATORS,
        useExisting: forwardRef(() => DateRangeComponent),
        multi: true,
    }]
} )
export class DateRangeComponent extends BaseUnicastComponent implements ControlValueAccessor, Validator{

    public startChange: EventEmitter<[Date, keyof DateRange]> = new EventEmitter();
    public endChange: EventEmitter<[Date, keyof DateRange]> = new EventEmitter();

    @Input()
    private groupTime: number = 1000;

    @Output('changed')
    private onChangeEmitter: EventEmitter<DateRange> = new EventEmitter();

    private onTouchedEmitter: EventEmitter<boolean> = new EventEmitter();
    private onValidationEmitter: EventEmitter<boolean> = new EventEmitter();

    public isDisabled: boolean = false;

    @Input()
    public isRange: boolean = true;

    @Input('value')
    public date: DateRange = undefined;

    constructor ( ) {
        super();
        this.subscribeChange(this.startChange);
        this.subscribeChange(this.endChange);
    }

    private subscribeChange(emitter: EventEmitter<[Date, keyof DateRange]>) {
        this._subscriptions.push(emitter
            .debounceTime(this.groupTime)
            .distinctUntilChanged()
            .subscribe( ([date, input]) => this.onChange(date, input) )
        );
    }

    private isDateRange(date: DateRange | Date): date is DateRange {
        return !(date instanceof Date);
    }

    public onChange(event?: Date, obj?: keyof DateRange): void {
        if (obj){
            this.date = this.date || {};
            this.date[obj] = event === null ? undefined : event;
        }
        this.onChangeEmitter.emit( this.date );
        this.onValidationEmitter.emit( this.isValid() );
    }

    writeValue(obj: DateRange | Date): void {
        this.date = this.isDateRange(obj) ? obj : { start: obj, end: obj };
        //this.onChange(undefined);
    }

    registerOnChange(fn: any): void {
        this._subscriptions.push(this.onChangeEmitter.subscribe(fn));
    }

    registerOnTouched(fn: any): void {
        this._subscriptions.push(this.onTouchedEmitter.subscribe(fn));
    }

    setDisabledState?(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

    public isValid(): boolean {
        return !!this.date && this.date.start > this.date.end;
    }

    validate(c: AbstractControl): ValidationErrors {
        return (this.isValid()) ? {
            rangeDate: { valid: false }
        } : null;
    }
    registerOnValidatorChange?(fn: () => void): void {
        this._subscriptions.push(this.onValidationEmitter.subscribe(fn));
    }

}
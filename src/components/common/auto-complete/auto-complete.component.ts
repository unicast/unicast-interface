import { Component, forwardRef, EventEmitter, Input, Output, SimpleChanges, ViewChild, ElementRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { DropdownComponent } from 'components/common/dropdown/dropdown.component';


export type AutoCompleteEntry = { id: string, title: string };

@Component( {
    selector: 'uc-auto-complete',
    templateUrl: './auto-complete.template.html',
    styleUrls: [ './auto-complete.styles.less' ],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => AutoCompleteComponent),
        multi: true
    }]
} )
export class AutoCompleteComponent extends BaseUnicastComponent implements ControlValueAccessor{

    @ViewChild( 'dropdown' )
    dropdown: DropdownComponent;

    public keyUp = new EventEmitter<KeyboardEvent>();

    @Output('changed')
    private onChangeEmitter: EventEmitter<Array<AutoCompleteEntry>> = new EventEmitter();
    private onTouchedEmitter: EventEmitter<boolean> = new EventEmitter();

    public isDisabled: boolean = false;

    @Input()
    private groupTime: number = 300;

    @Input()
    private maxElements: number = 10;

    @Input()
    contextListBody: ElementRef;

    @Input()
    listBody: ElementRef;

    @Input('optionsList')
    public optionsListObservable: Observable<Array<AutoCompleteEntry>> = Observable.of([]);
    public contextList: Array<AutoCompleteEntry> = [];

    @Output('lookup')
    public lookup: Observable<string> = new EventEmitter();

    @Output()
    public selected: EventEmitter<AutoCompleteEntry> = new EventEmitter();

    @Input('value')
    public selectedList: Array<AutoCompleteEntry> = [];

    public keys: Set<string> = new Set();

    private optionsListSubscription: Subscription;

    public constructor() {
        super();
        this.lookup = this.keyUp.map( e => e.target['value'] as string )
            .filter( e => e.length >= 3 )
            .debounceTime(this.groupTime)
            .distinctUntilChanged()
            .do( () => this.updateContextList([]) );
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['optionsList'] || changes['optionsListObservable']) {
            if (this.optionsListSubscription) {
                this.optionsListSubscription.unsubscribe();
            }
            this.optionsListSubscription = this.optionsListObservable
                .map( e => e.slice(0, Math.min(this.maxElements, e.length) ) )
                .subscribe( e => this.updateContextList(e))
        }

        if (changes['value'] || changes['selectedList']) {
            this.writeValue(this.selectedList);
        }
    }

    public updateContextList(list: Array<AutoCompleteEntry>) {
        this.contextList = (list || []);
        this.dropdown.opened = this.contextList.length > 0;
    }

    public _add(entry: AutoCompleteEntry): boolean {
        if (!this.keys.has(entry.id)) {
            this.selectedList.push(entry);
            this.keys.add(entry.id);
            return true;
        }
        return false;
    }

    public addItem(event: AutoCompleteEntry) {
        if (this._add(event)) {
            this.onChangeEmitter.emit(this.selectedList);
            this.selected.emit(event);
        }
    }

    public removeItem(event: AutoCompleteEntry, position: number) {
        if (this.selectedList[position] == event) {
            this.selectedList.splice(position, 1);
            this.keys.delete(event.id);
            this.onChangeEmitter.emit(this.selectedList);
        }
    }

    writeValue(list: Array<AutoCompleteEntry>): void {
        this.selectedList = [];
        this.keys = new Set();
        for (let obj of (list || [])) {
            this._add(obj);
        }
    }

    registerOnChange(fn: any): void {
        this._subscriptions.push(this.onChangeEmitter.subscribe(fn));
    }

    registerOnTouched(fn: any): void {
        this._subscriptions.push(this.onTouchedEmitter.subscribe(fn));
    }

    setDisabledState?(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }
}
import { Component, Input, Output, ViewChild, ComponentFactoryResolver , Injector, ViewContainerRef, EventEmitter, ComponentRef } from '@angular/core';

@Component( {
    selector: 'uc-dynamic-component',
    templateUrl: './dynamicComponent.template.html'
} )
export class DynamicComponentComponent {
    @Input() subject;
    @Input() component;
    @Input() injector;
    @Output() load = new EventEmitter;

    @ViewChild( 'anchor', { read: ViewContainerRef } ) anchor;

    public cfr:ComponentFactoryResolver;
    public parentInjector: Injector;
    public _resource: any;

    constructor ( cfr: ComponentFactoryResolver, injector : Injector ) {
        this.cfr = cfr;
        this.parentInjector = injector;
    }

    async ngOnChanges () {
        let component;
        let injector;

        if ( this.subject ) {
            component = this.subject.component;
            injector = this.subject.injector;
        } else {
            component = this.component;
            injector = this.injector;            
        }

        if ( this._resource ) {
            const componentRef : ComponentRef<any> = await this._resource;

            componentRef.destroy();

            this._resource = null;
        }

        if ( component ) {
            let factory = this.cfr.resolveComponentFactory( component );
            
            this._resource = this.anchor.createComponent( factory, 0, injector );

            this.load.emit( await this._resource );
        }
    }
}
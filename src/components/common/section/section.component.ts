import { Component, Input, Output, EventEmitter } from '@angular/core';

import { SectionBackground } from '../services/sectionBackground';
import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { StandardMediaQueries, ScreenSize } from 'components/services/mediaQueries';

export interface ValueAndtext {
    value: any;
    text: string;
    selected?: boolean;
}

export interface TitleDropdownAction {
    value: ValueAndtext;
    options: Array<ValueAndtext>;
    hasNone?: boolean;
}

export interface ViewAction {
    icon: string;
    title: string;
    name: string;
    active?: boolean;
}

@Component( {
    selector: 'uc-section',
    templateUrl: './section.template.html',
    styleUrls: [ './section.styles.less' ],
    providers: [ SectionBackground ]
} )
export class SectionComponent extends BaseUnicastComponent {
    @Input() title : string;

    @Input() background : number;

    @Input() bodyOnly : boolean = false;

    @Input() hideTitle : boolean = false;

    @Input() titleAction : string;

    @Input() iconAction : string;

    @Input() titleDropdowns: Array<TitleDropdownAction>;

    @Input() views: Array<ViewAction>;

    @Output() viewClick: EventEmitter<ViewAction> = new EventEmitter();

    @Output() titleClick : EventEmitter<void> = new EventEmitter();

    @Output() titleDropdownsSelect : EventEmitter<[number, any]> = new EventEmitter();

    private mediaQueries: StandardMediaQueries;
    public sectionBackground: SectionBackground;

    private actualSize: ScreenSize = ScreenSize.Desktop;

    public backgroundClass: string;

    public isIconActive: boolean;

    constructor ( sectionBackground : SectionBackground, mediaQueries: StandardMediaQueries ) {
        super();
        this.sectionBackground = sectionBackground;
        this.mediaQueries = mediaQueries;
        this.isIconActive = false;
        this.backgroundClass = '';
    }

    protected verifyIfIconIsActive(): boolean {
        return !!this.iconAction && (this.actualSize < ScreenSize.Tablet  || !this.titleAction);
    }

    protected _binds() {
        this._subscriptions.push(this.mediaQueries.screenSize.subscribe( (e) => {
            this.actualSize = e;
            this.isIconActive = this.verifyIfIconIsActive();
        }));
    }

    ngOnChanges ( changes ) {
        if ( changes[ 'background' ] ) {
            this.sectionBackground.level = this.background;

            this.backgroundClass = `background-${ this.sectionBackground.level }`;
        } else if ( changes[ 'titleAction' ] || changes[ 'iconAction' ]) {
            this.isIconActive = this.verifyIfIconIsActive();
        }
    }

    onTitleClicked () {
        this.titleClick.next();
    }

    onViewClicked(view : ViewAction) {
        for (const view of this.views) {
            view.active = false;
        }

        view.active = true;

        this.viewClick.next(view);
    }
}

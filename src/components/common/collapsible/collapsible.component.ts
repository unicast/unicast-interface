import { Component, Output, EventEmitter, Input, SimpleChanges } from '@angular/core';

import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';

@Component( {
    selector: 'uc-collapsible',
    templateUrl: './collapsible.template.html',
    styleUrls: [ './collapsible.styles.less' ]
} )
export class CollapsibleComponent extends BaseUnicastComponent{

    @Input()
    public collapsed: boolean = true;

    @Input()
    public collapsible: boolean = true;

    @Input()
    public iconOpened: string = 'angle-down';

    @Input()
    public iconClosed: string = 'angle-right';

    @Output()
    public itemVisibilityChange: EventEmitter<boolean> = new EventEmitter();

    @Output()
    public internalCollapseChange: EventEmitter<boolean> = new EventEmitter();

    public constructor() {
        super();
    }

    public onHostClick() {
        this.collapsible && this.toggleCollapsed();
    }

    public ngOnChanges(changes: SimpleChanges) {
        if (changes.collapsed) {
            this.itemVisibilityChange.emit(this.collapsed);
        }
    }

    private setHidden(value: boolean) {
        if (this.collapsed != value && this.collapsible) {
            this.collapsed = value;
            this.itemVisibilityChange.emit(this.collapsed);
            this.internalCollapseChange.emit(this.collapsed);
            return true;
        }
        return false;
    }

    collapseItem () {
        return this.setHidden(true);
    }

    expandItem () {
        return this.setHidden(false);
    }

    toggleCollapsed () {
        return this.collapsed ? this.expandItem() : this.collapseItem();
    }

}
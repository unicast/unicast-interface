import { Component, Input, SimpleChanges, OnChanges } from '@angular/core';

import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { Icon, IconComponent } from 'components/common/icons/icon/icon.component';

@Component( {
    selector: 'uc-icon-compound',
    templateUrl: './icon-compound.template.html',
    styleUrls: [ './icon-compound.styles.less' ]
} )
export class IconCompoundComponent extends BaseUnicastComponent implements OnChanges {
    @Input() asString: string;

    @Input() icons: Array<Icon> = [];

    ngOnChanges(change: SimpleChanges) {
        if (change.asString) {
            this.icons = this.transformStringToIcons(this.asString);
        }
    }

    transformStringToIcons(str: string): Array<Icon> {
        let arr: Array<string> = (str || '').split("|");
        return arr.map<Icon>((data: string) => IconComponent.transformStringToIcon(data));
    }
}
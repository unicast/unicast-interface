import { Component, Input, SimpleChanges } from '@angular/core';

import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';

export enum VerticalAlignment {
    Top = 'top',
    Middle = 'middle',
    Bottom = 'bottom'
}

export enum HorizontalAlignment {
    Left = 'left',
    Center = 'center',
    Right = 'right'
}

export type Icon = {name: string, size?: number, verticalPosition?: number, horizontalPosition?: number};

@Component( {
    selector: 'uc-icon',
    templateUrl: './icon.template.html',
    styleUrls: [ './icon.styles.less' ]
} )
export class IconComponent extends BaseUnicastComponent {
    @Input() asString: string;

    @Input() name: string = 'font-awesome';
    @Input() size: number;
    @Input() verticalPosition: number;
    @Input() horizontalPosition: number;

    ngOnChanges(change: SimpleChanges) {
        if (change.asString) {
            let icon = IconComponent.transformStringToIcon(this.asString);
            Object.assign(this, icon);
        }
    }

    static transformStringToIcon(str: string): Icon {
        const properties: Array<keyof Icon> = ['size', 'verticalPosition', 'horizontalPosition'];
        let narr: Array<string> = (str || '').split(",");
        let ret: Icon = {name: narr.shift()};

        let i = 0;
        while(narr.length > 0) {
            (ret as any)[properties[i++]] = Number.parseFloat(narr.shift());
        }
        return ret;
    }
}

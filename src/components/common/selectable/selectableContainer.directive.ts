import { Directive, Output, EventEmitter } from '@angular/core';
import { SelectableDirective } from 'components/common/selectable/selectable.directive';

@Directive( {
    selector: '[ucSelectableContainer]',
    exportAs: 'selectableContainer'
} )
export class SelectableContainerDirective {
    @Output( 'ucSelectableContainerChange' ) selectableChange : EventEmitter<SelectableDirective> = new EventEmitter();

    protected _selected : SelectableDirective;

    /** DO NOT CALL THIS METHOD DIRECTLY. Call `selectable.activate()` instead. */
    select ( selectable : SelectableDirective ) {
        if ( this._selected != selectable ) {
            if ( this._selected ) {
                this._selected.active = false;
            }

            this._selected = selectable;

            this.selectableChange.next( selectable );
        }
    }
}
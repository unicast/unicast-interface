import { Directive, Optional, Renderer2, ElementRef, NgZone } from '@angular/core';
import { StandardMediaQueries } from 'components/services/mediaQueries';
import { SelectableContainerDirective } from 'components/common/selectable/selectableContainer.directive';

@Directive( {
    selector: '[ucSelectable]',
    exportAs: 'selectable'

} )
export class SelectableDirective {
    public active : boolean = false;

    protected isMobile : boolean = false;

    public container : SelectableContainerDirective;

    protected zone : NgZone;

    constructor ( mediaQueries : StandardMediaQueries, @Optional() container : SelectableContainerDirective, zone : NgZone, element : ElementRef, renderer : Renderer2 ) {
        this.isMobile = mediaQueries.isMobile;
        this.container = container;
        this.zone = zone;

        zone.runOutsideAngular( () => {
            if ( this.isMobile ) {
                renderer.listen( element.nativeElement, 'mousedown', this.onMouseDown.bind( this ) );
                renderer.listen( element.nativeElement, 'touchstart', this.onMouseDown.bind( this ) );

                renderer.listen( element.nativeElement, 'mousemove', this.onMouseMove.bind( this ) );
                renderer.listen( element.nativeElement, 'touchmove', this.onMouseMove.bind( this ) );

                renderer.listen( element.nativeElement, 'mouseup', this.onMouseUp.bind( this ) );
                renderer.listen( element.nativeElement, 'touchend', this.onMouseUp.bind( this ) );

            } else {
                renderer.listen( element.nativeElement, 'mouseenter', this.onMouseEnter.bind( this ) );
                renderer.listen( element.nativeElement, 'mouseleave', this.onMouseLeave.bind( this ) );
            }
        } );
    }

    public activate () {
        if ( this.active == false && this.container != null ) {
            this.container.select( this );
        }

        this.active = true;
    }

    public deactivate () {
        if ( this.active == true && this.container != null ) {
            this.container.select( null );
        }

        this.active = false;
    }

    public toggle () {
        this.active = !this.active;

        if ( this.container != null ) {
            this.container.select( this.active ? this : null );
        }
    }

    // Mouse Interfaces
    onMouseEnter () {
        if ( !this.isMobile ) this.zone.run( () => this.activate() );
    }

    onMouseLeave () {
        if ( !this.isMobile ) this.zone.run( () => this.deactivate() );
    }

    // Touch Interfaces
    protected timeout : any = null;
    protected duration : number = 1000;

    protected cursor : [ number, number ] = null;

    protected getEventCursor ( event : MouseEvent | TouchEvent ) : [ number, number ] {
        if ( event.type.startsWith( 'mouse' ) ) {
            const mouseEvent = event as MouseEvent;

            return [ mouseEvent.clientX, mouseEvent.clientY ];
        } else if ( event.type.startsWith( 'touch' ) ) {
            const touchEvent = event as TouchEvent;

            return [ touchEvent.touches[ 0 ].clientX, touchEvent.touches[ 0 ].clientY ];
        } else {
            return null;
        }
    }

    onMouseDown ( event : MouseEvent | TouchEvent ) {
        if ( !this.isMobile || ( event.type == 'mousedown' && ( event as MouseEvent ).which !== 1 ) ) return;

        if ( this.timeout == null ) {
            this.cursor = this.getEventCursor( event );

            this.timeout = setTimeout( () => {
                this.timeout = null;

                this.cursor = null;

                this.zone.run( () => this.toggle() );
            }, this.duration );
        }
    }

    onMouseMove ( event : MouseEvent | TouchEvent ) {
        if ( !this.isMobile || ( event.type == 'mousemove' && ( event as MouseEvent ).which !== 1 ) ) return;

        if ( this.timeout == null ) return;

        const newCursor = this.getEventCursor( event );

        const xThres = Math.abs( newCursor[ 0 ] - this.cursor[ 0 ] ) > 10;
        const yThres = Math.abs( newCursor[ 1 ] - this.cursor[ 1 ] ) > 10;

        if ( xThres || yThres ) {
            clearTimeout( this.timeout );

            this.timeout = null;
        }
    }

    onMouseUp ( event : Event ) {
        if ( !this.isMobile ) return;

        if ( this.timeout ) {
            clearTimeout( this.timeout );

            this.timeout = null;

            this.cursor = null;
        } else {
            event.preventDefault();
            event.cancelBubble = true;
        }
    }
}

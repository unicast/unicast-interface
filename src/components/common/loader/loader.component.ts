import { Component, Input } from '@angular/core';

@Component( {
    selector: 'uc-loader',
    templateUrl: './loader.template.html',
    styleUrls: [ './loader.styles.less' ]
} )
export class LoaderComponent {
    @Input() type : string = 'folding-cube';

    @Input() size : number = 25;

    @Input() color : string = '#FFFFFF';

    ngAfterViewInit () {

    }
}
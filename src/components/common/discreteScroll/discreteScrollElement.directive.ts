import { Directive, ElementRef, Output, EventEmitter } from '@angular/core';

@Directive( {
    selector: '[ucDiscreteScrollerElement]'
} )
export class DiscreteScrollElementDirective {
    @Output( 'ucDiscreteScrollerElementSelected' ) elementSelected : EventEmitter<void> = new EventEmitter();

    @Output( 'ucDiscreteScrollerElementDeselected' ) elementDeselected : EventEmitter<void> = new EventEmitter();

    public selected : boolean = false;

    public element : ElementRef;

    public constructor ( element : ElementRef ) {
        this.element = element;
    }

    public select () {
        if ( !this.selected ) {
            this.selected = true;

            this.elementSelected.next();
        }
    }

    public deselect () {
        if ( this.selected ) {
            this.selected = false;

            this.elementDeselected.next();
        }
    }
}
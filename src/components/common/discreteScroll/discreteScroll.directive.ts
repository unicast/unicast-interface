import { Directive, HostListener, Input, ElementRef, ContentChildren, QueryList } from '@angular/core';
import { DiscreteScrollElementDirective } from 'components/common/discreteScroll/discreteScrollElement.directive';

export enum ScrollAlign {
    Left = 'left',
    Center = 'center',
    Right = 'right'
}

export enum ScrollDirection {
    Horizontal = 'horizontal',
    Vertical = 'vertical'
}

@Directive( {
    selector: '[ucDiscreteScroller]'
} )
export class DiscreteScrollDirective {
    @Input() enable : boolean;

    @Input() scrollEndTimeout : number = 200;

    @Input() scrollAlign : ScrollAlign = ScrollAlign.Center;

    @Input() scrollDirection : ScrollDirection = ScrollDirection.Horizontal;

    @ContentChildren( DiscreteScrollElementDirective ) elements !: QueryList<DiscreteScrollElementDirective>;

    protected selectedElement : DiscreteScrollElementDirective = null;

    protected timer : any = null;

    protected element : ElementRef;

    protected scrolling : boolean = false;

    public constructor ( element : ElementRef ) {
        this.element = element;
    }

    @HostListener( 'scroll', [ '$event' ] )
    onScroll ( event ) {
        if ( this.timer != null ) {
            clearTimeout( this.timer );
        }
        
        this.scrolling = true;

        this.timer = setTimeout( () => {
            this.timer = null;

            this.scrolling = false;

            const element = this.findClosestElement();

            if ( element != null ) {
                this.focusElement( element );
            }
        }, this.scrollEndTimeout );
    }

    getElementScroll ( element : HTMLElement ) : number {
        if ( this.scrollDirection == ScrollDirection.Horizontal ) {
            return element.scrollLeft;
        } else if ( this.scrollDirection == ScrollDirection.Vertical ) {
            return element.scrollTop;
        } else {
            return null;
        }
    }

    getContainerScroll () : number {
        return this.getElementScroll( this.element.nativeElement );
    }

    getElementPosition ( element : HTMLElement ) : number {
        if ( this.scrollDirection == ScrollDirection.Horizontal ) {
            return element.offsetLeft;
        } else if ( this.scrollDirection == ScrollDirection.Vertical ) {
            return element.offsetTop;
        } else {
            return null;
        }
    }

    getContainerPosition () : number {
        return this.getElementPosition( this.element.nativeElement );
    }

    getElementOuterSize ( element : HTMLElement ) : number {
        if ( this.scrollDirection == ScrollDirection.Horizontal ) {
            return element.offsetWidth;
        } else if ( this.scrollDirection == ScrollDirection.Vertical ) {
            return element.offsetHeight;
        } else {
            return null;
        }
    }
    
    getContainerOuterSize () : number {
        return this.getElementOuterSize( this.element.nativeElement );
    }

    getElementInnerSize ( element : HTMLElement ) : number {
        if ( this.scrollDirection == ScrollDirection.Horizontal ) {
            return element.scrollWidth;
        } else if ( this.scrollDirection == ScrollDirection.Vertical ) {
            return element.scrollHeight;
        } else {
            return null;
        }
    }

    getContainerInnerSize () : number {
        return this.getElementInnerSize( this.element.nativeElement );
    }


    getAlignPoint ( position : number, size : number, align : ScrollAlign ) : number {
        switch (align) {
            case ScrollAlign.Left: return position;
            case ScrollAlign.Center: return position + ( size / 2 );
            case ScrollAlign.Right: return position + size;
        }
    }

    getNeedle () : number {
        return this.getAlignPoint(
            this.getContainerScroll(),
            this.getContainerOuterSize(),
            this.scrollAlign
        );
    }

    getElementAnchor ( element : HTMLElement ) : number {
        return this.getAlignPoint(
            this.getElementPosition( element ) - this.getContainerPosition(),
            this.getElementOuterSize( element ),
            this.scrollAlign
        );
    }

    findElements () {
        return this.elements.toArray();
    }

    getContainerStart () {
        return this.getAlignPoint(
            0, 
            this.getContainerOuterSize(),
            this.scrollAlign
        );
    }

    getContainerEnd () {
        return this.getAlignPoint(
            this.getContainerInnerSize() - this.getContainerOuterSize(),
            this.getContainerOuterSize(),
            this.scrollAlign
        );
    }

    getSearchingNeedle () {
        const needle = this.getNeedle();

        if ( needle <= this.getContainerStart() ) {
            return 0;
        }

        if ( needle >= this.getContainerEnd() ) {
            return this.getContainerInnerSize();
        }

        return needle;
    }

    findClosestElement () {
        const needle = this.getSearchingNeedle();

        const elements = this.findElements();

        let closestDistance : number = Infinity, closestElement : DiscreteScrollElementDirective = null;

        for ( let element of elements ) {
            const anchor = this.getElementAnchor( element.element.nativeElement );

            const distance = Math.abs( needle - anchor );

            if ( closestDistance > distance ) {
                closestDistance = distance;
                closestElement = element;
            }
        }

        return closestElement;
    }

    focusElement ( element : DiscreteScrollElementDirective ) : void {
        const side = this.scrollDirection == ScrollDirection.Horizontal ? 'left' : 'top';
        
        this.element.nativeElement.scrollTo( {
            [ side ]: this.getContainerScroll() + this.getElementAnchor( element.element.nativeElement ) - this.getNeedle(),
            behavior: 'smooth'
        } );

        if ( this.selectedElement != null ) {
            this.selectedElement.deselect();
        }

        this.selectedElement = element;

        if ( this.selectedElement != null ) {
            this.selectedElement.select();
        }
    }
}
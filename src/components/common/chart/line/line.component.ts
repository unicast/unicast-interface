import { Component, Input } from '@angular/core';

import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';

//https://css-tricks.com/how-to-make-charts-with-svg/

export interface LineChartValue {
    caption: string | number;
    value: number;
}

interface ChartInformation {
    points: string;
    captions: Array<LineChartValue>;
}

@Component( {
    selector: 'uc-chart-line',
    templateUrl: './line.template.html',
    styleUrls: [ './line.styles.less' ]
} )
export class LineChartComponent extends BaseUnicastComponent {
    //Magic values retrieved from the basesource. In the future we can achieve a better
    //formula to get them according the other dynamic values.
    public readonly X_LABELS_DISTANCE = 100;
    public readonly X_TITLE_DISTANCE = 60;
    public readonly Y_LABELS_DISTANCE = 10;
    public readonly Y_TITLE_DISTANCE = 40;

    @Input() points: Array<LineChartValue | number> = [];

    @Input() height: number = 500;

    @Input() width: number = 800;

    @Input() margin: number = 90;

    @Input() xTitle: string = '';

    @Input() yTitle: string = '';

    public readonly _lineMargin: number = 5;
    public _internalPoints: string = "";
    public _footerSize: number = 130;
    public _leftSize: number = 90;
    public _chartInformation: ChartInformation;

    constructor ( ) {
        super();
    }

    protected _binds() {

    }

    ngOnChanges() {
        this._chartInformation = this.transformPoints(this.points, this.height, this.width, this.margin);
    }

    isValue(v: LineChartValue | number): v is LineChartValue {
        return !!v && v['value'] !== undefined;
    }

    transformPoints(values: Array<LineChartValue | number>, height: number, width: number, margin: number): ChartInformation {
        let points: Array<LineChartValue> = values.map<LineChartValue>((e, i) => this.isValue(e) ? e : {caption: i, value: e});
        let barSize = ((width - this._leftSize) / points.length);
        let pointsStr = '';
        let captions: Array<LineChartValue> = [];
        let i = 0;
        let xPosition = 0;
        let max: LineChartValue = points.reduce((previous, current) => previous.value > current.value ? previous : current);
        height -= this._footerSize;
        for(let v of points) {
            xPosition = (this._leftSize + (i++ * barSize));
            pointsStr += xPosition + ',' + (height - ((v.value * height) / max.value)) + ' ';
            captions.push({value: xPosition, caption: v.caption});
        }
        return { points: pointsStr, captions: captions };
    }

}


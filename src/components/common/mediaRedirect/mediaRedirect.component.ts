import { Component } from '@angular/core';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { SingularSubject } from 'components/common/helpers/singularSubject';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { NavigationManager } from 'components/services/navigationManager';
import { TopbarManager } from 'components/services/topbarManager';
import { PageTitle } from 'components/common/services/pageTitle';



@Component({
    templateUrl: './mediaRedirect.template.html',
    styleUrls: [ './mediaRedirect.styles.less' ]
})
export class MediaRedirectComponent  {
    private _serverApi: ServerApiManager;
    private _navigation: NavigationManager;
    private _activatedRoute: ActivatedRoute;

    constructor(servarApi: ServerApiManager, router: Router, activatedRoute: ActivatedRoute, topbar : TopbarManager, htmlTitle: PageTitle, location: Location) {
        this._serverApi = servarApi;
        this._navigation = new NavigationManager(router, location);
        this._activatedRoute = activatedRoute;

        let title = 'Media';

        topbar.setTitle(title);
        htmlTitle.setTitle(title);
    }

    ngOnInit() {
        let snapshot = this._activatedRoute.snapshot;
        let id = snapshot.paramMap.get('id');
        let kind = snapshot.data.kind || snapshot.paramMap.get('kind');

        let extras: NavigationExtras = { replaceUrl: true, queryParamsHandling: 'merge' };
        switch(kind) {
            case MediaKind.TvEpisode:
                return this._serverApi.getMedia().getShowApi().getEpisodeApi().getEpisode(id).subscribe((episode) => {
                    this._serverApi.getMedia().getShowApi().getSeasonApi().get(episode.tvSeasonId).subscribe( (season) => {
                        this._navigation.navigateToShow(season.tvShowId, season.number, episode.id, {}, extras );
                        return episode.id;
                    });
                });
            case MediaKind.TvSeason:
                return this._serverApi.getMedia().getShowApi().getSeasonApi().get(id).subscribe( (season) => {
                    this._navigation.navigateToShow(season.tvShowId, season.number, undefined, {}, extras);
                    return season.id;
                });
            case MediaKind.TvShow:
                this._navigation.navigateToShow(id, undefined, undefined, {} , extras);
                return SingularSubject.oneValue(id);
            case MediaKind.Movie:
                this._navigation.navigateToMovie(id, {}, extras);
                return SingularSubject.oneValue(id);
            case MediaKind.Custom:
                return '';
            default:
                return this._navigation.navigateToHome();
        }
    }
}
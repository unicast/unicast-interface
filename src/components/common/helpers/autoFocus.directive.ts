import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
    selector: "[uc-auto-focus]"
})
export class AutoFocusDirective {
    @Input('uc-auto-focus') focus = true;
    private elementRef: ElementRef;

    constructor(el: ElementRef) {
        this.elementRef = el;
    }

    ngOnInit() {
        if (this.focus !== false) {
            //Otherwise Angular throws error: Expression has changed after it was checked.
            window.setTimeout(() => {
                this.elementRef.nativeElement.focus(); //For SSR (server side rendering) this is not safe. Use: https://github.com/angular/angular/issues/15008#issuecomment-285141070)
            });
        }
    }

}
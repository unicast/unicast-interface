import { Pipe, PipeTransform } from '@angular/core';
import { MediaRecordArtSchema, MediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { MediaImage } from 'components/common/modal/galleryModal/galleryModal.component';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';

@Pipe({
    name: 'artWorkGallery'
})

export class ArtworkGalleryPipe implements PipeTransform {

    transform(value: MediaRecordSchema, others: Array<MediaRecordSchema> = [], otherType: string = 'poster'): Array<MediaImage> {
        let arr: Array<MediaImage> = [];
        let art = value.cachedArtwork;
        for (let k of Object.keys(art)) {
            if (art[k]) {
                arr.push({
                    id: k,
                    value: art[k],
                    type: value.kind,
                    art: k as keyof MediaRecordArtSchema
                });
            }
        }
        others = others || [];
        let i = 0;
        for (let other of others){
            i++;
            arr.push({
                id: other.id,
                value: other.cachedArtwork[otherType],
                type: MediaKind.TvSeason,
                art: i
            });
        }
        return arr;
    }

}
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Observable } from 'rxjs/Observable';

export abstract class SingularSubject {
    static create<T>(startValue?:T, distinct: boolean = true, buffer: number = 1): ReplaySubject<T>{
        let ret: ReplaySubject<T> = new ReplaySubject<T>(buffer);
        if (distinct){
            ret = <ReplaySubject<T>>ret.distinctUntilChanged<T>();
        }
        if (startValue !== undefined){
            ret.next(startValue);
        }
        return ret;
    }

    static oneValue<T>(value?:T): Observable<T> {
        return SingularSubject.create<T>(value, false).asObservable().first();
    }
}

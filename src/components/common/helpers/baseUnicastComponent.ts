import { OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

export class BaseUnicastComponent implements OnInit, OnDestroy {
    protected _subscriptions: Array<Subscription>;

    constructor() {
        this._subscriptions = [];
    }

    protected _binds () {  }

    /**
     * Be carefull when overriding, because of management of subscriptions on BaseUnicastComponent
     */
    ngOnInit () {
        this._binds();
    }

    protected clearSubscriptions() {
        this._subscriptions.forEach( sub => sub.unsubscribe() );
        this._subscriptions = [];
    }

    /**
     * Be carefull when overriding, because of management of subscriptions on BaseUnicastComponent
     */
    ngOnDestroy () {
        this.clearSubscriptions();
    }
}
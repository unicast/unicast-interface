import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'imageUrl'
})

export class ImageUrlPipe implements PipeTransform {
    transform(image: string, width?: number): string {
        if ( image && width != Infinity && !image.startsWith( "data:" ) ) {
            return image + '?width=' + width;
        } else {
            return image;
        }
    }
}
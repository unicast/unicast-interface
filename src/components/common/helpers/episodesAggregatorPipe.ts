import { Pipe, PipeTransform } from '@angular/core';

import { EpisodesAggregator } from 'components/common/helpers/episodesAggregator';
import { TvEpisodeMediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { TitleDropdownAction } from 'components/common/section/section.component';

@Pipe({
    name: 'episodesAggregator2Array'
})
export class EpisodesAggregatorPipe implements PipeTransform {
    transform(value: Array<TvEpisodeMediaRecordSchema> | EpisodesAggregator, byGroup?: Array<string>, filter?: Array<string> ): Array<TvEpisodeMediaRecordSchema> {
        let agg = EpisodesAggregator.from(value, byGroup);
        if (!filter) {
            return agg.getAllEpisodes();
        } else {
            return agg.getEpisodesForGroup(filter);
        }
    }

}

@Pipe({
    name: 'availableEpisodeSources2Dropdown'
})
export class AvailableEpisodeSources2DropdownPipe implements PipeTransform {
    transform(value: Array<TvEpisodeMediaRecordSchema> | EpisodesAggregator, episode: TvEpisodeMediaRecordSchema, byGroup?: Array<string> ): Array<TitleDropdownAction> {
        let agg = EpisodesAggregator.from(value, byGroup);
        let key = agg.buildGroupKey(episode);
        return [{ hasNone: false, value: {value: key, text: AvailableEpisodeSources2DropdownPipe.filterValueToText(key) },
                  options: agg.getAvailableGroups(episode.number).filter(e => e != key).map(e => ( {value: e, text: AvailableEpisodeSources2DropdownPipe.filterValueToText(e)})) }];
    }

    static filterValueToText(v: string): string {
        switch(v) {
            case '': return 'Other';
            case null: case undefined: return '<choose group>';
            default: return v;
        }
    }
}

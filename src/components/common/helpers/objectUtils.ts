export class ObjectUtils {
    public static resolve(obj: any, path: string | Array<string>): any{
        let paths: Array<string> = [].concat(path).join('.').split('.'); //Small tric to receive string | Array<string>
        let current: any = obj;
        while(paths.length && current !== undefined) {
            if (current instanceof Array) {
                let tmp: Array<string> = [];
                for (let pObj of current ){
                    tmp = tmp.concat(ObjectUtils.resolve(pObj, [].concat(paths)));
                }
                paths = [];
                current = tmp;
            } else if (typeof current !== 'object') {
                current = undefined;
            } else {
                let tmp: string = paths.shift();
                current = current[tmp];
            }

        }
        return current;
    }

    public static guidGenerator() {
        var S4 = function() {
           return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        };
        return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
    }
}
import { Directive } from '@angular/core';
import { AsyncClickGroupService } from 'components/common/helpers/asyncClickGroup.service';

@Directive({
    selector: '[uc-async-group]',
    providers: [ AsyncClickGroupService ]
})
export class AsyncClickGroupDirective {
    public service: AsyncClickGroupService;

    constructor (service: AsyncClickGroupService) {
        this.service = service;
    }
}
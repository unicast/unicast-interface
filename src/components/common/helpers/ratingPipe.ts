import { Pipe, Inject, LOCALE_ID } from '@angular/core';
import { DecimalPipe } from "@angular/common";

@Pipe({
    name: 'rating'
})

export class RatingPipe extends DecimalPipe {
    constructor(@Inject(LOCALE_ID) locale: string){
        super(locale);
    }

    transform(value: any, digits: string = '1.1-2'): string {
        return super.transform(value, digits);
    }
}
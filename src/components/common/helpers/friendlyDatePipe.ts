import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'friendlyDate'
})
export class FriendlyDatePipe implements PipeTransform {

    transform(value: string): any {
        return FriendlyDatePipe.transform(value);
    }

    static transform(value: string | Date): any {
        if ( value == null ) {
            return value;
        }

        const date = new Date( value );

        if ( FriendlyDatePipe.isDateToday( date ) ) {
            return 'Today';
        } else if ( FriendlyDatePipe.isDateYesterday( date ) ) {
            return 'Yesterday';
        } else {
            const month = date.toLocaleString( 'en-us', { month: 'short' } );

            const weekday = date.toLocaleString( 'en-us', { weekday: 'long' } );

            return `${ weekday }, ${ date.getDate().toString() } ${ month } ${ date.getFullYear() }`
        }
    }

    private static isDateToday ( d : Date ) {
        var td = new Date();

        return td.getDate() == d.getDate()
            && td.getMonth() == d.getMonth()
            && td.getFullYear() == d.getFullYear();
    }

    private static isDateYesterday ( d : Date ) {
        var td = new Date();

        return td.getDate() - 1 == d.getDate()
            && td.getMonth() - 1 == d.getMonth()
            && td.getFullYear() - 1 == d.getFullYear();
    }

}

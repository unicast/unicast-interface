import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class AsyncClickGroupService {
    private _currentActive: BehaviorSubject<unknown>;

    public get currentActive (): Observable<unknown> {
        return this._currentActive;
    }

    constructor () {
        this._currentActive = new BehaviorSubject<unknown>(null);
    }

    /**
     * Each async button should call this function when it is called and 
     * it is not waiting. If the function returns true, it can proceed, otherwise
     * it should disable itself and abort the operation
     * 
     * @param self The button that is trying to start an async operation
     */
    start (self: unknown): boolean {
        if (this._currentActive.value == null && self != null) {
            this._currentActive.next(self);
            return true;
        }

        return this._currentActive.value == self;
    }

    /**
     * Each async button should call this method after it has finished an async
     * operation, so that other buttons in the async group are enabled again
     * 
     * @param self The button that is reporting the stop of an async operation
     */
    stop (self: unknown) {
        if (this._currentActive.value == self && self != null) {
            // Null means there is no active button anymore
            this._currentActive.next(null);
        }
    }
}
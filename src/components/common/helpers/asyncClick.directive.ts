import { Directive, HostListener, Input, Output, EventEmitter, HostBinding, Optional } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { SingularSubject } from 'components/common/helpers/singularSubject';
import { AsyncClickGroupService } from 'components/common/helpers/asyncClickGroup.service';
import { Subscription } from 'rxjs/Subscription';

export interface WaitFor extends Event {
    waitFor (obs?: Observable<any>) : void
    release () : void;
    clickCount?: number;
    bufferedData?: any;
};

export interface BufferedEvent extends Event {
    clickCount: number;
    bufferedData: any;
};

@Directive({
  selector: '[uc-async]'
})
export class AsyncClickDirective {
    @Input( 'waitFor' ) waitFor: Observable<any>;
    @Input( 'uc-async-buffered-delay' ) bufferedDelay: number = 0;
    @Input( 'uc-async-buffered-count' ) bufferedCountEnabled: boolean = false;

    @Output( 'uc-async' ) click = new EventEmitter<WaitFor>(false);
    @Output( 'uc-async-buffered' ) bufferedClick = new EventEmitter<BufferedEvent>(false);

    @HostBinding('class.waiting') isWaiting: boolean = false;
    @HostBinding('class.delaying') isDelaying: boolean = false;
    @HostBinding('class.async-disabled') isDisabled: boolean = false;
    @HostBinding('class.clean') @Input() clean: boolean = false;

    private _bufferedCount: number = 0;
    private _bufferedData: unknown;
    private _bufferedEvent: Event;
    private _delayTimeout: any = null;

    private _groupService: AsyncClickGroupService;
    private _groupServiceSubscription: Subscription;

    constructor (@Optional() groupService: AsyncClickGroupService) {
        this.isWaiting = false;

        this._groupService = groupService;

        if (this._groupService != null) {
            this._groupServiceSubscription = this._groupService.currentActive.subscribe(active => {
                this.isDisabled = active != null && active != this;
            });
        }
    }

    ngOnDestroy () {
        if (this._groupServiceSubscription != null) {
            this._groupServiceSubscription.unsubscribe();
        }
    }

    private _flushBufferedEvent () {
        if (this._bufferedEvent != null && !this.isWaiting && !this.isDelaying) {
            const event = this._bufferedEvent;

            this._bufferedEvent = null;

            //Code is repeated to avoid two ifs
            //On that scenario, the observable will also have priority
            if (this.waitFor){
                this.isWaiting = true;
                this._buildEvent(event);
                (event as WaitFor).waitFor(this.waitFor);
            } else if (this.click.observers.length > 0){
                this.isWaiting = true;
                this._buildEvent(event);
                this.click.emit(event as WaitFor);
            }
            
            this._bufferedCount = 0;
            this._bufferedData = void 0;
        }
    }

    @HostListener('click', ['$event']) onclick(event: Event) {
        if (this.isDisabled) {
            return;
        }

        if (this._groupService != null && !this._groupService.start(this)) {
            this.isDisabled = true;
            return;
        }

        this._bufferedEvent = event;

        if (this.isWaiting){
            if (this.bufferedCountEnabled === true) {
                this._bufferedCount += 1;
            }
            if (this.bufferedClick.observers.length > 0) {
                this._buildBufferedEvent(event);

                this.bufferedClick.emit(event as BufferedEvent);
            }
            //If it is waiting avoid having multiple clicks
            event.stopPropagation();
            event.preventDefault();
        }

        if (this.isDelaying || this.bufferedDelay > 0) {
            if (this._delayTimeout !== null) {
                clearTimeout(this._delayTimeout);

                this._delayTimeout = null;
            }

            this.isDelaying = true;
            
            this._delayTimeout = setTimeout(() => {
                this._delayTimeout = null;
                this.isDelaying = false;

                this._flushBufferedEvent();
            }, this.bufferedDelay);
        } else {
            this._flushBufferedEvent();
        }
    }

    private _buildBufferedEvent(event: Event) {
        Object.defineProperties(event, {
            clickCount: {
                get: () => this._bufferedCount,
                set: (v) => this._bufferedCount = v,
            },
            bufferedData: {
                get: () => this._bufferedData,
                set: (v) => this._bufferedData = v,
            },
        });
    }

    private _buildEvent(event: Event) {
        const waitFor = (obs?: Observable<any>) => {
            obs = obs || SingularSubject.create(false).asObservable();
            let func = ( ) => {
                this.isWaiting = false;
       
                if (this._groupService != null) {
                    this._groupService.stop(this);
                }

                this._flushBufferedEvent();
            };
            obs.first().subscribe(func, func, func);
        };

        const newProperties: Partial<WaitFor> = {
            waitFor: waitFor,
            release: waitFor,
        };

        if (this.bufferedCountEnabled == true) {
            newProperties.clickCount = this._bufferedCount + 1;
        }
        
        if (this.bufferedClick.observers.length > 0) {
            newProperties.bufferedData = this._bufferedData;
        }

        Object.assign( event, newProperties );
    }

}
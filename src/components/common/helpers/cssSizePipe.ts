import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'cssSize'
})
export class CssSizePipe implements PipeTransform {

    transform(value: string | number, unit: string): string | number {
        if (value !== undefined && value !== null) {
            value = (value + '').trim() + unit;
        }
        return value;
    }
}
export type Point = {x: number, y: number};

export enum HtmlInputType {
    Text = 'text',
    TextArea = 'textarea',
    Number = 'number',
    Password = 'password',
    Email = 'email',
    Url = 'url',
    Time = 'time',
    Telephone = 'tel',
    Search = 'search',
    Month = 'month'
}

export class HtmlElementHelper {
    /**
     * @param event The mouse event that will be used to calculate the Mouse position
     *
     * @return {Point} The poistion of the mouse relative to the CurrentElement
     */
    public static calculateMousePosition(event: MouseEvent): Point;
    /**
     * @param event The mouse event that will be used to calculate the Mouse position
     * @param currentElement Optional, if you didn't want to use the currentTarget as currentElement
     *
     * @return {Point} The poistion of the mouse relative to the CurrentElement
     */
    public static calculateMousePosition(event: MouseEvent, currentElement: HTMLElement): Point;
    public static calculateMousePosition(event: MouseEvent, currentElement?: HTMLElement): Point{
        let totalOffsetX: number = 0;
        let totalOffsetY: number = 0;
        let canvasX: number = 0;
        let canvasY: number = 0;
        currentElement = currentElement || event.currentTarget as HTMLElement;

        do{
            let translate = HtmlElementHelper.getTranslatedPosition(currentElement);
            totalOffsetX += (currentElement.offsetLeft - currentElement.scrollLeft) + translate.x;
            totalOffsetY += (currentElement.offsetTop - currentElement.scrollTop) + translate.y;
        } while(currentElement = currentElement.offsetParent as HTMLElement);

        canvasX = event.pageX - totalOffsetX;
        canvasY = event.pageY - totalOffsetY;
        return {x:canvasX, y:canvasY};
    }

    private static getTranslatedPosition(element: HTMLElement): Point{
        var style = window.getComputedStyle(element);
        var matrix = new WebKitCSSMatrix(style.webkitTransform);
        return {x: matrix.m41, y: matrix.m42};
    }
}
import { Pipe, PipeTransform } from '@angular/core';
import { MediaRecordSchema, TvEpisodeMediaRecordSchema } from 'components/schemas/mediaRecord.schema';

@Pipe({
    name: 'episodeSubcaption'
})
export class EpisodeSubcaptionPipe implements PipeTransform {

    transform(value: MediaRecordSchema): string {
        let ret = '';
        if (this.isEpisode(value)) {
            ret = 'Season ' + value.seasonNumber + ', Episode ' + value.number;
        }
        return ret;
    }

    isEpisode(value: MediaRecordSchema): value is TvEpisodeMediaRecordSchema {
        return value['seasonNumber'] && value['number'];
    }
}
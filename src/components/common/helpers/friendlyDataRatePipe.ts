import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'friendlyDataRate'
})
export class FriendlyDataRatePipe implements PipeTransform {

    transform(value: number | null): any {
        return FriendlyDataRatePipe.transform(value);
    }

    static transform( value: number | null ): any {
        if ( value == null ) {
            return value;
        }

        if ( value <= 1000 ) {
            return `${this.round( value )} bits/s`;
        } else if ( value <= 1000 * 1000 ) {
            return `${this.round( value / 1000 )} kbits/s`;
        } else if ( value <= 1000 * 1000 * 1000 ) {
            return `${this.round( value / 1000 / 1000 )} mbits/s`;
        } else if ( value <= 1000 * 1000 * 1000 * 1000 ) {
            return `${this.round( value / 1000 / 1000 / 1000 )} gbits/s`;
        } else {
            return `${this.round( value / 1000 / 1000 / 1000 / 1000 )} tbits/s`;
        }
    }

    static round( value : number ) : number {
        return Math.round( value * 100 ) / 100;
    }
}

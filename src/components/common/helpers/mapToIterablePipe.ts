import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'mapToIterable'
})
export class MapToIterablePipe implements PipeTransform {
    transform(dict: Object, withKeys: boolean = false) {
        var a = [];
        for (var key in dict) {
            if (dict.hasOwnProperty(key)) {
                a.push(withKeys ? { key: key, val: dict[key] } : dict[key]);
            }
        }
        return a;
    }
}
import { Pipe, PipeTransform } from '@angular/core';
import { ObjectUtils } from 'components/common/helpers/objectUtils';

@Pipe({
    name: 'resolveObject'
})

export class ResolvePropertiesPipe implements PipeTransform {
    transform(value: {}, tags?: Array<string>, defaultTags: Array<string> = []): Array<string> {
        let values = [];
        for (let tag of (tags || defaultTags)) {
            values = values.concat(ObjectUtils.resolve(value, tag) );
        }
        values = values.filter( e => e !== undefined );
        return values;
    }
}
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'friendlySize'
})
export class FriendlySizePipe implements PipeTransform {

    transform(value: number | null): any {
        return FriendlySizePipe.transform(value);
    }

    static transform( value: number | null ): any {
        if ( value == null ) {
            return value;
        }

        if ( value <= 1024 ) {
            return `${this.round( value )} B`;
        } else if ( value <= 1024 * 1024 ) {
            return `${this.round( value / 1024 )} KB`;
        } else if ( value <= 1024 * 1024 * 1024 ) {
            return `${this.round( value / 1024 / 1024 )} MB`;
        } else if ( value <= 1024 * 1024 * 1024 * 1024 ) {
            return `${this.round( value / 1024 / 1024 / 1024 )} GB`;
        } else {
            return `${this.round( value / 1024 / 1024 / 1024 / 1024 )} TB`;
        }
    }

    static round( value : number ) : number {
        return Math.round( value * 100 ) / 100;
    }
}

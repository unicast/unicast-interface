import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'orDefaultValue'
})

export class OrDefaultValuePipe implements PipeTransform {
    transform<T>(value: T, defaultValue: T): T {
        return ( value === undefined || value === null ) ? defaultValue : value;
    }
}
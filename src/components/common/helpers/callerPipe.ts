import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'call'
})
/**
 * This pipe is usefull to call a method on evaluated expressions from angular
 * without the need of being constantly recalled
 */
export class CallerPipe implements PipeTransform {
    transform(value: any, method: (value: any) => any): any {
        return method(value);
    }
}
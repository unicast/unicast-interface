import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'friendlyDuration'
})
export class FriendlyDurationPipe implements PipeTransform {

    transform(value: number | null): any {
        return FriendlyDurationPipe.transform(value);
    }

    static transform( value: number | null ): any {
        if ( value == null ) {
            return value;
        }

        if ( value <= 60 ) {
            return `${this.round( value )} sec`;
        } else if ( value <= 60 * 60 ) {
            return `${this.round( value / 60 )} min`;
        } else {
            const minutes = value % ( 60 * 60 ) % 60;
            if ( minutes > 0 ) {
                return `${Math.floor( value / 60 / 60 )} h ${ Math.round( minutes ) } min`;
            } else {
                return `${Math.floor( value / 60 / 60 )} h`;
            }
        }
    }

    static round( value : number ) : number {
        return Math.round( value * 100 ) / 100;
    }
}

import { ObjectUtils } from 'components/common/helpers/objectUtils';
import { TvEpisodeMediaRecordSchema, PlayableMediaRecordSchema } from 'components/schemas/mediaRecord.schema';

export interface ColisionMediaResolver {
    getKey(media: PlayableMediaRecordSchema): string;
}

class ColisionPathResolver implements ColisionMediaResolver {
    getKey(media: PlayableMediaRecordSchema): string {
        const file = media.sources[0].id;
        const unixPath = file.split('/');
        const windowsPath = file.split('\\');
        const index = file.lastIndexOf( windowsPath.length > unixPath.length ? '\\' : '/');
        return index > -1 ? file.substring(0, index) : file;
    }
}

class ColisionSequenceResolver implements ColisionMediaResolver {
    private counters: Map<number, Map<string, number>> = new Map();

    getKey(media: TvEpisodeMediaRecordSchema): string {
        if (!this.counters.has(media.number)){
            this.counters.set(media.number, new Map());
        }

        const ids = this.counters.get(media.number);
        if(!ids.has(media.id)) {
            ids.set(media.id, ids.size + 1);
        }

        return ids.get(media.id).toString();

    }
}

export class EpisodesAggregator {
    private groupBy: Array<string>;
    private groupedEpisodes: Map<string, Map<number, TvEpisodeMediaRecordSchema>>;
    private groupEpisodeNumbers: Map<string, Set<number>>;
    private collisions: Set<string>;
    private collisionResolvers: Array<ColisionMediaResolver>;
    private episodesNumbers: Set<number>;

    constructor(episodesList: Array<TvEpisodeMediaRecordSchema>, groupBy: Array<string> = [ 'source' ]) {
        this.groupBy = groupBy;
        this.groupedEpisodes = new Map();
        this.groupEpisodeNumbers = new Map();
        this.collisions = new Set();
        this.collisionResolvers = [ new ColisionPathResolver(), new ColisionSequenceResolver() ];

        this.episodesNumbers = new Set();
        this.addEpisodes(episodesList);
    }

    public static from(value: Array<TvEpisodeMediaRecordSchema> | EpisodesAggregator, byGroup?: Array<string>): EpisodesAggregator {
        return value instanceof EpisodesAggregator ? value : new EpisodesAggregator(value, byGroup);
    }

    private addEpisodes(episodesList: Array<TvEpisodeMediaRecordSchema>){
        for( let episode of episodesList ) {
            let key = this.buildGroupKey(episode);
            if (!this.groupedEpisodes.has(key)) {
                this.groupedEpisodes.set(key, new Map());
                this.groupEpisodeNumbers.set(key, new Set());
            }
            if (this.groupEpisodeNumbers.get(key).has(episode.number)) {
                //Mark this key as not final, has it has collisions
                this.collisions.add(key);
                //Calculate the new key for that specific episode
                let newKey = this.buildGroupKey(episode);
                if (key == newKey) { //No enough CollisionResolvers to handle that collision, as both keys are still equal
                    console.error('The following episode will not be added to the list as it has too many collisions on the key to be distinguished:', episode);
                } else {
                    //Retrieve all the episodes currently under the old key, and map them under the new key rules
                    this.addEpisodes([...this.groupedEpisodes.get(key).values()].concat(episode));
                    //Remove stats from that old key, as it isn't final any longer
                    this.groupEpisodeNumbers.delete(key)
                    this.groupedEpisodes.delete(key);
                }

            } else {
                this.episodesNumbers.add(episode.number);
                this.groupedEpisodes.get(key).set(episode.number, episode);
                this.groupEpisodeNumbers.get(key).add(episode.number);
            }
        }
    }

    buildGroupKey(episode: TvEpisodeMediaRecordSchema) {
        let quality = episode.metadata;
        let key = this.groupBy.map(e => ObjectUtils.resolve(quality, e)).filter(e => e).join('+');
        for(let i = 0; this.collisions.has(key) && i < this.collisionResolvers.length; i++) {
            key = key + '+' + this.collisionResolvers[i].getKey(episode);
        }
        return key;
    }

    getAvailableGroups(): Array<string>;
    getAvailableGroups(episodeNumber: number): Array<string>;
    getAvailableGroups(episodeNumber?: number): Array<string> {
        if (episodeNumber) {
            let ret = [];
            this.groupEpisodeNumbers.forEach( (v, k) => {
                if (v.has(episodeNumber)) {
                    ret.push(k);
                }
            });
            return ret;
        } else {
            return new Array(...this.groupedEpisodes.keys());
        }
    }

    getEpisodeOfGroup(episodeNumber: number, group: string) {
        return this.groupedEpisodes.has(group) ? this.groupedEpisodes.get(group).get(episodeNumber) : null;
    }

    isGroupComplete(groups: Array<string>): boolean {
        let newSet: Set<number> = new Set();
        groups = this.cleanGroupsToValid(groups);
        for (let group of groups) {
            this.groupEpisodeNumbers.get(group).forEach(e => newSet.add(e));
            if (newSet.size >= this.episodesNumbers.size) {
                return true;
            }
        }
        return false;
    }

    getEpisodesForGroup(groups: Array<string>): Array<TvEpisodeMediaRecordSchema> {
        groups = this.cleanGroupsToValid(groups);
        if (groups.length == 0) {
            return this.getAllEpisodes();
        }
        let ret: Array<TvEpisodeMediaRecordSchema> = [];
        for (let number of this.episodesNumbers) {
            let key = groups.find(e => this.groupEpisodeNumbers.get(e).has(number));
            if (key !== undefined) { //Episode can still be non-existent on all the selected groups
                ret.push(this.groupedEpisodes.get(key).get(number));
            }
        }
        return ret;
    }

    private cleanGroupsToValid(groups: Array<string>): Array<string> {
        return groups.filter(e => this.groupEpisodeNumbers.has(e));
    }

    getAllEpisodes(): Array<TvEpisodeMediaRecordSchema> {
        let ret: Array<TvEpisodeMediaRecordSchema> = [];
        for (let episode of this.episodesNumbers) {
            for (let [, group] of this.groupedEpisodes) {
                if (group.has(episode)) {
                    ret.push(group.get(episode));
                }
            }
        }
        return ret;
    }

    *iterateAllEpisodes(): Iterator<TvEpisodeMediaRecordSchema> {
        for (let episode of this.episodesNumbers) {
            for (let [, group] of this.groupedEpisodes) {
                if (group.has(episode)) {
                    yield group.get(episode);
                }
            }
        }
    }
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'min'
})
export class MinPipe implements PipeTransform {
    transform(value: number, defaultValue: number) {
        return Math.min(value, defaultValue);
    }
}

@Pipe({
    name: 'max'
})
export class MaxPipe implements PipeTransform {
    transform(value: number, defaultValue: number) {
        return Math.max(value, defaultValue);
    }
}
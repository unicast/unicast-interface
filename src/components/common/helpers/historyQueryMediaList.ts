import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';

export class HistoryQueryMediaList {
    private kind: string;
    private id: string;
    private title: string;

    public constructor(kind: string, id: string, title?: string) {
        this.kind = kind;
        this.id = id;
        this.title = title;
    }

    public getId() {
        return this.id;
    }

    public getkind() {
        return this.kind;
    }

    public getTitle() {
        return this.title;
    }

    static of(media: MediaRecordSchema) {
        return new HistoryQueryMediaList(media.kind, media.id, media.title);
    }
    toJSON() {
        return {
            kind: this.kind,
            id: this.id,
            title: this.title
        };
    }
    toString() {
        return this.kind + ',' + this.id;
    }
}

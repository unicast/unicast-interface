export class Time{
    private _hours: number;
    private _minutes: number;
    private _seconds: number;
    private _milliseconds: number;
    private _isNegative: boolean;

    constructor(hh: number = 0, mm: number = 0, ss: number= 0, ms: number = 0, negative: boolean = false){
        this._hours = Math.abs(hh);
        this._minutes = Math.abs(mm);
        this._seconds = Math.abs(ss);
        this._milliseconds = Math.abs(ms);
        this._isNegative = negative;
    }

    static fromString (full: string){
        let negative = full.charAt(0) == '-';
        var aux = full.split(":");
        return new Time( (aux.length > 0 ? Number(aux[0]) : 0),     //Hours
                         (aux.length > 1 ? Number(aux[1]) : 0),     //Minutes
                         (aux.length > 2 ? Number(aux[2]) : 0),     //Seconds
                         (aux.length > 3 ? Number(aux[3]) : 0),     //Milliseconds
                         (negative)
                        );
    }

    static fromMilliseconds (milliseconds: number): Time{
        let absolute = Math.abs(milliseconds);
        return new Time((Math.floor(absolute / (1000*60*60)) % 24),    //Hours
                        (Math.floor(absolute / (1000*60)) % 60),       //Minutes
                        (Math.floor(absolute / 1000) % 60),            //Seconds
                        (absolute % 1000),                             //Milliseconds
                        (milliseconds < 0)                             //Negative
                    );
    }

    static fromSeconds (secs: number): Time{
        return Time.fromMilliseconds(secs * 1000);
    }

    static min ( first : Time, ...times : Time[] ) : Time {
        return times.reduce( ( a, b ) => a.isBefore( b ) ? a : b, first );
    }

    static max ( first : Time, ...times : Time[] ) : Time {
        return times.reduce( ( a, b ) => a.isAfter( b ) ? a : b, first );
    }

    static clamp ( min : Time, max : Time, value : Time ) : Time {
        return Time.min( Time.max( value, min ), max );
    }

    plus (other: Time): Time{
        return Time.fromMilliseconds( this.toMilliseconds() + other.toMilliseconds() );
    }

    minus (other: Time): Time{
        return Time.fromMilliseconds( this.toMilliseconds() - other.toMilliseconds() );
    }

    isNegative (): boolean{
        return this._isNegative;
    }

    isAfter (other: Time): boolean{
        return this.toMilliseconds() > other.toMilliseconds();
    }

    isBefore (other: Time): boolean{
        return this.toMilliseconds() < other.toMilliseconds();
    }

    invert (): Time {
        return new Time( this._hours, this._minutes, this._seconds, this._milliseconds, !this.isNegative() );
    }

    toSeconds (): number{
        return (this._isNegative ? -1 : 1) *
               (((this._hours * 60)
                + this._minutes) * 60
                + this._seconds);
    }

    toMilliseconds (): number{
        return this.toSeconds() * 1000 + this._milliseconds;
    }

    toString (withMilliseconds: boolean = false): string{
        var str = (this._isNegative ? '-' : '') +
            this._pad(this._hours, 2) + ':'   +
            this._pad(this._minutes, 2) + ':' +
            this._pad(this._seconds, 2);
        if (withMilliseconds){
            str += ':' + this._pad(this._milliseconds, 2);
        }
        return str;
    }

    _pad (n: string | number, width: number, z?: string): string {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    private static _convertTo(op: string, value: Time | number){
        return (value instanceof Time ? value[op]() : value );
    }

    public static convertToMilliseconds(value: Time | number): number{
        return Time._convertTo('toMilliseconds', value);
    }

    public static convertToSeconds(value: Time | number): number{
        return Time._convertTo('toSeconds', value);
    }
}
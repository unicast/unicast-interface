import { Observable } from 'rxjs/Observable';

export class ObservablesHelper {
    public static merge(...list: Array<Observable<any>>): Observable<any>{
        let it: Iterator<Observable<any>> = list.values();
        let obs: IteratorResult<Observable<any>> = it.next();
        let ret: Observable<any> = obs.value;
        while (obs){
            obs = it.next()
            ret = ret.merge(obs.value);
        }
        return ret;
    }
}
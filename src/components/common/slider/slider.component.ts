import { Component, Input, Output, EventEmitter } from '@angular/core';

import { HtmlElementHelper } from 'components/common/helpers/htmlElementHelper';
import { DelayedExecutions } from 'components/common/services/delayedExecutions';
import { BackgroundWorker } from 'components/services/backgroundWorker';

export interface IProgressChangedEvent<T> {
    bar: number;
    outsideChange: boolean;
    previousPecentage: number;
    actualPercentage: number;
    actualValue: T;
}

export interface IBarInfo<T> {
    name: string;
    actual: T;
    speed: number;
    maximum: T;
}

@Component({
    selector: 'uc-slider',
    templateUrl: './slider.template.html',
    styleUrls: [ './slider.styles.less' ]
})
export class SliderComponent<T> {
    protected readonly MOVING_MS: number = 300;

    @Input() selectedBar: number = 0;
    @Input() bars: Array<IBarInfo<T>> = [];
    @Output() progressChanged: EventEmitter<IProgressChangedEvent<T>> = new EventEmitter();

    protected _moving: any;
    protected _delayed: DelayedExecutions;
    protected _backgroundWorker: BackgroundWorker;
    protected _isMoving: boolean;

    public _firstBar: number = 0;
    public _lastBar: number = 0;
    public _percentage: Array<number> = [0];
    public _differences: Array<number> = [0];
    public _isPreview: boolean = false;
    public _previewValue: T;
    public _maximum: T;

    public printValue = this._printValue.bind(this);

    constructor(delayed: DelayedExecutions, backgroundWorker: BackgroundWorker){
        this._delayed = delayed;
        this._backgroundWorker = backgroundWorker;

        this._backgroundWorker.isVisibleObservable.subscribe( () => {
            this._alertChanged();
        });

    }

    private _alertChanged(){
        this.ngOnChanges({ bars: this.bars });
    }

    startMoving(bars?: Array<IBarInfo<T>>, updateTime: number = this.MOVING_MS) {
        let increments: Array<number> = [];
        //Calculate the numeric increments to only do it one time, until the speed changes
        (bars || this.bars).forEach( (v) => increments.push(v.speed * updateTime) );

        return this._delayed.setInterval(() => {
            this.updateActuals(increments);
            //Emulate outside, since it is like I'm moving
            this._calculatePercentage(true);
            if (this._backgroundWorker.isHidden() && this._moving){
                this._delayed.clearInterval(this._moving);
            }
        }, updateTime);
    }

    ngOnDestroy() {
        this._delayed.clearInterval(this._moving);
    }

    private updateActuals(increments: Array<number>) {
        increments.forEach((v, k) => {
            let bar: IBarInfo<T> = this.bars[k];
            let newValue: number = this._getValue(bar.actual) + v;
            bar.actual = (newValue < this._getValue( bar.maximum ) ? this._createNewValue( newValue ) : bar.maximum);
        });
    }

    ngOnChanges(change){
        if (change.bars) {
            this._percentage = this.bars.map<number>((value: IBarInfo<T>, index:number) => index);
            this._differences = [].concat(this._percentage);
            this._isMoving = false;
            for (let bar of this.bars){
                //Vefify if the maximum, isn't the greatest value
                if (this._maximum === undefined || this._compareValue(this._maximum, bar.maximum) < 0) {
                    this._maximum = bar.maximum;
                }
                this._isMoving = this._isMoving || (bar.speed > 0);
            }

            this._delayed.clearInterval(this._moving);

            if (this._isMoving){
                this._moving = this.startMoving( this.bars );
            }
        }
        this._calculatePercentage(true);
    }

    protected _printValue(value: T): string {
        return (value || "").toString();
    }

    protected _createNewValue(value: number): T{
        return undefined;
    }

    protected _getValue(value: T): number{
        return 0;
    }

    /**
     * Returns the way of compare like C or Java
     *
     * @return a negative integer, zero, or a positive integer as the first value is less than, equal to,
     * or greater than the specified second value.
     */
    protected _compareValue(value1: T, value2: T): number {
        return this._getValue(value1) - this._getValue(value2);
    }

    protected _getPercentage(value: number, max?: number){
        if (max == undefined) {
            max = this._getValue(this._maximum);
        }
        return (value / max) * 100;
    }

    protected _calculatePercentage(outside: boolean = false){
        let notEmpty: Array<number> = [];
        for (let key: number = 0; key < this.bars.length; key++){
            let previous: number = this._percentage[key];
            this._percentage[key] = this._getPercentage(this._getValue(this.bars[key].actual));
            this._differences[key] = this._percentage[key] - (this._percentage[key - 1] || 0);
            if (this._differences[key] > 0){
                notEmpty.push(key);
            }
            if (Math.abs(this._percentage[key] - previous) >= 0.01){
                this.progressChanged.emit({
                    bar: key,
                    outsideChange: outside,
                    previousPecentage: previous,
                    actualPercentage: this._percentage[key],
                    actualValue: this.bars[key].actual
                });
            }
        }
        if (notEmpty.length > 0) {
            this._firstBar = notEmpty[0];
            this._lastBar = notEmpty[notEmpty.length - 1];
        }
    }



    private _calculateValue(event: MouseEvent): T{
        let position = HtmlElementHelper.calculateMousePosition(event);
        let percentage: number = position.x / (event.currentTarget as HTMLElement).offsetWidth;
        return this._createNewValue(this._getValue(this._maximum) * percentage);
    }

    public mouseMove(event: any){
        this._isPreview = true;
        this._previewValue = this._calculateValue(event);
    }

    public mouseOut(event: Event){
        this._isPreview = false;
    }

    public clicked(event: any){
        this.bars[this.selectedBar].actual = this._calculateValue(event);
        this._calculatePercentage(false);
    }

}
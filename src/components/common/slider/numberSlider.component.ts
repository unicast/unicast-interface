import { Component, Input } from '@angular/core';

import { SliderComponent } from 'components/common/slider/slider.component';
import { DelayedExecutions } from 'components/common/services/delayedExecutions';
import { BackgroundWorker } from 'components/services/backgroundWorker';

@Component({
    selector: 'uc-number-slider',
    templateUrl: './slider.template.html',
    styleUrls: [ './slider.styles.less' ]
})
export class NumberSliderComponent extends SliderComponent<number> {
    @Input() precision: number = 0;

    constructor(delayed: DelayedExecutions, backgroundWorker: BackgroundWorker){
        super(delayed, backgroundWorker);
    }

    protected _createNewValue(value: number): number{
        return (value || 0);
    }

    protected _getValue(value: number): number{
        return (value || 0);
    }

    protected _printValue(value: number){
        return value.toFixed(this.precision);
    }


}
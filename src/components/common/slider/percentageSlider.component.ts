import { Component, Input } from '@angular/core';

import { NumberSliderComponent } from 'components/common/slider/numberSlider.component';
import { DelayedExecutions } from 'components/common/services/delayedExecutions';
import { BackgroundWorker } from 'components/services/backgroundWorker';


@Component({
    selector: 'uc-percentage-slider',
    templateUrl: './slider.template.html',
    styleUrls: [ './slider.styles.less' ]
})
export class PercentageSliderComponent extends NumberSliderComponent {
    @Input() precision: number = 2;


    constructor(delayed: DelayedExecutions, backgroundWorker: BackgroundWorker){
        super(delayed, backgroundWorker);
    }

    protected _printValue(value: number){
        return this._getPercentage(value || 0).toFixed(this.precision) + "%";
    }


}
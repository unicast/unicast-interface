import { Component } from '@angular/core';

import { SliderComponent } from 'components/common/slider/slider.component';

//Helpers
import { Time } from 'components/common/helpers/time';
import { DelayedExecutions } from 'components/common/services/delayedExecutions';
import { BackgroundWorker } from 'components/services/backgroundWorker';


@Component({
    selector: 'uc-time-slider',
    templateUrl: '../slider/slider.template.html',
    styleUrls: [ '../slider/slider.styles.less', '../slider/timeSlider.styles.less' ]
})
export class TimeSliderComponent extends SliderComponent<Time> {

    constructor(delayed: DelayedExecutions, backgroundWorker: BackgroundWorker){
        super(delayed, backgroundWorker);
    }

    protected _createNewValue(value: number): Time{
        return Time.fromMilliseconds(value || 0);
    }

    protected _getValue(value: Time): number{
        return (value ? value.toMilliseconds() : 0);
    }

}
import { Component, Output, EventEmitter, ElementRef, ChangeDetectorRef } from '@angular/core';

import { ModalWrapper } from 'components/common/modal/modalWrapper';
import { CloseReason } from 'components/common/modal/modal.component';
import { Observable } from 'rxjs/Observable';

export interface ButtonClick {
    ctx: {};
    type: 'ok' | 'cancel';
}

@Component( {
    selector: 'uc-confirm-modal',
    templateUrl: './confirmModal.template.html'
} )
export class ConfirmModalComponent extends ModalWrapper {
    @Output()
    cancel: EventEmitter<ButtonClick> = new EventEmitter();

    @Output()
    confirm: EventEmitter<ButtonClick> = new EventEmitter();

    constructor ( elementRef : ElementRef, changeDetectorRef : ChangeDetectorRef ) {
        super(elementRef, changeDetectorRef);
    }

    ngOnInit() {
        super.ngOnInit();
        this._subscriptions.push(this.closingEvent.filter(e => e != CloseReason.ModalButton).subscribe(e => this.callCancel()) );
    }

    callConfirm() {
        this.confirm.emit( {ctx: this.ctx, type: 'ok'} );
    }

    close () : void {
        super.close();
        this.callCancel();
    }

    callCancel() {
        this.cancel.emit ( {ctx: this.ctx, type: 'cancel'} );
    }

    open(ctx = {}): Observable<ButtonClick> {
        super.open(ctx);
        return this.confirm.merge<ButtonClick>(this.cancel).first();
    }


}
import { Component, ElementRef, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';

import { ModalWrapper } from 'components/common/modal/modalWrapper';
import { MediaRecordArtSchema } from 'components/schemas/mediaRecord.schema';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { Observable } from 'rxjs/Observable';
import { SingularSubject } from 'components/common/helpers/singularSubject';


export type MediaImage = { id: string, type: MediaKind, value: string, art: keyof MediaRecordArtSchema | number, isLeaf?: boolean }

export type SelectedImage = { parent: MediaImage, selected: MediaImage };

export interface GalleryImageFetcher {
    fecth(mediaImage?: MediaImage): Observable<Array<MediaImage>>;
}

@Component( {
    selector: 'uc-gallery-modal',
    templateUrl: './galleryModal.template.html',
    styleUrls: ['./galleryModal.styles.less']
} )
export class GalleryModalComponent extends ModalWrapper {
    private static readonly HOMEKEY: string = '';
    selectingType: boolean = true;

    @Input()
    galleryFetcher: GalleryImageFetcher = { fecth: () => SingularSubject.oneValue([])};

    @Input()
    mediaImages: Array<MediaImage>;

    @Input()
    gallery: Array<MediaImage> = [];

    @Output()
    selectedType: EventEmitter<SelectedImage> = new EventEmitter<SelectedImage>();

    current: Array<MediaImage> = [];

    parent: MediaImage = undefined;

    cache: Map<string, Array<MediaImage>> = new Map<string, Array<MediaImage>>();

    constructor ( elementRef : ElementRef, changeDetectorRef : ChangeDetectorRef ) {
        super(elementRef, changeDetectorRef);
    }

    ngOnInit() {
        super.ngOnInit();
        //this.modal.closeOnOutsideClick = false;
        this.closeOnButtonClick = false;
        this.cache.set(GalleryModalComponent.HOMEKEY, this.mediaImages);
    }

    clickedImage(image: MediaImage) {
        if (this.cache.has(image.id)){
            this.setImages(image, this.cache.get(image.id));
        } else if (image.isLeaf == undefined || !image.isLeaf) {
            this.galleryFetcher.fecth(image).first().subscribe( e => {
                this.cache.set(image.id, e);
                this.setImages(image, e);
            });
        }
        this.selectedType.next({parent: this.parent, selected: image});
        this.parent = image;
    }

    setImages(image: MediaImage, e: Array<MediaImage>) {
        this.selectingType = !this.selectingType;
        this.mediaImages = e;
    }

    goHome() {
        this.mediaImages = this.cache.get(GalleryModalComponent.HOMEKEY);
    }

    close() {
        this.mediaImages = undefined;
        super.close();
    }

    open (ctx = {}) : void {
        this.current = [];
        this.parent = undefined;
        //When passing both fetcher and images, the images will prevail for the first request
        if (!this.mediaImages) {
            this.galleryFetcher.fecth().first().subscribe(e => {
                this.mediaImages = e || [];
                this.cache.set(GalleryModalComponent.HOMEKEY, this.mediaImages);
            });
        }
        super.open(ctx);
    }

}
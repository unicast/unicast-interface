import { ElementRef, ChangeDetectorRef, ViewChild, Component } from '@angular/core';

import { ModalComponent, CloseReason } from 'components/common/modal/modal.component';

@Component( {
    selector: 'hd-uc-modal-wrapper',
    templateUrl: './modal.template.html',
    styleUrls: ['./modal.styles.less']
} )
export abstract class ModalWrapper extends ModalComponent {
    @ViewChild( 'modal' )
    modal : ModalComponent;

    constructor ( elementRef : ElementRef, changeDetectorRef : ChangeDetectorRef ) {
        super(elementRef, changeDetectorRef);
    }

    ngOnInit() {
        super.ngOnInit();
        this.closingEvent = this.modal.closingEvent;
    }

    public readonly show = (ctx = {}) => { //Alias for open. Not overridable
        this.open(ctx);
    }

    open (ctx = {}) : void {
        this.modal.open( ctx );
        this.ctx = this.modal.ctx;
    }

    close(reason?: CloseReason) {
        this.modal.close(reason);
    }

}
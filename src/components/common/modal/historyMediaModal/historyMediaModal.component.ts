import { Component, ElementRef, ChangeDetectorRef, Input } from '@angular/core';

import { HistoryRecordSchema } from 'components/schemas/mediaRecord.schema';
import { ModalWrapper } from 'components/common/modal/modalWrapper';
import { Time } from 'components/common/helpers/time';


@Component( {
    selector: 'uc-media-history-modal',
    templateUrl: './historyMediaModal.template.html',
    styleUrls: ['./historyMediaModal.styles.less']
} )
export class HistoryMediaModalComponent extends ModalWrapper {

    session: HistoryRecordSchema;
    runtime: number;

    @Input()
    title: string = 'BUTTONS.ACTIONS.INFO';

    constructor ( elementRef : ElementRef, changeDetectorRef : ChangeDetectorRef ) {
        super(elementRef, changeDetectorRef);
    }

    open (ctx: HistoryRecordSchema) : void {
        super.open(ctx);
        this.session = ctx;
        this.runtime = this.session.record.runtime || (this.session.positionHistory || []).map(e => e.end).reduce((previous, current) => previous > current ? previous : current, 0);
    }

    showTime = (value: number) => {
        return Time.fromSeconds(value).toString();
    }

}
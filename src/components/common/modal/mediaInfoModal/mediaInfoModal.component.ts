import { Component, ElementRef, ChangeDetectorRef, Input, OnChanges, SimpleChanges } from '@angular/core';

import { MediaMetadataAudioSchema, MediaMetadataSubtitlesSchema, MediaMetadataVideoSchema, MediaRecordSchema, PlayableMediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { Observable } from 'rxjs/Observable';
import { ModalWrapper } from 'components/common/modal/modalWrapper';
import { CloseReason } from 'components/common/modal/modal.component';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';


@Component( {
    selector: 'uc-media-info-modal',
    templateUrl: './mediaInfoModal.template.html',
    styleUrls: ['./mediaInfoModal.styles.less']
} )
export class MediaInfoModalComponent extends ModalWrapper implements OnChanges {
    @Input()
    media: MediaRecordSchema;

    @Input()
    title: string = 'BUTTONS.ACTIONS.INFO';

    serverApi : ServerApiManager;

    selectedTab: string = 'summary';

    probe : any | null = null;

    isPlayableMedia : boolean = false;

    videoStreams: MediaMetadataVideoSchema[] = [];

    audioStreams: MediaMetadataAudioSchema[] = [];

    subtitleStreams: MediaMetadataSubtitlesSchema[] = [];

    constructor ( elementRef : ElementRef, changeDetectorRef : ChangeDetectorRef, serverApi: ServerApiManager ) {
        super( elementRef, changeDetectorRef );
        this.serverApi = serverApi;
    }

    open(ctx = {}): Observable<CloseReason> {
        super.open(ctx);

        return this.modal.closingEvent.first();
    }

    selectTab(name: string) {
        this.selectedTab = name;
    }

    ngOnChanges (changes: SimpleChanges): void {
        if ('media' in changes) {
            var media = changes['media'].currentValue as PlayableMediaRecordSchema;

            this.probe = null;

            if (media == null || media.metadata == null) {
                this.videoStreams = [];
                this.audioStreams = [];
                this.subtitleStreams = [];
                this.isPlayableMedia = false;
            } else {
                this.isPlayableMedia = media.kind == MediaKind.Movie || media.kind == MediaKind.TvEpisode || media.kind == MediaKind.Custom;

                this.videoStreams = [
                    media.metadata.video,
                    ...(media.metadata.additionalVideo || [])
                ].filter(stream => stream != null);

                this.audioStreams = [
                    media.metadata.audio,
                    ...(media.metadata.additionalAudio || [])
                ].filter(stream => stream != null);

                this.subtitleStreams = [
                    media.metadata.subtitles,
                    ...(media.metadata.additionalSubtitles || [])
                ].filter(stream => stream != null);

                const mediaId = media.id;

                this.serverApi.getMedia().getProbe(media.kind, media.id).toPromise().then(probe => {
                    if (this.media != null && this.media.id == mediaId) {
                        this.probe = probe;
                    }
                });
            }
        }
    }
}

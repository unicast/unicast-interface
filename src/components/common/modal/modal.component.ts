import { Component, ContentChild, Input, TemplateRef, ElementRef, ChangeDetectorRef, HostListener, Output, EventEmitter } from '@angular/core';

import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';

export enum CloseReason {
    Outsideclick = 1,
    CloseButton = 2,
    EscButton = 3,
    ModalButton = 4,
    Other = 0
}

@Component( {
    selector: 'uc-modal',
    templateUrl: './modal.template.html',
    styleUrls: ['./modal.styles.less'],
    providers: []
} )
export class ModalComponent extends BaseUnicastComponent {
    @ContentChild( 'modalHeader' )
    header : TemplateRef<any>;

    @ContentChild( 'modalBody' )
    body : TemplateRef<any>;

    @ContentChild( 'modalFooter' )
    footer : TemplateRef<any>;

    @Input()
    closeOnOutsideClick = true;

    @Input()
    closeOnButtonClick = true;

    @Input()
    size : string = 'small';

    @Output()
    closingEvent: EventEmitter<CloseReason> = new EventEmitter();

    private elementRef : ElementRef;
    private changeDetectorRef : ChangeDetectorRef;

    public visible : boolean = false;
    public visibleAnimate : boolean = false;
    public ctx: {} = {};

    constructor ( elementRef : ElementRef, changeDetectorRef : ChangeDetectorRef ) {
        super();

        this.elementRef = elementRef;
        this.changeDetectorRef = changeDetectorRef;
    }

    ngOnDestroy () {
        // Prevent modal from not executing its closing actions if the user navigated away (for example,
        // through a link).
        super.ngOnDestroy();
        this.close(CloseReason.Other);
    }

    show (ctx = {}) : void {
        this.open(ctx);
    }

    open (ctx = {}) : void {
        this.ctx = { ...ctx };
        document.body.classList.add( 'modal-open' );

        this.visible = true;
        setTimeout( () => {
            this.visibleAnimate = true;
        } );
    }

    close (reason?: CloseReason) : void {
        document.body.classList.remove( 'modal-open' );

        this.visibleAnimate = false;

        this.closingEvent.emit(reason || CloseReason.Other);

        setTimeout( () => {
            this.visible = false;
            if (this.changeDetectorRef) { //Verify if the page itself kept the reference
                this.changeDetectorRef.markForCheck();
            }
        }, 200 );
    }

    footerClicked( event: MouseEvent ):void {
        if (this.closeOnButtonClick) {
            this.close(CloseReason.ModalButton);
        }
    }

    @HostListener( 'click', [ '$event' ] )
    onContainerClicked( event : MouseEvent ) : void {
        if ( ( <HTMLElement>event.target ).classList.contains( 'modal' ) && this.isTopMost() && this.closeOnOutsideClick ) {
            this.close(CloseReason.Outsideclick);
        }
    }

    @HostListener( 'document:keydown', [ '$event' ] )
    onKeyDownHandler ( event : KeyboardEvent ) {
        // If ESC key and TOP MOST modal, close it.
        if ( event.key === 'Escape' && this.isTopMost() ) {
            this.close(CloseReason.EscButton);
        }
    }

    /**
     * Returns true if this modal is the top most modal.
     */
    isTopMost () : boolean {
        return !this.elementRef.nativeElement.querySelector( ':scope modal > .modal' );
    }
}
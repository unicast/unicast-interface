import { Component, Input, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ConfirmModalComponent, ButtonClick } from 'components/common/modal/confirmModal/confirmModal.component';
import { HtmlInputType } from 'components/common/helpers/htmlElementHelper';

export interface InputButtonClick<T> extends ButtonClick {
    value: T;
}

@Component( {
    selector: 'uc-input-modal',
    templateUrl: './inputModal.template.html',
    styleUrls: ['./inputModal.styles.less']
} )
export class InputModalComponent<T> extends ConfirmModalComponent {
    @Input()
    type: HtmlInputType;

    @Input()
    textAreaRows: number = 10;

    actualValue: T;

    constructor ( elementRef : ElementRef, changeDetectorRef : ChangeDetectorRef ) {
        super(elementRef, changeDetectorRef);
    }

    addValue(e: ButtonClick): InputButtonClick<T> {
        return { ...e, value: this.actualValue };
    }

    open(ctx = {}): Observable<ButtonClick> {
        let ret = super.open(ctx);
        if (ctx['startValue']) {
            this.actualValue = ctx['startValue'];
        }
        return ret;
    }
}
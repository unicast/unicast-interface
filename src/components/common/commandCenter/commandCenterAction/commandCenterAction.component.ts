import { Component, ViewChild, ViewContainerRef, ComponentRef, Injector, ComponentFactoryResolver } from '@angular/core';

import { CommandCenter } from 'components/common/commandCenter/commandCenter.service';

export interface ActionButton {
    label ?: string;
    icon ?: string;
    type ?: 'warning' | 'primary' | 'success' | 'danger' | 'white' | 'info';
    action ?: () => void;
    closes ?: boolean;
}

export interface Class<T> {
    new ( ...args : any[] ) : T;
}

@Component( {
    selector: 'uc-command-center-action',
    templateUrl: './commandCenterAction.template.html',
    styleUrls: [ './commandCenterAction.styles.less' ],
    host: {
        '[class.background-1]': 'backgroundLevel == 1',
        '[class.background-2]': 'backgroundLevel == 2'
    }
} )
export class CommandCenterActionComponent<T = any> {
    @ViewChild( 'actionBody', { read: ViewContainerRef } ) actionBody : ViewContainerRef;

    // Needs to be injected manually by whoever creates this component
    commandCenter : CommandCenter;

    protected closeTimerToken : any = null;

    protected _closeTimer : number = Infinity;

    component : ComponentRef<T>;

    isOpened : boolean = true;

    title : string;

    actionButtons ?: ActionButton[] = [];

    closable : boolean;

    backgroundLevel : number = 1;

    get closeTimer () : number {
        return this._closeTimer;
    }

    set closeTimer ( value : number ) {
        if ( value === this._closeTimer ) {
            return;
        }

        if ( this.closeTimerToken ) {
            clearTimeout( this.closeTimerToken );
        }

        this._closeTimer = value;

        if ( this._closeTimer && this._closeTimer !== Infinity ) {
            this.closeTimerToken = setTimeout( this.closeSelf.bind( this ), this.closeTimer );
        }
    }

    protected factoryResolver : ComponentFactoryResolver;

    constructor ( factoryResolver : ComponentFactoryResolver ) {
        this.factoryResolver = factoryResolver;
    }

    async setActionComponent ( component : Class<T>, injector ?: Injector ) : Promise<ComponentRef<T>> {
        let actionFactory = this.factoryResolver.resolveComponentFactory( component );

        let action = await this.actionBody.createComponent( actionFactory, 0, injector );

        this.component = action;

        return action;
    }

    public onActionButton ( button : ActionButton ) {
        if ( button.action ) {
            button.action();
            if (button.closes) {
                this.closeSelf();
            }
        }
    }

    public closeSelf () {
        this.commandCenter.unregister( this.component.instance );
    }
}

export class ActionComponent {
    actionRef : CommandCenterActionComponent;

    constructor ( actionRef : CommandCenterActionComponent ) {
        this.actionRef = actionRef;
    }

    get isOpened () : boolean {
        return this.actionRef.isOpened;
    }

    set isOpened ( value : boolean ) {
        this.actionRef.isOpened = value;
    }

    get title () : string {
        return this.actionRef.title;
    }

    set title ( value : string ) {
        this.actionRef.title = value;
    }

    get actionButtons () : ActionButton[] {
        return this.actionRef.actionButtons;
    }

    set actionButtons ( value : ActionButton[] ) {
        this.actionRef.actionButtons = value;
    }

    get closeTimer () : number {
        return this.actionRef.closeTimer;
    }

    set closeTimer ( value : number ) {
        this.actionRef.closeTimer = value;
    }

    get closable () : boolean {
        return this.actionRef.closable;
    }

    set closable ( value : boolean ) {
        this.actionRef.closable = value;
    }

    close () : void {
        this.actionRef.closeSelf();
    }
}
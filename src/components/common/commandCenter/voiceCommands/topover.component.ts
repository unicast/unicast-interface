import { Component, EventEmitter } from '@angular/core';

import { TopoverMessage, ITopoverProperties, TopoverSequencialMessage, ITopoverExecutingProperties } from 'components/common/commandCenter/voiceCommands/topover.utils';
import { CommandSequencialMessage } from 'components/services/speechRecognition/sequencialMessages';
import { ActionComponent, CommandCenterActionComponent, ActionButton } from 'components/common/commandCenter/commandCenterAction/commandCenterAction.component';


@Component( {
    selector: 'uc-topover',
    templateUrl: './topover.template.html',
    styleUrls: [ './topover.styles.less' ]
} )
export class TopoverComponent extends ActionComponent {

    private _btnEmitter: EventEmitter<boolean>;

    public readonly SHOWS_NOTHING:    number = 0;
    public readonly SHOWS_INPUT:      number = 1;
    public readonly SHOWS_PROCESSING: number = 2;
    public readonly SHOWS_EXECUTING:  number = 3;

    showing: number = 0;
    confirmed: boolean = false;
    canceled: boolean = false;

    processingMessage: TopoverMessage;

    executingTitle: TopoverSequencialMessage;
    executingMessage: TopoverSequencialMessage;
    executingCancellable: boolean = false;
    executingUndoable: boolean = false;

    private _timeout;

    public constructor(actionRef: CommandCenterActionComponent){
        super(actionRef);
        this._btnEmitter = new EventEmitter<boolean>();
    }

    private isTopoverSequencialMessage(obj: any): obj is TopoverSequencialMessage{
        return obj['msgs'] instanceof Array;
    }

    private handleTopoverSequencialMessage(msg: TopoverSequencialMessage | string): TopoverSequencialMessage{
        if (this.isTopoverSequencialMessage(msg)){
            return msg;
        } else {
            return CommandSequencialMessage.fromString(msg);
        }
    }

    public isVisible(): boolean {
        return this.showing != this.SHOWS_NOTHING;
    }

    public showInput(properties?: ITopoverProperties<TopoverMessage>): void{
        properties = properties || { message: new TopoverMessage(), milliseconds: 0 };

        this.processingMessage = properties.message;

        this._hideTimeout(properties);

        this.showing = this.SHOWS_INPUT;
    }

    public refreshMessage(obj: {normal: string, interim: string}): void{
        this.processingMessage.refresh(obj.normal, obj.interim);
    }

    public showProcessing(): void{
        this.showing = this.SHOWS_PROCESSING;
        this.closeTimer = 0;
    }

    public showExecuting(properties: ITopoverExecutingProperties): void{
        this.confirmed = false;
        this.canceled = false;

        this.title = properties.title;

        this.executingMessage = this.handleTopoverSequencialMessage(properties.message);

        properties.cancellable = !!properties.cancellable;
        properties.undoable = !!properties.undoable;

        //Only one button can be visible at each time
        this.executingCancellable = properties.cancellable;
        this.executingUndoable = properties.undoable && !properties.cancellable;

        let actionBtns: Array<ActionButton> = [];

        if (this.executingCancellable || this.executingUndoable){
            actionBtns.push({
                type: 'warning',
                label: (this.executingCancellable ? 'COMMANDS.CANCEL.TITLE' : 'COMMANDS.UNDO.TITLE'),
                action: () => {
                    this.cancel();
                }
            });
            actionBtns.push({
                type: 'success',
                label: 'COMMANDS.CONFIRM.TITLE',
                action: () => {
                    this.confirm();
                }
            });
        }

        this.actionButtons = actionBtns;

        this._hideTimeout(properties);

        this.showing = this.SHOWS_EXECUTING;
    }

    private _hideTimeout(properties: ITopoverProperties<any>){
        if (this._timeout){
            clearTimeout(this._timeout);
        }

        if (!properties.milliseconds){
            properties.milliseconds = 10000;
        }

        if (properties.milliseconds > 0){
            this.closeTimer = properties.milliseconds;
            this._timeout = setTimeout(() => {
                this.hide();
            }, properties.milliseconds);
        }

    }

    public hide(): void{
        this.showing = this.SHOWS_NOTHING;
        if (this._timeout){
            clearTimeout(this._timeout);
        }
        super.close();
    }

    confirm() {
        if (!this.canceled){
            this.confirmed = true;
            this._btnEmitter.emit(true);
        }
    }

    getBtnEmitter() {
        return this._btnEmitter;
    }

    cancel() {
        if (!this.confirmed){
            this.canceled = true;
            this._btnEmitter.emit(false);
        }
    }
}


import { Injectable } from '@angular/core';
import { TopoverComponent } from 'components/common/commandCenter/voiceCommands/topover.component';
import { CommandCenter } from 'components/common/commandCenter/commandCenter.service';

@Injectable()
export class TopoverManager{
    private _comp: TopoverComponent;
    private commandCenter: CommandCenter;

    public constructor (commandCenter : CommandCenter){
        this.commandCenter = commandCenter;
    }

    public async getComponent(): Promise<TopoverComponent>{
        if (!this.commandCenter.getRef(this._comp)){
            this._comp = await this.commandCenter.register( TopoverComponent );
        }
        return this._comp;
    }
}
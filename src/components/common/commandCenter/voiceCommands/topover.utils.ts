export interface ITopoverProperties<T> {
    milliseconds?: number;
    message: T;
}

export interface ITopoverExecutingProperties extends ITopoverProperties<TopoverSequencialMessage | string>{
    title: string;
    cancellable?:boolean;
    undoable?:boolean;
}

export class TopoverMessage {
    normal: string;
    interim: string;
    clean: boolean;

    public constructor(){
        this.normal = "";
        this.interim = "";
        this.clean = true;
    }

    public addInterimMessage(msg: string){
        this.clean = false;
        this.normal += this.interim + ' ';
        this.interim = msg + '';
    }

    public refresh(normal: string, interim: string){
        this.clean = false;
        this.normal = normal + '';
        this.interim = interim + '';
    }
}

export class TopoverSequencialMessage {
    msgs: Array<{ msg: string, type: 'FIELD' | 'NORMAL' | 'INTERIM'}>;
}
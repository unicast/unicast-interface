import { Component, ViewChild, ViewContainerRef } from '@angular/core';

import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { CommandCenter } from 'components/common/commandCenter/commandCenter.service';

@Component( {
    selector: 'uc-command-center',
    templateUrl: './commandCenter.template.html',
    styleUrls: [ './commandCenter.styles.less' ],
} )
export class CommandCenterComponent extends BaseUnicastComponent{
    @ViewChild( 'view', { read: ViewContainerRef } ) view : ViewContainerRef;

    isHidden : boolean = false;

    public service : CommandCenter;

    constructor ( service : CommandCenter ) {
        super();
        this.service = service;
    }

    protected _binds() {
        this._subscriptions.push( this.service.actionsChanged.subscribe( actions => {
            let background : number = 0;

            for ( let action of actions ) {
                if ( action.isOpened ) {
                    action.backgroundLevel = background % 2 == 0 ? 1 : 2;

                    background++;
                }
            }
        } ) );
    }

    public toggleIsHidden () {
        this.isHidden = !this.isHidden;
    }

    async ngAfterContentInit () {
        this.service.viewContainer = this.view;
    }
}

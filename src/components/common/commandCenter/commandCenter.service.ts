import { Injectable, ComponentFactoryResolver, ViewContainerRef, ComponentRef, Injector, ReflectiveInjector } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { CommandCenterActionComponent, Class } from 'components/common/commandCenter/commandCenterAction/commandCenterAction.component';


export type ActionRef<T = any> = ComponentRef<CommandCenterActionComponent<T>>;

@Injectable()
export class CommandCenter {
    protected actions : ActionRef[] = [];

    protected factoryResolver : ComponentFactoryResolver;

    public actionsChanged : Subject<CommandCenterActionComponent<any>[]> = new Subject();

    viewContainer : ViewContainerRef;

    get isOpened () : boolean {
        return this.actions.length && this.actions.every( action => action.instance.isOpened );
    }

    constructor ( factoryResolver : ComponentFactoryResolver ) {
        this.factoryResolver = factoryResolver;
    }

    async register<T> ( actionClass : Class<T>, injector ?: Injector ) : Promise<T> {
        let actionContainerFactory = this.factoryResolver.resolveComponentFactory<CommandCenterActionComponent<T>>( CommandCenterActionComponent );

        let actionContainer = await this.viewContainer.createComponent( actionContainerFactory, this.viewContainer.length, injector );

        actionContainer.instance.commandCenter = this;

        let actionInjector = ReflectiveInjector.resolveAndCreate( [ { provide: CommandCenterActionComponent, useValue: actionContainer.instance } ], actionContainer.injector );

        let actionRef = await actionContainer.instance.setActionComponent( actionClass, actionInjector );

        this.actions.unshift( actionContainer );

        this.actionsChanged.next( this.actions.map( a => a.instance ) );

        return actionRef.instance;
    }

    async unregister<T> ( action : T ) {
        const index = this.actions.findIndex( ref => ref.instance.component.instance === action );

        if ( index >= 0 ) {
            let actionRef = this.actions[ index ];

            actionRef.destroy();

            this.actions.splice( index, 1 );

            this.actionsChanged.next( this.actions.map( a => a.instance ) );
        }
    }

    getRef<T> ( action : T ) : ActionRef<T> {
        const index = this.actions.findIndex( ref => ref.instance.component.instance === action );

        if ( index >= 0 ) {
            return this.actions[ index ] as ActionRef<T>;
        }

        return null;
    }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { CommandCenter } from 'components/common/commandCenter/commandCenter.service';
import { NotificationComponent, NotificationOptions, NotificationType } from 'components/common/commandCenter/notifications/notification.component';
import { TranslatorManager } from 'components/services/translatorManager';

export interface NotificationCallbacks {
    yes: () => void;
    no: () => void;
}

@Injectable()
export class NotificationsService {
    protected commandCenter : CommandCenter;
    protected translatorManager : TranslatorManager;

    constructor ( commandCenter : CommandCenter, translatorManager: TranslatorManager ) {
        this.commandCenter = commandCenter;
        this.translatorManager = translatorManager;
    }

    showStream ( observable : Observable<NotificationOptions>, defaults ?: Partial<NotificationOptions> );
    showStream ( observable : Observable<NotificationOptions>, timeout ?: number );
    showStream ( observable : Observable<NotificationOptions>, defaultsOrTimeout ?: Partial<NotificationOptions> | number ) {
        let defaults : Partial<NotificationOptions>;

        if ( typeof defaultsOrTimeout == 'number' ) {
            defaults = { timeout: defaultsOrTimeout };
        } else defaults = defaultsOrTimeout;

        observable.subscribe( options => {
            this.show( {
                ...defaults,
                ...options
            } );
        } );
    }

    async show (  options : NotificationOptions, timeout : number = 10000) : Promise<NotificationComponent> {
        const ref = await this.commandCenter.register( NotificationComponent );

        ref.update( { timeout: timeout, ...options } );

        return ref;
    }

    async ask (  options : Partial<NotificationOptions>, callback?: Partial<NotificationCallbacks>, timeout : number = 10000, ) : Promise<NotificationComponent> {
        const ref = await this.commandCenter.register( NotificationComponent );

        ref.closable = false;

        ref.update( { timeout: timeout,  type: NotificationType.QUESTION, ...options } );

        callback = callback || {};
        callback.yes = callback.yes || (() => {});
        callback.no = callback.no || (() => {});

        ref.actionButtons.push({
            label: 'BUTTONS.GENERAL.YES',
            type: 'warning',
            closes: true,
            action: callback.yes
        });

        ref.actionButtons.push({
            label: 'BUTTONS.GENERAL.NO',
            type: 'white',
            closes: true,
            action: callback.no
        });

        return ref;
    }
}
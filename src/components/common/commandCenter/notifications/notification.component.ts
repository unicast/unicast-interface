import { Component } from '@angular/core';
import { Router, UrlTree, NavigationExtras } from '@angular/router';

import { CommandCenterActionComponent, ActionComponent } from 'components/common/commandCenter/commandCenterAction/commandCenterAction.component';
import { TranslatorManager } from 'components/services/translatorManager';

export enum NotificationType {
    SUCCESS = 'success',
    INFO = 'info',
    WARN = 'warning',
    WAIT = 'wait',
    ERROR = 'error',
    QUESTION = 'question'
}

export interface NotificationOptions {
    message ?: string;
    title ?: string;
    icon ?: string;
    type : NotificationType;
    url ?: string | UrlTree | any[];
    urlOptions ?: NavigationExtras;
    timeout ?: number;
}

@Component( {
    selector: 'uc-action-notification',
    templateUrl: './notification.template.html',
    styleUrls: [ './notification.styles.less' ],
} )
export class NotificationComponent extends ActionComponent {
    public notificationType: typeof NotificationType = NotificationType;
    public paragraphs : string[] = [];

    public get message () : string {
        return this.paragraphs.join( '\n' );
    }

    public set message ( value : string ) {
        this.paragraphs = value ? value.split( '\n' ) : [];
    }

    public type : NotificationType;

    protected _closable : boolean = true;

    public override get closable () {
        return this._closable;
    }

    public override set closable ( value : boolean ) {
        this._closable = value;
    }

    protected router : Router;
    protected translatorManager: TranslatorManager;

    constructor ( actionRef : CommandCenterActionComponent, router : Router, translatorManager: TranslatorManager ) {
        super( actionRef );

        this.router = router;
        this.translatorManager = translatorManager;
    }

    update ( options : Partial<NotificationOptions> ) {
        if ( 'title' in options ) this.title = options.title;
        if ( 'timeout' in options ) this.closeTimer = options.timeout;
        if ( 'type' in options ) this.type = options.type;
        if ( 'message' in options ) this.message = this.translatorManager.fromResource(options.message);
        if ( 'url' in options ) {
            if ( options.url ) {
                this.actionButtons = [ {
                    type: this.type as any,
                    action: this.onButtonAction.bind( this, options.url, options.urlOptions ),
                    label: 'Open'
                } ];
            } else {
                this.actionButtons = [];
            }
        }
    }

    onButtonAction ( url : string | UrlTree | any[], options ?: NavigationExtras ) {
        if ( url instanceof Array ) {
            this.router.navigate( url, options );
        } else {
            this.router.navigateByUrl( url, options );
        }
    }
}

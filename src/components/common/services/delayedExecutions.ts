import { Injectable, ApplicationRef } from '@angular/core';

@Injectable()
export class DelayedExecutions {
    private _applicationRef: ApplicationRef;

    constructor(applicationRef: ApplicationRef){
        this._applicationRef = applicationRef;
    }

    setTimeout(callback: (...args: Array<any>) => void, ms: number, ...args: Array<any>): NodeJS.Timer {
        return setTimeout(() => {
            callback(...args);
            this._applicationRef.tick();
        }, ms, args);
    }

    setInterval(callback: (...args: Array<any>) => void, ms: number, ...args: Array<any>): NodeJS.Timer {
        return setInterval(() => {
            callback(...args);
            this._applicationRef.tick();
        }, ms, args);
    }

    clearTimeout(timeoutId: NodeJS.Timer): void {
        clearTimeout(timeoutId);
    }

    clearInterval(intervalId: NodeJS.Timer): void {
        clearInterval(intervalId);
    }
}
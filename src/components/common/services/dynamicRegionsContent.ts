import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { DisposableStack } from './disposableStack';

@Injectable()
export class DynamicRegionsContent {
    regions : Object = {};

    createRegion ( name : string ) {
        this.regions[ name ] = this.createRegionObservable();

        return this;
    }

    createRegionObservable () {
        const stack = new DisposableStack();
        const observable = new BehaviorSubject( null );

        return { stack, observable };
    }

    setRegionContent ( regionName : string, component, injector? ) {
        if ( !( regionName in this.regions ) ) {
            throw new Error( `Could not find a menu region named "${regionName}"` );
        }

        const region = this.regions[ regionName ];

        const token = region.stack.push( { component, injector } );

        region.observable.next( region.stack.last() );

        return token;
    }

    disposeRegionContent ( regionName : string, token ) {
        if ( !( regionName in this.regions ) ) {
            throw new Error( `Could not find a menu region named "${regionName}"` );
        }

        const region = this.regions[ regionName ];

        region.stack.dispose( token );

        region.observable.next( region.stack.last() );
    }

    getRegionContent ( region : string ) {
        if ( !( region in this.regions ) ) {
            throw new Error( `Could not find a menu region named "${region}"` );
        }

        return this.regions[ region ].observable;
    }
}
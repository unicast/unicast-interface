import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { DisposableStack } from 'components/common/services/disposableStack';

export class DisposableObservableStack<T = any> extends DisposableStack<T> {
    public changes : BehaviorSubject<Array<T>>;
    public current : Observable<T>;
    public currentToken : Observable<Symbol>;

    constructor () {
        super();

        this.changes = new BehaviorSubject<T[]>( [] );
        this.current = this.changes.map( list => list[ list.length - 1 ] );
        this.currentToken = this.changes.map( list => this.lastToken() );
    }

    push ( item : T ) {
        const token = super.push( item );

        this.changes.next( Array.from( this ) );

        return token;
    }

    dispose ( token ) {
        if ( this.hasToken( token ) ) {
            super.dispose( token );

            this.changes.next( Array.from( this ) );
        }
    }
}

import { Injectable } from '@angular/core';
import { Optional } from 'data-optional';

class Node {
    value: Optional<any>;
    lifetime: Lifetime;
    inheritableLifetime: InheritableLifetime;
    children: Map<string, Node> = new Map();
    autoSync: boolean = true;

    public constructor ( value: Optional<any>, lifetime: Lifetime, inheritableLifetime: InheritableLifetime = true ) {
        this.value = value;
        this.lifetime = lifetime;
        this.inheritableLifetime = inheritableLifetime;
    }

    public get size () : number {
        const childrenSize = Array.from( this.children.values() )
            .map( node => node.size )
            .reduce( ( a, b ) => a + b, 0 );

        if ( !this.value.isEmpty() ) {
            return 1 + childrenSize;
        }

        return childrenSize;
    }

    public resolveInheritableLifetime () : Lifetime | null {
        if ( this.inheritableLifetime === false ) {
            return null;
        }

        if ( this.inheritableLifetime === true ) {
            return this.lifetime;
        }

        return this.lifetime;
    }

    public get isEmpty () : boolean {
        return this.children.size === 0 && ( this.value.isEmpty() || !this.lifetime.isAlive() );
    }

    public purge ( keyPath : string[], cb : ( keyPath: string[], node: Node ) => void = null ) : void {
        for ( let key of Array.from( this.children.keys() ) ) {
            keyPath.push( key );

            const childNode = this.children.get( key );

            childNode.purge( keyPath, cb );

            if ( childNode.isEmpty ) {
                this.children.delete( key );

                if ( cb != null && childNode.value.isPresent() ) {
                    cb( keyPath, childNode );
                }
            }

            keyPath.pop();
        }
    }
};

interface FindNodeOptions {
    startNode?: Node;
    startKeyIndex?: number;
    endKeyIndex?: number;
    trackLifetimeInheritance?: boolean;
}

interface FindNodeResult {
    node: Node;
    partial: boolean;
    keyIndex: number;
    inheritedLifetime?: Lifetime;
    inheritedLifetimeKey?: StoreKey;
}

interface SetAdvancedOptions {
    value?: Optional<any> | undefined;
    lifetime?: LifetimeInput | undefined;
    inheritableLifetime?: InheritableLifetime | undefined;
    autoSync?: boolean;
}

type StoreKey = string[];

export class MultiLevelCache {
    protected root: Node;

    /**
     * Indicates if this cache object is the root. Useful to determine who
     * stores values in the permanent storage (only the root does, to avoid duplication)
     * and who notifies other tabs (through broadcast messages) that a setting has changed
     */
    // protected isRoot: boolean = true;

    /**
     * The default lifetime is used when the user does not 
     * specify any lifetime.
     */
    public get defaultLifetime (): Lifetime {
        return this.root.resolveInheritableLifetime() || this.root.lifetime;
    };

    public readonly storage: CacheStorage;

    protected lifetimes: LifetimeConverter = new LifetimeConverter();

    protected readOnlyMode: boolean = false;

    constructor ( defaultLifetime: LifetimeInput = "permanent" ) {
        this.root = new Node( Optional.empty(), new PermanentLifetime(), toLifetime( defaultLifetime ) );
        this.storage = new CacheStorage( this.lifetimes, localStorage );
    }

    public get size () : number {
        return this.root.size;
    }

    public reload () : void {
        // TODO End lifetimes
        this.root.children.clear();

        this.readOnlyMode = true;

        try {
            var purgedKeys: StoreKey[] = [];

            for ( let key of this.storage.list() ) {
                const node = this.storage.get( key );

                if ( node.lifetime.isAlive() ) {
                    purgedKeys.push( key );
                } else {
                    if ( node.lifetime.begin ) {
                        node.lifetime.begin();
                    }

                    this.setAdvanced( key, {
                        value: node.value,
                        lifetime: node.lifetime,
                        inheritableLifetime: node.inheritableLifetime,
                        autoSync: node.autoSync,
                    } );
                }
            }

            for ( let key of purgedKeys ) {
                this.storage.delete( key );
            }
        } finally {
            this.readOnlyMode = false;
        }
    }

    protected setInStorage ( key: StoreKey, value: Node ) : void {
        if ( this.readOnlyMode ) return;

        if ( value.lifetime.isAlive ) {
            this.deleteInStorage( key );
        } else {
            this.storage.set( key, value );
        }
    }

    protected deleteInStorage ( key: StoreKey ) : void {
        if ( this.readOnlyMode ) return;

        this.storage.delete( key );
    }

    protected findNode ( key: StoreKey, opts : FindNodeOptions = {} ) : FindNodeResult {
        let inheritedLifetimeKey: StoreKey = [];
        let inheritedLifetime: Lifetime = this.defaultLifetime;

        let partial = false;
        let currentNode: Node = opts.startNode || this.root;
        const trackLifetimeInheritance: boolean = !!opts.trackLifetimeInheritance;
        const startKeyIndex = typeof opts.startKeyIndex === 'number' ? opts.startKeyIndex : 0;
        // Not inclusive. Ranges from [startKeyIndex, endKeyIndex[
        const endKeyIndex = typeof opts.endKeyIndex === 'number' ? opts.endKeyIndex : key.length;

        let keyIndex: number = startKeyIndex;
        
        for ( ; keyIndex < endKeyIndex; keyIndex++ ) {
            const segment = key[ keyIndex ];
            
            let nextNode = currentNode.children.get( segment );

            if ( nextNode == null ) {
                partial = true;
                break;
            }

            currentNode = nextNode;

            if ( trackLifetimeInheritance ) {
                const nextInheritedLifetime = currentNode.resolveInheritableLifetime();
    
                if ( nextInheritedLifetime !== null ) {
                    inheritedLifetime = nextInheritedLifetime;
        
                    if ( inheritedLifetime instanceof InheritedLifetime ) {
                        inheritedLifetimeKey = inheritedLifetime.originalKey;
                        inheritedLifetime = inheritedLifetime.childLifetime;
                    } else {
                        inheritedLifetimeKey = key.slice( 0, keyIndex + 1 );
                    }
                }
            }
        }

        return {
            node: currentNode,
            partial, keyIndex, inheritedLifetime, inheritedLifetimeKey,
        };
    }

    protected findOrCreateNode ( key: StoreKey, opts : FindNodeOptions = {} ) : FindNodeResult {
        const result = this.findNode( key, opts );

        if ( result.partial ) {
            const endKeyIndex = typeof opts.endKeyIndex === 'number' ? opts.startKeyIndex : key.length;

            for ( ; result.keyIndex < endKeyIndex; result.keyIndex++ ) {
                // We don't need to clone the lifetime here because this node is empty anyway
                // When something is stored in it, and it transitions from empty to
                // containing a value, the clone method will be called then
                const nextNode = new Node( Optional.empty(), new VolatileLifetime(), false );
        
                result.node.children.set( key[ result.keyIndex ], nextNode );
        
                result.node = nextNode;
            }
            
            result.partial = false;
        }

        return result;
    }

    public getDefault<V> ( key : StoreKey, defaultValue : V ) : V {
        return this.get<V>( key ).orElse( defaultValue );
    }

    public has ( key : StoreKey ) : boolean {
        return !this.get( key ).isEmpty();
    }

    public get<V> ( key : StoreKey ) : Optional<V> {
        let result = this.findNode( key );

        if ( result.partial ) {
            return Optional.empty();
        }

        if ( result.node.lifetime.isAlive() ) {
            return result.node.value;
        }

        result.node.value = Optional.empty();

        return result.node.value;
    }

    public getLifetime ( key : StoreKey ) : Lifetime | null {
        let result = this.findNode( key );

        if ( result.partial ) {
            return null;
        }

        return result.node.lifetime;
    }

    public getInheritableLifetime ( key : StoreKey ) : InheritableLifetime | null {
        let result = this.findNode( key );

        if ( result.partial ) {
            return null;
        }

        return result.node.inheritableLifetime;
    }

    public resolveInheritableLifetime ( key : StoreKey ) : InheritedLifetime {
        let result = this.findNode( key, { trackLifetimeInheritance: true } );

        return new InheritedLifetime( result.inheritedLifetimeKey, result.inheritedLifetime );
    }

    public * getChildrenKeys ( key: StoreKey, includeEmpty: boolean = false ) : IterableIterator<string> {
        const result = this.findNode( key );

        if ( !result.partial ) {
            for ( let [ childKey, node ] of result.node.children ) {
                if ( includeEmpty || node.value.isPresent() ) {
                    yield childKey;
                }
            }
        }
    }

    public * getChildren ( key : StoreKey ) : IterableIterator<any> {
        const result = this.findNode( key );

        if ( !result.partial ) {
            for ( let node of result.node.children.values() ) {
                if ( node.value.isPresent() ) {
                    yield node;
                }
            }
        }
    }

    /**
     * Set's a value stored in association with a key. If no lifetime is provided,
     * then the system uses the closest associated lifetime from one of the parents.
     * If no parent has a lifetime, then the default lifetime is used.
     * 
     * @param key Unique identifier for the value.
     * @param value The value to store.
     * @param lifetime Object specifying for how long this value should be kept in store
     */
    public setAdvanced ( key : StoreKey, options: SetAdvancedOptions ) : void {
        let intendedLifetime = toLifetime( options.lifetime );

        const result = this.findOrCreateNode( key, { trackLifetimeInheritance: true } );

        if ( result.node.value.isPresent() && result.node.lifetime.end != null ) {
            result.node.lifetime.end();
        }
        
        result.node.value = options.value;
        result.node.inheritableLifetime = options.inheritableLifetime === void 0 ? true : options.inheritableLifetime;
        result.node.autoSync = options.autoSync === void 0 ? true : options.autoSync;

        if ( intendedLifetime != null ) {
            result.node.lifetime = intendedLifetime;
        } else if ( result.node.value.isPresent() ) {
            result.node.lifetime = new InheritedLifetime( result.inheritedLifetimeKey, result.inheritedLifetime.clone() );
        }

        if ( result.node.value.isPresent() ) {
            if ( result.node.lifetime.begin != null ) {
                result.node.lifetime.begin();
            }

            if ( !result.node.lifetime.isAlive ) {
                result.node.value = Optional.empty();

                this.deleteInStorage( key );
    
                if ( result.node.lifetime.end ) {
                    result.node.lifetime.end();
                }
            } else {
                if ( result.node.lifetime.isVolatile ) {
                    this.deleteInStorage( key );
                } else {
                    this.setInStorage( key, result.node );
                }
            }
        } else {
            this.deleteInStorage( key );
        }
    }
    
    /**
     * Set's a value stored in association with a key. If no lifetime is provided,
     * then the system uses the closest associated lifetime from one of the parents.
     * If no parent has a lifetime, then the default lifetime is used.
     * 
     * @param key Unique identifier for the value.
     * @param value The value to store.
     * @param lifetime Object specifying for how long this value should be kept in store
     */
    public set ( key : StoreKey, value : any, lifetime : LifetimeInput = null ) : void {
        this.setAdvanced( key, {
            value: Optional.of( value ),
            lifetime: lifetime
        } );
    }

    public delete ( key : StoreKey, purge: boolean = false ) : boolean {
        if ( key.length == 0 ) {
            return false;
        }

        this.storage.delete( key );

        const result = this.findNode( key, {
            endKeyIndex: key.length - 2
        } );

        // If our result is partial, we ended our search somewhere before
        // finding the target node, so there's nothing to delete
        if ( result.partial ) {
            return false;
        }

        const node = result.node.children.get( key[ key.length - 1 ] );

        if ( node == null ) {
            return false;
        }

        if ( node.children.size === 0 ) {
            result.node.children.delete( key[ key.length ] );
        } else {
            result.node.value = Optional.empty();

            if ( result.node.lifetime.end ) {
                result.node.lifetime.end();
            }

            if ( purge ) {
                this.purge( key );
            }
        }

    }

    public purge ( key : StoreKey = [] ) : void {
        const result = this.findNode( key );

        if ( result.partial ) return;

        result.node.purge( [ ...key ], ( deletedKey ) => {
            // TODO Delete in storage too
            this.storage.delete( deletedKey );
        } );
    }
}

@Injectable()
export class SessionCache extends MultiLevelCache {
    constructor() {
        super();
    }
}

export class LifetimeConverter {
    /**
     * Map of lifetime deserializers, used when loading values from the permanent
     * storage.
     */
    protected lifetimeDeserializers: Map<string, LifetimeDeserializer>; 

    public serialize ( lifetime: Lifetime ) : { name: string, payload: any } {
        return {
            name: lifetime.name,
            payload: lifetime.serialize( this ),
        };
    }

    public deserialize ( rawLifetime: { name: string, payload: any } ) : Lifetime {
        const deserializer = this.lifetimeDeserializers.get( rawLifetime.name );

        if ( deserializer == null ) {
            throw new Error( `Cannot find deserializer for cache lifetime named: "${ rawLifetime.name }"` );
        }

        return deserializer( rawLifetime.payload, this, rawLifetime.name );
    }
}

export interface Lifetime {
    /**
     * Is false when this lifetime needs to be stored in local storage, as 
     * opposed to just being stored in memory
     */
    readonly isVolatile: boolean;

    readonly name : string;

    serialize ( converter: LifetimeConverter ) : any;

    isAlive () : boolean;

    clone () : Lifetime;

    // Useful if the lifetime has to setup resources like timeouts
    begin ? () : void;
    
    // Useful if the lifetime uses resources like timeouts
    end ? () : void;
}

/**
 * This lifetime indicates that the value should live in the storage forever
 * until it is manually removed
 */
export class PermanentLifetime implements Lifetime {
    public readonly isVolatile = false;

    public readonly name = 'permanent';

    public serialize () : any {
        return null;
    }

    public isAlive () : boolean {
        return true;
    }

    public clone () : Lifetime {
        return this;
    }
}

export class VolatileLifetime implements Lifetime {
    public readonly isVolatile: boolean = true;

    public readonly name = 'volatile';

    public serialize () : any {
        return null;
    }

    public isAlive () : boolean {
        return true;
    }
    
    public clone () : Lifetime {
        return this;
    }
}

export class InheritedLifetime implements Lifetime {
    public originalKey: StoreKey;

    public childLifetime: Lifetime;

    public constructor ( originalKey: StoreKey, childLifetime: Lifetime ) {
        this.originalKey = originalKey;
        this.childLifetime = childLifetime;
    }

    public get isVolatile () : boolean {
        return this.childLifetime.isVolatile;
    }

    public readonly name: string = 'inherited';

    public serialize ( converter: LifetimeConverter ) : any {
        const child = converter.serialize( this.childLifetime );
        
        return {
            key: this.originalKey,
            child: child,
        };
    }

    public static deserialize ( payload: any, converter: LifetimeConverter ) : InheritedLifetime {
        const childLifetime = converter.deserialize( payload.child );

        return new InheritedLifetime( payload.key as StoreKey, childLifetime );
    }

    public isAlive () : boolean {
        return this.childLifetime.isAlive();
    }

    public clone () : Lifetime {
        return new InheritedLifetime( this.originalKey, this.childLifetime.clone() );
    }
}

export type LifetimeInput = Lifetime | "permanent" | "volatile";

export type InheritableLifetime = Lifetime | boolean;

export function toLifetime ( lifetime: LifetimeInput | null ) : Lifetime | null;
export function toLifetime ( lifetime: LifetimeInput ) : Lifetime;
export function toLifetime ( lifetime: LifetimeInput | null ) : Lifetime | null {
    if ( lifetime === 'permanent' ) {
        return new PermanentLifetime();
    } else if ( lifetime === 'volatile' ) {
        return new VolatileLifetime();
    }

    return lifetime;
}

export interface LifetimeDeserializer {
    ( payload: any, converter: LifetimeConverter, name: string ) : Lifetime;
}

export abstract class BaseStorage<K, V> {
    public storage : Storage;

    public readonly prefix: string;
    
    public constructor ( storage: Storage, prefix: string = 'mlc_' ) {
        this.storage = storage;
        this.prefix = prefix;
    }

    public set ( keys : K, value : V ) : void {
        const rawKey: string = this.serializeKey( keys );

        this.storage.setItem( this.prefix + rawKey, this.serializeValue( value ) );
    }

    public get<T extends V = V> ( keys : K ) : T {
        const rawKey: string = this.serializeKey( keys );

        return this.deserializeValue( this.storage.getItem( this.prefix + rawKey ) ) as T;
    }

    public delete ( keys: K ) : void {
        const rawKey: string = this.serializeKey( keys );

        this.storage.removeItem( this.prefix + rawKey );
    }

    public * list () : IterableIterator<K> {
        for ( let i = 0; i < this.storage.length; i++ ) {
            const rawKey = this.storage.key( i );

            if ( rawKey.startsWith( this.prefix ) ) {
                yield this.deserializeKey( rawKey.slice( this.prefix.length ) );
            }
        }
    }

    public clear (): void {
        const rawKeysList: string[] = [];

        for ( let i = 0; i < this.storage.length; i++ ) {
            const rawKey = this.storage.key( i );

            if ( rawKey.startsWith( this.prefix ) ) {
                rawKeysList.push( rawKey );
            }
        }

        for ( const rawKey of rawKeysList ) {
            this.storage.removeItem( rawKey );
        }
    }

    protected abstract serializeValue ( value: V ) : string;

    protected abstract deserializeValue ( value: string ) : V;

    protected abstract deserializeKey ( rawKey: string ) : K;

    protected abstract serializeKey ( key : K ) : string;
}

export class CacheStorage extends BaseStorage<StoreKey, Node> {
    protected readonly keyEscape = new EscapedStringDelimiter( '.' );

    protected lifetimes : LifetimeConverter;
    
    public constructor ( lifetimes : LifetimeConverter, storage: Storage, prefix: string = 'mlc_' ) {
        super( storage, prefix );

        this.lifetimes = lifetimes;
    }

    protected serializeValue ( node: Node ): string {
        const lifetime = node.lifetime.serialize( this.lifetimes );
        const value = node.value;
        const inheritableLifetime = typeof node.inheritableLifetime === 'object' 
            ? node.inheritableLifetime.serialize( this.lifetimes )
            : node.inheritableLifetime;
        const autoSync = node.autoSync;

        return JSON.stringify( {
            lifetime, value, inheritableLifetime, autoSync
        } );
    }

    protected deserializeValue ( rawValue: string ) : Node | null {
        if ( rawValue === null || rawValue === void 0 ) {
            return null;
        }

        const rawNode = JSON.parse( rawValue );

        const value: Optional<any> = rawNode.value.empty 
            ? Optional.empty()
            : Optional.of( rawNode.value.value );

        const lifetime = this.lifetimes.deserialize( rawNode.lifetime );

        const inheritableLifetime = typeof rawNode.inheritableLifetime === 'object' 
            ? this.lifetimes.deserialize( rawNode.inheritableLifetime )
            : rawNode.inheritableLifetime;

        const node = new Node( value, lifetime, inheritableLifetime );

        node.autoSync = rawNode.autoSync;

        return node;
    }

    protected deserializeKey ( rawKey: string ) : StoreKey {
        return this.keyEscape.split( rawKey );
    }

    protected serializeKey ( key : StoreKey ) : string {
        return this.keyEscape.join( key );
    }
}

class EscapedStringDelimiter {
    protected delimiter: string;
    protected escaper: string;

    protected splitMatch: RegExp;
    protected splitReplace: RegExp;

    protected joinDelimiterMatch: RegExp;
    protected joinDelimiterReplace: string;
    protected joinEscaperMatch: RegExp;
    protected joinEscaperReplace: string;

    public constructor ( delimiter: string, escaper: string = '\\' ) {
        this.delimiter = delimiter;
        this.escaper = escaper;

        const d = this.escapeRegExp( delimiter );
        const e = this.escapeRegExp( escaper );

        this.joinDelimiterMatch = new RegExp(String.raw`${e}`, 'g');
        this.joinDelimiterReplace = `${escaper}${escaper}`;
        this.joinEscaperMatch = new RegExp(String.raw`${d}`, 'g');
        this.joinEscaperReplace = `${escaper}${delimiter}`;

        this.splitMatch = new RegExp(String.raw`[^${d}]*?(?:${e}${d}[^${d}]*?)*(?:${d}|$)`, 'g');
        this.splitReplace = new RegExp(String.raw`([^\\].|.[^\\]|^.?)${d}$`);
        this.splitReplace = new RegExp(String.raw`([^\\].|.[^\\]|^.?)${d}$`);
    }

    public escape ( input: string ) : string {
        return input
            .replace( this.joinDelimiterMatch, this.joinDelimiterReplace )
            .replace( this.joinEscaperMatch, this.joinEscaperReplace );
    }

    public inverseEscape ( input: string ) : string {
        const d = this.escapeRegExp( this.delimiter );
        const e = this.escapeRegExp( this.escaper );

        console.log( String.raw`(${e}${e})*${e}${d}`, '$1'+this.delimiter );
        console.log( String.raw`${e}${e}`, this.escaper );

        return input
            .replace( this.splitReplace, '$1' )
            .replace( new RegExp(String.raw`(${e}${e})*${e}${d}`, 'g'), '$1'+this.delimiter )
            .replace( new RegExp(String.raw`${e}${e}`, 'g'), this.escaper );
    }

    public join ( input: string[] ): string {
        return input.map( seg => this.escape( seg ) )
            .join( this.delimiter );
    }

    public split ( input: string ): string[] {
        return ( input.match( this.splitMatch ) as RegExpMatchArray)
            .slice( 0, -1 )
            .map( seg => this.inverseEscape( seg ) );
    }

    protected escapeRegExp ( string: string) {
        return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
    }
}

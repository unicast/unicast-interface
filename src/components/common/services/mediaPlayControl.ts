import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Time } from 'components/common/helpers/time';
import { IProgressChangedEvent, IBarInfo } from 'components/common/slider/slider.component';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { WaitFor } from 'components/common/helpers/asyncClick.directive';
import { DeviceStatusSchema, TimeStatus, DeviceStatusState } from 'components/schemas/status.schema';
import { BackgroundWorker } from 'components/services/backgroundWorker';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';

@Injectable()
export class MediaPlayControl {
    private static readonly BUFFERING_UPDATE_TIME = 1000;
    private static readonly STABLE_UPDATE_TIME = 60000;

    private _serverApi: ServerApiManager;
    private _backgroundWorker: BackgroundWorker;
    private _statusTimeout: NodeJS.Timer;
    private _actualItem: MediaRecordSchema;

    isPlaying: Observable<boolean>;

    sliderBars: Array<IBarInfo<Time>>;

    constructor ( serverApi : ServerApiManager, backgroundWorker: BackgroundWorker ) {
        this._serverApi = serverApi;
        this._backgroundWorker = backgroundWorker;

        this.sliderBars = [{
            name: 'watched',
            actual: new Time(),
            speed: 0,
            maximum: new Time(0,0,0,0.1)
        }];

        let deviceApi = this._serverApi.getPlayer().getDeviceApi();

        //Trt to get an updated status everytime the page becomes visible
        this._backgroundWorker.isVisibleObservable.subscribe( () => {
            deviceApi.getStatus().getValue();
        });

        //Ensure that when the new status is got, the new data is retrieved and used
        deviceApi.getStatus().getValue().subscribe( (data) =>  {
            if (this._statusTimeout){
                clearTimeout(this._statusTimeout);
            }

            this._actualItem = data.media.record;

            let stable = this._processChange(data);
            let isBuffering: boolean = data.state == DeviceStatusState.Buffering;
            //When the state is buffering or not stable yet, force getStatus more frequents to keep in touch
            if ( isBuffering || !stable ){
                this._statusTimeout = setTimeout( () => {
                    deviceApi.getStatus().requestValue();
                    this._statusTimeout = undefined;
                }, isBuffering ? MediaPlayControl.BUFFERING_UPDATE_TIME : MediaPlayControl.STABLE_UPDATE_TIME );
            }
        });

        this.isPlaying = deviceApi.getStatus().isPlayingObservable;
    }

    private _processChange(data: DeviceStatusSchema): boolean{
        let newSlider: Array<IBarInfo<Time>> = [];
        let stable: boolean = true;
        newSlider.push(this._processTime(data.media.time, 'watched'));
        if (data.media.transcoding) {
            newSlider.push(this._processTime(data.media.transcoding, 'striped transcoding'));
            stable = stable && data.media.transcoding.stable !== false; //It is stable if it is undefined or true
        }
        this.changeBars(newSlider);
        return stable;
    }

    private _processTime(time: TimeStatus, bar: string) : IBarInfo<Time> {
        return {
            name: bar,
            actual : Time.fromSeconds( time.current || 0 ),
            speed: time.speed || 0,
            maximum: Time.fromSeconds( time.duration || 0 )
        }
    }

    getPreviewUrl(offset: Time){
        return this._serverApi.getPlayer().getDeviceApi().getUrl( [
                    'preview', this._actualItem.kind, this._actualItem.id,
                    this.sliderBars[0].actual.plus(offset).toSeconds().toString()
                ] );
    }

    seekRequested(offset: Time){
        this._serverApi.getPlayer().getDeviceApi().seek(offset);
    }

    sliderChanged(event: IProgressChangedEvent<Time>){
        if (!event.outsideChange){
            this._serverApi.getPlayer().getDeviceApi().seekTo(event.actualValue);
        }
    }

    changeBars(bars: Array<IBarInfo<Time>>) {
        this.sliderBars = [].concat(bars);

        let maxNewTime = this.sliderBars[0].actual;
        for( let value of this.sliderBars){
            if (value.actual > maxNewTime) {
                maxNewTime = value.actual;
            }
        }

        //if the maximum was exceeded, updates it. We have new maximum now
        if (this.sliderBars[0].maximum.isBefore(maxNewTime)){
            this.sliderBars[0].maximum = maxNewTime;
        }
    }

    changeTime(newTime: Time, maxTime?: Time): void;
    changeTime(newTime: Array<Time>, maxTime?: Time): void;
    changeTime(newTime: Array<Time> | Time, maxTime?: Time): void{
        let times: Array<Time> = [].concat(newTime);
        let bars: Array<IBarInfo<Time>> = [];
        for (let i = 0; i < times.length; i++) {
            bars.push({
                ...this.sliderBars[i],
                actual: times[i],
                maximum: maxTime || this.sliderBars[i].maximum,
            });
        }
        this.changeBars(bars);
    }

    play(event: WaitFor){
        let deviceApi = this._serverApi.getPlayer().getDeviceApi();
        let statusApi = deviceApi.getStatus();
        event.waitFor(statusApi.requestValue().flatMap( (status) => {
            let obs = undefined;
            if (statusApi.isLoaded()){
                if (statusApi.isPaused()) {
                    obs = deviceApi.resume();
                }
            } else {
                obs = deviceApi.play();
            }
            return obs;
        }));
    }

    stop(event: WaitFor){
        event.waitFor(this._serverApi.getPlayer().getDeviceApi().stop());
    }

    pause(event: WaitFor){
        event.waitFor(this._serverApi.getPlayer().getDeviceApi().pause());
    }

    backward(event: WaitFor){
        event.waitFor(this._serverApi.getPlayer().getDeviceApi().previous());
    }

    forward(event: WaitFor){
        event.waitFor(this._serverApi.getPlayer().getDeviceApi().next());
    }

}
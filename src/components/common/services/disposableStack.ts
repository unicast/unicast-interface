export class DisposableStack<T = any> {
    map : Map<Symbol, T> = new Map;
    ordered : Set<Symbol> = new Set;

    hasToken ( token : Symbol ) : boolean {
        return this.map.has( token );
    }

    push ( item : T ) : Symbol {
        const token = Symbol();

        this.ordered.add( token );

        this.map.set( token, item );

        return token;
    }

    getToken ( index : number ) : Symbol {
        return Array.from( this.ordered )[ this.ordered.size - index - 1 ] || null;
    }

    get ( index : number ) : T {
        const token = this.getToken( index );

        if ( !token ) {
            return null;
        }

        return this.map.get( token );
    }

    last () : T {
        return this.get( 0 );
    }

    lastToken () : Symbol {
        return this.getToken( 0 );
    }

    dispose ( token : Symbol ) {
        this.map.delete( token );
        this.ordered.delete( token );
    }

    * [ Symbol.iterator ] () {
        for ( let token of this.ordered ) {
            yield this.map.get( token );
        }
    }
}
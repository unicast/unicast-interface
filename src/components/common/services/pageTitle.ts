import { Injectable } from "@angular/core";
import { Title } from "@angular/platform-browser";

@Injectable()
export class PageTitle {
    private _suffix: string = '';
    private _htmlTitle: Title;

    public constructor(htmlTitle: Title){
        this._htmlTitle = htmlTitle;
    }

    setSuffix(suffix: string){
        this._suffix = suffix;
    }

    setTitle(title: string){
        this._htmlTitle.setTitle(title + this._suffix);
    }
}
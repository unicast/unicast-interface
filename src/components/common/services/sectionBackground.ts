import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class SectionBackground {

    private _level: number;
    public aliases: { [p:string]: number};
    public levelChanges: BehaviorSubject<any>;

    constructor () {
        this._level = 1;

        this.aliases = { first: 1, second: 2, third: 3, fourth: 4 };

        this.levelChanges = new BehaviorSubject( 1 );
    }

    get level () {
        return this._level % 5;
    }

    set level ( value ) {
        if ( value in this.aliases ) {
            this._level = this.aliases[ value ];
        } else {
            this._level = +value;
        }

        this.levelChanges.next( this.level );
    }
}
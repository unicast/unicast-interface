import ng, { ElementRef, Component, Input } from '@angular/core';
import { MediaDownloadSchema, MediaRecordSchema, TvEpisodeMediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';

@Component( {
    selector: 'uc-download-media-dropdown',
    templateUrl: './downloadMediaDropdown.template.html',
    styleUrls: [ './downloadMediaDropdown.styles.less' ]
} )
export class DownloadMediaDropdownComponent implements ng.OnChanges {
    /** Template Constants */
    public SourcesLoadingState = SourcesLoadingState;

    
    protected elementRef : ElementRef;

    protected sources: MediaDownloadSchema;
    
    protected sourcesRequestId: number = 0;


    /** When false, the dropdown will ave position: absolute. When true, whe dropdown will be position inside the document. Defaults to `false`. */
    @Input() embedded : boolean = false;

    /** When true, hides the "Download" label and displays only the icon */
    @Input() iconOnly : boolean = true;

    /** When true, the dropdown body will have the same width as the trigger. Defaults to false. */
    @Input() fullWidth : boolean = false;

    @Input() width : number = null;

    @Input() media: MediaRecordSchema;

    public sourcesState = SourcesLoadingState.NotLoaded;

    public constructor (
        private serverApi: ServerApiManager
    ) { }

    protected async fetchDownloadInformation (): Promise<MediaDownloadSchema> {
        // await new Promise<void>(resolve => setTimeout(resolve, 10000));

        if (this.media.kind === MediaKind.Movie) {
            return await this.serverApi.getMedia().getMovieApi().getDownload( this.media ).toPromise();
        } else if (this.media.kind === MediaKind.TvEpisode) {
            const showApi = this.serverApi.getMedia().getShowApi();

            const seasonId = ( this.media as TvEpisodeMediaRecordSchema ).tvSeasonId;

            const season = await showApi.getSeasonApi().get( seasonId ).toPromise();
            
            const show = await showApi.get( season.tvShowId ).toPromise();
            
            return await showApi.getEpisodeApi().getDownload( this.media, show ).toPromise()
        } else {
            throw new Error(`Media kind ${this.media.kind} does not provide media streams.`);
        }
    }

    public async load () {
        this.sourcesState = SourcesLoadingState.Loading;
        const requestId = this.sourcesRequestId;

        try {
            const sources = await this.fetchDownloadInformation();

            if (this.sourcesRequestId == requestId) {
                this.sources = sources;
                this.sourcesState = SourcesLoadingState.Loaded;
            }
        } catch {
            if (this.sourcesRequestId == requestId) {
                this.sourcesState = SourcesLoadingState.Error;
            }
        }
    }

    public onDropdownToggle (opened: boolean) {
        if ( opened && this.sourcesState === SourcesLoadingState.NotLoaded ) {
            this.load();
        }
    }

    public ngOnChanges (changes: ng.SimpleChanges) {
        if ( 'media' in changes ) {
            const media = changes.media.currentValue;
            const oldMedia = changes.media.previousValue;

            if ( media ) {
                // Only need to change if it's not the same media record
                if ( !oldMedia || oldMedia.kind !== media.kind || oldMedia.id !== media.id ) {    
                    this.sourcesState = SourcesLoadingState.NotLoaded;
                    this.sources = null;
                }                
            } else {
                // Do this to cancel any possible pending requests
                this.sourcesRequestId++;
                this.sourcesState = SourcesLoadingState.NotLoaded;
                this.sources = null;
            }
        }
    }

    clipboardCopy ( text : string ) {
        if ( typeof( ( navigator as any ).clipboard ) === 'undefined' ) {
            var textArea = document.createElement("textarea");

            textArea.value = text;

            textArea.style.position="fixed";  //avoid scrolling to bottom

            document.body.appendChild(textArea);

            textArea.focus();

            textArea.select();
        
            try {
                document.execCommand('copy');
            }finally {
                document.body.removeChild(textArea)            
            }
        } else {
            ( navigator as any ).clipboard.writeText(text);
        }
    }
}

enum SourcesLoadingState {
    NotLoaded = 0,
    Loading = 1,
    Loaded = 2,
    Error = 3,
}
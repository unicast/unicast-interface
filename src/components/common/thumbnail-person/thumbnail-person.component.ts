import { Component, Input, Output, EventEmitter } from '@angular/core';

import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { PersonRecordSchema } from 'components/schemas/mediaRecord.schema';
import { StandardMediaQueries } from 'components/services/mediaQueries';

@Component( {
    selector: 'uc-thumbnail-person',
    templateUrl: './thumbnail-person.template.html',
    styleUrls: [ './thumbnail-person.styles.less' ]
} )
export class ThumbnailPersonComponent extends BaseUnicastComponent{
    @Input() link : string[] | string;
    @Input( 'id' ) id: string;
    @Input( 'person' ) person : PersonRecordSchema;
    @Input( 'active' ) active : boolean;
    @Input( 'highlighted' ) highlighted : boolean;
    @Input( 'growOnActive' ) growOnActive : boolean = false;
    @Input( 'tags' ) tags: Array<string>;

    @Output( 'action' ) action : EventEmitter<void> = new EventEmitter;

    public showOverlayInfo: boolean;
    public mediaQueries : StandardMediaQueries;

    constructor ( mediaQueries : StandardMediaQueries ) {
        super();
        this.mediaQueries = mediaQueries;
    }

    protected _binds(){

    }

    ngOnChanges () {
        if ( this.person ) {
            this.id = this.id || this.person.id;
            if (this.person.cast && this.person.cast.role) {
                this.tags = ['cast.role'];
            }
        }
    }
}

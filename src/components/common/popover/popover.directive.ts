import { Directive, ElementRef, Input } from "@angular/core";
import Popper from 'popper.js'

@Directive( {
    selector: '[uc-popover]',
    exportAs: 'popover'
} )
export class PopoverDirective {
    @Input( 'popoverOpened' ) opened : boolean;

    @Input( 'popoverAnchor' ) anchor : ElementRef;

    @Input( 'popoverPlacement' ) placement : Popper.Placement = 'top';

    @Input( 'popoverFullscreen' ) fullscreen : boolean = false;

    protected element : ElementRef;

    protected isOpen : boolean = false;

    protected popperInstance : Popper;

    constructor ( element : ElementRef ) {
        this.element = element;
    }

    ngOnChanges ( ) {
        if ( ( !this.anchor && this.isOpen ) || ( this.isOpen && !this.opened ) ) {
            this.close();
        } else if ( !this.isOpen && this.opened ) {
            this.open();
        }
    }

    getFullscreenHeight () : number {
        const anchor = this.anchor.nativeElement as HTMLElement;

        return anchor.getBoundingClientRect().top - 5;
    }

    open () {
        this.isOpen = true;

        if ( this.fullscreen ) {
            this.element.nativeElement.style.height = '' + this.getFullscreenHeight() + 'px';
            this.element.nativeElement.style.minHeight = this.element.nativeElement.style.height;
            this.element.nativeElement.style.maxHeight = this.element.nativeElement.style.height;
        }

        this.popperInstance = new Popper( this.anchor.nativeElement, this.element.nativeElement, {
            placement: this.placement,
            modifiers: {
                offset: {
                    enabled: true,
                    order: 801,
                    fn : ( data ) => {
                        if ( this.placement === 'top' || this.placement === 'bottom' ) {
                            data.offsets.popper.left = 0;
                        }

                        if ( this.placement === 'top-end' || this.placement === 'bottom-end' ) {
                            data.offsets.popper.left = data.offsets.reference.width - data.offsets.popper.width;
                        } else if ( this.placement === 'top-start' || this.placement === 'bottom-start' ) {
                            data.offsets.popper.left = data.offsets.popper.width - data.offsets.reference.width;
                        }

                        return data;
                    }
                }
            }
        } );
        this.update(1);
    }

    update ( delay : number = 0 ) {
        if ( this.popperInstance ) {
            if ( delay === 0 ) {
                this.popperInstance.update();
            } else {
                setTimeout( () => this.popperInstance.update(), delay );
            }
        }
    }

    close () {
        this.isOpen = false;

        this.popperInstance.destroy();

        this.popperInstance = null;
    }
}
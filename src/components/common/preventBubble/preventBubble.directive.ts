import { Directive, Input, ElementRef, Renderer2 } from '@angular/core';

// WARNING: Be careful of using this component on elements with ngIf where the click
// event synchronously turns the condition to false. The HTML elemente is destroyed
// and the listener is never called.
@Directive({ selector: '[notBubble]' })
export class PreventBubbleDirective {
    @Input('notBubble') notBubble:String;
    private _elementRef: ElementRef;
    private _renderer: Renderer2;

    public listeners: Function[] = [];

    constructor(elementRef: ElementRef, renderer: Renderer2){
        this._elementRef = elementRef;
        this._renderer = renderer;
    }

    ngAfterViewInit () {
        this.notBubble = this.notBubble || "click";
        let events = this.notBubble.split(",");
        for(let evt of events){
            this.listeners.push(this._renderer.listen(this._elementRef.nativeElement, evt, (event) => {
                return this._event(event);
            }));
        }
    }

    ngOnDestroy() {
        if (this.listeners.length > 0){
            for (let fn of this.listeners) fn();

            this.listeners = [];
        }
    }

    _event(event:Event) {
        event.preventDefault();
        event.stopPropagation();
        return true;
    }
}

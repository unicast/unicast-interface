import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { Scrollable } from './scrollable.service';
import { Subscription } from 'rxjs/Subscription';

@Directive( { 
    selector: '[scrollable]',
    host: {
        // '[class.no-scroll]': '!allowed'
    }
} )
export class ScrollableDirective {
    @Input( 'scrollableSelector' ) scrollableSelector : string = null;
    
    @Input( 'scrollableParent' ) scrollableParent : boolean = false;

    protected scrollable : Scrollable;

    protected element : ElementRef;

    protected subscription : Subscription;

    protected scrollTop : number;

    protected renderer : Renderer2;
    
    public allowed = true;

    protected get targetElement () {
        let element : any = null;

        if ( this.scrollableSelector ) {
            element = document.querySelector( this.scrollableSelector );
        } else {
            element = this.element.nativeElement;
        }
        
        if ( element != null && this.scrollableParent ) {
            element = element.parentElement;
        }
        
        return element;
    }

    constructor ( scrollable : Scrollable, element : ElementRef, renderer : Renderer2 ) {
        this.scrollable = scrollable;
        this.element = element;
        this.renderer = renderer;

        this.subscription = this.scrollable.allowedChanges.subscribe( allowed => this.setScrollableAllowed( allowed ) );
    }

    setScrollableAllowed ( allowed : boolean ) {
        const element = this.targetElement;

        if ( element && allowed != this.allowed ) {
            if ( allowed ) {
                this.renderer.removeClass( element, 'no-scroll' );

                setTimeout( () => {
                    element.scrollTop = this.scrollTop;
                }, 10 );
            } else {
                this.scrollTop = element.scrollTop;

                this.renderer.addClass( element, 'no-scroll' );
            }

            this.allowed = allowed;
        }
    }

    ngOnDestroy () {
        this.subscription.unsubscribe();
    }
}

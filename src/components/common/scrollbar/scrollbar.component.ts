import { Component, Optional } from '@angular/core';
import { StandardMediaQueries } from '../../services/mediaQueries';
import { Scrollable } from './scrollable.service';

@Component( {
    selector: 'uc-scrollbar',
    styles: [ `.no-scroll {
        overflow: hidden;
    }
    :host {
        height: 100%;
        width: 100%;
        overflow: hidden;
    }
    ` ],
    template: `
    <perfect-scrollbar *ngIf="!isMobile; else elseTpl" [disabled]="scrollable && !scrollable.allowed">
        <ng-container *ngTemplateOutlet="elseTpl"></ng-container>
    </perfect-scrollbar>

    <ng-template #elseTpl><ng-content [class.no-scroll]="scrollable && !scrollable.allowed"></ng-content> </ng-template>
    `,
} )
export class ScrollbarComponent {
    isMobile : boolean;

    scrollable : Scrollable;

    constructor ( queries : StandardMediaQueries, @Optional() scrollable : Scrollable ) {
        this.isMobile = queries.isMobile;

        this.scrollable = scrollable;
    }
}
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class Scrollable {
    protected _allowed : boolean;

    public get allowed () : boolean {
        return this._allowed;
    }

    public set allowed ( value : boolean ) {
        if ( this._allowed != value ) {
            this._allowed = value;

            this.allowedChanges.next( this._allowed );
        }
    }

    public allowedChanges : BehaviorSubject<boolean>;

    constructor () {
        this.allowedChanges = new BehaviorSubject( true );

        this.allowed = true;
    }
}
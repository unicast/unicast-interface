import { Directive, Input, ElementRef, OnChanges, SimpleChanges } from '@angular/core';
import { ModalManager } from '../../services/modalManager';
import { Subscription } from 'rxjs/Subscription';
import { Scrollable } from './scrollable.service';

@Directive( { 
    selector: '[modalScrollbar]' ,
    providers: [ Scrollable ]
} )
export class ModalScrollbarDirective implements OnChanges {
    @Input( 'modalScrollbar' ) target : string;

    @Input( 'modalScrollbarEnabled' ) enabled : boolean = true;

    protected modals : ModalManager;
    protected modalToken : Symbol;
    protected modalSubscription : Subscription;
    protected element : ElementRef;
    protected scrollable : Scrollable;

    constructor ( modals : ModalManager, element : ElementRef, scrollable : Scrollable ) {
        this.modals = modals;
        this.element = element;
        this.scrollable = scrollable;

        if ( this.enabled ) {
            this.register();
        }
    }

    register () {
        if ( this.modalToken ) {
            this.dispose();
        }

        this.modalToken = this.modals.register( this );

        this.modalSubscription = this.modals.currentToken.subscribe( token => this.calculateScrollState( token ) );
    }

    dispose () {
        if ( this.modalToken ) {
            this.modalSubscription.unsubscribe();

            this.modals.dispose( this.modalToken );

            this.scrollable.allowed = true;

            this.modalToken = null;

            this.modalSubscription = null;
        }
    }

    calculateScrollState ( currentToken : Symbol ) {
        const disabled = this.modalToken && currentToken != this.modalToken;

        this.setScrollState( !disabled );
    }

    setScrollState ( enabled : boolean ) {
        this.scrollable.allowed = enabled;
    }

    ngOnChanges ( changes : SimpleChanges ) {
        if ( 'enabled' in changes ) {
            const curr = changes[ 'enabled' ].currentValue;

            if ( curr == true ) {
                this.register();
            } else if ( curr == false ) {
                this.dispose();
            }
        }
    }
}

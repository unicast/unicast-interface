import { Component, Input } from '@angular/core';
import { Toast } from './toast.service';

@Component( {
    selector: 'uc-toast',
    template: `<div class="toast-container" [class.show]="message != null"><div class="toast">{{message}}</div></div>`,
    styleUrls: [ './toast.styles.less' ],
} )
export class ToastComponent {
    @Input() tags;

    public message : string = null;

    public service : Toast;

    public exitTimeout : any = null;

    public constructor ( service : Toast ) {
        this.service = service;

        this.service.onMessage.subscribe( ( message ) => {
            this.message = message;

            if ( this.exitTimeout != null ) {
                clearTimeout( this.exitTimeout );
            }

            this.exitTimeout = setTimeout( () => {
                this.exitTimeout = null;

                this.message = null;
            }, 2500 );
        } );
    }
}
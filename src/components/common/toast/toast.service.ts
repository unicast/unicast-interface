import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class Toast {
    protected onMessageSubject = new Subject<string>();

    public onMessage : Observable<string> = this.onMessageSubject.asObservable();

    show ( message : string ) {
        this.onMessageSubject.next( message );
    }
}
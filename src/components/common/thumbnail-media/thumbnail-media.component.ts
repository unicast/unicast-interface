import { Component, Input, Output, EventEmitter } from '@angular/core';

import { BaseUnicastComponent } from 'components/common/helpers/baseUnicastComponent';
import { MediaRecordSchema, TvShowMediaRecordSchema, MovieMediaRecordSchema, TvEpisodeMediaRecordSchema, TvSeasonMediaRecordSchema, CustomMediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { StandardMediaQueries } from 'components/services/mediaQueries';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { MediaButtonAction } from 'components/pages/common/buttonActions/buttonAction';
import { DefaultMediaButtonActions } from 'components/pages/common/buttonActions/defaultMediaButtonActions';


@Component( {
    selector: 'uc-thumbnail-media',
    templateUrl: './thumbnail-media.template.html',
    styleUrls: [ './thumbnail-media.styles.less' ]
} )
export class ThumbnailMediaComponent extends BaseUnicastComponent{

    @Input() link : string[] | string;
    @Input( 'id' ) id: string;
    @Input( 'media' ) media : MediaRecordSchema;
    @Input( 'active' ) active : boolean;
    @Input( 'highlighted' ) highlighted : boolean;
    @Input( 'growOnActive' ) growOnActive : boolean = false;
    @Input( 'actionObject' ) actionObject: unknown;
    @Input( 'tags' ) tags: Array<string>;

    @Output( 'action' ) action : EventEmitter<void> = new EventEmitter;

    public showOverlayInfo: boolean;
    public mediaQueries : StandardMediaQueries;

    public mediaOrientation : string = 'vertical';
    public mediaImage : string;
    public mediaLabel : string;
    public mediaCaption : string;
    public mediaRating : number;
    public mediaState : string;
    public mediaHighlighted : boolean;

    public defaultTags: Array<string>;

    @Input( 'contextMenu' ) contextMenu : Array<MediaButtonAction> = [];

    constructor ( mediaQueries : StandardMediaQueries, defaultButtons: DefaultMediaButtonActions ) {
        super();
        this.mediaQueries = mediaQueries;

        this.contextMenu = defaultButtons.getAllActions();
    }

    protected _binds(){

    }

    onCustomChange () {
        const media : CustomMediaRecordSchema = this.media as any;

        this.mediaOrientation = media.cachedArtwork.poster ? 'vertical' : 'horizontal';
        this.mediaImage = media.cachedArtwork.poster || media.cachedArtwork.thumbnail;
        this.mediaLabel = media.title;
        this.mediaCaption = media.subtitle;
        this.mediaState = media.watched ? 'eye' : null;
        this.mediaHighlighted = media.watched;
        this.defaultTags = ['metadata.video.resolution', 'metadata.source', 'metadata.video.colorspace', 'metadata.video.bitdepth', 'metadata.video.codec', 'metadata.audio.channels'];
    }

    onMovieChange () {
        const media : MovieMediaRecordSchema = this.media as any;

        this.mediaOrientation = 'vertical';
        this.mediaImage = media.cachedArtwork.poster;
        this.mediaLabel = media.title;
        this.mediaCaption = '' + (media.year || '-');
        this.mediaRating = media.rating;
        this.mediaState = media.watched ? 'eye' : null;
        this.mediaHighlighted = media.watched;
        this.defaultTags = ['metadata.video.resolution', 'metadata.source', 'metadata.video.colorspace', 'metadata.video.bitdepth', 'metadata.video.codec', 'metadata.audio.channels'];
    }

    onTvShowChange () {
        const media : TvShowMediaRecordSchema = this.media as any;

        this.mediaOrientation = 'vertical';
        this.mediaImage = media.cachedArtwork.poster;
        this.mediaLabel = media.title;
        this.mediaCaption = '' + media.seasonsCount + ' Seasons';
        this.mediaRating = media.rating;
        this.mediaHighlighted =  media.watchedEpisodesCount >= media.episodesCount;
        this.mediaState = media.watchedEpisodesCount >= media.episodesCount ? 'eye' : null;
        this.defaultTags = [];
    }

    onTvSeasonChange () {
        const media : TvSeasonMediaRecordSchema = this.media as any;

        this.mediaOrientation = 'vertical';
        this.mediaImage = media.cachedArtwork.poster;
        this.mediaLabel = 'Season ' + media.number;
        this.mediaCaption = '' + media.episodesCount + ' episodes';
        this.mediaRating = undefined;
        this.mediaState = media.watchedEpisodesCount >= media.episodesCount ? 'eye' : null;
        this.mediaHighlighted = media.watchedEpisodesCount >= media.episodesCount;
        this.defaultTags = [];
    }

    onTvEpisodeChange () {
        const media : TvEpisodeMediaRecordSchema = this.media as any;

        this.mediaOrientation = 'horizontal';
        this.mediaImage = media.cachedArtwork.thumbnail;
        this.mediaLabel = media.title;
        this.mediaCaption = 'Episode ' + media.number;
        this.mediaRating = media.rating;
        this.mediaState = media.watched ? 'eye' : null;
        this.mediaHighlighted = media.watched;
        this.defaultTags = ['metadata.video.resolution', 'metadata.source', 'metadata.video.colorspace', 'metadata.video.bitdepth', 'metadata.video.codec', 'metadata.audio.channels'];
    }

    ngOnChanges () {
        if ( this.media ) {
            this.id = this.id || this.media.id;
            this.actionObject = this.actionObject || this.media;
            switch ( this.media.kind ) {
                case MediaKind.Movie:
                    this.onMovieChange();
                    break;
                case MediaKind.TvShow:
                    this.onTvShowChange();
                    break;
                case MediaKind.TvSeason:
                    this.onTvSeasonChange();
                    break;
                case MediaKind.TvEpisode:
                    this.onTvEpisodeChange();
                    break;
                case MediaKind.Custom:
                default:
                    this.onCustomChange();
                    break;

            }
        }
    }
}

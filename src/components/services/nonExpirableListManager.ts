import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { MediaRecordKeySchema } from 'components/schemas/mediaRecord.schema';
import { ServerStorageOperationResult } from 'components/services/serverApis/storageServerApi';

enum KnownNonExpirableLists {
    ResyncList = 'resyncList'
}

@Injectable()
export class NonExpirableListManager {
    private serverApi: ServerApiManager;
    private cache: Map<string, Observable<Array<unknown>>>;

    constructor(serverApi : ServerApiManager) {
        this.serverApi = serverApi;

        this.cache = new Map();
    }

    getList<T>(key: string, refresh: boolean = false): Observable<Array<T>> {
        if( !this.cache.has(key) || refresh ){
            const obs = this.serverApi.getStorage().getItem<Array<T>>(key).map(e => e || {value : []}).map(e => e.value || []).do(v => this.cache.set(key, of(v)));
            this.cache.set(key, obs);
        }

        return this.cache.get(key) as Observable<Array<T>>;
    }

    private findIndexInList<T>(key: string, value: T, primaryKeys?: Array<keyof T>): Observable<number> {
        return this.getList<T>(key).map(v => {
            primaryKeys = (primaryKeys || Object.keys(value) as Array<keyof T>);
            return v.findIndex(e => primaryKeys.every(p => e[p] == value[p]));
        });
    }

    storeList<T>(key: string, value: Array<T>): Observable<Array<T>> {
        return this.serverApi.getStorage().setItem<Array<T>>(key, value || []).map(e => e.value);
    }

    addToList<T>(key: string, value: T, primaryKeys?: Array<keyof T>): Observable<ServerStorageOperationResult> {
        return this.serverApi.getStorage().addToSet(key, value, primaryKeys);
    }

    removeFromList<T>(key: string, value: T, primaryKeys?: Array<keyof T>): Observable<ServerStorageOperationResult> {
        let result;
        return this.serverApi.getStorage().removeFromSet(key, value, primaryKeys)
            .do(v => result = v)
            .concatMap(v => v.changed ? this.findIndexInList(key, value, primaryKeys) : of(-1))
            .map(v => result)
    }

    containsInList<T>(key: string, value: T, primaryKeys?: Array<keyof T>): Observable<boolean> {
        //Can be improved to no search the array all the times?
        return this.getList<T>(key).map(v => {
            primaryKeys = (primaryKeys || Object.keys(value) as Array<keyof T>);
            return v.some(e => primaryKeys.every(p => e[p] == value[p]));
        });
    }

    private transformMediaRecordKeySchema(value: MediaRecordKeySchema): MediaRecordKeySchema {
        return { id: value.id, kind: value.kind };
    }

    getResyncMediaList(): Observable<Array<MediaRecordKeySchema>> {
        return this.getList(KnownNonExpirableLists.ResyncList);
    }

    addToResyncMediaList(value: MediaRecordKeySchema): Observable<boolean> {
        return this.addToList(KnownNonExpirableLists.ResyncList, this.transformMediaRecordKeySchema(value)).map(e => e.changed);
    }

    removeFromResyncMediaList(value: MediaRecordKeySchema): Observable<boolean> {
        return this.removeFromList(KnownNonExpirableLists.ResyncList, this.transformMediaRecordKeySchema(value)).map(e => e.changed);
    }

    containsInResyncMediaList(value: MediaRecordKeySchema): Observable<boolean> {
        return this.containsInList(KnownNonExpirableLists.ResyncList, this.transformMediaRecordKeySchema(value));
    }

}
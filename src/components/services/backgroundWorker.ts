import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { SingularSubject } from 'components/common/helpers/singularSubject';


@Injectable()
export class BackgroundWorker {
    private static readonly TIME_TO_WAIT_MS: number = 2000;
    private static readonly MARGIN_FACTOR: number = 5;

    private _visibility: ReplaySubject<DocumentVisibilityState>;
    public visibilityChanged: Observable<DocumentVisibilityState>;
    public isVisibleObservable: Observable<DocumentVisibilityState>;
    public isHiddenObservable: Observable<DocumentVisibilityState>;

    constructor() {

        this._visibility = SingularSubject.create<DocumentVisibilityState>(document.visibilityState, true);
        this.visibilityChanged = this._visibility.asObservable();
        [this.isVisibleObservable, this.isHiddenObservable] = this.visibilityChanged.partition( (v) => v == 'visible' );

        window.addEventListener('visibilitychange', () => {
            this._visibility.next( document.visibilityState );
        }, true);

        /*
         * If it is a Safari Web App, the previous listner will not work
         * Because of that is needed a less elagnt solution and also more processor consuming
         */
        if (window.navigator['standalone'] != undefined && window.navigator['standalone']){
            let lastTime = Date.now();
            let func = () => {
                if (lastTime + (BackgroundWorker.TIME_TO_WAIT_MS * BackgroundWorker.MARGIN_FACTOR) < Date.now()){
                    lastTime = Date.now();
                    //Looks like I felt asleep and woke up again. Let me enforce it
                    this.reinforceState();
                }
                lastTime = Date.now();
            };
            //Start the timer
            setInterval(func, BackgroundWorker.TIME_TO_WAIT_MS);
        }

    }

    isVisible(): boolean {
        return document.visibilityState == 'visible';
    }

    isHidden(): boolean {
        return !this.isVisible();
    }

    reinforceState() {
        this._visibility.next(this.isVisible() ? 'hidden' : 'visible');
        this._visibility.next(document.visibilityState);
    }

    reinforceVisible(){
        this._visibility.next('hidden');
        this._visibility.next('visible');
    }


}

import { Injectable } from '@angular/core';

@Injectable()
export class TopbarManager {
    pageTitle : string = null;

    defaultTitle : string = 'App';

    get title () {
        return this.pageTitle || this.defaultTitle;
    }

    setTitle ( title : string = null ) {
        this.pageTitle = title;
    }

    clearTitle () {
        this.pageTitle = null;
    }

    setDefaultTitle ( title : string = null ) {
        this.defaultTitle = title;
    }
}
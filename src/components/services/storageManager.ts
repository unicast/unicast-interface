import { Injectable } from '@angular/core';

export abstract class StorageManager {
    private static readonly DATE_ISO_REGEX = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/;

    protected _storage: Storage;

    protected constructor(storage: Storage){
        this._storage = storage;
    }

    private getRealValue(value: { toJSON: () => {}} | string | {}) {
        return ( !!value && typeof value['toJSON'] === "function" ? value['toJSON'](): value);
    }

    public set(key: string, value: { toJSON: () => {}} | string | {}): void{
        let obj = this.getRealValue(value);
        this._storage.setItem(key, JSON.stringify(obj));
    }

    public get(key: string, defaultValue: any = {}): any{
        let item = this._storage.getItem(key);
        if (item === null){
            item = JSON.stringify(defaultValue);
        }
        return JSON.parse(item, (key, value) => {
            let a = StorageManager.DATE_ISO_REGEX.exec(value);
            return a ?  new Date(value) : value;
        });
    }

    public remove(key: string) {
        this._storage.removeItem(key);
    }


}

@Injectable()
export class LocalStorageManager extends StorageManager{
    public constructor(){
        super(window.localStorage);
    }
}

@Injectable()
export class SessionStorageManager extends StorageManager{
    public constructor(){
        super(window.sessionStorage);
    }
}
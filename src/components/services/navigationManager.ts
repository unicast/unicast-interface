import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Location } from '@angular/common';

import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { RouteType } from 'components/routes/utils';

@Injectable()
export class NavigationManager {
    private _router: Router;
    private _location: Location;

    public constructor(router: Router, location: Location) {
        this._router = router;
        this._location = location;
    }

    /**
     * This method will navigate to /
     */
    public navigateToHome() {
        return this.navigateTo('');
    }

    /**
     * Generic navigation method
     *
     * @param item an media item to navigate to
     * @param params Parameters to be on url, like subtitles
     * @param extras NavigationParameters to provide to router
     */
    public navigateTo(item: MediaRecordSchema, params?: {}, extras?: NavigationExtras);
    public navigateTo(kind: MediaKind, params?: {}, extras?: NavigationExtras);
    public navigateTo(str: string, params?: {}, extras?: NavigationExtras);
    public navigateTo(obj: MediaRecordSchema | string | MediaKind, params?: {}, extras?: NavigationExtras) {
        if (this.isMediaRecordSchema(obj)){
            this._navigateToItem(obj, params, extras);
        } else if ( typeof obj === 'string') {
            this._navigateTo(obj, [], params, extras);
        } else { //MediaKind
            this._router.navigate([...this.getEndpoint(obj), params], extras);
        }
    }

    /**
     * This method should be used to genererate a direct link to a Movie's page, unlike the generic @function navigateTo
     *
     * @param id Movie's id
     * @param params Parameters to be on url, like subtitles
     * @param extras NavigationParameters to provide to router
     */
    public navigateToMovie(id?: string, params?: {}, extras?: NavigationExtras) {
        this._navigateTo(RouteType.MOVIE, [id], params, extras);
    }

    /**
     * This method should be used to genererate a direct link to a TV Show's page, unlike the generic @function navigateTo
     *
     * @param id Show's id
     * @param season Number of the season
     * @param episode Episode's id
     * @param params Parameters to be on url, like subtitles
     * @param extras NavigationParameters to provide to router
     */
    public navigateToShow(id?: string, season?: number, episode?: string, params?: {}, extras?: NavigationExtras) {
        params = params || {};
        if (season) {
            params['season'] = season;
        }
        if (episode){
            params['episode'] = episode;
        }
        return this._navigateTo(RouteType.SHOW, [id], params, extras);
    }

    public navigateToPeople(id?: string, params?: {}, extras?: NavigationExtras) {
        return this._navigateTo(RouteType.PEOPLE, [id], params, extras);
    }

    public navigateToOutside(url: string, opts?: Partial<{ schema: 'http' | 'https', target: string, event: MouseEvent | KeyboardEvent}> ) {
        opts = { schema: 'https', target: opts.event && opts.event.ctrlKey ? '_blank' : '_self' , ...opts };
        window.open(opts.schema + '://' + url, opts.target);
    }

    public navigateBack() {
        if (this._router.navigated) {
            this._location.back();
        }
    }

    public navigateForward() {
        this._location.forward();
    }

    private getEndpoint(kind: MediaKind, item?: MediaRecordSchema): Array<string>{
        let id = (item ? item.id : '');
        switch(kind){
            case MediaKind.Movie:
            case MediaKind.TvEpisode:
            case MediaKind.TvSeason:
            case MediaKind.TvShow:
                return [RouteType.MEDIAS + '/' + item.kind, id];
            case MediaKind.Custom:
                return [RouteType.CUSTOM, id];
            default:
                return ['', undefined];
        }
    }

    private isMediaRecordSchema(item: any): item is MediaRecordSchema {
        return !!item['kind'];
    }

    private _navigateToItem(item: MediaRecordSchema, params?: {}, extras?: NavigationExtras) {
        let endpoint = this.getEndpoint(item.kind, item);
        params = params || {};
        this._navigateTo(endpoint[0], endpoint.slice(1), params, extras);
    }

    private _navigateTo(endpoint: string, url?: Array<string>, params?: {}, extras?: NavigationExtras){
        let arr: Array<any> = ['/' + endpoint].concat(url).filter( value => value !== undefined );
        if (params){
            arr.push(params);
        }
        return this._router.navigate(arr, extras);
    }
}
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DisposableObservableStack } from 'components/common/services/disposableObservableStack';

@Injectable()
export class ModalManager {

    public stack: DisposableObservableStack;

    constructor () {
        this.stack = new DisposableObservableStack;
    }

    get current (): Observable<any> {
        return this.stack.current;
    }

    get currentToken () : Observable<Symbol> {
        return this.stack.currentToken;
    }

    register ( modal ) : Symbol {
        return this.stack.push( modal );
    }

    dispose ( token : Symbol ) {
        return this.stack.dispose( token );
    }
}
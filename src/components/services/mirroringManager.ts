import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { SingularSubject } from 'components/common/helpers/singularSubject';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';

export interface MirroringOptions {
    silent?: boolean;
    force?: boolean;
}

@Injectable()
export class MirroringManager {
    private static readonly DEFAULT_OPTS: MirroringOptions = { silent: false, force: false };

    private serverApi: ServerApiManager;
    private media: MediaRecordSchema;
    private mirroring: boolean = false;
    private mirroringSubject: ReplaySubject<boolean> = SingularSubject.create(false);
    public mirroringObsevable: Observable<boolean> = this.mirroringSubject.asObservable();

    constructor(serverApi: ServerApiManager) {
        this.serverApi = serverApi;

        this.mirroringObsevable.subscribe( e => this.mirroring = e );
    }

    ngOnDestroy() {
        this.mirroringSubject.complete();
    }

    mirrorMedia(media: MediaRecordSchema, mirrorOpts?: MirroringOptions) {
        mirrorOpts = { ...MirroringManager.DEFAULT_OPTS, ...mirrorOpts };
        this.media = mirrorOpts.silent ? this.media : media;
        if ( this.mirroring || mirrorOpts.force ) {
            return this.serverApi.getPlayer().getDeviceApi().openMediaPage(media);
        }
    }

    mirrorLastMedia() {
        return this.mirrorMedia(this.media, { force: true });
    }

    toggleMirroring() {
        this.setMirroring(!this.mirroring);
    }

    setMirroring(value: boolean) {
        this.mirroringSubject.next(value);
    }
}
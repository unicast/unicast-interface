import { Injectable } from '@angular/core';

import { KindServerApi } from 'components/services/serverApis/mediaServers/kindServerApi';
import { SeasonServerApi } from 'components/services/serverApis/mediaServers/seasonServerApi';
import { EpisodeServerApi } from 'components/services/serverApis/mediaServers/episodeServerApi';
import { MediaServerApi } from 'components/services/serverApis/mediaServers/mediaServerApi';
import { TvShowMediaRecordSchema } from 'components/schemas/mediaRecord.schema';

@Injectable()
export class ShowServerApi extends KindServerApi<TvShowMediaRecordSchema> {
    private _episodeApi: EpisodeServerApi;
    private _seasonApi: SeasonServerApi;

    constructor(serverApi:MediaServerApi) {
        super(serverApi, 'show', [ 'art', 'title', 'rating', 'season', 'watchedepisodes', 'plot', 'mpaa', 'genre' ], 'seasons');
        this._episodeApi = new EpisodeServerApi(serverApi);
        this._seasonApi = new SeasonServerApi(serverApi);
    }

    getSeasonApi(): SeasonServerApi{
        return this._seasonApi;
    }

    getEpisodeApi(): EpisodeServerApi{
        return this._episodeApi;
    }
}

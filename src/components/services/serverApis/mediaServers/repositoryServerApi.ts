import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApi } from 'components/services/serverApis/serverApi';
import { MediaServerApi } from 'components/services/serverApis/mediaServers/mediaServerApi';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { VirtualRepository, Repository } from 'components/schemas/repository.schema';


@Injectable()
export class RepositoryServerApi extends ServerApi<MediaServerApi> {

    constructor(serverApi:MediaServerApi) {
        super(serverApi, 'repositories');
    }

    list(kinds: Array<MediaKind>, opts: {virtual?: true}): Observable<Array<VirtualRepository>>;
    list(kinds: Array<MediaKind>, opts?: {virtual?: boolean}): Observable<Array<Repository>>;
    list(kinds: Array<MediaKind>, opts: {virtual?: boolean} = {}): Observable<Array<VirtualRepository | Repository>> {
        return this.request({method: 'GET', data: {kinds: kinds.join(','), virtual: opts.virtual || false}});
    }

}
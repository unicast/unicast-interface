import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApi } from 'components/services/serverApis/serverApi';
import { MediaServerApi } from 'components/services/serverApis/mediaServers/mediaServerApi';
import { HistoryRecordSchema } from 'components/schemas/mediaRecord.schema';


@Injectable()
export class HistoryServerApi extends ServerApi<MediaServerApi> {

    constructor(serverApi:MediaServerApi) {
        super(serverApi, 'sessions');
    }

    list(take: number, skip: number = 0, opts: {} = { }): Observable<Array<HistoryRecordSchema>> {
        return this.request( {data: { ...opts, take: take, skip: skip}, method: 'GET'});
    }

    remove(id: string): Observable<any>{
        return this.request( {command: id, method: 'DELETE'});
    }
}
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApi } from 'components/services/serverApis/serverApi';
import { KindServerApi } from 'components/services/serverApis/mediaServers/kindServerApi';



@Injectable()
export class GenreServerApi extends ServerApi<KindServerApi<any>> {

    constructor(serverApi:KindServerApi<any>) {
        super(serverApi, 'genres');
    }

    list(): Observable<Array<string>> {
        return this.request({method: 'GET'});
    }

}
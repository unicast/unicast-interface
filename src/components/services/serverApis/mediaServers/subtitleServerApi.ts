import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApi } from 'components/services/serverApis/serverApi';
import { MediaServerApi } from 'components/services/serverApis/mediaServers/mediaServerApi';
import { GroupedSubtitlesSchema, SubtitleSchema } from 'components/schemas/mediaRecord.schema';

export enum SubtitleSyncAlign {
    Validate = 'validate',
    Binary = 'binary',
    Point = 'point',
    Delay = 'delay'
}

export enum SubtitleType {
    Local = 'local',
    Remote = 'remote'
}

@Injectable()
export class SubtitleServerApi extends ServerApi<MediaServerApi> {

    private languagues: Array<string> = ['por'];

    constructor(serverApi:MediaServerApi) {
        super(serverApi, 'subtitles');
    }

    private getLanguaguesOpt(type: SubtitleType = SubtitleType.Remote) : string {
        return type == SubtitleType.Remote ? this.languagues.join(',') : undefined;
    }

    getLanguages(): Array<string> {
        return [].concat(this.languagues);
    }

    setLanguages(...languages: Array<string>) {
        this.languagues = [].concat(languages);
    }

    add(kind: string, id: string, subtitles: SubtitleSchema[], grouped: boolean = false){
        return this.request( {command: [kind, id], data: {subtitles: subtitles, grouped: grouped}, method: 'POST'});
    }

    remove(kind: string, mediaId: string, subtitleId: string){
        return this.request( {command: [kind, mediaId, SubtitleType.Local, subtitleId], data: {}, method: 'DELETE'});
    }

    rename(kind: string, mediaId: string, subtitle: Partial<SubtitleSchema>){
        return this.request( {command: [kind, mediaId, SubtitleType.Local, subtitle.id, 'rename'], data: { ...subtitle }, method: 'POST'});
    }

    validate(kind: string, mediaId: string, subtitleId: string, type: SubtitleType = SubtitleType.Local, opts: {} = {}){
        return this.request( {command: [kind, mediaId, type, subtitleId, 'validate'], data: { langs:this.getLanguaguesOpt(type), ...opts }, method: 'GET'});
    }

    synchronize(kind: string, mediaId: string, subtitleId: string, type: SubtitleType = SubtitleType.Local, mode: SubtitleSyncAlign = SubtitleSyncAlign.Binary, opts: {} = {} ){
        return this.request( {command: [kind, mediaId, type, subtitleId, 'synchronize', mode], data: { langs:this.getLanguaguesOpt(type), ...opts }, method: 'GET'});
    }

    validateLocal(kind: string, mediaId: string, subtitleId: string){
        return this.validate(kind, mediaId, subtitleId, SubtitleType.Local);
    }

    validateRemote(kind: string, mediaId: string, subtitleId: string){
        return this.validate(kind, mediaId, subtitleId, SubtitleType.Remote);
    }

    list(kind: string, id: string, type: SubtitleType = SubtitleType.Local, opts: {} = {}): Observable<Array<SubtitleSchema>>{
        return this.request( {command: [kind, id, type], data: { langs:this.getLanguaguesOpt(type), ...opts }, method: 'GET' });
    }

    listGrouped(kind: string, id: string, type: SubtitleType = SubtitleType.Local, opts: {} = {}): Observable<Array<GroupedSubtitlesSchema>>{
        return this.request( {command: [kind, id, type], data: { langs: this.getLanguaguesOpt(type), grouped: true, ...opts }, method: 'GET' });
    }

    listLocal(kind: string, id: string, opts: Object = {}): Observable<Array<SubtitleSchema>>{
        return this.list(kind, id, SubtitleType.Local, opts);
    }

    listRemote(kind: string, id: string, opts: Object = {}): Observable<Array<SubtitleSchema>>{
        return this.list(kind, id, SubtitleType.Remote, opts);
    }
}

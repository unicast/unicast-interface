import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { KindServerApi } from 'components/services/serverApis/mediaServers/kindServerApi';
import { MediaServerApi } from 'components/services/serverApis/mediaServers/mediaServerApi';
import { TvEpisodeMediaRecordSchema } from 'components/schemas/mediaRecord.schema';

@Injectable()
export class EpisodeServerApi extends KindServerApi<TvEpisodeMediaRecordSchema> {

    constructor(serverApi:MediaServerApi) {
        super(serverApi, 'episode');
    }

    getEpisode(id: string): Observable<TvEpisodeMediaRecordSchema>{
        return this._serverApi.get(this._kind, id);
    }

    getEpisodes(show: string, season: string): Observable<Array<TvEpisodeMediaRecordSchema>>{
        return this._serverApi.get(this._kind, '', {show: show, seasonNumber: season});
    }
}

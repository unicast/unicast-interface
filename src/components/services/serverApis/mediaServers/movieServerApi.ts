import { Injectable } from '@angular/core';

import { KindServerApi } from 'components/services/serverApis/mediaServers/kindServerApi';
import { MediaServerApi } from 'components/services/serverApis/mediaServers/mediaServerApi';
import { MovieMediaRecordSchema } from 'components/schemas/mediaRecord.schema';


@Injectable()
export class MovieServerApi extends KindServerApi<MovieMediaRecordSchema> {

    constructor(serverApi:MediaServerApi) {
        super(serverApi, 'movie', [ 'art', 'title', 'rating', 'plot', 'mpaa', 'genre', 'collections' ]);
    }

}
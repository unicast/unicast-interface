import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApi } from 'components/services/serverApis/serverApi';
import { MediaServerApi } from 'components/services/serverApis/mediaServers/mediaServerApi';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';

export enum RankDirection {
    Before = 'before',
    After = 'after'
}

@Injectable()
export class UserRankServerApi extends ServerApi<MediaServerApi> {

    constructor(serverApi:MediaServerApi) {
        super(serverApi, 'user-ranks');
    }

    get(id: string): Observable<Array<MediaRecordSchema>>{
        return this.request({ command: [ id ], method: 'GET'});
    }

    setRankDirection(direction: RankDirection, id: string, media: MediaRecordSchema, records: Array<MediaRecordSchema>): Observable<any>{
        return this.request({ command: [ id, 'set-rank-' + direction ], data: { anchor: media, records: records } });
    }

    setRankBefore(id: string, media: MediaRecordSchema, records: Array<MediaRecordSchema>): Observable<any>{
        return this.setRankDirection(RankDirection.Before, id, media, records);
    }

    setRankAfter(id: string, media: MediaRecordSchema, records: Array<MediaRecordSchema>): Observable<any>{
        return this.setRankDirection(RankDirection.After, id, media, records);
    }

}
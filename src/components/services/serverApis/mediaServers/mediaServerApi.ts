import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { ServerApiBase } from 'components/services/serverApis/serverApiBase';
import { ServerApi } from 'components/services/serverApis/serverApi';
import { MovieServerApi } from 'components/services/serverApis/mediaServers/movieServerApi';
import { ShowServerApi } from 'components/services/serverApis/mediaServers/showServerApi';
import { SubtitleServerApi } from 'components/services/serverApis/mediaServers/subtitleServerApi';
import { CollectionServerApi } from 'components/services/serverApis/mediaServers/collectionServerApi';
import { SingularSubject } from 'components/common/helpers/singularSubject';
import { MediaRecordSchema, PlayableMediaRecordSchema, ArtRecordSchema, PersonRecordSchema, PlayableMediaQualities, MediaProbeRecordSchema } from 'components/schemas/mediaRecord.schema';
import { ProviderServerApi } from 'components/services/serverApis/mediaServers/providerServerApi';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { HistoryServerApi } from 'components/services/serverApis/mediaServers/historyServerApi';
import { LayoutUtils } from 'components/common/helpers/layoutUtils';
import { PeopleServerApi } from 'components/services/serverApis/mediaServers/peopleServerApi';
import { RepositoryServerApi } from 'components/services/serverApis/mediaServers/repositoryServerApi';
import { UserRankServerApi } from 'components/services/serverApis/mediaServers/userRankServerApi';


@Injectable()
export class MediaServerApi extends ServerApi<ServerApiBase> {
    private _movieApi: MovieServerApi;
    private _showApi: ShowServerApi;
    private _subtitleApi: SubtitleServerApi;
    private _collectionApi: CollectionServerApi;
    private _providersApi: ProviderServerApi;
    private _historyApi: HistoryServerApi;
    private _peopleApi: PeopleServerApi;
    private _repositoryApi: RepositoryServerApi;
    private _userRanksApi: UserRankServerApi;

    private _filtered: string = undefined;

    private _filterChanged: ReplaySubject<[string, boolean]>;
    public filterChanged: Observable<[string, boolean]>;

    constructor(serverApi: ServerApiBase){
        super(serverApi, 'media');
        this._movieApi = new MovieServerApi(this);
        this._showApi = new ShowServerApi(this);
        this._subtitleApi = new SubtitleServerApi(this);
        this._collectionApi = new CollectionServerApi(this);
        this._providersApi = new ProviderServerApi(this);
        this._historyApi = new HistoryServerApi(this);
        this._peopleApi = new PeopleServerApi(this);
        this._repositoryApi = new RepositoryServerApi(this);
        this._userRanksApi = new UserRankServerApi(this);

        //TODO: Rethink that observable. Perhaps having one with memory and other without, to avoid skips when called
        this._filterChanged = SingularSubject.create<[string, boolean]>([undefined, false], true);
        this.filterChanged = this._filterChanged.asObservable();
    }

    public emptyRecord(): MediaRecordSchema {
        let emptyImg = LayoutUtils.EMPTY_IMG;
        return {
            kind: MediaKind.Empty,
            id : '',
            internalId : '',
            repository : '',
            title : '',
            collections: [],
            art : {
                thumbnail : emptyImg,
                poster : emptyImg,
                background : emptyImg,
                banner : emptyImg,
            },
            cachedArtwork: {
                thumbnail : emptyImg,
                poster : emptyImg,
                background : emptyImg,
                banner : emptyImg
            },
            external : {}
        };
    }

    watched(kind: string, id: string, value: boolean = true): Observable<PlayableMediaRecordSchema>{
        return this.request( {command: [kind, id, 'watch', value.toString()], method: 'POST'});
    }

    filter(value: string, byUser: boolean): void{
        this._filtered = value || undefined;
        this._filterChanged.next([this._filtered, byUser]);
    }

    getActualFilter(): string {
        return this._filtered || '';
    }

    list(kind: string, take: number, skip: number = 0, opts: {} = { }): Observable<any>{
        return this.request( {command: kind, data: { take: take, skip: skip, search: this._filtered, ...opts}, method: 'GET'});
    }

    get(kind: string, id: string, opts: {} = {}): Observable<any>{
        return this.request( {command: [kind, id], data: opts, method: 'GET'});
    }

    getArtwork(kind: string, id: string): Observable<Array<ArtRecordSchema>>{
        return this.request( {command: [kind, id, 'artwork'], method: 'GET'});
    }

    setArtwork(kind: string, id: string, property: string, artwork: string): Observable<any>{
        return this.request( {command: [kind, id, 'artwork'], data: {property , artwork}, method: 'POST'});
    }

    getCast(kind: string, id: string): Observable<PersonRecordSchema[]> {
        return this.request( {command: [kind, id, 'cast'], method: 'GET'});
    }

    getProbe(kind: string, id: string): Observable<MediaProbeRecordSchema> {
        return this.request( {command: [kind, id, 'probe'], method: 'GET'});
    }

    listQualitites(kind: string): Observable<PlayableMediaQualities> {
        return this.request( {command: [kind, 'qualities'], method: 'GET'});
    }

    getShowApi() : ShowServerApi{
        return this._showApi;
    }

    getMovieApi(): MovieServerApi{
        return this._movieApi;
    }

    getSubtitleApi(): SubtitleServerApi{
        return this._subtitleApi;
    }

    getCollectionApi(): CollectionServerApi{
        return this._collectionApi;
    }

    getProvidersApi(): ProviderServerApi {
        return this._providersApi;
    }

    getHistoryApi(): HistoryServerApi {
        return this._historyApi;
    }

    getPeopleApi(): PeopleServerApi {
        return this._peopleApi;
    }

    getRepositoryApi(): RepositoryServerApi {
        return this._repositoryApi;
    }

    getUserRankApi(): UserRankServerApi {
        return this._userRanksApi;
    }
}

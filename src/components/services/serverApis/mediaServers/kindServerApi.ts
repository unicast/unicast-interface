import { Observable } from 'rxjs/Observable';

import { ServerApi } from 'components/services/serverApis/serverApi';
import { MediaServerApi } from 'components/services/serverApis/mediaServers/mediaServerApi';
import { GenreServerApi } from 'components/services/serverApis/mediaServers/genreServerApi';
import {
    PlayableMediaRecordSchema,
    MediaRecordSchema,
    HistoryRecordSchema,
    ArtRecordSchema,
    PersonRecordSchema,
    MediaDownloadSchema,
    StreamSchema,
    TvShowMediaRecordSchema,
    TvEpisodeMediaRecordSchema,
    PlayableMediaQualities,
    MediaProbeRecordSchema
} from 'components/schemas/mediaRecord.schema';

export class KindServerApi<T extends MediaRecordSchema> extends ServerApi<MediaServerApi> {
    protected _serverApi: MediaServerApi;
    protected _kind: string;
    protected _fields: Array<string>;
    protected _join: string;

    private _genreApi: GenreServerApi;

    constructor(serverApi:MediaServerApi, kind: string, fields: Array<string> = undefined, join: string = undefined ) {
        super(serverApi, kind);
        this._genreApi = new GenreServerApi(this);
        this._kind = kind;
        this._fields = fields;
        this._join = join;
    }

    watched(id: string, value: boolean = true): Observable<PlayableMediaRecordSchema>{
        return this._serverApi.watched(this._kind, id, value);
    }

    list(take: number, skip: number = 0, opts: any = {}): Observable<Array<T>>{
        return this._serverApi.list(this._kind, take, skip, opts);
    }

    history(take: number, skip: number = 0, opts: any = {}): Observable<Array<HistoryRecordSchema>> {
        return this._serverApi.getHistoryApi().list(take, skip, opts);
    }

    getArtwork(id: string): Observable<Array<ArtRecordSchema>> {
        return this._serverApi.getArtwork(this._kind, id);
    }

    setArtwork(id: string, property: string, artwork: string): Observable<any> {
        return this._serverApi.setArtwork(this._kind, id, property, artwork);
    }

    getCast(id : string) : Observable<PersonRecordSchema[]> {
        return this._serverApi.getCast(this._kind, id);
    }

    getProbe(id : string) : Observable<MediaProbeRecordSchema> {
        return this._serverApi.getProbe(this._kind, id);
    }

    get(id: string, join?: string, fields?: Array<string>): Observable<T>{
        let obj: {fields?: Array<string>, join?: string} = {};
        if (fields || this._fields){
            obj.fields = (fields || this._fields);
        }
        if (join || this._join){
            join = (join || this._join);
            for (let prop of join.split(',')){
                obj[prop] = 'true';
            }
        }
        return this._serverApi.get(this._kind, id, obj);
    }

    getGenreApi(): GenreServerApi{
        return this._genreApi;
    }

    listQualities(): Observable<PlayableMediaQualities> {
        return this._serverApi.listQualitites(this._kind);
    }

    getDownload(record: MediaRecordSchema, show?: TvShowMediaRecordSchema) : Observable<MediaDownloadSchema> {
        return this.request( { command: [ record.id, 'streams' ], method: 'GET' } ).map( ( streams: StreamSchema[] ) => {
            const videoStream = streams.find( s => s.type === 'video' );
            const subtitlesStream = streams.find( s => s.type === 'subtitles' );

            const sources: MediaDownloadSchema = {};

            const playable = record as PlayableMediaRecordSchema;

            let title = record.title;

            if (show != null) {
                const episode = record as TvEpisodeMediaRecordSchema;

                title = `${show.title} - T${episode.seasonNumber}E${episode.number} - ${episode.title}`;
            }

            title = title.replace(/[/\\?%*:|"<>]/g, '-');

            if ( videoStream ) {
                const videoExtension = playable.sources[0].id.split('.')[ playable.sources[0].id.split('.').length - 1 ];

                sources.video = ( this._serverApi as any )._serverApi.getUrl( [ videoStream.path.slice(1) ] );
                sources.videoName = title + '.' + videoExtension;
            }

            if ( subtitlesStream ) {
                sources.subtitles = ( this._serverApi as any )._serverApi.getUrl( [ subtitlesStream.path.slice(1) ] );
                sources.subtitlesName = title + '.' + subtitlesStream.format;
            }

            return sources;
        } );
    }
}

export class CustomMediaServerApi extends KindServerApi<MediaRecordSchema>{
    constructor(serverApi:MediaServerApi){
        super(serverApi, 'custom');
    }
}

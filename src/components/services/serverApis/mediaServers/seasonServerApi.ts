import { Injectable } from '@angular/core';
import { MediaServerApi } from './mediaServerApi';
import { KindServerApi } from './kindServerApi';
import { TvSeasonMediaRecordSchema } from 'components/schemas/mediaRecord.schema';

@Injectable()
export class SeasonServerApi extends KindServerApi<TvSeasonMediaRecordSchema> {

    constructor(serverApi:MediaServerApi) {
        super(serverApi, 'season');
    }

}

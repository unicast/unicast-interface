import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApi } from 'components/services/serverApis/serverApi';
import { MediaServerApi } from 'components/services/serverApis/mediaServers/mediaServerApi';
import { SyncOptions, SyncDefaultServerOptions } from 'components/schemas/sync.schema';

@Injectable()
export class ProviderServerApi extends ServerApi<MediaServerApi> {

    private readonly defaulSyncOptions: {};

    constructor(serverApi:MediaServerApi) {
        super(serverApi, 'providers');
        this.defaulSyncOptions = this.flatten(SyncDefaultServerOptions);
    }

    sync(opts: SyncOptions & {wait? : boolean}): Observable<any>{
        let obj = this.flatten(opts);
        for (let opt of Object.keys(obj)){
            //Only add if it is different from the default server option
            if (obj[opt] != this.defaulSyncOptions[opt]){
                obj[opt] = this.qp(obj[opt]);
            } else {
                delete obj[opt];
            }
        }
        return this.request({command: [ 'sync' ], data: obj });
    }

    clean(wait: boolean = true): Observable<any>{
        return this.sync({ cleanMissing: true, wait});
    }

    repair(): Observable<any>{
        return this.request({command: 'repair'});
    }

}
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApi } from 'components/services/serverApis/serverApi';
import { MediaServerApi } from 'components/services/serverApis/mediaServers/mediaServerApi';
import { CollectionSchema, MediaRecordSchema } from 'components/schemas/mediaRecord.schema';


@Injectable()
export class CollectionServerApi extends ServerApi<MediaServerApi> {

    constructor(serverApi:MediaServerApi) {
        super(serverApi, 'collection');
    }

    list(kind: string, opts: {tree?: boolean} = {}): Observable<Array<CollectionSchema>> {
        return this.request({method: 'GET', data: {kind: kind, tree: opts.tree || false}});
    }

    add(kinds: [string], title: string, parentId: string = null): Observable<CollectionSchema> {
        return this.request({data: {kinds, title, parentId}});
    }

    delete(kind: string, id: string): Observable<any> {
        return this.request({ command: [id, kind], method: 'DELETE'});
    }

    insertMedia(mediaKind: string, collectionId: string, mediaId: string): Observable<MediaRecordSchema>{
        return this.request({command: [ collectionId, 'insert', mediaKind, mediaId]});
    }

    removeMedia(mediaKind: string, collectionId: string, mediaId: string): Observable<MediaRecordSchema>{
        return this.request({command: [ collectionId, 'remove', mediaKind, mediaId]});
    }
}
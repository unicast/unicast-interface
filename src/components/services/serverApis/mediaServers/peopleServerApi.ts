import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApi } from 'components/services/serverApis/serverApi';
import { MediaServerApi } from 'components/services/serverApis/mediaServers/mediaServerApi';
import { PersonRecordSchema } from 'components/schemas/mediaRecord.schema';


@Injectable()
export class PeopleServerApi extends ServerApi<MediaServerApi> {

    private static readonly ROUTE: string = 'people';

    constructor(serverApi:MediaServerApi) {
        super(serverApi, PeopleServerApi.ROUTE);
    }

    list(take: number, skip: number = 0, opts: any = {}): Observable<Array<PersonRecordSchema>> {
        return this._serverApi.list( PeopleServerApi.ROUTE, take, skip, opts );
    }

    get(id: string, opts: any = {}): Observable<PersonRecordSchema> {
        return this._serverApi.get( PeopleServerApi.ROUTE, id, opts) ;
    }

}
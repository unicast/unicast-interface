import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ServerApi } from 'components/services/serverApis/serverApi';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';

//Services import
import { ServerApiBase } from 'components/services/serverApis/serverApiBase';
import { MediaServerApi } from 'components/services/serverApis/mediaServers/mediaServerApi';
import { PlayerServerApi } from 'components/services/serverApis/playerServers/playerServerApi';
import { RequesterHttp } from 'components/services/requesterHttp';
import { TasksServerApi } from 'components/services/serverApis/tasksServerApi';
import { StorageServerApi } from 'components/services/serverApis/storageServerApi';
import { CustomActionsServerApi } from 'components/services/serverApis/customActionsServerApi';

@Injectable()
export class ServerApiController {
    _http: HttpClient;
    constructor(http: HttpClient){
        this._http = http;
    }

    hasHttp(): boolean{
        return !!this._http;
    }

    setHttp(http: HttpClient): void{
        this._http = http;
    }

    startService(manager: ServerApiManager, name: string): ServerApi<any>{
        switch (name) {
            case 'Base' : return new ServerApiBase(new RequesterHttp(this._http));
            case 'Media' : return new MediaServerApi(manager.getBase());
            case 'Player' : return new PlayerServerApi(manager.getBase());
            case 'Storage' : return new StorageServerApi(manager.getBase());
            case 'Task' : return new TasksServerApi(manager.getBase());
            case 'CustomActions' : return new CustomActionsServerApi(manager.getBase());
            default: return null;
        }
    }
}

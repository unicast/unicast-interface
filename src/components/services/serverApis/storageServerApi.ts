import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';

import { ServerApi } from 'components/services/serverApis/serverApi';
import { ServerApiBase } from 'components/services/serverApis/serverApiBase';


export interface ServerStorageRecord<T> {
    createdAt: string;
    id: string;
    key: string;
    updatedAt: string;
    value: T;
}

export interface ServerStorageOperationResult {
    changed: boolean;
}


@Injectable()
export class StorageServerApi extends ServerApi<ServerApiBase> {

    constructor(serverApi: ServerApiBase){
        super(serverApi, 'storage');
    }

    public size(): Observable<number> {
        return this.list().map(e => e.length);
    }

    public list(): Observable<Array<ServerStorageRecord<any>>> {
        return this.request( { command: '', method: 'GET' } );
    }

    public clear(): Observable<Array<ServerStorageRecord<any>>> {
        return this.list().map( list => {
            forkJoin(...list.map( e => this.removeItem<any>(e.key)));
            return list;
        });
    }

    public getItem<T>(key: string): Observable<ServerStorageRecord<T>> {
        return this.request( { command: [ key ], method: 'GET' } );
    }

    public removeItem<T>(key: string): Observable<ServerStorageRecord<T>> {
        return this.request( { command: [ key ], method: 'DELETE' } );
    }

    public setItem<T extends {}>(key: string, value: T): Observable<ServerStorageRecord<T>> {
        return this.request( { command: [ key ], method: 'POST', data: value } );
    }

    public addToSet<T extends {}>(key: string, value: T, primaryKeys?: Array<keyof T>): Observable<ServerStorageOperationResult> {
        return this.request( { command: [ key, 'add-to-set' ], method: 'POST', data: { object: value, primaryKeys } } );
    }

    public removeFromSet<T extends {}>(key: string, value: T, primaryKeys?: Array<keyof T>): Observable<ServerStorageOperationResult> {
        return this.request( { command: [ key, 'delete-from-set' ], method: 'POST', data: { object: value, primaryKeys } } );
    }

}
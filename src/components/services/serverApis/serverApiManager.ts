import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ServerApiHost } from 'components/services/serverApis/serverApiHost';
import { ServerApi } from 'components/services/serverApis/serverApi';
import { ServerApiController } from 'components/services/serverApis/serverApiController';

//Services import
import { ServerApiBase } from 'components/services/serverApis/serverApiBase';
import { MediaServerApi } from 'components/services/serverApis/mediaServers/mediaServerApi';
import { PlayerServerApi } from 'components/services/serverApis/playerServers/playerServerApi';
import { TasksServerApi } from 'components/services/serverApis/tasksServerApi';
import { StorageServerApi } from 'components/services/serverApis/storageServerApi';
import { CustomActionsServerApi } from 'components/services/serverApis/customActionsServerApi';

@Injectable()
export class ServerApiManager {
    private _services: {};
    private _http: HttpClient;
    private _controller: ServerApiController;

    constructor(http: HttpClient){
        this._http = http;
        this._services = {};
    }

    start(hostInfo: ServerApiHost, controller: ServerApiController): ServerApiManager{
        return this.restart(hostInfo, controller);
    }

    restart(hostInfo: ServerApiHost, controller?: ServerApiController): ServerApiManager{
        //Only restart the controller if passed. Otherwise use the same
        //And resuses the services, since only the host changed
        if (controller){
            this._services = {};
            //Make sure the Controller knows who needs to contact
            if (!controller.hasHttp()){
                controller.setHttp(this._http);
            }
            this._controller = controller;
        }
        this.getBase().start(hostInfo);
        return this;
    }

    getBase() : ServerApiBase{
        return this.getService<ServerApiBase>('Base');
    }

    getMedia() : MediaServerApi{
        return this.getService<MediaServerApi>('Media');
    }

    getPlayer() : PlayerServerApi{
        return this.getService<PlayerServerApi>('Player');
    }

    getTask() : TasksServerApi{
        return this.getService<TasksServerApi>('Task');
    }

    getStorage() : StorageServerApi{
        return this.getService<StorageServerApi>('Storage');
    }

    getCustomActions() : CustomActionsServerApi{
        return this.getService<CustomActionsServerApi>('CustomActions');
    }

    getService<T extends ServerApi<any>>(name:string): T {
        if (!this._services[name]) {
            this._services[name] =  this._controller.startService(this, name);
        }
        return this._services[name];
    }
}

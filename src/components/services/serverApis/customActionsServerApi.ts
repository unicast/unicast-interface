import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ServerApiBase } from 'components/services/serverApis/serverApiBase';
import { ServerApi } from 'components/services/serverApis/serverApi';
import { CustomActionGroup } from 'components/schemas/customActions.schema';

@Injectable()
export class CustomActionsServerApi extends ServerApi<ServerApiBase> {
    constructor(serverApi: ServerApiBase){
        super(serverApi, 'custom-actions');
    }

    public list(): Observable<Array<CustomActionGroup>> {
        return this.request( { command: [], method: 'GET' } );
    }

    public execute(action: string, context: {} = {}): Observable<any> {
        return this.request( { command: ['execute', action], data: { context } , method: 'POST' } );
    }

}

export class ServerApiHost {
    public host: string;
    public port: number;
    public extra: string;
    private _url: string;

    constructor(...args){
        if (args.length > 1){
            args.push('/'); //Add the extra url
            this._url = undefined;
        } else {
            this._url = args[0];
            if (!this._url.endsWith('/')){
                this._url += '/';
            }
            args = (/[a-z]+:\/\/([0-9\.]+):([0-9]+)(\/.*)/).exec(this._url);
            args.splice(0,1);
        }
        this.host = args[0];
        this.port = args[1];
        this.extra = args[2];
        if (!this.extra.endsWith('/')) this.extra += '/';
    }

    getProtocol(): string {
        return window.location.protocol;
    }

    toUrl(): string{
        return (this._url || this.getProtocol() + '//' + this.host + ':' + this.port + this.extra);
    }
}

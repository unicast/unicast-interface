import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ServerApiBase } from 'components/services/serverApis/serverApiBase';
import { ServerApi } from 'components/services/serverApis/serverApi';

@Injectable()
export class TasksServerApi extends ServerApi<ServerApiBase> {
    constructor(serverApi: ServerApiBase){
        super(serverApi, 'tasks');
    }

    list ( type : string = null, state : string = null, opts : any = {} ) : Observable<any> {
        return this.request( { command: [], data: { ...opts, state: state, type: type }, method: 'GET' } );
    }

    get ( id: string, opts: any = {} ) : Observable<any> {
        return this.request( { command: [ id ], data: opts, method: 'GET' } );
    }

    stop ( id : string ) : Observable<any> {
        return this.request( { command: [id, 'stop'], data: {}, method: 'POST' } );
    }
}

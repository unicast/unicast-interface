import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { ExtraUrl, IdExtraUrl } from 'components/services/serverApis/helpers/extraUrl';
import { SingularSubject } from 'components/common/helpers/singularSubject';
import { EventType, EventsOptions } from 'components/services/serverApis/helpers/eventOptions';
import { HttpRequest, RequestMethods, RequestCommand, RequestDataType, RequestBaseType } from 'components/services/requesterHttp';

export interface ServerApiRequest extends HttpRequest {
    starter?: ServerApi<any>;
}

export class ServerApi <T extends (ServerApi<any> | null)> {
    protected _serverApi: T;
    protected _extraUrl: ExtraUrl;
    protected _matcherEvents: Map<string, ReplaySubject<EventsOptions>>;

    constructor(serverApi: T, extraUrl?:ExtraUrl | string){
        if (extraUrl instanceof ExtraUrl){
            this._extraUrl = extraUrl;
        } else {
            this._extraUrl = new ExtraUrl(extraUrl);
        }
        this._serverApi = serverApi;
        this._matcherEvents = new Map<string, ReplaySubject<EventsOptions>>();
    }

    protected qp(value: RequestBaseType): RequestDataType{
        return {queryParameter: true,  value: value};
    }

    public flatten ( data : any, prefix: string = "" ) : any {
        const result = {};
        const keys = Object.keys( data );
        if ( keys.length == 0 ) {
            return data; //No keys, return the value itself
        }
        for ( let key of keys ) {
            const value = data[ key ];
            const realKey = (!prefix ? key : prefix + key[0].toUpperCase() + key.substring(1));
            if ( value instanceof Date ) {
                result[ realKey ] = value.getTime();
            } else if ( typeof value === 'object' ) {
                for ( let subKey of Object.keys( value ) ) {
                    result[ `${ realKey }[${ subKey }]` ] = value[ subKey ];
                }
            } else {
                result[ realKey ] = value;
            }
        }

        return result;
    }

    protected buildCmd(cmd: RequestCommand): string {
        return [].concat(cmd).join("/");
    }

    getUrl (command?: RequestCommand): string{
        let cmd: string = this.buildCmd(command);
        return (this._serverApi ? this._serverApi.getUrl(this._extraUrl.toString() + cmd) : "");
    }

    request (request: ServerApiRequest): Observable<any>{
        let cmd: string = this.buildCmd(request.command);
        return this.superRequest( this._extendRequest(request, {command: this._extraUrl.toString() +  cmd}) );
    }

    parent (): T{
        return this._serverApi;
    }

    superRequest (request: ServerApiRequest): Observable<any>{
        return this._serverApi.request(request);
    }

    getEvents(): Observable<EventsOptions>{
        return this._getEvents(this.getIdentinfier()).asObservable();
    }

    getIdentinfier(): string{
        return this.constructor.name;
    }

    getApi(name: string): ServerApi<this> {
        return this['_'  + name + 'Api'];
    }

    protected _extendRequest(request: ServerApiRequest, newProperties: {}): ServerApiRequest{
        return Object.assign(Object.assign({starter: this}, request), newProperties);
    }

    protected _internalRequest(request: ServerApiRequest, id?: string): Observable<any> {
        let aux = this._extraUrl;
        if (id !== undefined){
            this._extraUrl = new IdExtraUrl(this._extraUrl._base, id);
        }
        let req =  this.request(request);
        this._extraUrl = aux;
        return req;
    }

    protected _mainRequest(type: RequestMethods): Observable<any>{
        return this._internalRequest({method: type, command: ''});
    }

    protected _getEvents(matcher: string): ReplaySubject<EventsOptions>{
        if (this.parent()){
            return this.parent()._getEvents(matcher);
        } else {
            if (!this._matcherEvents.has(matcher)){
                this._matcherEvents.set(matcher, SingularSubject.create<EventsOptions>(undefined, false));
            }
            return this._matcherEvents.get(matcher);
        }

    }

    protected _emitEvent(url: string, request: ServerApiRequest, type: EventType, msg?: string, title?: string ): void {
        let notification: EventsOptions = { type, message: url + (msg ? '\n' + msg : ''), title, silent: request.silent};
        let emitter: ServerApi<any> = request.starter;
        console.debug("Emitting to '" + emitter.getIdentinfier() + "' and parents, from url: '" + url + "'");
        while (emitter){
            this._getEvents(emitter.getIdentinfier()).next(notification);
            emitter = emitter.parent();
        }

    }

}


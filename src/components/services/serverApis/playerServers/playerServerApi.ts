import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Observable } from 'rxjs/Observable';

import { ServerApiBase } from 'components/services/serverApis/serverApiBase';
import { ServerApi } from 'components/services/serverApis/serverApi';
import { DeviceServerApi } from 'components/services/serverApis/playerServers/deviceServerApi';
import { Device } from 'components/services/serverApis/helpers/device';
import { SingularSubject } from 'components/common/helpers/singularSubject';
import { EventType } from 'components/services/serverApis/helpers/eventOptions';

export enum RequestsState {
    None = 0,
    Requesting = 1,
    InError = 2
}

@Injectable()
export class PlayerServerApi extends ServerApi<ServerApiBase> {

    private _deviceApi: DeviceServerApi;

    private _requestsInProgress: number = 0;

    private _subjectConnectionState: ReplaySubject<RequestsState>;

    public connectionState: Observable<RequestsState>;

    constructor(serverApi: ServerApiBase){
        super(serverApi, 'player');

        this._subjectConnectionState = SingularSubject.create<RequestsState>(undefined, false, 1);
        this.connectionState = this._subjectConnectionState.asObservable().debounceTime(1).distinctUntilChanged();

        this._deviceApi = new DeviceServerApi(this);

        this.getEvents().subscribe( (event) => {
            //Only process events if they should alert the user
            if ( event.silent ) {
                return;
            }
            switch (event.type) {
                case EventType.START:
                    this._requestsInProgress++;
                    break;
                case EventType.ERROR:
                case EventType.COMPLETED:
                    this._requestsInProgress--;
                    break;
            }

            if (event.type == EventType.ERROR){
                this._subjectConnectionState.next(RequestsState.InError);
            } else {
                this._subjectConnectionState.next(this._requestsInProgress > 0 ? RequestsState.Requesting : RequestsState.None);
            }
        });

    }

    getDeviceApi(): DeviceServerApi{
        return this._deviceApi;
    }

    startDevice(defaultDevice: Device) {
        this._deviceApi.list().subscribe( (data: {devices: Array<Device>}) => {
            let actualDevice = (data.devices.length > 0 ? data.devices[0] : Device.NOT_CONNECTED);
            data.devices.forEach( (elem) => {
                if (elem.equals(defaultDevice) ){
                    actualDevice = elem;
                }
            });
            this._deviceApi.setDevice(actualDevice);
        }, (err) => {
            this._deviceApi.setDevice(Device.NOT_CONNECTED);
        });
    }

}




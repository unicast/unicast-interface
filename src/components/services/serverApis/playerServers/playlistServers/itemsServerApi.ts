import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { ServerApi } from 'components/services/serverApis/serverApi';
import { PlaylistServerApi } from 'components/services/serverApis/playerServers/playlistServers/playlistServerApi';
import { PlayableMediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { SingularSubject } from 'components/common/helpers/singularSubject';

@Injectable()
export class PlaylistItemsServerApi extends ServerApi<PlaylistServerApi> {

    private _itemsList: Array<PlayableMediaRecordSchema>;

    private _itemsSubject: ReplaySubject<Array<PlayableMediaRecordSchema>>;
    public itemsObservable: Observable<Array<PlayableMediaRecordSchema>>;


    constructor(serverApi: PlaylistServerApi){
        super(serverApi, 'items');
        //The subject cannot be distinct, beacuse the array used could be the same.
        this._itemsSubject = SingularSubject.create<Array<PlayableMediaRecordSchema>>([], false);
        this.itemsObservable = this._itemsSubject.asObservable();
        this._itemsList = [];
    }

    get(): Observable<Array<PlayableMediaRecordSchema>>{
        let req = this.request({method: 'GET'});
        req.subscribe((data: Array<PlayableMediaRecordSchema>) => {
            this._itemsList = data;
            this._itemsSubject.next(this._itemsList);
        }, (err) => {});
        return req;
    }

    delete(position: number): Observable<any>{
        let [item, ] = this._itemsList.splice(position, 1);
        let ret = this._updateItems(this._itemsList);
        ret.subscribe(() => {}, () => {
            this._itemsList.splice(position, 0, item);
            this._itemsSubject.next(this._itemsList);
        });
        return ret;
    }

    play(position: number): Observable<any>{
        let item = this._itemsList[position];
        let playlistApi = this.parent();
        let deviceApi = playlistApi.parent();
        return deviceApi.play(item.kind, item.id, {playlistId: playlistApi.getId(), playlistPosition: position });
    }

    queue(newItem: PlayableMediaRecordSchema){
        this._itemsList.push(newItem);
        let ret = this._updateItems(this._itemsList);
        ret.subscribe(() => {}, () => {
            this._itemsList.pop();
            this._itemsSubject.next(this._itemsList);
        });
        return ret;
    }

    reorder(oldPosition: number, newPosition: number): Observable<any>{
        let item = this._itemsList.splice(oldPosition,1);
        this._itemsList.splice(newPosition, 0, item[0]);
        return this._updateItems(this._itemsList);
    }

    private _updateItems(items: Array<PlayableMediaRecordSchema>): Observable<any>{
        this._itemsSubject.next(items);
        return this.request({ data: items.map( data => {
            let obj = { 'kind' : data.kind };
            let key = data.id ? 'id' : 'sources';
            obj[key] = data[key];
            return obj;
        }) });
    }

}

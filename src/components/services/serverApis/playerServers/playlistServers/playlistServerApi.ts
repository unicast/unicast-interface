import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { ServerApi, ServerApiRequest } from 'components/services/serverApis/serverApi';
import { DeviceServerApi } from 'components/services/serverApis/playerServers/deviceServerApi';
import { PlaylistItemsServerApi } from 'components/services/serverApis/playerServers/playlistServers/itemsServerApi';
import { IdExtraUrl } from 'components/services/serverApis/helpers/extraUrl';
import { PlaylistModel } from 'components/models/playlist.model';
import { SingularSubject } from 'components/common/helpers/singularSubject';

@Injectable()
export class PlaylistServerApi extends ServerApi<DeviceServerApi> {

    private static readonly ENDPOINT: string = 'playlists';

    protected _extraUrl: IdExtraUrl;

    private _subjectPlaylistId: ReplaySubject<string>;
    private _subjectPlaylistData: ReplaySubject<any>;

    public playlistIdObservable: Observable<string>;
    public playlistDataObservable: Observable<PlaylistModel>;

    private _itemsApi: PlaylistItemsServerApi;

    constructor(serverApi: DeviceServerApi){
        super(serverApi, new IdExtraUrl(PlaylistServerApi.ENDPOINT, '0000'));
        this._itemsApi = new PlaylistItemsServerApi(this);

        this._subjectPlaylistId = SingularSubject.create<string>(undefined, false);
        this._subjectPlaylistData = SingularSubject.create<PlaylistModel>();

        this.playlistIdObservable = this._subjectPlaylistId.asObservable();
        this.playlistDataObservable = this._subjectPlaylistData.asObservable();
    }

    getItemsApi(): PlaylistItemsServerApi{
        return this._itemsApi;
    }

    create(): Observable<PlaylistModel>{
        return this.superRequest({method: 'POST', command: PlaylistServerApi.ENDPOINT}).map(data => new PlaylistModel(data));
    }

    remove(playlistId?: string): Observable<any>{
        return this._internalRequest({method: 'DELETE'}, playlistId);
    }

    play(): Observable<any>{
        return this._itemsApi.play(0);
    }

    setId(id: string, force: boolean = false): void{
        if (force || id != this._extraUrl._id){
            this._extraUrl._id = id;
            if (id){
                this._itemsApi.get().subscribe((data) => {
                    this._subjectPlaylistId.next(id);
                });
            } else {
                this._subjectPlaylistId.next(undefined);
            }


        }
    }

    getId(): string{
        return this._extraUrl._id;
    }

    notifyUpdate(value?: PlaylistModel): void{
        if (value){
            this._subjectPlaylistData.next(value);
        } else {
            this.get().subscribe(data => {
                this._subjectPlaylistData.next(data);
            });
        }

    }

    get(id?: string): Observable<PlaylistModel>{
        return this._internalRequest({ data: {items: true}, method: 'GET'}, id).map(data => new PlaylistModel(data)).share();
    }


    _internalRequest(request: ServerApiRequest, id?: string): Observable<any> {
        return super._internalRequest( request, (id === undefined ? this.getId() : id));
    }

    list(includeItems: boolean = false): Observable<{playlists: Array<PlaylistModel>}>{
        return this._internalRequest({data: {items: includeItems}, method: 'GET'}, '').map(data => {
            let ret: { playlists: Array<PlaylistModel>}  = { playlists: []};
            for(let item of data){
                ret.playlists.push(new PlaylistModel(item));
            }
            return ret;
        });
    }

    last(includeItems: boolean = false): Observable<PlaylistModel>{
        return this._internalRequest({command: 'last', data: {items: includeItems}, method: 'GET'}, '').map(data => {
            return new PlaylistModel(data);
        });
    }



}


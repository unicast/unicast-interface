import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Observable } from 'rxjs/Observable';

import { SingularSubject } from 'components/common/helpers/singularSubject';
import { ServerApi, ServerApiRequest } from 'components/services/serverApis/serverApi';
import { PlayerServerApi } from 'components/services/serverApis/playerServers/playerServerApi';
import { PlaylistServerApi } from 'components/services/serverApis/playerServers/playlistServers/playlistServerApi';
import { Status } from 'components/services/serverApis/helpers/status';
import { Device } from 'components/services/serverApis/helpers/device';
import { ExtraUrl } from 'components/services/serverApis/helpers/extraUrl';
import { MediaKind } from 'components/services/serverApis/helpers/mediaKind';
import { Time } from 'components/common/helpers/time';
import { DeviceStatusSchema } from 'components/schemas/status.schema';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';


@Injectable()
export class DeviceServerApi extends ServerApi<PlayerServerApi> {

    private _playlistApi: PlaylistServerApi;
    private _status: Status;

    private _subjectDevice: ReplaySubject<Device>;

    public deviceObservable: Observable<Device>;

    constructor(serverApi: PlayerServerApi){
        super(serverApi, '');

        this._status = new Status(this);
        this._subjectDevice = SingularSubject.create<Device>();
        this.deviceObservable = this._subjectDevice.asObservable();

        this._playlistApi = new PlaylistServerApi(this);

    }

    loadStatus(): Observable<DeviceStatusSchema>{
        return this.request({command: 'status', method: 'GET', silent: true});
    }

    getStatus(): Status{
        return this._status;
    }

    setDevice(device: Device): Device{

        let isConnected = !device.equals(Device.NOT_CONNECTED);

        //Change the connection url
        this._extraUrl = new ExtraUrl( isConnected ? device.name : '');

        //Warn all the subscribers that the device was changed (so they can act accordingly)
        this._subjectDevice.next(device);

        if (isConnected){
            //Change the actual selected playlist
            this._playlistApi.last().subscribe((playlist) => {
                this._playlistApi.setId(playlist.data.id);
            });

            //Ensure that the last status is requested and the previous one invalidated
            this._status.getValue(true);
        }

        return device;
    }

    private _seek(command: string, value: Time | number): Observable<DeviceStatusSchema>{
        return this._requestWithStatus({command: [command, Time.convertToSeconds(value).toString()]});
    }

    seek(delta: Time | number): Observable<DeviceStatusSchema>{
        return this._seek('seek', delta);
    }

    seekTo(to: Time | number): Observable<DeviceStatusSchema>{
        return this._seek('seek-to', to);
    }

    list(): Observable<{devices: Array<Device>}>{
        return this.superRequest({command: 'list', method: 'GET'}).map((data) => {
            return {devices: data.map( elem => new Device(elem.name, elem.type, elem.address) )};
        });
    }

    play(): Observable<DeviceStatusSchema>;
    play(kind: MediaKind, id: string, opts?: {}): Observable<DeviceStatusSchema>
    play(kind?: MediaKind, id?: string, opts?: {}): Observable<DeviceStatusSchema>{
        opts = opts || {};
        switch(kind){
            case undefined:
            case null:
                return this._playlistApi.play();
            default:
                return this._requestWithStatus({command: ['play', 'media', kind, id], data: opts});
        }
    }

    private isMediaRecordSchema(v: any) : v is MediaRecordSchema{
        return !!v['id'] && !!v['kind'] && !!v['internalId'] && !!v['title'];
    }

    callCommand(name: string, args: any[] = []): Observable<DeviceStatusSchema> {
        return this._requestWithStatus({ command: ['command', name], method: 'POST', data: { args } });
    }

    openMediaPage(media: MediaRecordSchema);
    openMediaPage(kind: MediaKind, id: string);
    openMediaPage(kind: MediaRecordSchema | MediaKind, id?: string) {
        if (this.isMediaRecordSchema(kind)) {
            id = kind.id;
            kind = kind.kind;
        }
        return this.callCommand('open-media-page', [ kind, id ]);
    }

    resume(): Observable<DeviceStatusSchema>{
        return this._requestWithStatus({command: 'resume'});
    }

    pause(): Observable<DeviceStatusSchema>{
        return this._requestWithStatus({command: 'pause'});
    }

    stop(): Observable<DeviceStatusSchema>{
        return this._requestWithStatus({command: 'stop'});
    }

    next(): Observable<DeviceStatusSchema>{
        return this._requestWithStatus({command: ['play', 'next']});
    }

    previous(): Observable<DeviceStatusSchema>{
        return this._requestWithStatus({command: ['play', 'previous']});
    }

    mute(): Observable<DeviceStatusSchema>{
        return this._requestWithStatus({command: 'mute'});
    }

    unmute(): Observable<DeviceStatusSchema>{
        return this._requestWithStatus({command: 'unmute'});
    }

    setVolume(volume: number): Observable<DeviceStatusSchema>{
        return this._requestWithStatus({command: ['volume', volume.toString()]});
    }

    setSubtitlesSize(size: number): Observable<DeviceStatusSchema>{
        return this._requestWithStatus({command: ['subtitles-size', size.toString()]});
    }

    disconnect(){
        return this.request({command: 'disconnect'});
    }

    turnOff(): Observable<DeviceStatusSchema>{
        return this._requestWithStatus({command: 'turnoff'});
    }

    execute(cmd: string, args?: Array<any>){
        return this.request({command: ['command', cmd], data: {'args': args}});
    }

    getPlaylistApi(): PlaylistServerApi{
        return this._playlistApi;
    }

    /**
     * This method will ensure that an connected device exists before doing the request
     * @Override from {@link ServerApi}
     */
    request (request: ServerApiRequest): Observable<any>{
        if (!this._extraUrl.isEmpty()){
            return super.request(request);
        } else {
            return this.deviceObservable.filter( t => !t.equals(Device.NOT_CONNECTED) )
                                        .first()
                                        .flatMap( device => super.request(request) );
        }
    }

    /**
     * This method will call the method request, but will use the returned response from the server
     * to enrich the actual status with the one more accurated from the server
     * @param request See method {@link DeviceServerApi} request
     */
    protected _requestWithStatus(request: ServerApiRequest): Observable<DeviceStatusSchema>{
        return this.request(request).do((data) => this._status.enrich(data));
    }

}

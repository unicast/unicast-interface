import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { ServerApi, ServerApiRequest } from 'components/services/serverApis/serverApi';
import { ServerApiHost } from 'components/services/serverApis/serverApiHost';
import { RequesterHttp, HttpRequest, RequestCallbacks, RequestCommand } from 'components/services/requesterHttp';
import { EventType } from 'components/services/serverApis/helpers/eventOptions';


@Injectable()
export class ServerApiBase extends ServerApi<null> {
    protected _hostInfo: ServerApiHost;
    protected _forceRequest: boolean;
    protected _requester: RequesterHttp;

    constructor(requester: RequesterHttp){
        super(null);
        this._forceRequest = true;
        this._requester = requester;
    }

    start(hostInfo: ServerApiHost): ServerApiBase{
        this._hostInfo = hostInfo;
        return this;
    }

    setForceRequest(value: boolean = true): void{
        this._forceRequest = value;
    }

    getUrl (command?: RequestCommand): string{
        return this._hostInfo.toUrl() + this.buildCmd(command);
    }

    request (request: ServerApiRequest): Observable<any>{
        return this._internalRequest( this._extendRequest(request, {url: this._hostInfo.toUrl()}) );
    }

    protected _internalRequest ( request: ServerApiRequest): Observable<any>{
        let callback: RequestCallbacks = this._requester.fillCallback(request.callback);
        request.silent = !!request.silent;
        let req = this._requester.request(this._extendRequest(request, {
            callback: {
                start: (url: string, httpRequest: HttpRequest, data: any) => {
                    this._emitEvent(url, request, EventType.START);
                    callback.start(url, httpRequest, data);
                },
                completed: (url: string, httpRequest: HttpRequest, data: any) => {
                    this._emitEvent(url, request, EventType.COMPLETED);
                    callback.completed(url, httpRequest, data);
                },
                error: (url: string, httpRequest: HttpRequest, data: any) => {
                    this._emitEvent(url, request, EventType.ERROR, (data.message ? data.message : data), 'Request Error');
                    callback.error(url, httpRequest, data);
                }
            }
        }));
        if (this._forceRequest) {
            req.subscribe((data) => {}, (err) => {});
        }
        return req;
    }

}

import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Observable } from 'rxjs/Observable';

import { SingularSubject } from 'components/common/helpers/singularSubject';
import { DeviceStatusSchema, DeviceStatusState } from 'components/schemas/status.schema';

export type Loader = {loadStatus: () => Observable<DeviceStatusSchema>};

export class Status{
    private static readonly CACHE_TIME_MS = 4000;

    private _loader: Loader;
    private _subjectResult: ReplaySubject<DeviceStatusSchema>;
    private _subjectIsPlaying: ReplaySubject<boolean>;
    private _subjectState: ReplaySubject<DeviceStatusState>;

    private _waiting: boolean;
    private _time: number;

    private _value: Observable<DeviceStatusSchema>;
    isPlayingObservable: Observable<boolean>;
    stateObservable: Observable<DeviceStatusState>;


    result: DeviceStatusSchema;

    constructor (loader: Loader){
        this._loader = loader;

        this._subjectResult = SingularSubject.create();
        this._value = this._subjectResult.asObservable();

        this._subjectIsPlaying = SingularSubject.create(false);
        this.isPlayingObservable = this._subjectIsPlaying.asObservable();

        this._subjectState = SingularSubject.create();
        this.stateObservable = this._subjectState.asObservable();

        this.result = null;
        this._waiting = false;
        this._time = 0;
    }

    getLastValue(): DeviceStatusSchema{
        return this.result;
    }

    enrich(data: DeviceStatusSchema){
        this.result = data;
        this._subjectResult.next(data);
        this._subjectIsPlaying.next(this.isPlaying());
        this._subjectState.next(data.state);
        this._time = new Date().getTime();
    }

    getValue(force: boolean = false): Observable<DeviceStatusSchema>{
        let skip = this._time > 0 ? 1 : 0;
        if (this._waiting){
            return this._value.skip( skip );
        } else if ((this._time + Status.CACHE_TIME_MS < new Date().getTime() || force)){
            this.requestValue();
            return this._value.skip( skip );
        } else {
            return this._value;
        }
    }

    requestValue(): Observable<DeviceStatusSchema> {
        this._waiting = true;
        let req = this._loader.loadStatus();
        req.subscribe((data) => {
            this.enrich(data);
            this._waiting = false;
        },(data) => {
            this._waiting = false;
        });
        return req;
    }

    isLoaded (): boolean {
        return (this.result && this.result.media.record != null);
    }

    isPlaying (): boolean {
        return (this.result &&
                (this.result.state == DeviceStatusState.Playing || this.result.state == DeviceStatusState.Buffering));
    }

    isPaused (): boolean {
        return (this.result && this.result.state == DeviceStatusState.Paused)
    }

    getSubtitlesSize(): number{
        let style = (this.result ? this.result.subtitlesStyle : undefined);
        if (style && style.size){
            return style.size;
        } else {
            return null;
        }
    }

    /*
     * Methods to manually change properties and avoid extra cache reload
     */
    updateSubtitlesSize (value: number){
        if (this.getSubtitlesSize() !== null){
            this.result.subtitlesStyle.size = value;
        }
    }

}


export interface EventsOptions {
    message ?: string;
    title ?: string;
    icon ?: string;
    color ?: string;
    type: EventType;
    silent: boolean;
}

export enum EventType {
    START = 1,
    COMPLETED = 2,
    ERROR = 3
}
export enum MediaStreamType {
    Video = 'video',
    Subtitles = 'subtitles'
}

export enum MediaKind {
    Movie = 'movie',
    TvShow = 'show',
    TvSeason = 'season',
    TvEpisode = 'episode',
    Custom = 'custom',
    Empty = 'empty'
}

export class ExtraUrl {
    _base: string;
    constructor(base: string){
        this._base = base;
    }

    isEmpty() {
        return this._base.length <= 0;
    }

    getBase(){
        return this._base;
    }

    static append(...args){
        let ret: string = '';
        for (let s of args){
            s = s + '';
            if (s && !s.endsWith('/')){
                s += '/';
            }
            ret += s;
        }
        return ret;
    }

    toString(){
        return ExtraUrl.append(this._base);
    }
}

export class IdExtraUrl extends ExtraUrl{

    _id: string;

    constructor(base: string, id: string){
        super(base);
        this._id = id;
    }

    toString(){
        return ExtraUrl.append(super.toString(), this._id);
    }
}

export class Device{
    public static readonly NOT_CONNECTED = new Device('(Not Connected)', '', '');

    address: string;
    name: string;
    type: string;
    constructor(name: string, type: string, address?: string){
        this.name = name;
        this.type = type;
        this.address = address;
    }

    public toJSON(){
        return {
            address: this.address,
            name: this.name,
            type: this.type
        }
    }

    static fromJSON(value: {address: string, name: string, type: string}) : Device{
        return new Device(value.name, value.type, value.address);
    }

    public equals(other: Device){
        return this.name == other.name && this.type == other.type;
    }

    public toString(){
        return this.name + ' (' + this.type + ')';
    }
}

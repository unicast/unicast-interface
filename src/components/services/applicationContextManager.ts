import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

export interface ContextToken<T> {};

export interface ContextChanges {
    key: any;
    oldValue: any;
    newValue: any;
}

@Injectable()
export class ApplicationContextManager {

    private validKeys: Map<any, ContextToken<any>> = new Map();
    private registedTokens: Set<ContextToken<any>> = new Set();
    private context: Map<any, any> = new Map();
    private contextChangeSubject = new Subject<ContextChanges>();

    public readonly contextChange = this.contextChangeSubject.asObservable();

    constructor() {

    }


    public registerToken<T> (object: Array<keyof T>) : ContextToken<T> {
        //If the token is already registed, just ignore the step
        if (!this.registedTokens.has(object)) {
            const duplicatedKey = object.find(k => this.validKeys.has(k));
            if (duplicatedKey) {
                throw new Error('Keys to create new token are overlapping other already registered token: "' + duplicatedKey.toString() + "'" );
            }
            this.registedTokens.add(object);
            object.forEach(k => this.validKeys.set(k, object));
        }
        return object;
    }

    public setContext<T, K extends keyof T>(token: ContextToken<T>, key: K, newValue: T[K]) {
        //Validate the token is registered and allowed o store such keys
        if (this.registedTokens.has(token) && this.validKeys.get(key) == token ) {
            const oldValue = this.context.get(key);
            this.context.set(key, newValue);
            this.contextChangeSubject.next({ key, oldValue, newValue });
        } else {
            throw new Error('Invalid token for key: ' + key.toString());
        }
    }

    public unregisterToken<T> (token: ContextToken<T>) {
        if (this.registedTokens.has(token)) {
            const object = token as Array<keyof T>;
            object.forEach(k => {
                this.validKeys.delete(k);
                this.context.delete(k);
            });
            this.registedTokens.delete(token);
        } else {
            throw new Error('Invalid token supplied to remove: ' + token);
        }
    }

    public getContext() : {} {
        const obj = {};

        for (const pair of this.context) {
            if (Object(pair) !== pair) {
            throw new TypeError('iterable for fromEntries should yield objects');
            }

            // Consistency with Map: contract is that entry has "0" and "1" keys, not
            // that it is an array or iterable.

            const { '0': key, '1': val } = pair;

            Object.defineProperty(obj, key, {
            configurable: true,
            enumerable: true,
            writable: true,
            value: val,
            });
        }

        return obj;
    }

}

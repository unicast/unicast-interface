import { Injectable, ModuleWithProviders } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { TranslateService, TranslateLoader, TranslateModule, TranslateParser } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';


export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/locale-', '.json');
}

export type TranslationTuple = {text: string, value: string};

@Injectable()
export class TranslatorManager{
    private static _instance: TranslatorManager;

    private _translation: TranslateService;
    private _parser: TranslateParser;

    constructor(translation: TranslateService, parser: TranslateParser) {
        this._translation = translation;
        this._parser = parser;
        translation.setDefaultLang('pt');
        TranslatorManager._instance = this;
    }

    public static forRoot(): ModuleWithProviders{
        return TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        });
    }

    public static getAvailableLanguages(): Array<TranslationTuple> {
        return [
            {text: 'PT', value: 'pt'},
            {text: 'EN', value: 'en'}
        ]
    }

    public static getInstance(): TranslatorManager{
        return TranslatorManager._instance;
    }

    public getTranslation(): TranslateService{
        return this._translation;
    }

    public fromResourceAsync<T extends string | Array<string>>(resourceCode: string): Observable<T>;
    public fromResourceAsync<T extends string | Array<string>>(resourceCode: string, params: Array<string>): Observable<T>;
    public fromResourceAsync<T extends string | Array<string>>(resourceCode: string, params: {[p: string]: string}): Observable<T>;
    public fromResourceAsync<T extends string | Array<string>>(resourceCode: string, params?: {[p: string]: string} | Array<string>): Observable<T> {
        return this._translation.get(resourceCode).map<string | Array<string>, T>( (data) : T => this.transformData<T>(data, params));
    }

    public fromResource<T extends string | Array<string>>(resourceCode: string): T;
    public fromResource<T extends string | Array<string>>(resourceCode: string, params: Array<string>): T;
    public fromResource<T extends string | Array<string>>(resourceCode: string, params: {[p: string]: string}): T;
    public fromResource<T extends string | Array<string>>(resourceCode: string, params?: {[p: string]: string} | Array<string>): T {
        return this.transformData<T>(this._translation.instant(resourceCode), params);
    }

    private transformData<T extends string | Array<string>>(data: string | Array<string>, params: {[p: string]: string} | Array<string>) : T {
        if (data instanceof Array){
            return data.map<string>( ele => this._parser.interpolate(ele, params)) as T;
        } else {
            return this._parser.interpolate(data, params) as T;
        }
    }
}
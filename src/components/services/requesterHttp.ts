import { Injectable, EventEmitter } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

export type RequestMethods = 'POST' | 'GET' | 'DELETE' | 'PUT';

export type CallbackMethod = (url: string, request: HttpRequest, data: any) => any

export type RequestBaseType = string | number | boolean | Object;

export type RequestValueType = RequestBaseType | Array<RequestBaseType> ;

export type RequestDataType = { [key: string]: RequestValueType | {queryParameter: boolean, value: RequestValueType } } | Array<any>;

export interface RequestCallbacks {
    start?: CallbackMethod;
    error?: CallbackMethod;
    completed?: CallbackMethod;
}

export type RequestCommand = Array<string | number> | string;

export interface HttpRequest {
    url?: string;
    command?: RequestCommand;
    data?: RequestDataType;
    /**
     * Default method is POST
     */
    method?: RequestMethods;
    callback?: RequestCallbacks;
    silent?: boolean;
}

/**
 * Internal interface to keep in check HttpConfigs
 */
interface HttpConfigs {
    method: string,
    headers: HttpHeaders,
    params: HttpParams,
    body: string
}

@Injectable()
export class RequesterHttp {
    private static readonly WAIT_FOR_CONNECTION = 30000;
    private _http: HttpClient;
    private _onlineEmitter: EventEmitter<boolean> = new EventEmitter();

    public constructor(http: HttpClient){
        this._http = http;

        window.addEventListener('online', (e) => {
            this._onlineEmitter.emit(true);
        });
    }

    public request( request: HttpRequest): Observable<any>{
        let method: string = ( request.method || 'POST' ).toLowerCase();
        let data = ( request.data || {} );
        let command: string = ( [].concat(request.command).join("/") || '' ).replace('//', '/');
        let url: string = request.url;
        let callback: RequestCallbacks = this.fillCallback(request.callback);
        /*
        The configs are using text/plain to avoid the OPTIONS request made by the browser
        on all the requests. The correct should be application/json, but it generates more requests
        */
        let configs: HttpConfigs = {
            method: method,
            headers: new HttpHeaders({
                'Content-Type': 'text/plain'
            }),
            params: undefined,
            body: undefined
        };
        configs = this._transformData(configs, data);
        /*
        Start the http request
        */
        url = url + command;
        callback.start(url, request, data);
        let ret = this.verifyConnection().mergeMap(v => this._http.request<{}>(configs.method, url, configs )).map(data => {
            callback.completed(url, request, data);
            return data;
        }).catch(err => {
            this._handleNotJson(err, url, request, callback);
            callback.error(url, request, err);
            return Observable.throw(err);
        }).share();
        return ret;

    }

    private verifyConnection(): Observable<Boolean> {
        //If the feature is unsupported on browser it should return undefined
        return (navigator.onLine === false ? this._onlineEmitter.timeoutWith(RequesterHttp.WAIT_FOR_CONNECTION, of(false)).first() : of(true));
    }

    private _transformData(config: HttpConfigs, data: RequestDataType): HttpConfigs{
        let stringifyObj: RequestDataType = {};
        let params: HttpParams = new HttpParams();
        let isPost = config.method == 'post';
        if (data instanceof Array) {
            stringifyObj = data;
        } else {
            for(let key of Object.keys(data)){
                let value = data[key];
                if (value !== undefined && value !== null) {
                    let isParameter = !!value['queryParameter'];
                    value = (isParameter ? value['value'] : value);
                    if (isPost && !isParameter){
                        stringifyObj[key] = value;
                    } else {
                        if (value instanceof Array) {
                            value.forEach((item) => params = params.append(`${key.toString()}[]`, item + ''));
                        } else {
                            params = params.append(key, value + '');
                        }
                    }
                }

            }
        }
        config.body = JSON.stringify(stringifyObj);
        config.params = params;
        return config;
    }

    public fillCallback(override?: RequestCallbacks): RequestCallbacks {
        return {start: () => {}, completed: () => {}, error: () => {}, ...override};
    }


    private _handleNotJson(value, url: string, request: HttpRequest, callback: RequestCallbacks): void{
        if (value instanceof Error){
            throw value;
        } else {
            let headers = value.headers.get("content-type");
            if (headers && headers.indexOf('application/json') < 0){
                let message = 'The request didn\'t sent a JSON response';
                callback.error(url, request, message);
                throw new Error(url + ': ' + message);
            }
        }
    }
}
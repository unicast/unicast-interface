import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { RequesterHttp, HttpRequest } from 'components/services/requesterHttp';
import { WitaiResponseSchema } from 'components/schemas/witai.schema';
import { SingularSubject } from 'components/common/helpers/singularSubject';

@Injectable()
export class WitaiApi {
    private static readonly TOKEN_API: string = 'WCOWYYI6KYH24MV3JTRZIJIGX6KFREQI';
    protected _requester: RequesterHttp;
    protected _sentences: Map<string, Observable<WitaiResponseSchema> | WitaiResponseSchema>;

    constructor(requester: RequesterHttp){
        this._requester = requester;
        this._sentences = new Map<string, Observable<WitaiResponseSchema> | WitaiResponseSchema>();
    }

    ask(sentence: string): Observable<WitaiResponseSchema>{
        if (this._sentences.has(sentence)){
            let obj = this._sentences.get(sentence);
            if (obj instanceof Observable){
                return obj;
            } else {
                return SingularSubject.create<WitaiResponseSchema>(obj).first();
            }
        } else {
            let req = this._requester.request({
                url: 'https://api.wit.ai/',
                command: 'message',
                data: {
                    q: sentence,
                    access_token: WitaiApi.TOKEN_API
                },
                method: 'GET',
                callback: {
                    completed: (url: string, request: HttpRequest, data: any) => {
                        this._sentences.set(sentence, data);
                    }
                }
            });
            this._sentences.set(sentence, req);
            return req;
        }

    }
}
import { Injectable, EventEmitter } from '@angular/core';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Observable } from 'rxjs/Observable';

import { DynamicRegionsContent } from 'components/common/services/dynamicRegionsContent';
import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';
import { SingularSubject } from 'components/common/helpers/singularSubject';

@Injectable()
export class SidebarManager extends DynamicRegionsContent {
    private activeMedia: ReplaySubject<MediaRecordSchema>;
    public activeMediaObservable: Observable<MediaRecordSchema>;
    public mainActionClicked : EventEmitter<void> = new EventEmitter();

    constructor () {
        super();

        this.activeMedia = SingularSubject.create<MediaRecordSchema>(undefined, true, 1);
        this.activeMediaObservable = this.activeMedia.asObservable();

        this.createRegion( 'primary' );
        this.createRegion( 'secondary' );
    }

    setMedia(media: MediaRecordSchema){
        this.activeMedia.next(media);
    }

    setPrimaryMenu ( component, injector? ) {
        return this.setRegionContent( 'primary', component, injector );
    }

    disposePrimaryMenu ( token ) {
        return this.disposeRegionContent( 'primary', token );
    }

    getPrimaryMenu () {
        return this.getRegionContent( 'primary' );
    }

    setSecondaryMenu ( component, injector? ) {
        return this.setRegionContent( 'secondary', component, injector );
    }

    disposeSecondaryMenu ( token ) {
        return this.disposeRegionContent( 'secondary', token );
    }

    getSecondaryMenu () {
        return this.getRegionContent( 'secondary' );
    }
}
import { Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { SpeechResult } from 'components/services/speechRecognition/speechResult';
import { ICommandMessage, CommandSequencialMessage } from 'components/services/speechRecognition/sequencialMessages';
import { SingularSubject } from 'components/common/helpers/singularSubject';

export interface IVoiceCommand<T>{
    setInjector(injector: Injector): void;
    convertResponse(response: SpeechResult): Observable<T>;
    recognizes(response: T): boolean;
    executes(response: T): Observable<any>;
    needsConfirmation(): boolean;
    presentsCommand(response: T): Observable<ICommandMessage<T>>;
}

export interface IInversableVoiceCommand<T> extends IVoiceCommand<T> {
    inverse: IVoiceCommand<any>;
}

export abstract class BaseVoiceCommand<T> implements IVoiceCommand<T> {
    protected _commandName: string;

    protected constructor(commandName: string){
        this._commandName = commandName;
    }

    abstract setInjector(injector: Injector): void;
    abstract convertResponse(response: SpeechResult): Observable<T>;
    abstract recognizes(response: T): boolean;
    abstract executes(response: T): Observable<any>;
    abstract needsConfirmation(): boolean;

    presentsCommand(response: T): Observable<ICommandMessage<T>> {
        return SingularSubject.oneValue({
            title: 'COMMANDS.' + this._commandName + '.TITLE',
            message: CommandSequencialMessage.fromResource('COMMANDS.' + this._commandName + '.MESSAGE')
        });
    }
}
import { Injectable } from '@angular/core';

@Injectable()
export class SpeechSynthesisManager{
    private _synth: SpeechSynthesis;

    constructor(){
        this._synth = window.speechSynthesis;
    }

    public speak(text: string){
        let utterance = new SpeechSynthesisUtterance(text);
        utterance.lang = 'pt-PT';
        this._synth.speak(utterance);
    }

    public abort(){
        this._synth.cancel();
    }
}
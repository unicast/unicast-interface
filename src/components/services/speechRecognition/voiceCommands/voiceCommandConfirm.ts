import { Observable } from 'rxjs/Observable';

import { ContainsBaseCommand } from 'components/services/speechRecognition/voiceCommands/containsBaseCommand';

export class VoiceCommandConfirm extends ContainsBaseCommand {

    constructor(executor: (response: string) => Observable<any>){
        super('CONFIRM', 'confirmar', executor);
    }

}
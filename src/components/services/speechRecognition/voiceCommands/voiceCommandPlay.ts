import { Observable } from 'rxjs/Observable';

import { WitaiResponseSchema, WitaiIntent } from 'components/schemas/witai.schema';
import { WitAiBaseCommand } from 'components/services/speechRecognition/voiceCommands/witaAiBaseCommand';

export class VoiceCommandPlay extends WitAiBaseCommand {

    constructor(){
        super('PLAY', WitaiIntent.Play);
    }

    executes(response: WitaiResponseSchema): Observable<any> {
        //TODO: Improve the play to accept arguments like movies or episodes
        return this._serverApi.getPlayer().getDeviceApi().play();
    }

    needsConfirmation(): boolean {
        return false;
    }

}
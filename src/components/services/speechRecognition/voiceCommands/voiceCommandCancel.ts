import { Observable } from 'rxjs/Observable';

import { ContainsBaseCommand } from 'components/services/speechRecognition/voiceCommands/containsBaseCommand';

export class VoiceCommandCancel extends ContainsBaseCommand {

    constructor (executor: (response: string) => Observable<any>){
        super('CANCEL', 'cancelar', executor);
    }

}
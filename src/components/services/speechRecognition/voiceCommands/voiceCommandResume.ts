import { Observable } from 'rxjs/Observable';

import { WitaiResponseSchema, WitaiIntent } from 'components/schemas/witai.schema';
import { WitAiBaseCommand } from 'components/services/speechRecognition/voiceCommands/witaAiBaseCommand';

export class VoiceCommandResume extends WitAiBaseCommand {

    constructor(){
        super('RESUME', WitaiIntent.ResumeMedia);
    }

    executes(response: WitaiResponseSchema): Observable<any> {
        return this._serverApi.getPlayer().getDeviceApi().resume();
    }

    needsConfirmation(): boolean {
        return false;
    }

}
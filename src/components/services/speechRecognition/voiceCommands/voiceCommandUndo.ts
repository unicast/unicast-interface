import { Observable } from 'rxjs/Observable';

import { ContainsBaseCommand } from 'components/services/speechRecognition/voiceCommands/containsBaseCommand';

export class VoiceCommandUndo extends ContainsBaseCommand {

    constructor (executor: (response: string) => Observable<any>){
        super('UNDO', 'desfazer', executor);
    }

}
import { Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { WitaiResponseSchema, WitaiIntent } from 'components/schemas/witai.schema';
import { SpeechResult } from 'components/services/speechRecognition/speechResult';
import { WitaiApi } from 'components/services/witaiApi';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { BaseVoiceCommand } from 'components/services/speechRecognition/voiceCommands';


export abstract class WitAiBaseCommand extends BaseVoiceCommand<WitaiResponseSchema> {
    protected _witai: WitaiApi;
    protected _serverApi: ServerApiManager;
    protected _intents: Array<WitaiIntent>;

    constructor(commandName: string, ...intents: Array<WitaiIntent>){
        super(commandName);
        this._intents = intents;
    }

    convertResponse(response: SpeechResult): Observable<WitaiResponseSchema> {
        return this._witai.ask(response.toString());
    }

    recognizes(response: WitaiResponseSchema): boolean {
        return (response.entities.intent && this.getIntent(response) != undefined);
    }

    getIntent(response: WitaiResponseSchema): WitaiIntent {
        return this._intents.find((value ) => response.entities.intent[0].value == value);
    }

    abstract executes(response: WitaiResponseSchema): Observable<any>;
    abstract needsConfirmation(): boolean;

    setInjector(injector: Injector): void{
        this._witai = injector.get(WitaiApi);
        this._serverApi = injector.get(ServerApiManager);
    }

}
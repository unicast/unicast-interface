import { Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { SpeechResult } from 'components/services/speechRecognition/speechResult';
import { SingularSubject } from 'components/common/helpers/singularSubject';
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { BaseVoiceCommand } from 'components/services/speechRecognition/voiceCommands';

export abstract class ContainsBaseCommand extends BaseVoiceCommand<string> {
    protected _serverApi: ServerApiManager;

    private _contains: string;
    private _executor: (response: string) => Observable<any>;

    constructor(commandName: string, contains: string, executor: (response: string) => Observable<any>){
        super(commandName);
        this._contains = contains;
        this._executor = executor;
    }

    convertResponse(response: SpeechResult): Observable<string> {
        return SingularSubject.create<string>(response.toString()).first();
    }

    recognizes(response: string): boolean{
        return (response || "").indexOf(this._contains) > -1;
    }

    executes(response: string): Observable<any> {
        return this._executor(response);
    }

    needsConfirmation(): boolean {
        return false;
    }

    getLookupString(): string {
        return this._contains;
    }

    setInjector(injector: Injector): void {
        this._serverApi = injector.get(ServerApiManager);
    }
}
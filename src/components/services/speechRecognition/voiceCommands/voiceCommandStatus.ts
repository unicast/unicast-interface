import { ContainsBaseCommand } from 'components/services/speechRecognition/voiceCommands/containsBaseCommand';

export class VoiceCommandStatus extends ContainsBaseCommand {

    constructor(){
        super('STATUS', 'atualizar estado', (response) => {
            return this._serverApi.getPlayer().getDeviceApi().getStatus().getValue(true).first();
        });
    }

}
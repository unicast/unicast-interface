import { Observable } from 'rxjs/Observable';

import { WitAiBaseCommand } from 'components/services/speechRecognition/voiceCommands/witaAiBaseCommand';
import { WitaiResponseSchema, WitaiIntent } from 'components/schemas/witai.schema';

export class VoiceCommandStop extends WitAiBaseCommand {


    constructor(){
        super('STOP', WitaiIntent.StopMedia);
    }

    executes(response: WitaiResponseSchema): Observable<any> {
        return this._serverApi.getPlayer().getDeviceApi().stop();
    }

    needsConfirmation(): boolean {
        return true;
    }

}
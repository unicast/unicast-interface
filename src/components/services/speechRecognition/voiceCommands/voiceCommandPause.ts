import { Observable } from 'rxjs/Observable';

import { WitaiResponseSchema, WitaiIntent } from 'components/schemas/witai.schema';
import { WitAiBaseCommand } from 'components/services/speechRecognition/voiceCommands/witaAiBaseCommand';

export class VoiceCommandPause extends WitAiBaseCommand {

    constructor(){
        super('PAUSE', WitaiIntent.PauseMedia);
    }

    executes(response: WitaiResponseSchema): Observable<any> {
        return this._serverApi.getPlayer().getDeviceApi().pause();
    }

    needsConfirmation(): boolean {
        return false;
    }

}
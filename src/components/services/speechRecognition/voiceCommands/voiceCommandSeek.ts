import { Observable } from 'rxjs/Observable';

import { WitAiBaseCommand } from 'components/services/speechRecognition/voiceCommands/witaAiBaseCommand';
import { WitaiResponseSchema, WitaiIntent } from 'components/schemas/witai.schema';
import { ICommandMessage, CommandSequencialMessage, ISequencialMessageType } from 'components/services/speechRecognition/sequencialMessages';
import { Time } from 'components/common/helpers/time';
import { DeviceStatusSchema } from 'components/schemas/status.schema';
import { TranslatorManager } from 'components/services/translatorManager';


export class VoiceCommandSeek extends WitAiBaseCommand {

    constructor(){
        super("SEEK", WitaiIntent.Seek, WitaiIntent.SeekForward, WitaiIntent.SeekBackwards);
    }

    private _transformInTime(response: WitaiResponseSchema): Time{
        let sum = 0;
        for (let val of response.entities.duration){
            sum += val.normalized.value;
        }
        return Time.fromSeconds(sum);
    }

    executes(response: WitaiResponseSchema): Observable<any> {
        let intent = this.getIntent(response);
        let time = this._transformInTime(response);
        switch (intent){
            case WitaiIntent.SeekBackwards:
                time = new Time().minus(time);
            case WitaiIntent.SeekForward:
                return this._serverApi.getPlayer().getDeviceApi().seek(time);
            case WitaiIntent.Seek:
            default:
                return this._serverApi.getPlayer().getDeviceApi().seekTo(time);
        }


    }

    needsConfirmation(): boolean {
        return true;
    }

    presentsCommand(response: WitaiResponseSchema): Observable<ICommandMessage<WitaiResponseSchema>> {
        let intent = this.getIntent(response);
        let time = this._transformInTime(response);
        return this._serverApi.getPlayer().getDeviceApi().getStatus().getValue().first()
            .map<DeviceStatusSchema, ICommandMessage<WitaiResponseSchema>>((value, index ) =>  {
                let current = Time.fromSeconds(value.media.time.current);
                let duration = Time.fromSeconds(value.media.time.duration);
                let message: CommandSequencialMessage;
                let interleaved: Array<ISequencialMessageType> = ['NORMAL', 'FIELD'];
                switch (intent){
                    case WitaiIntent.SeekBackwards:
                        current = current.minus(time);
                        message = CommandSequencialMessage.fromResource('COMMANDS.SEEK.BACKWARD', interleaved, { 'offset': time.toString(), 'to': current.toString()} );
                        break;
                    case WitaiIntent.SeekForward:
                        current = current.plus(time);
                        message = CommandSequencialMessage.fromResource('COMMANDS.SEEK.FORWARD', interleaved, { 'offset': time.toString(), 'to': current.toString()} );
                        break;
                    case WitaiIntent.Seek:
                    default:
                        current = time;
                        message = CommandSequencialMessage.fromResource('COMMANDS.SEEK.MOVE_TO', interleaved, { 'to': time.toString() });
                }

                if (current.isAfter(duration)){
                    throw new Error(TranslatorManager.getInstance().fromResource('COMMANDS.SEEK.ERROR_AFTER_DURATION'));
                }

                if (current.isNegative()){
                    throw new Error(TranslatorManager.getInstance().fromResource('COMMANDS.SEEK.ERROR_NEGATIVE'));
                }

                return {
                    title: 'COMMANDS.SEEK.TITLE',
                    message: message
                };
            });

    }

}
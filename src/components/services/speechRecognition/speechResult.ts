export interface ITranscript{
    transcript: string;
    confidence: number;
    isFinal: boolean;
}

export class SpeechResult{
    private static readonly MIN_TOLERANCE = 0.8;
    private static readonly TIME_PATTERN = /([0-9]+):([0-9]+)/g;

    private _list: Array<ITranscript>;
    private _msg: {normal: string, interim: string};
    private _isFinal: boolean;

    public constructor(msg: string);
    public constructor(event: SpeechRecognitionEvent);
    public constructor(event: SpeechRecognitionEvent | string){
        this._list = [];
        this._isFinal = true;
        if (this.isSpeechRecognitionEvent(event)){
            for (let i = 0; i < event.results.length; i++){
                let result = event.results[i][0];
                let isFinal = event.results[i].isFinal;
                if (result.transcript && result.transcript.trim().length > 0){
                    this._add(result.transcript, result.confidence, isFinal);
                }
            }
        } else {
            this._add(event, 1, true);
        }

        this._buildMsg();
    }

    private _add(msg: string, confidence: number, isFinal: boolean): void{
        this._list.push({transcript: msg, confidence: confidence, isFinal: isFinal});
        this._isFinal = this._isFinal && isFinal;
    }

    private isSpeechRecognitionEvent( evt: any): evt is SpeechRecognitionEvent{
        return !!evt['results'];
    }

    public isFinal(): boolean {
        return this._isFinal;
    }

    public isEmpty(): boolean {
        return this._list.length == 0;
    }

    private _buildMsg(){
        let ret = {normal: '', interim: ''};
        let act = 'normal';
        for (let result of this._list){
            if (!result.isFinal && result.confidence < SpeechResult.MIN_TOLERANCE){
                act = 'interim';
            }
            ret[act] += result.transcript;
        }
        this._msg = ret;
    }

    private _escape(msg: string): string{
        return msg.replace(SpeechResult.TIME_PATTERN, '$1 horas e $2 minutos');
    }

    toMessage(): {normal: string, interim: string}{
        return Object.assign({}, this._msg);
    }

    toString(): string {
        let msg = this.toMessage();
        return this._escape(msg.normal) + msg.interim;
    }

}
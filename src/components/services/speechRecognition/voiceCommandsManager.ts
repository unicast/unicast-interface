import { Injectable, Injector } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';

import { SpeechRecognitionManager } from 'components/services/speechRecognition/speechRecognition';
import { SpeechSynthesisManager } from 'components/services/speechRecognition/speechSynthesis';
import { SpeechResult } from 'components/services/speechRecognition/speechResult';
import { VoiceCommandUndo } from 'components/services/speechRecognition/voiceCommands/voiceCommandUndo';
import { VoiceCommandCancel } from 'components/services/speechRecognition/voiceCommands/voiceCommandCancel';
import { VoiceCommandConfirm } from 'components/services/speechRecognition/voiceCommands/voiceCommandConfirm';
import { SingularSubject } from 'components/common/helpers/singularSubject';
import { DelayedExecutions } from 'components/common/services/delayedExecutions';
import { TopoverManager } from 'components/common/commandCenter/voiceCommands/topover.service';
import { ICommandMessage } from 'components/services/speechRecognition/sequencialMessages';
import { IVoiceCommand, IInversableVoiceCommand } from 'components/services/speechRecognition/voiceCommands';


export type listenCallbacks = {before?: (event: SpeechResult) => any, after?: (event: SpeechResult, cmd: IVoiceCommand<any>) => any};

@Injectable()
export class VoiceCommandsManager {
    private static readonly WAIT_MS = 10000;

    private _injector: Injector;
    private _commands: Map<Object, Array<IVoiceCommand<any>>>;
    private _recognition: SpeechRecognitionManager;
    private _speech: SpeechSynthesisManager;
    private _delayedExecutions: DelayedExecutions;
    private _topover: TopoverManager;

    private _timeoutExecute;
    private _subscriptionExecute: Subscription;

    constructor(injector: Injector, recognition: SpeechRecognitionManager, speech: SpeechSynthesisManager, topover: TopoverManager, delayedExecutions: DelayedExecutions){
        this._injector = injector;
        this._recognition = recognition;
        this._speech = speech;
        this._delayedExecutions = delayedExecutions;
        this._commands = new Map<Object, Array<IVoiceCommand<any>>>();
        this._topover = topover;

    }

    public getRecognition(): SpeechRecognitionManager{
        return this._recognition;
    }

    private getAllCommands(parent: Object = undefined): Array<IVoiceCommand<any>>{
        let list: Array<IVoiceCommand<any>> = [];
        if (parent) {
            list = this._commands.get(parent) || [];
        } else {
            for(let cmds of this._commands.values()){
                list = list.concat(cmds);
            }
        }
        return list;
    }

    private commandIsInversable(cmd: IVoiceCommand<any>): cmd is IInversableVoiceCommand<any>{
        return !!cmd['inverse'];
    }

    private async _listenInversible(cmd: IInversableVoiceCommand<any>, data: any){
        let topover = await this._topover.getComponent();

        let undo = new VoiceCommandUndo((response) => {
            this._finallyExecuted();
            return cmd.inverse.executes(data);
        });

        let confirm = new VoiceCommandConfirm( (response) => {
            this._finallyExecuted();
            return SingularSubject.oneValue(response);
        });

        this._subscriptionExecute = topover.getBtnEmitter().subscribe( (data)  => {
            if (data) {
                this._recognition.voiceEmulate(confirm.getLookupString(), undefined, false);
            } else {
                this._recognition.voiceEmulate(undo.getLookupString(), undefined, false);
            }
        });

        this.listenCommand({}, [undo, confirm], false);
    }

    private async _listenConfirmable(cmd: IVoiceCommand<any>, data: any){
        let topover = await this._topover.getComponent();

        let exec = () => {
            return cmd.executes(data);
        };

        let cancel = new VoiceCommandCancel((response) => {
            this._finallyExecuted();
            return SingularSubject.oneValue(response);
        });
        let confirm = new VoiceCommandConfirm( (response) => {
            this._finallyExecuted();
            return exec();
        });

        this._subscriptionExecute = topover.getBtnEmitter().subscribe( (data)  => {
            if (data) {
                this._recognition.voiceEmulate(confirm.getLookupString(), undefined, false);
            } else {
                this._recognition.voiceEmulate(cancel.getLookupString(), undefined, false);
            }
        });

        this._timeoutExecute = this._delayedExecutions.setTimeout(exec, VoiceCommandsManager.WAIT_MS);
        this.listenCommand({}, [cancel, confirm], false);
    }

    private _finallyExecuted(){
        this._speech.abort();

        if (this._subscriptionExecute){
            this._subscriptionExecute.unsubscribe();
        }

        this._delayedExecutions.setTimeout( async () => {
            (await this._topover.getComponent()).hide();
        }, 2500);

        if (this._timeoutExecute){
            this._delayedExecutions.clearTimeout(this._timeoutExecute);
        }
    }

    private async _informUserAction(obs: Observable<ICommandMessage<any>>, cancellable:boolean = false, undoable:boolean = false){
        let topover = await this._topover.getComponent();
        obs.subscribe( (ret) => {
            this._delayedExecutions.setTimeout( () => {
                this._speech.speak(ret.voiceMessage || ret.message.toString());
                topover.showExecuting({title: ret.title, message: ret.message, cancellable: cancellable, undoable: undoable});
            }, 0);
        }, (error) => {
            this._delayedExecutions.setTimeout( () => {
                topover.showExecuting({title: "Error", message: error.message});
            }, 0);
            throw error;
        });
    }

    private async _checkCommands(iterator: IterableIterator<[number, IVoiceCommand<any>]>, result: SpeechResult, subject: ReplaySubject<IVoiceCommand<any>>){
        let next = iterator.next();
        if (!!next.value){
            let cmd = next.value[1];
            cmd.convertResponse(result).subscribe(async (data) => {
                if (cmd.recognizes(data)){
                    let ret = cmd.presentsCommand(data);
                    let isInversible = this.commandIsInversable(cmd);
                    await this._informUserAction(ret, cmd.needsConfirmation(), isInversible);
                    if (cmd.needsConfirmation()){
                        this._listenConfirmable(cmd, data);
                    } else {
                        let inv = (output) => {
                            if (isInversible){
                                this._listenInversible(cmd as IInversableVoiceCommand<any>, data);
                            }
                        };
                        cmd.executes(data).subscribe(inv, inv);
                    }
                    subject.next(cmd);
                } else {
                    this._delayedExecutions.setTimeout(() => {
                        this._checkCommands(iterator, result, subject);
                    }, 0);
                }
            }, (err) => {
                console.info("Failed to interpret '" + result.toString() + "'");
                console.error(err);
            });
        } else {
            subject.next(null);
            (await this._topover.getComponent()).hide();
        }
    }

    public execute(result: SpeechResult, parent?: Object): Observable<IVoiceCommand<any>>;
    public execute(result: SpeechResult, commands: Array<IVoiceCommand<any>>): Observable<IVoiceCommand<any>>;
    public execute(result: SpeechResult, parent: Array<IVoiceCommand<any>> | Object = undefined): Observable<IVoiceCommand<any>>{
        let req = SingularSubject.create<IVoiceCommand<any>>();
        let commands = (parent instanceof Array ? parent : this.getAllCommands(parent)).entries();
        this._checkCommands(commands, result, req);
        return req.asObservable().first();
    }

    public emulateListenCommand(msg: string, callback: listenCallbacks, parent?: Object, topover?: boolean);
    public emulateListenCommand(msg: string, callback: listenCallbacks, commands?: Array<IVoiceCommand<any>>, topover?: boolean);
    public emulateListenCommand(msg: string, callback: listenCallbacks, parent?: Array<IVoiceCommand<any>> | Object, topover: boolean = true){
        this._recognition.voiceEmulate(msg, this.getListenCallback(callback, parent), topover);
    }

    public listenCommand(callback: listenCallbacks, parent?: Object, topover?: boolean);
    public listenCommand(callback: listenCallbacks, commands?: Array<IVoiceCommand<any>>, topover?: boolean);
    public listenCommand(callback: listenCallbacks, parent?: Array<IVoiceCommand<any>> | Object, topover: boolean = true){
        this._recognition.start(this.getListenCallback(callback, parent), topover).catch( (err) => {
            console.error(err);
        });
    }

    private getListenCallback(callback: listenCallbacks, parent?: Array<IVoiceCommand<any>> | Object) {
        return (event: SpeechResult) => {
            if (callback.before){
                callback.before(event);
            }
            if (event.isEmpty()){
                return;
            }
            this.execute(event, parent).subscribe( (data) => {
                if (callback.after){
                    callback.after(event, data);
                }
            }, (error) => {
                console.error(error);
            });
        };
    }

    public register(parent: Object, vc: IVoiceCommand<any>);
    public register(parent: Object, vc: IVoiceCommand<any>, inverse: IVoiceCommand<any>);
    public register(parent: Object, vc: IVoiceCommand<any>, inverse?: IVoiceCommand<any>){
        if (!this._commands.has(parent)){
            this._commands.set(parent, []);
        }
        let list = this._commands.get(parent);
        vc.setInjector(this._injector);
        list.push(vc);
        if (inverse){
            vc = Object.assign(vc, {inverse: inverse});
            inverse = Object.assign(inverse, {inverse: vc});
            inverse.setInjector(this._injector);
            list.push(inverse);
        }
    }

    public unregister(parent: Object): VoiceCommandsManager{
        this._commands.set(parent, []);
        return this;
    }

}
import { TranslatorManager } from 'components/services/translatorManager';

export type ISequencialMessageType = 'FIELD' | 'NORMAL' | 'INTERIM';

export type ISequencialMessage = { msg: string, type: ISequencialMessageType};

export class CommandSequencialMessage {
    msgs: Array<ISequencialMessage>;

    constructor(msgs: Array<ISequencialMessage>){
        this.msgs = msgs;
    }

    public static fromString(msg: string) : CommandSequencialMessage {
        return new CommandSequencialMessage([{msg: msg, type: 'NORMAL'}]);
    }

    public static fromInterleaved(types: Array<ISequencialMessageType>, msgs: Array<string>): CommandSequencialMessage {
        let ret: Array<ISequencialMessage> = [];
        let typesSize = types.length;
        for (let i = 0; i < msgs.length; i++) {
            ret.push({type: types[i % typesSize], msg: msgs[i]});
        }
        return new CommandSequencialMessage(ret);
    }

    public static fromResource(resourceCode: string, types?: Array<ISequencialMessageType>): CommandSequencialMessage;
    public static fromResource(resourceCode: string, types: Array<ISequencialMessageType>, params: {[p: string]: string}): CommandSequencialMessage;
    public static fromResource(resourceCode: string, types: Array<ISequencialMessageType> = ['NORMAL'], params?: {[p: string]: string}): CommandSequencialMessage {
        let arr = TranslatorManager.getInstance().fromResource<Array<string>>(resourceCode, params);
        if (arr instanceof Array){
            return CommandSequencialMessage.fromInterleaved(types,arr);
        } else {
            return CommandSequencialMessage.fromString(arr);
        }

    }

    public toString(): string {
        return this.msgs.map((value, index, array) => value.msg).join(' ');
    }
}

export interface ICommandMessage<T>{
    title: string;
    message: CommandSequencialMessage;
    voiceMessage?: string;
    cancel?(response: T): void;
}


import { ApplicationRef, Injectable } from '@angular/core';
import { TopoverManager } from 'components/common/commandCenter/voiceCommands/topover.service';
import { SpeechResult } from 'components/services/speechRecognition/speechResult';

/**
 * This trick is needed because some browsers use the Web Speech API with different names
 * (ugly words there)
 * So we need to adapt, and ensure that it keeps working, if the browsers supports it
 */
interface IWindow extends Window {
    webkitSpeechRecognition?: any;
    webkitSpeechGrammarList?: any;
    webkitSpeechRecognitionEvent?: any;
}

const {
    webkitSpeechRecognition,
    webkitSpeechGrammarList,
    webkitSpeechRecognitionEvent
} = window as IWindow;

var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;

declare global {
    interface SpeechGrammarList {
        addFromUri(src: string, weight: number): void;
    }
}

/**
 * Trick end
 */

@Injectable()
export class SpeechRecognitionManager {

    private static readonly MAX_ATTEMPS: number = 5;

    private _recognition: SpeechRecognition;
    private _applicationRef: ApplicationRef;
    private _topover: TopoverManager;

    constructor(applicationRef: ApplicationRef, topover: TopoverManager){
        this._applicationRef = applicationRef;
        this._topover = topover;

        if (SpeechRecognition && SpeechGrammarList) {
            this._recognition = new SpeechRecognition();

            this._recognition.grammars = new SpeechGrammarList();
            this._recognition.grammars.addFromUri('speechGrammar.jsgf', 1);
        } else {
            this._recognition = new SpeechRecognitionWrapper();
        }


        this._recognition.lang = 'pt-PT';
        this._recognition.interimResults = true;
        this._recognition.maxAlternatives = 1;

        this._recognition.onend = () => {
            this.stop();
        }
    }

    private _listen(toExecute?: (event: SpeechResult) => any){
        this._recognition.onresult = async (event: SpeechRecognitionEvent) => {
            let msg = new SpeechResult(event);
            let topover = await this._topover.getComponent();
            if (topover.isVisible()){
                topover.refreshMessage(msg.toMessage());
            }
            if (toExecute && msg.isFinal()){
                if (topover.showing == topover.SHOWS_INPUT && !msg.isEmpty()){
                    topover.showProcessing();
                }
                toExecute(msg);
            }
            //It is out of the angular detection change enviroment. So let ask to all interface to redetect
            this._applicationRef.tick();
        }
    }

    public async start(toExecute?: (event: SpeechResult) => any, topover: boolean = true){
        if (topover){
            (await this._topover.getComponent()).showInput();
        }
        this._listen(toExecute);
        this._recognition.abort();
        let attemps: number = 0;
        while (attemps < SpeechRecognitionManager.MAX_ATTEMPS){
            try {
                this._recognition.start();
                break;
            } catch (err) {
                await new Promise<void>(resolve => setTimeout(resolve, 10));
                ++attemps;
            }
        }

        this._applicationRef.tick();
    }


    public async stop(){
        this._recognition.stop();
        if (this._recognition.onresult){
            this._recognition.onresult(this.createEvent("voiceEnded", ""));
            this._recognition.onresult = undefined;
        }
        this._applicationRef.tick();
    }

    public async voiceEmulate(msg: string, toExecute?: (event: SpeechResult) => any, topover: boolean = true){
        if (topover){
            (await this._topover.getComponent()).showInput();
        }
        let evt = this.createEvent("voiceEmulated", msg);
        if (!this._recognition.onresult){
            this._listen((event) => {
                if (toExecute){
                    toExecute(event);
                }
                this._recognition.onresult = undefined;
            });
        }

        this._recognition.onresult(evt);
        this._recognition.stop();
    }

    private createEvent(type: string, msg: string): SpeechRecognitionEvent{
        let evt = new Event("voiceEmulated");
        evt['results'] = [ { 0: {transcript: msg, confidence: 1 }, isFinal: true } ];
        return evt as SpeechRecognitionEvent;
    }

}


export class SpeechRecognitionWrapper implements SpeechRecognition{
    grammars: SpeechGrammarList;
    lang: string;
    continuous: boolean;
    interimResults: boolean;
    maxAlternatives: number;
    serviceURI: string;
    start(): void {

    }
    stop(): void {

    }
    abort(): void {

    }
    onaudiostart: (ev: Event) => any;
    onaudioend: (ev: Event) => any;
    onsoundstart: (ev: Event) => any;
    onspeechstart: (ev: Event) => any;
    onspeechend: (ev: Event) => any;
    onsoundend: (ev: Event) => any;
    onresult: (ev: SpeechRecognitionEvent) => any;
    onnomatch: (ev: SpeechRecognitionEvent) => any;
    onerror: (ev: SpeechRecognitionError) => any;
    onstart: (ev: Event) => any;
    onend: (ev: Event) => any;
    addEventListener(type: string, listener?: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void {

    }
    dispatchEvent(evt: Event): boolean {
        return false;
    }
    removeEventListener(type: string, listener?: EventListenerOrEventListenerObject, options?: boolean | EventListenerOptions): void {

    }
}

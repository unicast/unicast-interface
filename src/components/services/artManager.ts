import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { DisposableObservableStack } from 'components/common/services/disposableObservableStack';

@Injectable()
export class ArtManager {

    public stack: DisposableObservableStack;

    constructor () {
        this.stack = new DisposableObservableStack;
    }

    get current (): Observable<any> {
        return this.stack.current;
    }

    register ( art ) {
        return this.stack.push( art );
    }

    dispose ( token ) {
        return this.stack.dispose( token );
    }
}
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { APP_ROUTER_PROVIDDERS } from 'components/app.routes';
import { AppComponent }  from 'components/app';
import { AppStorage } from 'components/app.storage';

//Services
import { ServerApiManager } from 'components/services/serverApis/serverApiManager';
import { TopbarManager } from 'components/services/topbarManager';
import { SidebarManager } from 'components/services/sidebarManager';
import { ArtManager } from 'components/services/artManager';
import { ModalManager } from './services/modalManager';
import { LocalStorageManager } from 'components/services/storageManager';
import { CommandCenter } from 'components/common/commandCenter/commandCenter.service';
import { TopoverManager } from 'components/common/commandCenter/voiceCommands/topover.service';
import { DelayedExecutions } from 'components/common/services/delayedExecutions';
import { PageTitle } from 'components/common/services/pageTitle';
import { SpeechRecognitionManager } from 'components/services/speechRecognition/speechRecognition';
import { SpeechSynthesisManager } from 'components/services/speechRecognition/speechSynthesis';
import { VoiceCommandsManager } from 'components/services/speechRecognition/voiceCommandsManager';
import { WitaiApi } from 'components/services/witaiApi';
import { ServerApiController } from 'components/services/serverApis/serverApiController';
import { RequesterHttp } from 'components/services/requesterHttp';
import { StandardMediaQueries, MediaQueries } from 'components/services/mediaQueries';
import { MediaPlayControl } from 'components/common/services/mediaPlayControl';
import { TranslatorManager } from 'components/services/translatorManager';
import { SessionCache } from 'components/common/services/sessionCache';
import { NotificationsService } from 'components/common/commandCenter/notifications/notifications.service';
import { BackgroundWorker } from 'components/services/backgroundWorker';
import { NavigationManager } from 'components/services/navigationManager';
import { Toast } from 'components/common/toast/toast.service';
import { FiltersMediaSidemenuService } from 'components/pages/common/sidemenuFilters/filtersMediaSidemenu.service';
import { MirroringManager } from 'components/services/mirroringManager';
import { ApplicationContextManager } from 'components/services/applicationContextManager';
import { NonExpirableListManager } from 'components/services/nonExpirableListManager';

//Pages
import { HomePageComponent } from 'components/pages/home/home.component';
import { MoviesComponent } from 'components/pages/movies/movies.component';
import { MoviesDetailsComponent } from 'components/pages/movies/details/moviesDetails.component';
import { MoviesDetailsSidemenuComponent } from 'components/pages/movies/details/sidemenu/sidemenu.component';
import { MoviesListComponent } from 'components/pages/movies/list/moviesList.component';
import { MoviesListSidemenuComponent } from 'components/pages/movies/list/sidemenu/sidemenu.component';
import { ShowsComponent } from 'components/pages/shows/shows.component';
import { ShowsDetailsComponent } from 'components/pages/shows/details/showsDetails.component';
import { ShowsDetailsSidemenuComponent } from 'components/pages/shows/details/sidemenu/sidemenu.component';
import { ShowsListComponent } from 'components/pages/shows/list/showsList.component';
import { ShowsListSidemenuComponent } from 'components/pages/shows/list/sidemenu/sidemenu.component';
import { SeasonsListComponent } from 'components/pages/shows/details/seasonsList/seasonsList.component';
import { EpisodesDetailsComponent } from 'components/pages/shows/details/episodesDetails/episodesDetails.component';
import { EpisodesListComponent } from 'components/pages/shows/details/episodesList/episodesList.component';
import { SubtitlesComponent } from 'components/pages/common/subtitles/subtitles.component';
import { MediaCastComponent } from 'components/pages/common/mediaCast/mediaCast.component';
import { SettingsComponent } from 'components/pages/settings/settings.component';
import { HistoryComponent } from 'components/pages/history/history.component';
import { HistorySidemenuComponent } from 'components/pages/history/sidemenu/sidemenu.component';
import { PeopleComponent } from 'components/pages/people/people.component';
import { PeopleListComponent } from 'components/pages/people/list/peopleList.component';
import { PeopleListSidemenuComponent } from 'components/pages/people/list/sidemenu/sidemenu.component';
import { PeopleDetailsComponent } from 'components/pages/people/details/peopleDetails.component';
import { PeopleDetailsSidemenuComponent } from 'components/pages/people/details/sidemenu/sidemenu.component';

//Pipes
import { CallerPipe } from 'components/common/helpers/callerPipe';
import { NegatePipe } from 'components/common/helpers/negatePipe';
import { RatingPipe } from 'components/common/helpers/ratingPipe';
import { ArtworkGalleryPipe } from 'components/common/helpers/artworkGalleryPipe';
import { ResolvePropertiesPipe } from 'components/common/helpers/resolvePropertiesPipe';
import { ImageUrlPipe } from 'components/common/helpers/imageUrl';
import { OrDefaultValuePipe } from 'components/common/helpers/orDefaultValuePipe';
import { MapToIterablePipe } from 'components/common/helpers/mapToIterablePipe';
import { MaxPipe, MinPipe } from 'components/common/helpers/mathPipe';
import { EpisodesAggregatorPipe, AvailableEpisodeSources2DropdownPipe } from 'components/common/helpers/episodesAggregatorPipe';
import { FriendlyDatePipe } from 'components/common/helpers/friendlyDatePipe';
import { FriendlyDataRatePipe } from 'components/common/helpers/friendlyDataRatePipe';
import { FriendlySizePipe } from 'components/common/helpers/friendlySizePipe';
import { FriendlyDurationPipe } from 'components/common/helpers/friendlyDurationPipe';
import { CssSizePipe } from 'components/common/helpers/cssSizePipe';
import { EpisodeSubcaptionPipe } from 'components/common/helpers/episodeSubtitlePipe';

//Common Components
import { PreventBubbleDirective } from 'components/common/preventBubble/preventBubble.directive';
import { DynamicComponentComponent } from 'components/common/dynamicComponent/dynamicComponent.component';
import { LoaderComponent } from 'components/common/loader/loader.component';
import { ThumbnailComponent } from 'components/common/thumbnail/thumbnail.component';
import { ThumbnailMediaComponent } from 'components/common/thumbnail-media/thumbnail-media.component';
import { ThumbnailPersonComponent } from 'components/common/thumbnail-person/thumbnail-person.component';
import { ThumbnailInfoTagsComponent } from 'components/common/thumbnail/info-tags/info-tags.component';
import { ThumbnailContextMenuComponent } from 'components/common/thumbnail/context-menu/context-menu.component';
import { ThumbnailsListComponent } from 'components/common/thumbnailsList/thumbnailsList.component';
import { ScrollbarComponent } from 'components/common/scrollbar/scrollbar.component';
import { DropdownComponent } from 'components/common/dropdown/dropdown.component';
import { ButtonComponent } from 'components/common/buttons/button/button.component';
import { SectionComponent } from 'components/common/section/section.component';
import { SliderComponent } from 'components/common/slider/slider.component';
import { NumberSliderComponent } from 'components/common/slider/numberSlider.component';
import { PercentageSliderComponent } from 'components/common/slider/percentageSlider.component';
import { TimeSliderComponent } from 'components/common/slider/timeSlider.component';
import { TopoverComponent } from 'components/common/commandCenter/voiceCommands/topover.component';
import { CommandCenterComponent } from 'components/common/commandCenter/commandCenter.component';
import { CommandCenterActionComponent } from 'components/common/commandCenter/commandCenterAction/commandCenterAction.component';
import { NotificationComponent } from 'components/common/commandCenter/notifications/notification.component';
import { SidemenuFiltersListsComponent } from 'components/pages/common/sidemenuFilters/lists/sidemenuFiltersLists.component';
import { SidemenuFiltersCollectionsComponent } from 'components/pages/common/sidemenuFilters/collections/sidemenuFiltersCollections.component';
import { SidemenuFiltersGenresComponent } from 'components/pages/common/sidemenuFilters/genres/sidemenuFiltersGenres.component';
import { SidemenuFiltersQualitiesComponent } from 'components/pages/common/sidemenuFilters/qualities/sidemenuFiltersQualities.component';
import { SidemenuFiltersSortComponent } from 'components/pages/common/sidemenuFilters/sort/sidemenuFiltersSort.component';
import { SidemenuFiltersWatchComponent } from 'components/pages/common/sidemenuFilters/watch/sidemenuFiltersWatch.component';
import { SidemenuMediaCollectionsComponent } from 'components/pages/common/sidemenuFilters/collections/sidemenuMediaCollections.component';
import { SidemenuFiltersRepositoriesComponent } from 'components/pages/common/sidemenuFilters/repositories/sidemenuFiltersRepositories.component';
import { SidemenuMediaRepositoriesComponent } from 'components/pages/common/sidemenuFilters/repositories/sidemenuMediaRepositories.component';
import { MediaRedirectComponent } from 'components/common/mediaRedirect/mediaRedirect.component';
import { ToastComponent } from 'components/common/toast/toast.component';
import { LineChartComponent } from 'components/common/chart/line/line.component';
import { SidemenuFiltersDateComponent } from 'components/pages/common/sidemenuFilters/date-range/sidemenuFiltersDate.component';
import { SidemenuFiltersMediaComponent } from 'components/pages/common/sidemenuFilters/medias/sidemenuFiltersMedia.component';
import { AutoCompleteComponent } from 'components/common/auto-complete/auto-complete.component';
import { DateRangeComponent } from 'components/common/date-range/date-range.component';
import { ListSelectorComponent } from 'components/pages/common/listSelector/listSelector.component';
import { FilterPresetsComponent } from 'components/pages/common/sidemenuFilters/filter-presets/filter-presets.component';
import { AutoScrollUpDownComponent } from 'components/common/buttons/up-down/up-down.component';
import { CollapsibleComponent } from 'components/common/collapsible/collapsible.component';
import { SidemenuMediaListComponent } from 'components/pages/common/sidemenuFilters/lists/sidemenuMediaLists.component';
import { DownloadMediaDropdownComponent } from 'components/common/downloadMediaDropdown/downloadMediaDropdown.component';
import { IconCompoundComponent } from 'components/common/icons/compound/icon-compound.component';
import { IconComponent } from 'components/common/icons/icon/icon.component';
import { DroploaderComponent } from 'components/pages/common/droploader/droploader.component';

//Modals
import { ModalComponent } from 'components/common/modal/modal.component';
import { ModalWrapper } from 'components/common/modal/modalWrapper';
import { ConfirmModalComponent } from 'components/common/modal/confirmModal/confirmModal.component';
import { InputModalComponent } from 'components/common/modal/inputModal/inputModal.component';
import { GalleryModalComponent } from 'components/common/modal/galleryModal/galleryModal.component';
import { MediaInfoModalComponent } from 'components/common/modal/mediaInfoModal/mediaInfoModal.component';
import { HistoryMediaModalComponent } from 'components/common/modal/historyMediaModal/historyMediaModal.component';

// Layout Components
import { PlayerRemoteComponent } from 'components/layout/footerPlayer/playerSelect/playerRemote/playerRemote.component';
import { AdvancedSeekComponent } from 'components/layout/footerPlayer/playerSelect/playerRemote/advancedSeek/advancedSeek.component';
import { PlayerBadgeComponent } from 'components/layout/footerPlayer/playerSelect/playerBadge/playerBadge.component';
import { PlayerControlComponent } from 'components/layout/footerPlayer/playerControl/playerControl.component';
import { PlayerSelectComponent } from 'components/layout/footerPlayer/playerSelect/playerSelect.component';
import { MediaBadgeComponent } from 'components/layout/footerPlayer/playerSelect/mediaBadge/mediaBadge.component';
import { MovieBadgeComponent } from 'components/layout/footerPlayer/playerSelect/movieBadge/movieBadge.component';
import { EpisodeBadgeComponent } from 'components/layout/footerPlayer/playerSelect/episodeBadge/episodeBadge.component';
import { CustomBadgeComponent } from 'components/layout/footerPlayer/playerSelect/customBadge/customBadge.component';
import { SwitchBadgeComponent } from 'components/layout/footerPlayer/playerSelect/switchBadge/switchBadge.component';
import { PlayerRemotePlaylistsComponent } from 'components/layout/footerPlayer/playerSelect/playerRemote/playerRemotePlaylists/playerRemotePlaylists.component';
import { PlayerRemoteControlsComponent } from 'components/layout/footerPlayer/playerSelect/playerRemote/playerRemoteControls/playerRemoteControls.component';
import { PlayerRemoteSessionsComponent } from 'components/layout/footerPlayer/playerSelect/playerRemote/playerRemoteSessions/playerRemoteSessions.component';
import { PlayerRemoteCustomActionsComponent } from 'components/layout/footerPlayer/playerSelect/playerRemote/playerRemoteCustomActions/playerRemoteCustomActions.component';
import { FooterPlayerComponent } from 'components/layout/footerPlayer/footerPlayer.component';
import { SearchBarComponent } from 'components/layout/searchbar/searchbar.component';
import { SideBarComponent } from 'components/layout/sidebar/sidebar.component';
import { SideBarMainRegionComponent } from 'components/layout/sidebar/mainRegion/mainRegion.component';
import { MenuItemComponent } from 'components/layout/sidebar/menuItem/menuItem.component';
import { MenuSubsectionComponent } from 'components/layout/sidebar/menuSubsection/menuSubsection.component';
import { TopBarComponent } from 'components/layout/topbar/topbar.component';

// Layout Directives
import { PageArtDirective } from 'components/layout/pageArt/pageArt.directive';
import { PopoverDirective } from 'components/common/popover/popover.directive';
import { AsyncClickDirective } from 'components/common/helpers/asyncClick.directive';
import { AsyncClickGroupDirective } from 'components/common/helpers/asyncClickGroup.directive';
import { AutoFocusDirective } from 'components/common/helpers/autoFocus.directive';
import { SelectableContainerDirective } from 'components/common/selectable/selectableContainer.directive';
import { ModalScrollbarDirective } from 'components/common/scrollbar/modalScrollbar.directive';
import { ScrollableDirective } from 'components/common/scrollbar/scrollable.directive';
import { BackgroundDirective } from 'components/common/background/background.directive';
import { HamburgerDirective } from 'components/layout/hamburger/hamburger.directive';
import { GestureSwipeDirective } from 'components/layout/gestures/gesture.directive';
import { SelectableDirective } from 'components/common/selectable/selectable.directive';
import { DiscreteScrollDirective } from 'components/common/discreteScroll/discreteScroll.directive';
import { DiscreteScrollElementDirective } from 'components/common/discreteScroll/discreteScrollElement.directive';

// Thumbnails Button Actions
import { DefaultMediaButtonActions } from 'components/pages/common/buttonActions/defaultMediaButtonActions';
import { PlayButtonAction } from 'components/pages/common/buttonActions/playButtonAction';
import { QueueButtonAction } from 'components/pages/common/buttonActions/queueButtonAction';
import { SubtitlesButtonAction } from 'components/pages/common/buttonActions/subtitlesButtonAction';
import { WatchButtonAction, UnwatchButtonAction } from 'components/pages/common/buttonActions/watchButtonAction';
import { GalleryButtonAction } from 'components/pages/common/buttonActions/galleryButtonAction';

// External
import { InfiniteScrollModule  } from 'ngx-infinite-scroll';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

const PAGES_DECLARATIONS = [HomePageComponent, MoviesComponent, MoviesDetailsComponent, MoviesDetailsSidemenuComponent, MoviesListComponent,
    MoviesListSidemenuComponent, SeasonsListComponent, EpisodesDetailsComponent, EpisodesListComponent, ShowsComponent, ShowsDetailsComponent,
    ShowsDetailsSidemenuComponent, ShowsListComponent, ShowsListSidemenuComponent, SubtitlesComponent, MediaCastComponent, SettingsComponent, HistoryComponent,
    HistorySidemenuComponent, PeopleComponent, PeopleListComponent, PeopleListSidemenuComponent, PeopleDetailsComponent, PeopleDetailsSidemenuComponent];

const COMMON_COMPONENTS = [ ScrollbarComponent, DropdownComponent, ButtonComponent, DynamicComponentComponent, LoaderComponent,
    SliderComponent, NumberSliderComponent, PercentageSliderComponent, PreventBubbleDirective, SectionComponent, ThumbnailComponent, ThumbnailMediaComponent, ThumbnailPersonComponent,
    ThumbnailsListComponent, ThumbnailInfoTagsComponent, ThumbnailContextMenuComponent, TimeSliderComponent, PopoverDirective, CommandCenterComponent,
    CommandCenterActionComponent, TopoverComponent, SelectableDirective, SelectableContainerDirective, ModalScrollbarDirective,
    ScrollableDirective, NotificationComponent, SidemenuFiltersCollectionsComponent, SidemenuFiltersGenresComponent, SidemenuFiltersQualitiesComponent, SidemenuFiltersSortComponent, SidemenuFiltersWatchComponent,
    SidemenuMediaCollectionsComponent, MediaRedirectComponent, BackgroundDirective, ToastComponent, LineChartComponent, SidemenuFiltersListsComponent, SidemenuFiltersDateComponent,
    SidemenuFiltersRepositoriesComponent, SidemenuMediaRepositoriesComponent, SidemenuMediaListComponent, DownloadMediaDropdownComponent,
    SidemenuFiltersMediaComponent, AutoCompleteComponent, DateRangeComponent, ListSelectorComponent, FilterPresetsComponent, AutoScrollUpDownComponent, CollapsibleComponent,
    IconComponent, IconCompoundComponent, DroploaderComponent ];

const MODALS = [ ModalComponent, ModalWrapper, ConfirmModalComponent, InputModalComponent, GalleryModalComponent, MediaInfoModalComponent, HistoryMediaModalComponent ];

const PIPES = [ CallerPipe, NegatePipe, RatingPipe, ArtworkGalleryPipe, ResolvePropertiesPipe, ImageUrlPipe, OrDefaultValuePipe, MapToIterablePipe, MinPipe, MaxPipe,
    EpisodesAggregatorPipe, AvailableEpisodeSources2DropdownPipe, FriendlyDatePipe, FriendlyDataRatePipe, FriendlySizePipe, FriendlyDurationPipe, CssSizePipe, EpisodeSubcaptionPipe ];

const LAYOUTS_COMPONENTS = [ HamburgerDirective, GestureSwipeDirective, PlayerRemoteSessionsComponent, PlayerRemotePlaylistsComponent, PlayerRemoteControlsComponent, PlayerBadgeComponent,
    MediaBadgeComponent, PlayerSelectComponent, PlayerRemoteComponent, AdvancedSeekComponent, PlayerControlComponent, FooterPlayerComponent, PlayerRemoteCustomActionsComponent,
    SearchBarComponent, SideBarComponent, SideBarMainRegionComponent, MenuItemComponent, MenuSubsectionComponent, TopBarComponent,
    MovieBadgeComponent, EpisodeBadgeComponent, CustomBadgeComponent, SwitchBadgeComponent, DiscreteScrollDirective, DiscreteScrollElementDirective ];

const ENTRY_COMPONENTS = [SideBarMainRegionComponent, MoviesListSidemenuComponent, MoviesDetailsSidemenuComponent, ShowsListSidemenuComponent,
    ShowsDetailsSidemenuComponent, NotificationComponent, CommandCenterActionComponent, TopoverComponent, PeopleListSidemenuComponent, PeopleDetailsSidemenuComponent,
    HistorySidemenuComponent ];

const EXTERNAL_COMPONENTS = [InfiniteScrollModule, TranslatorManager.forRoot() ];

const BUTTON_ACTIONS = [DefaultMediaButtonActions, PlayButtonAction, QueueButtonAction, SubtitlesButtonAction, WatchButtonAction, UnwatchButtonAction, GalleryButtonAction];

@NgModule({
    imports: [ BrowserModule, BrowserAnimationsModule, FormsModule, APP_ROUTER_PROVIDDERS, HttpClientModule, EXTERNAL_COMPONENTS, PerfectScrollbarModule ],       // module dependencies
    declarations: [ AppComponent, PIPES, COMMON_COMPONENTS, MODALS, LAYOUTS_COMPONENTS, PAGES_DECLARATIONS, PageArtDirective, AsyncClickDirective, AsyncClickGroupDirective, AutoFocusDirective ],   // components and directives
    entryComponents: [ ENTRY_COMPONENTS ],
    bootstrap: [ AppComponent ],     // root component
    providers: [ ArtManager, SidebarManager, TopbarManager, ModalManager, ServerApiManager, NotificationsService, CommandCenter, TopoverManager, DelayedExecutions,
                AppStorage, LocalStorageManager, PageTitle, SpeechRecognitionManager, SpeechSynthesisManager, VoiceCommandsManager,
                RequesterHttp, ServerApiController, WitaiApi, MediaQueries, StandardMediaQueries, MediaPlayControl, TranslatorManager, ApplicationContextManager,
                FiltersMediaSidemenuService.forRoot(undefined), SessionCache, BackgroundWorker, NavigationManager, NonExpirableListManager, MirroringManager, Toast, BUTTON_ACTIONS ], // services
    schemas: [ ModalWrapper ]
})
export class AppModule { }

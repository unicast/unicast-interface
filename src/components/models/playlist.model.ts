import { Model } from './model';
import { PlaylistRecordSchema, CustomMediaRecordSchema, TvEpisodeMediaRecordSchema, MovieMediaRecordSchema } from 'components/schemas/mediaRecord.schema';

export class PlaylistModel extends Model{

    id?: string;
    data: PlaylistRecordSchema;

    constructor(obj?: PlaylistRecordSchema){
        super();
        obj = obj || {createdAt: new Date(), references: [], updatedAt: new Date()};
        obj.items = obj.items || [];
        obj.createdAt = new Date(Date.parse(obj.createdAt.toString()));
        obj.updatedAt = new Date(Date.parse(obj.updatedAt.toString()));
        this.id = obj.id;
        this.data = obj;
    }

    getItem(i: number): TvEpisodeMediaRecordSchema | MovieMediaRecordSchema | CustomMediaRecordSchema{
        return this.data.items[i];
    }

}

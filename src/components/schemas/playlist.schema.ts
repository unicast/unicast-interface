export type PlaylistSchema = {
    items?: Array<PlaylistItemSchema>,
    created_at?: number,
    id?: string
};

export type PlaylistItemSchema = {
    id?: string,
    type?: string,
    year?: string,
    isMovie: boolean,
    caption: string,
    cover?: string,
    title?: string,
};
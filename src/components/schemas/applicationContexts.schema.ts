import { MovieMediaRecordSchema, TvShowMediaRecordSchema, TvSeasonMediaRecordSchema, TvEpisodeMediaRecordSchema } from './mediaRecord.schema';
import { PlaylistSchema } from 'components/schemas/playlist.schema';
import { Device } from 'components/services/serverApis/helpers/device';

export interface PageContextSchema {
    page: { name: string, type: string };
}

export interface MovieContextSchema {
    movie: Pick<MovieMediaRecordSchema, 'id' | 'kind'>;
}

export interface ShowContextSchema {
    show?: Pick<TvShowMediaRecordSchema, 'id' | 'kind'>;
    season: Pick<TvSeasonMediaRecordSchema, 'id' | 'number' | 'kind'>;
    episode: Pick<TvEpisodeMediaRecordSchema, 'id' | 'number' | 'seasonNumber' | 'kind'>;
}

export interface DeviceContextSchema {
    device: Pick<Device, 'name'>;
}

export interface PlaylistContextSchema {
    playlist: Pick<PlaylistSchema, 'id'>;
}

export interface FiltersContextSchema<T> {
    filters: T;
}
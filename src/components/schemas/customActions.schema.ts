export interface CustomActionGroup {
    name: string;
    actions: Array<CustomAction>;
}

export interface CustomAction {
    name: string;
    label: string;
    icon: string;
    group: string;
}
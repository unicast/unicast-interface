import { MediaRecordSchema } from 'components/schemas/mediaRecord.schema';

export enum TaskState {
    Unstarted = 'unstarted',
    Running = 'running',
    Paused = 'paused',
    Cancelled = 'cancelled',
    Finished = 'finished'
}

export interface TaskSchema<M = any> {
    id: string;
    state: TaskState;
    startedAt: number;
    startedAtHuman: string;
    metadata: M;
    done: number;
    total: number;
    elapsedTime: number;
    remainingTime: number;
    errors: string[];
    metrics: any[];
    cancelable: boolean;
    pausable: boolean;
}

export interface TranscodingTaskMetadataSchema {
    type : string;
    media : MediaRecordSchema;
}

export interface TranscodingTaskSchema extends TaskSchema<TranscodingTaskMetadataSchema> { }
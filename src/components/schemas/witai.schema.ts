export interface WitaiResponseSchema {
    msg_id: string;
    _text: string;
    entities: {
        intent?: Array<WitaiEntitySchema>;
        duration?: Array<WitaiTimeSchema>;
        [key: string] : Array<WitaiEntitySchema | WitaiTimeSchema>;
    };
    intent?: string;
    intent_id?: string;
}


export type WitaiTimeUnit = 'hour' | 'minute' | 'second';

export type WitaiTimeUnitProps = {
    [K in WitaiTimeUnit]?: number;
}

export interface WitaiTimeSchema extends WitaiTimeUnitProps {
    confidence: number,
    value: number,
    unit: WitaiTimeUnit,
    normalized: {
       value: number,
       unit: WitaiTimeUnit
    }
}

export interface WitaiEntitySchema{
    confidence?: number;
    start?: number;
    end?: number;
    body: string;
    value?: string;
    entity?: string;
}

export enum WitaiIntent {
    VolumeMute = 'volume-mute',
    Seek = 'seek',
    SeekForward = 'seek-forward',
    VolumeUp = 'volume-up',
    SeekBackwards = 'seek-backwards',
    Play = 'play',
    BackwardMedia = 'backward-media',
    Search = 'search',
    VolumeDown = 'volume-down',
    ForwardMedia = 'forward-media',
    StopMedia = 'stop-media',
    PauseMedia = 'pause-media',
    ResumeMedia = 'resume-media'
}
export enum RepositoryState {
    Unknown = 'unknown',
    Offline = 'offline',
    Online = 'online'
}

export interface VirtualRepository {
    name: string;
    displayName: string;
    state: RepositoryState;
}

export interface RepositoryMount {
    name: string;
    path: string;
    displayName: string;
}

export interface RepositoryConfig {
    name: string,
    displayName: string,
    type: string,
    content: string,
    scrapper: string,
    folders: Array<string>,
    exclude: Array<string>,
    mounts: Array<RepositoryMount>
}

export interface Repository {
    indexable: boolean,
    searchable: boolean,
    ignoreUnreachableMedia: boolean,
    name: string,
    config: RepositoryConfig
}

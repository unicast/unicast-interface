/*
 * Built from the schemas presents on Unicast Server
 */
import { MediaKind, MediaStreamType } from 'components/services/serverApis/helpers/mediaKind';

export interface MediaSourceDetailsSchema {
    id : string;
    provider ?: string;
    primary ?: boolean;
    types ?: MediaStreamType | Array<MediaStreamType>;
    take ?: number;
    skip ?: number;

    enabled ?: string | Array<string>;
    disabled ?: string | Array<string>;

    [ property : string ] : any;
}

export type ExternalReferencesSchema = { [ key : string ] : string };

export interface MediaRecordArtSchema {
    thumbnail : string;
    poster : string;
    background : string;
    banner : string;
}

export enum ArtRecordKind {
    Poster = "poster",
    Background = "background",
    Banner = "banner",
    Thumbnail = "thumbnail"
}

// This object represents one single piece of art, that might not even be associated with any MediaRecord
export interface ArtRecordSchema {
    id: string;
    kind: ArtRecordKind;
    width: number;
    height: number;
    url : string;
    season ?: number;
    score ?: number;
}

export interface TvSeasonMediaRecordArtSchema extends MediaRecordArtSchema {
    tvshow : MediaRecordArtSchema;
}

export type MediaResolution = '4320p' | '2160p' | '1440p' | '1080p' | '720p' | '480p' | '360p' | '240p';
export type MediaBitDepth = '8bit' | '10bit';
export type MediaColorSpace = 'SDR' | 'HDR';
export type MediaSource = 'BluRay' | 'WEB-DL' | 'WEBRIP' | 'WEBCAP' |
                          'HDTV' | 'DVDR' | 'SCR' | 'CAMRip' | 'TS' |
                          'WP' | 'PPV' | 'VODRip' | 'DVDRip' | 'R5'
                          ;

export interface MediaMetadataVideoSchema {
    resolution: MediaResolution | null;
    codec: string;
    framerate: number;
    bitdepth: MediaBitDepth | null;
    colorspace: MediaColorSpace | null;
}

export interface MediaMetadataAudioSchema {
    codec: string;
    bitrate: number;
    /** List of possible values: http://web.archive.org/web/20250105210052/https://github.com/FFmpeg/FFmpeg/blob/539cea31830e71f1ce290c56ff2d639b209c2ac2/libavutil/channel_layout.c#L189C21-L189C41  */
    channels: string;
    channelsLayout?: string;
    language: string;
}

export interface MediaMetadataSubtitlesSchema {
    codec: string;
    language: string;
    forced: boolean;
    hearingImpaired: boolean;
}

export interface MediaMetadataSchema {
    video: MediaMetadataVideoSchema;
    additionalVideo: MediaMetadataVideoSchema[];
    audio?: MediaMetadataAudioSchema;
    additionalAudio: MediaMetadataAudioSchema[];
    subtitles?: MediaMetadataSubtitlesSchema;
    additionalSubtitles: MediaMetadataSubtitlesSchema[];
    source: MediaSource | null;
    bitrate: number | number;
    duration: number;
    size: number;
}

export interface MediaRecordKeySchema {
    id : string;
    kind : MediaKind;
}

export interface MediaRecordSchema extends MediaRecordKeySchema {
    internalId : string;
    repository : string;
    collections? : Array<CollectionSchema>;
    role?: RoleRecordSchema;
    title : string;
    art : MediaRecordArtSchema;
    cachedArtwork : MediaRecordArtSchema;
    external : ExternalReferencesSchema;
    repositoryPaths?: Array<string>;

    $userRank?: number;
}

export interface PlayableMediaRecordSchema extends MediaRecordSchema {
    runtime : number;
    sources : Array<MediaSourceDetailsSchema>;
    metadata : MediaMetadataSchema | null;
    playCount : number;
    watched : boolean;
    lastPlayedAt : Date;
    addedAt : Date;
}

export interface MovieMediaRecordSchema extends PlayableMediaRecordSchema {
    rating : number;
    genres : Array<string>;
    trailer : string;
    parentalRating : string;
    plot : string;
    year : number;
    tagline : string;
    kind: MediaKind.Movie;
}

export interface TvShowMediaRecordSchema extends MediaRecordSchema {
    episodesCount : number;
    genres : Array<string>;
    plot : string;
    parentalRating : string;
    rating : number;
    seasonsCount : number;
    year : number;
    watchedEpisodesCount : number;
    watched : boolean;
    addedAt : Date;
    kind: MediaKind.TvShow;
    seasons?: Array<TvSeasonMediaRecordSchema>;
}

export interface TvSeasonMediaRecordSchema extends MediaRecordSchema {
    art : TvSeasonMediaRecordArtSchema;
    cachedArtwork : TvSeasonMediaRecordArtSchema;
    number : number;
    tvShowId : string;
    episodesCount : number;
    watchedEpisodesCount : number;
    kind: MediaKind.TvSeason;
}

export interface TvEpisodeMediaRecordSchema extends PlayableMediaRecordSchema {
    number : number;
    plot: string;
    seasonNumber : number;
    tvSeasonId : string;
    rating : number;
    kind: MediaKind.TvEpisode;
}

export interface CustomMediaRecordSchema extends PlayableMediaRecordSchema {
    kind: MediaKind.Custom;
    plot: string;
    subtitle: string;
}

export interface PlaylistRecordSchema {
    id ?: string;
    references : Array<{ kind : MediaKind, id : string }>;
    items ?: Array<TvEpisodeMediaRecordSchema | MovieMediaRecordSchema | CustomMediaRecordSchema>;
    createdAt : Date;
    updatedAt : Date;
}

export interface HistoryRecordSchema {
    id ?: string;
    reference? : { kind : MediaKind, id : string };
    playlist ?: string;
    playlistPosition ?: number;
    receiver? : string;
    record : PlayableMediaRecordSchema;
    position? : number;
    positionHistory? : Array<{ start: number, end: number }>;
    watched?: boolean;
    createdAt?: Date;
    updatedAt?: Date;
}

export interface CategoryRecordSchema {
    id ?: string;
    title : string;
    color : string;
    kinds : Array<string>;
}

export interface CategoryMediaRecordSchema {
    id : string;
    categoryId : string;
    mediaKind : string;
    mediaId : string;
    createdAt : Date;
}

export interface GroupedSubtitlesSchema {
    media: MediaRecordSchema;
    subtitles: SubtitleSchema[];
}

export interface SubtitleSchema {
    id: string;
    releaseName : string;
    encoding : string;
    format : string;
    language : string;
    publishedAt : Date;
    downloads : number;
    provider : string;
}

export interface CollectionSchema {
    id : string;
    parentId ?: string;
    title : string;
    color? : string;
    kinds? : string[];
    primary? : boolean;

    // Foreign Relation
    /** Available when retrieving collections with `hierarchical=true` */
    children ?: CollectionSchema[];
}

export interface CollectionMediaSchema {
    id : string;
    collectionId : string;
    mediaKind : string;
    mediaId : string;
    createdAt : Date;
}

export interface PersonRecordSchema {
    id ?: string;
    name : string;
    art : MediaRecordArtSchema;
    cachedArtwork : MediaRecordArtSchema;
    biography ?: null;
    birthday ?: Date;
    deathday ?: Date;
    naturalFrom ?: string;

    // Foreign Relation
    /** Available when retrieving the cast from a movie/tv show */
    cast ?: MediaCastRecordSchema;
    /** Available when retrieving the movies/tv shows from a person */
    credits ?: MediaRecordSchema[];
}

export interface MediaCastRecordSchema {
    id ?: string;
    internalId : string;
    scraper : string;
    external : ExternalReferencesSchema;
    mediaKind : MediaKind;
    mediaId : string;
    personId : string;
    role : string;
    order : number;
    createdAt : Date;
    updatedAt : Date;
}


export interface RoleRecordSchema {
    id: string;
    createdAt: string;
    external: ExternalReferencesSchema;
    internalId: number;
    mediaId: string;
    mediaKind: MediaKind;
    order: number;
    personId: string;
    role: string;
    scraper: string;
    updatedAt: string;
}

export interface StreamSchema {
    id:        string;
    provider:  string;
    type:      "video" | "subtitles";
    mime:      string;
    size:      number;
    duration?: number;
    path:      string;
    format?:   string;
}

export interface MediaDownloadSchema {
    video?: string;
    videoName?: string;
    subtitles?: string;
    subtitlesName?: string;
}

export interface PlayableMediaQualities {
    resolutions: string[];
    videoCodecs: string[];
    colorspaces: string[];
    bitdepths: string[];
    audioCodecs: string[];
    channels: string[];
    languages: string[];
    sources: string[];
}

export interface MediaProbeRecordSchema {
    id ?: string;
    mediaId : string;
    mediaKind : MediaKind;
    metadata : any;
    raw : any;
    createdAt ?: Date;
    updatedAt ?: Date;
}

import { MediaRecordSchema, HistoryRecordSchema } from 'components/schemas/mediaRecord.schema';

export enum DeviceStatusState {
    Stopped = 'STOPPED',
    Playing = 'PLAYING',
    Paused = 'PAUSED',
    Buffering = 'BUFFERING'
}

export interface DeviceStatusSchema {
    session : string;
    timestamp : Date;
    state : DeviceStatusState;
    media : {
        time : TimeStatus;
        transcoding: TranscodingStatus;
        record : MediaRecordSchema;
        session: HistoryRecordSchema;
    }
    volume : DeviceVolumeStatus;
    subtitlesStyle : DeviceSubtitlesStyleStatus;
}

export interface TranscodingStatus extends TimeStatus{
    task : string;
}

export interface TimeStatus {
    current : number;
    duration : number;
    speed: number;
    stable?: boolean;
}

export interface DeviceVolumeStatus {
    level : number;
    muted : boolean;
}

export interface DeviceSubtitlesStyleStatus {
    size : number;
}
export interface SyncOptions {
    dryRun?: boolean;
    cleanMissing?: boolean;
    refetchExisting?: boolean;
    refetchIncomplete?: boolean;
    updateMoved?: boolean;
    cache?: {
        read?: boolean;
        write?: boolean;
    }
}

export const SyncDefaultOptions: SyncOptions = {
    dryRun: true,
    cleanMissing: false,
    refetchExisting: false,
    refetchIncomplete: false,
    updateMoved: true,
    cache: {
        read: true,
        write: true
    }
}

/* 
 * This represent the default values the server uses for each option,
 * if that option is not explicitly sent in the request
 */
export const SyncDefaultServerOptions: SyncOptions = {
    dryRun: false,
    cleanMissing: false,
    refetchExisting: false,
    refetchIncomplete: false,
    updateMoved: false,
    cache: {
        read: true,
        write: true
    }
}

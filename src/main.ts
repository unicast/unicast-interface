import 'reflect-metadata';
import 'es6-shim';
import 'zone.js';

import './polyfills';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './components/app.module';

const isAotBuild: boolean = false;

if (isAotBuild) {
    enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);

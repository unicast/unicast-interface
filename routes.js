"use strict";

var replaceStream = require( 'replacestream' );
var path = require( 'path' );
var restify = require( 'restify' );
var { Router } = require( 'restify-router' );
var { MockRequests } = require( './mockRequests' );

const templateStream = function ( stream, keys ) {
    for ( let key of Object.keys( keys ) ) {
        stream = stream.pipe( replaceStream( `{{${ key }}}`, keys[ key ] ) );
    }

    return stream;
};

function createRouter ( webHref, apiHref, mock = false ) {
    const router = new Router();

    const DIST_FOLDER = path.join( __dirname, 'dist' );
    const DIST_FOLDER_INDEX = path.join( DIST_FOLDER, 'index.html' );

    const transformIndex = function ( stream, file, req ) {
        if ( file != DIST_FOLDER_INDEX ) {
            return null;
        }

        return templateStream( stream, {
            WEB_HREF: '/'/*webHref*/,
            API_HREF: apiHref
        } );
    };

    if ( mock ){
        console.info('Mocking some requests');
        new MockRequests().create(router);
    }

    router.get( '/*', require( './static' )( {
        directory: DIST_FOLDER,
        default: 'index.html',
        // file: 'index.html',
        transformer: transformIndex
    } ) );

    return router;
}

function createApp ( protocol, url, api, options, mock = false, log = false ) {
    const server = restify.createServer(options);

    module.exports( server, protocol + url, null, protocol + api, mock );

    if ( mock && log ){
        server.pre(function(req, response, next) {
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
            response.setHeader("Expires", "0"); // Proxies.
            return next();
        });
        server.on('after', function (request, response, route, error) {
            console.info('[' + request.date().toISOString() + ']', request.connection.remoteAddress, request.method, request.url, response.statusCode, (Date.now() - request.time()) + 'ms');
        });
    }

    return server;
}

module.exports = function ( server, url, route, api, mock = false ) {
    api = api || `${url}/api`;

    if ( route ) {
        module.exports.routes( `${url}${route}/`, api, mock ).applyRoutes( server, route );
    } else {
        module.exports.routes( `${url}/`, api, mock ).applyRoutes( server );
    }
};

module.exports.routes = createRouter;

module.exports.app = createApp;

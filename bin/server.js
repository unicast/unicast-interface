var app = require( '../routes.js' ).app;
var internalIp = require( 'internal-ip' );
var argv = require( 'yargs' ).argv;
var fs = require( 'fs' );

const port = ( argv["port"] || 3030 );
const sslPort = ( argv["sslPort"] || port + 1 );
const log = ( argv["log"] !== undefined || false );

internalIp.v4().then( ip => {
    const global_options = {
        ignoreTrailingSlash: true
    };
    app( 'http://', ip + ':' + port, argv["api"], global_options, true, log ).listen( port, function () {
        console.info( 'Server started. Listening to port ' + port );
    } );
    const https_options = {
        ...global_options,
        key: fs.readFileSync('./bin/server.key'),
        certificate: fs.readFileSync('./bin/server.cert'),
        passphrase: 'server'
    };
    app( 'https://',  ip + ':' + sslPort, argv["api"], https_options, true, log ).listen( sslPort, function () {
        console.info( 'SSL Server started. Listening to port ' + sslPort );
    } );
} );

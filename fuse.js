const { tsc, src, task, context } = require( 'fuse-box/sparky' );
const argv = require( 'yargs' ).argv;
const path = require( 'path' );
const fs = require ( 'mz/fs' );

require('./fuse/ngcTask');
const { buildTaskBuild, clean, copy, buildCss, npmPack } = require('./fuse/utils');

const MAIN_DIR = path.resolve( __dirname);

//Init fusebox context
context({
    'CACHE': '.fusebox',
    'MODULES_TO_BUILD': ['ngx-perfect-scrollbar'],
    'MAIN_DIR': MAIN_DIR,
    'SRC': 'src',
    'SRC_AOT': 'aot',
    'CLIENT': 'client',
    'DIST': 'dist',
    'NODE_MODULES': 'node_modules',
    'NODE_MODULES_DIST': path.join('node_modules', 'dist' )
});

task( 'clean:cache', async (ctx) => {
    return await clean(ctx.CACHE);
} );

task( 'clean:dist', async (ctx) => {
    return await clean(ctx.DIST);
} );

task( 'copy:client', async (ctx) => {
    return await copy(ctx.CLIENT, ctx.DIST);
} );

task( 'server', async (ctx) => {
    if ( argv["api"] ){
        require( './bin/server.js' );
    }
} );

task( 'check', async (ctx) => {
    return await tsc( __dirname, {
        ...JSON.parse(await fs.readFile('tsconfig.json', 'utf8')).compilerOptions,
        watch: true,
        noEmit: true
    } );
} );

task( 'pack:jit', async (ctx) => {
    return await buildTaskBuild(false, ctx);
} );

/**
 * AOT tasks
 */
task( 'clean:aot', async (ctx) => {
    return await clean(ctx.SRC_AOT);
} );
task( 'clean:modules', async (ctx) => {
    return await clean(ctx.NODE_MODULES_DIST);
} );

task('copy:modules', async (ctx) => {
    return await copy(ctx.NODE_MODULES_DIST, path.join(ctx.SRC_AOT, 'modules'));
});

task('less:compile', async (ctx) => {
    return await src('**/*.less', { base: ctx.SRC })
        .file('*.less', (file) => buildCss(file, ctx))
        .exec();
});

task('build:work', async (ctx) => {
    return await copy(ctx.SRC, ctx.SRC_AOT);
});

task( 'pack:aot', async (ctx) => {
    return await buildTaskBuild(true, ctx);
} );

task( 'npm:pack', (ctx) => {
    return npmPack();
} );


//Aggregation tasks
task( 'default', 'build:jit' );

task( 'clean:all', [ 'clean:cache', 'clean:sources' ]);

task( 'clean:sources', [ 'clean:aot', 'clean:dist', 'clean:modules' ]);

task( 'build:jit', [ 'clean:dist', 'copy:client', 'server', '&check', 'pack:jit' ]);

task( 'build:jit:pack', [ 'build:jit', 'npm:pack' ]);

task( 'build:aot', [ 'clean:sources', 'build:work', 'less:compile', 'ngc', 'copy:modules', 'copy:client', 'pack:aot' ]);

task( 'build:aot:pack', [ 'build:aot', 'npm:pack' ]);